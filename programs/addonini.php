;<?php /*

[general]
name							="widgets"
version							="1.1.89"
addon_type						="LIBRARY"
encoding						="UTF-8"
mysql_character_set_database	="latin1"
description						="User Interface Widgets"
description.fr					="Librairie partagée fournissant une API de création d'interfaces utilisateur"
long_description.fr             ="README.md"
delete							=1
ov_version						="8.6.97"
php_version						="5.3.0"
addon_access_control			="0"
author							="Laurent Choulette (laurent.choulette@cantico.fr)"
icon							="edit-rename.png"
mysql_character_set_database	="latin1,utf8"

[addons]
LibFileManagement   ="0.3.2"
LibOrm              ="0.11.7"
;*/