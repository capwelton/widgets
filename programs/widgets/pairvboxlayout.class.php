<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/boxlayout.class.php';



/**
 *
 *
 * @return Widget_PairVBoxLayout
 */
function Widget_PairVBoxLayout($id = null, $title = null)
{
	return new Widget_PairVBoxLayout($id, $title);
}


class Widget_PairVBoxLayout extends Widget_BoxLayout implements Widget_Displayable_Interface
{

	/**
	 * @param string	$id			The item unique id.
	 */
	public function __construct($id = null, $title = null)
	{
        parent::__construct($id);
		
		if (null !== $title) {
			$this->setTitle($title);
		}
	}

	
	/**
	 * (non-PHPdoc)
	 * @see Widget_Layout::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		return $canvas->PairVBox(
		    $this->getId(),
            $this->getClasses(),
			$this->getTitle(),
			array_values($this->getItems()),
		    $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
