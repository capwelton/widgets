<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/widget.class.php';



/**
 * Constructs a Widget_Title.
 *
 * @param string | Widget_Displayable_Interface     $item       The title text.
 * @param string                                    $level      The title level [1-6].
 * @param string                                    $id         The item unique id.
 * @return Widget_Title
 */
function Widget_Title($item = '', $level = 2, $id = null)
{
    return new Widget_Title($item, $level, $id);
}



class Widget_Title extends Widget_Widget implements Widget_Displayable_Interface
{
    /**
     *
     * @var string | Widget_Displayable_Interface
     */
    private $item;

    /**
     *
     * @var int
     */
    private $level;

    /**
     * @var bool
     */
    private $colon = null;


    /**
     * @param string | Widget_Displayable_Interface     $item       The title text.
     * @param string                                    $level      The title level [1-6].
     * @param string                                    $id         The item unique id.
     * @return Widget_Title
     */
    public function __construct($item = '', $level = 2, $id = null)
    {
        parent::__construct($id);
        $this->item = $item;
        $this->setLevel($level);
    }

    /**
     * Sets the icon's text label.
     *
     * @param string    $labelText
     * @return Widget_Title
     */
    public function setText($text)
    {
        $this->item = $text;
        return $this;
    }
    
    
    public function getText()
    {
        return $this->item;
    }


    /**
     * Sets the title contained item.
     *
     * @param Widget_Item $item
     * @return Widget_Link
     */
    public function setItem(Widget_Item $item)
    {
        $this->item = $item;
        return $this;
    }


    /**
     * Get the item or the text contained in the title.
     *
     * @return Widget_Item || string
     */
    public function getItem()
    {
        return $this->item;
    }



    /**
     * Sets the header level from 1 to 6.
     *
     * @param string    $level
     * @return Widget_Title
     */
    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }



    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_Item#getClasses()
     *
     * @return array
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-title';
        return $classes;
    }




    /**
     * display a colon after label text
     * @return Widget_Label
     */
    public function colon($colon = true)
    {
        $this->colon = $colon;
        return $this;
    }


    /**
     * Test if the label is displayed with a colon
     * @return bool
     */
    protected function isColon()
    {
        if (null !== $this->colon) {
            return $this->colon;
        }

        return false;
    }



    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $item = $this->getItem();

        if (is_string($item) && $this->isColon()) {
            $item = widget_addColon($item);
        }
        
        if (is_string($item)) {
            $item = $canvas->text($item);
        }


        if ($this->level > 6) {
            return $canvas->span(
                $this->getId(),
                $this->getClasses(),
                array($item),
                $this->getCanvasOptions(),
                $this->getTitle(),
                $this->getAttributes()
            )
            . $canvas->metadata($this->getId(), $this->getMetadata());
        }

        return $canvas->h(
            $this->getId(),
            $this->getClasses(),
            array($item),
            $this->level,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata());
    }
}
