<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_UrlLineEdit.
 *
 * @param string        $id         The item unique id.
 * @return Widget_UrlLineEdit
 */
function Widget_UrlLineEdit($id = null)
{
    return new Widget_UrlLineEdit($id);
}


/**
 * A Widget_LineEdit is a widget that lets the user enter an url address.
 */
class Widget_UrlLineEdit extends Widget_LineEdit implements Widget_Displayable_Interface
{


    /**
     * @param string $id            The item unique id.
     * @return Widget_LineEdit
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        $this->setSubmitMessage(widget_translate('Invalid url address'));

        $this->setSchemeCheck(true);
        $this->setAllowedSchemes(array('http', 'https', 'ftp'));
    }

    /**
     * Message displayed on form submit if there is a url line edit widget with an invalid url address
     * @return Widget_UrlLineEdit
     */
    public function setSubmitMessage($str)
    {
        $this->setMetadata('submitMessage', $str);
        return $this;
    }

    /**
     * Set a list of allowed sheme in url
     * default allowed shemes are http, https, ftp
     *
     * @param  string   $allowedSchemes
     * @return Widget_UrlLineEdit
     */
    public function setAllowedSchemes(array $allowedSchemes)
    {
        $this->setMetadata('allowedSchemes', $allowedSchemes);
        return $this;
    }


    /**
     * @param bool $check
     */
    public function setSchemeCheck($check = true)
    {
        $this->setMetadata('checkSchemes', $check);
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_LineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-urllineedit';
        return $classes;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_LineEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {

        if (!$this->isMandatory()) {
            $message = $this->getMetadata('submitMessage');
            $message .= " \n".widget_translate('Do you want to submit the form anyway?');
            $this->setSubmitMessage($message);
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        $output = parent::display($canvas)
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.urllineedit.jquery.js');

        return $output;
    }
}
