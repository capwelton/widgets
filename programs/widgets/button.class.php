<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/containerwidget.class.php';


/**
 * Constructs a Widget_Button.
 *
 * @param string 			$id			The item unique id.
 * @param Widget_Layout 	$layout		The layout that will manage how widgets are displayed in this container.
 * @return Widget_Button
 */
function Widget_Button($id = null, $layout = null)
{
	return new Widget_Button($id);
}



/**
 *
 */
class Widget_Button extends Widget_ContainerWidget
{


	/**
	 * @param string 			$id			The item unique id.
	 * @param Widget_Layout 	$layout		The layout that will manage how widgets are displayed in this container.
	 * @return Widget_Button
	 */
	public function __construct($id = null, $layout = null)
	{
		if (null === $layout) {
			require_once FUNC_WIDGETS_PHP_PATH . 'layout.class.php';
			$layout = new Widget_Layout();
		}

		$this->addAttribute('type', 'button');

		parent::__construct($id, $layout);
	}


	/**
	 * Set button type
	 * @param string $type button|submit|reset
	 * @return Widget_Button
	 */
	public function setType($type)
	{
	    return $this->addAttribute('type', $type);
	}

	public function getType()
	{
	    return $this->getAttribute('type');
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Widget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-button';
		$classes[] = 'ui-default-state';
		return $classes;
	}



	/**
	 *
	 * @param Widget_Form $form
	 * @return self
	 */
	public function setForm($form)
	{
	    $this->form = $form;
	    return $this;
	}


	/**
	 * Returns the form to which this input widget is attached.
	 *
	 * @return Widget_Form|null
	 */
	public function getForm()
	{
	    if (isset($this->form)) {
	        return $this->form;
	    }
	    for ($widget = $this; $widget && (!($widget instanceof Widget_Form)); $widget = $widget->getParent()) {
	        ;
	    }

	    return $widget;
	}


	/**
	 *
	 * @return string[]
	 */
	public function getAttributes()
	{
	    $form = $this->getForm();
	    if ($form) {
	        $this->addAttribute('form', $form->getId());
	    }
	    return parent::getAttributes();
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Item::display()
	 */
	function display(Widget_Canvas $canvas)
	{
		if ($this->isDisplayMode()) {
			return '';
		}

		$button = $canvas->button(
			$this->getId(),
			$this->getClasses(),
			$this->isDisabled(),
			array($this->getLayout()),
			$this->getCanvasOptions(),
			$this->getTitle(),
            $this->getAttributes()
		) . $canvas->metadata($this->getId(), $this->getMetadata());

		return $button;
	}
}
