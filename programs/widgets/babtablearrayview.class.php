<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */

bab_Widgets()->includePhpClass('Widget_TableArrayView');



class Widget_BabTableArrayView extends Widget_TableArrayView
{

	/**
	 * Get row classes before drawing the row
	 * @param int $rowId
	 * @return string[]
	 */
	public function getRowClasses($rowId)
	{
		static $oddRow = true;
		$oddRow = !$oddRow;

		$section = $this->getLayout()->getCurrentSection();

		if (isset($section) && 'header' === $section) {
			$classes = array('BabSiteAdminTitleFontBackground');
		} else {
			$classes = array($oddRow ? 'BabSiteAdminFontBackground' : 'BabForumBackground1');
		}

		if (isset($this->_rowClasses[$rowId])) {
			$classes += $this->_rowClasses[$rowId];
		}

		return $classes;
	}


	/**
	 * @return string[]
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-babtableview';
		return $classes;
	}
}
