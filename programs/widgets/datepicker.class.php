<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/inputwidget.class.php';
require_once $GLOBALS['babInstallPath'] . '/utilit/dateTime.php';


/**
 * Constructs a Widget_DatePicker.
 *
 * @param string    $id	      The item unique id.
 * @return Widget_DatePicker
 */
function Widget_DatePicker($id = null)
{
    return new Widget_DatePicker($id);
}


/**
 * A Widget_DatePicker is a widget that let the user enter a date.
 *
 * @property bool           $inline
 * @property bool           $selectableYear
 * @property bool           $selectableMonth
 * @property BAB_DateTime   $mindate
 * @property BAB_DateTime   $maxdate
 * @property array          $numberOfMonths
 * @property string         $dateClickUrl
 * @property string         $dateClickUrlAjax
 * @property int            $size
 * @property bool           $showWeek
 * @property int            $_maxSize
 * @property array          $selectableDates
 */
class Widget_DatePicker extends Widget_InputWidget implements Widget_Displayable_Interface
{
    /** @var bool $inline  */
    private $inline = false;

    /** @var bool $selectableYear  */
    private $selectableYear;

    /** @var bool $selectableMonth  */
    private $selectableMonth;

    /** @var bool $selectableToday  */
    private $selectableToday;

    /** @var BAB_DateTime $mindate  */
    private $mindate;

    /** @var BAB_DateTime $maxdate  */
    private $maxdate;

    /** @var int[] $numberOfMonths  */
    private $numberOfMonths = array(1, 1);

    /** @var string $dateClickUrl  */
    private $dateClickUrl = '';

    /** @var string $dateClickUrlAjax  */
    private $dateClickUrlAjax = false;

    /** @var int $size  */
    private $size = 10;

    /** @var bool $showWeek  */
    private $showWeek = null;

    /**
     * @var int
     */
    private $_maxSize;

    /**
     * @var string[][]
     */
    private $selectableDates = array();


    /**
     * @param string $id        The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->setMaxSize(10);
        $this->setFormat('%d-%m-%Y');
        $this->setMonthSelectable();
        $this->setYearSelectable();
        $this->setTodaySelectable();
    }

    /**
     * specfiy selectable Dates
     * @param [] $dates array or array that has to have thos keys: 'date', 'textcolor', 'backgroundcolor', 'title'
     *                  -date: iso date
     *                  -textcolor: hexacolor (ex: #ff5566)
     *                  -backgroundcolor: hexacolor (ex: #ff5566)
     *                  -title: a text
     *
     * @return Widget_DatePicker
     */
    public function setSelectableDates(array $dates) {
        $this->selectableDates = $dates;

        return $this;
    }


    /**
     * add a selectable Date
     * @param [] $date array that has to have thos keys: 'date', 'textcolor', 'backgroundcolor', 'title'
     *                 -date: iso date
     *                  -textcolor: hexacolor (ex: #ff5566)
     *                  -backgroundcolor: hexacolor (ex: #ff5566)
     *                 -title: a text
     *
     * @return Widget_DatePicker
     */
    public function addSelectableDate(array $date) {
        $this->selectableDates[] = $date;
    }


    /**
     * Sets the format of the date.
     * Only usage of %d, %m, %Y are allowed
     *
     * @param string $format    The format string as described in the php strftime function.
     * @return self
     */
    public function setFormat($format)
    {
        assert('is_string($format); /* The "format" parameter must be a string */');
        $this->setMetadata('format', $format);
        return $this;
    }

    /**
     * Returns the format of the date.
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->getMetadata('format');
    }


    /**
     * Set the date to highlight on first opening if the field is blank.
     * @param string $date YYYY-MM-DD
     * @return self
     */
    public function setDefaultDate($date)
    {
        $ts = bab_mktime($date);
        $datePickerFormat = date(str_replace('%', '', $this->getMetadata('format')), $ts);
        $this->setMetadata('defaultDate', $datePickerFormat);
        return $this;
    }


    /**
     * @return string ISO date or empty string
     */
    public function getValue()
    {
        $value = parent::getValue();
        if ('' === $value) {
            return '';
        }

        if (!preg_match('/\d{4}-\d\d-\d\d/', $value)) {
            if (false === $value = $this->getISODate($value)) {
                return '';
            }
        }

        assert('preg_match(\'/\d{4}-\d\d-\d\d/\', $value); /* The "value" must be an ISO date or empty string, not (' . $value . ') */');
        return $value;
    }


    /**
     * @deprecated.
     * @see Widget_DatePicker::setYearSelectable()
     * @param bool $active
     * @return Widget_DatePicker
     */
    public function setSelectableYear($active = true)
    {
        $this->selectableYear = $active;
        return $this;
    }

    /**
     * @deprecated.
     * @see Widget_DatePicker::isYearSelectable()
     * @return bool
     */
    public function getSelectableYear()
    {
        return $this->selectableYear;
    }


    /**
     * Activates or deactivates the possibility for the user to choose the
     * year from a drop down list.
     *
     * @param bool  $active
     * @return self
     */
    public function setYearSelectable($active = true)
    {
        $this->selectableYear = $active;
        return $this;
    }

    /**
     * Returns true if the user has the possibility to choose the
     * year from a drop down list.
     *
     * @return bool
     */
    public function isYearSelectable()
    {
        return $this->selectableYear;
    }
    
    /**
     * Activates or deactivates the possibility for the user to choose the
     * today's date
     *
     * @param bool  $active
     * @return self
     */
    public function setTodaySelectable($active = true)
    {
        $this->selectableToday = $active;
        return $this;
    }

    /**
     * Returns true if the user has the possibility to choose today's date
     *
     * @return bool
     */
    public function isTodaySelectable()
    {
        return $this->selectableToday;
    }

    /**
     * Specify an highlighted period on the datepicker.
     * If $end is not specified or null, only the $start day is highlighted.
     * If $end is specified, its date is not included in the highlighted period.
     *
     * @since 1.0.74
     *
     * @param BAB_DateTime|string       $start
     * @param BAB_DateTime|string|null  $end.
     */
    public function setHighlightedDate($start, $end = null)
    {
        if ($start instanceof BAB_DateTime) {
            $start = $start->getIsoDate();
        }
        if ($end instanceof BAB_DateTime) {
            $end = $end->getIsoDate();
        }
        $this->setMetadata('highlightedDateMin', $start);
        $this->setMetadata('highlightedDateMax', $end);

        return $this;
    }

    /**
     * Specify a date minimum which the date can't be before.
     * If $date is another datepicker the value of this widget is dynamically used.
     *
     * @param BAB_DateTime|Widget_DatePicker $date
     * @return self
     */
    public function setMinDate($date)
    {
        if ($date instanceof BAB_DateTime) {
            $this->mindate = $date;
        } elseif ($date instanceof Widget_DatePicker) {
            $this->setMetadata('from', $date->getId());
            $date->setMetadata('to', $this->getId());
        }

        return $this;
    }

    /**
     * Return the minimum date "BAB_DateTime" object
     *
     * @return BAB_DateTime
     */
    public function getMinDate()
    {
        return $this->mindate;
    }


    /**
     * Specify a date maximum which the date can't be before
     * If $date is another datepicker the value of this widget is dynamically used.
     *
     * @param BAB_DateTime|Widget_DatePicker $date
     * @return self
     */
    public function setMaxDate($date)
    {
        if ($date instanceof BAB_DateTime) {
            $this->maxdate = $date;
        } elseif ($date instanceof Widget_DatePicker) {
            $this->setMetadata('to', $date->getId());
            $date->setMetadata('from', $this->getId());
        }

        return $this;
    }

    /**
     * Return the maximum date "BAB_DateTime" object
     *
     * @return BAB_DateTime
     */
    public function getMaxDate()
    {
        return $this->maxdate;
    }


    /**
     * Selects if the date picker should appear as a dropdown or inline.
     *
     * @param bool $active
     * @return self
     */
    public function setInline($active = true)
    {
        $this->inline = $active;
        return $this;
    }

    /**
     * Returns true if the date picker is set to appear inline.
     *
     * @return bool
     */
    public function isInline()
    {
        return $this->inline;
    }


    /**
     * @deprecated
     * @see Widget_DatePicker::isInline()
     * @return bool
     */
    public function getInline()
    {
        return $this->inline;
    }


    /**
     * @deprecated
     * @see Widget_DatePicker::setMonthSelectable()
     * @param bool $active
     * @return self
     */
    public function setSelectableMonth($active = true)
    {
        $this->selectableMonth = $active;
        return $this;
    }

    /**
     * @deprecated
     * @see Widget_DatePicker::isMonthSelectable()
     * @return bool
     */
    public function getSelectableMonth()
    {
        return $this->selectableMonth;
    }


    /**
     * Activates or deactivates the possibility for the user to choose the
     * month from a drop down list.
     *
     * @param bool $active
     * @return self
     */
    public function setMonthSelectable($active = true)
    {
        $this->selectableMonth = $active;
        return $this;
    }

    /**
     * Returns true if the user has the possibility to choose the
     * month from a drop down list.
     *
     * @return bool
     */
    public function isMonthSelectable()
    {
        return $this->selectableMonth;
    }


    /**
     * Change the size of the date picker input field (in characters) when not inline.
     *
     * @param int $size
     * @return self
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * Returns the size of the date picker input field (in characters) when not inline.
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }


    /**
     * Change the number of months displayed by the date picker
     *
     * @param int $numberOfMonthsHorizontal
     * @param int $numberOfMonthsVertical
     * @return self
     */
    public function setNumberOfMonths($numberOfMonthsHorizontal, $numberOfMonthsVertical = 1)
    {
        $this->numberOfMonths = array($numberOfMonthsVertical, $numberOfMonthsHorizontal);
        return $this;
    }

    /**
     * Returns the number of months displayed by the date picker
     *
     * @return int[]
     */
    public function getNumberOfMonths()
    {
        return $this->numberOfMonths;
    }



    /**
     * Sets
     * @param string	$url use __date__ in url formation to get the selected date
     * @return self
     */
    public function setDateClickUrl($url, $ajax = false)
    {
        $this->dateClickUrl = $url;
        $this->dateClickUrlAjax = $ajax;
        return $this;
    }

    /**
     * Returns
     *
     * @return string
     */
    public function getDateClickUrl()
    {
        return $this->dateClickUrl;
    }



    /**
     * get title of widget (tooltip)
     * @return string
     */
    public function getTitle()
    {
        $title = parent::getTitle();
        if (!isset($title)) {
           $dateFormat = str_replace(array('%d', '%m', '%Y'), array('jj', 'mm', 'aaaa'), $this->getFormat());
           $dateExample = strftime($this->getFormat(), bab_mktime('2005-12-25'));
           return sprintf(widget_translate("Date format: '%s'.  Eg. '%s'"), $dateFormat, $dateExample);
        }
        return $title;
    }


    /**
     * get ISO from a date typed in the format set in the setFormat method
     * @param	string	$date
     * @return 	string|false	ISO date or false if error
     */
    public function getISODate($date)
    {
        if (empty($date)) {
            return '0000-00-00';
        }

        // We use the ?P<name> (named subpattern) notation which is compatible with
        // pre-5.2.2 versions of PHP and seems to also work with more recent versions.
        // @see http://php.net/preg_match => "Using named subpattern"

        $expreg = str_replace(
            array('%d', '%m', '%Y'),
            array('(?P<day>[0-9]{1,2})', '(?P<month>[0-9]{1,2})', '(?P<year>[0-9]{4})'),
            '/'.preg_quote($this->getFormat(), '/').'/'
        );

        $regs = array();
        if (preg_match($expreg, $date, $regs)) {
            return sprintf('%04d-%02d-%02d', $regs['year'], $regs['month'], $regs['day']);
        } else {
            return false;
        }
    }


    /**
     * Display or hide week numbers on the datepicker.
     *
     * @param bool $show
     *            True/false to display/hide week numbers on datepicker.
     *            If not specified, showWeek returns the current value.
     * @return self|bool
     */
    public function showWeek($show = null)
    {
        if (isset($show)) {
            $this->showWeek = $show;
            return $this;
        }
        return $this->showWeek;
    }

    /**
     * Sets the maximum input size (in characters) of the line edit.
     *
     * @param int $maxSize
     * @return self
     */
    public function setMaxSize($maxSize)
    {
        assert('is_int($maxSize);  /* The "maxSize" parameter must be an integer */');
        $this->_maxSize = $maxSize;
        return $this;
    }

    /**
     * Returns the maximum input size (in characters) of the line edit.
     *
     * @return int
     */
    public function getMaxSize()
    {
        return $this->_maxSize;
    }



    /**
     * @see Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        if (!$this->isDisplayMode()) {
            $classes[] = 'widget-datepicker';
        } else {
            $classes[] = 'widget-displaymode';
        }
        if ($this->isInline()) {
            $classes[] = 'inline';
        }
        if ($this->isMonthSelectable()) {
            $classes[] = 'widget-datepicker-changemonth';
        }
        if ($this->isYearSelectable()) {
            $classes[] = 'widget-datepicker-changeyear';
        }
        if ($this->isTodaySelectable()) {
            $classes[] = 'widget-datepicker-todaybutton';
        }
        return $classes;
    }



    /**
     * Initializes metadata.
     */
    protected function initMetadata()
    {
        $this->setMetadata('numberOfMonths', $this->numberOfMonths);
        $this->setMetadata('dateClickUrl', $this->dateClickUrl);
        $this->setMetadata('dateClickUrlAjax', $this->dateClickUrlAjax);

        if ($this->getMinDate() != null) {
            $this->setMetadata('mindatey', $this->getMinDate()->getYear());
            $this->setMetadata('mindatem', $this->getMinDate()->getMonth());
            $this->setMetadata('mindated', $this->getMinDate()->getDayOfMonth());
        }

        if ($this->getMaxDate() != null) {
            $this->setMetadata('maxdatey', $this->getMaxDate()->getYear());
            $this->setMetadata('maxdatem', $this->getMaxDate()->getMonth());
            $this->setMetadata('maxdated', $this->getMaxDate()->getDayOfMonth());
        }

        if ($this->showWeek) {
            $this->setMetadata('showWeek', $this->showWeek);
        }

        if(!empty($this->selectableDates)) {
            $this->setMetadata('selectableDate', $this->selectableDates);
        }
    }




    /**
     * @param Widget_Canvas $canvas
     *
     * @return string
     */
    protected function getScripts(Widget_Canvas $canvas)
    {
        $widgetAddon = bab_getAddonInfosInstance('widgets');
        $jquery = bab_jquery();

        return $canvas->loadAddonScript($this->getId(), $widgetAddon, 'widgets.datepicker.jquery.js')
            . $canvas->loadScript($this->getId(), $jquery->getI18nUrl(bab_getLanguage()));
    }



    /**
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $value = $this->getValue();
        if (empty($value) || $value == '0000-00-00' || $value == '0000-00-00 00:00:00') {
            $value = '';
        } else {
            $value = strftime($this->getFormat(), bab_mktime($this->getValue()));
        }

        $classes = $this->getClasses();

        if ($this->isDisplayMode()) {
            return $canvas->richtext(
                $this->getId(),
                $classes,
                $value,
                BAB_HTML_ALL ^ BAB_HTML_P,
                $this->getCanvasOptions()
            );
        }

        $this->initMetadata();

        if ($this->isInline()) {
            $datePicker = $canvas->div(
                $this->getId(),
                $this->getClasses(),
                array($this->getValue())
            );
        } else {
            $datePicker = $canvas->lineInput(
                $this->getId(),
                $this->getClasses(),
                $this->getFullName(),
                $value,
                $this->getSize(),
                $this->getMaxSize(),
                $this->isDisabled(),
                false,
                false,
                'text',
                $this->getPlaceHolder(),
                $this->getCanvasOptions(),
                $this->getTitle(),
                $this->getAttributes()
            );
        }

        return $datePicker
            . $canvas->metadata($this->getId(), $this->getMetadata())
            . $this->getScripts($canvas);
    }
}
