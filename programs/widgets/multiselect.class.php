<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2010 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
include_once dirname(__FILE__).'/inputwidget.class.php';
include_once dirname(__FILE__).'/select.class.php';



/**
 * Constructs a Widget_Select.
 *
 * @param string		$id			The item unique id.
 * @return Widget_Select
 */
function Widget_Multiselect($id = null)
{
    return new Widget_Multiselect($id);
}


/**
 * A Widget_Select is a widget that let the user select an option from a list of
 * available choices.
 * It is usually displayed as a drop down list unless the setSize method is used
 * with a non null parameter.
 */
class Widget_Multiselect extends Widget_Select implements Widget_Displayable_Interface
{
    private $_options = array();

    private $_size = null;

    private $_multiple = true;

    /**
     * All options are objects Widget_SelectOption
     * @var bool
     */
    private $optionsAsObject = true;

    /**
     * @param string $id			The item unique id.
     * @return Widget_Select
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->setSelectedText(widget_translate('# selected'));
        $this->setSelectText(widget_translate('Select options'));
        $this->setSelectedList(1);
        $this->setHeadersDisplay(false);
        $this->setCheckAllext(widget_translate('Check all'));
        $this->setUncheckAllext(widget_translate('Uncheck all'));

        $this->setFilterLabel(widget_translate('Filter'));
        $this->setFilterPlaceholder(widget_translate('Enter keywords'));
        $this->setFixed(false);

        return $this;
    }

    public function setFilterLabel($str)
    {
        $this->setMetadata('filter_label', $str);
        return $this;
    }

    public function setFilterPlaceholder($str)
    {
        $this->setMetadata('filter_placeholder', $str);
        return $this;
    }

    public function setSelectedText($str)
    {
        $this->setMetadata('selected_text', $str);
        return $this;
    }

    public function setSelectText($str)
    {
        $this->setMetadata('select_text', $str);
        return $this;
    }

    public function setCheckAllext($str)
    {
        $this->setMetadata('checkall_text', $str);
        return $this;
    }

    public function setUncheckAllext($str)
    {
        $this->setMetadata('uncheckall_text', $str);
        return $this;
    }

    public function setSingleSelect()
    {
        $this->_multiple = false;
        $this->setUncheckAllext(widget_translate('Remove selection'));
        return $this;
    }

    public function setMultipleSelect()
    {
        $this->_multiple = true;
        $this->setUncheckAllext(widget_translate('Uncheck all'));
        return $this;
    }

    public function getMultiple()
    {
        return $this->_multiple;
    }

    /**
     * Have to be called if the multiselect is in a fixed element
     * @param bool $bool
     * @return Widget_Multiselect
     */
    public function setFixed($bool = true)
    {
        $this->setMetadata('fixed', $bool);
        return $this;
    }

    /**
     * Enable uncheck / check all options
     * @param bool $bool
     * @return Widget_Multiselect
     */
    public function setHeadersDisplay($bool = true)
    {
        $this->setMetadata('headers_display', $bool);
        return $this;
    }



    /**
     * Sets the number of selected item values to display (or nb items selected if greater than this value).
     *
     * @param int $number
     * @return Widget_Multiselect
     */
    public function setSelectedList($number = 0)
    {
        $this->setMetadata('selected_list', $number);
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_InputWidget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-multiselect';
        foreach ($classes as $k => $classe) {
            if ($classe == 'widget-select') {
                unset($classes[$k]);
            }
        }
        return $classes;
    }


    /**
     * Display in mutli columns.
     *
     * @param int $nbColumns    1 to 4
     *
     * @return $this
     */
    public function setMultiColumn($nbColumns)
    {
        $this->setMetadata('multicolumn', $nbColumns);

        return $this;
    }



    /**
     * Display filter form in the list
     *
     * @param bool $filter
     *
     * @return $this
     */
    public function setFilterable($filter)
    {
        $this->setMetadata('filter', $filter);
        if ($filter) {
            $this->setMetadata('headers_display', $filter);
        }

        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if (is_array($this->_options)) {
            foreach ($this->_options as &$optgroup) {
                if (isset($optgroup[$this->getValue()])) {
                    $selected = $optgroup[$this->getValue()];
                    if (!($selected instanceof Widget_SelectOption)) {
                        $selected = new Widget_SelectOption($this->getValue(), $selected);
                        $optgroup[$this->getValue()] = $selected;
                    }

                    $selected->selected();
                    break;
                }
            }
        }

        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            $value = '';
            $keys = $this->getValue();

            if (!is_array($keys)) {
                $keys = array($keys);
            }
            $options = $this->getOptions();
            $grps = $this->getOptGroups();
            foreach ($keys as $key) {
                if (isset($options[$key])) {
                    $value.= $options[$key].', ';
                } else {

                    foreach ($grps as $group => $arr) {
                        if (isset($arr[$key])) {
                            $value.= $group.' > '.$arr[$key].', ';
                            break;
                        }
                    }
                }
            }

            return $canvas->richtext(
                $this->getId(),
                $classes,
                $value,
                BAB_HTML_ALL ^ BAB_HTML_P,
                $this->getCanvasOptions()
            );

        }

        $jquery = bab_jQuery();

        $this->setMetadata('multiple', $this->getMultiple());

        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->select(
            $this->getId(),
            $this->getClasses(),
            $this->getFullName(),
            $this->getValue(),
            $this->getOptions(),
            $this->getOptGroups(),
            $this->isDisabled(),
            $this->getSize(),
            null,
            null,
            true,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadStyleSheet($GLOBALS['babInstallPath'].'styles/'.$jquery->getStyleSheetUrl())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.multiselect.filter.jquery.js')
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.multiselect.jquery.js');
    }
}
