<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/tablemodelview.class.php';



class widget_TableArrayView extends widget_TableModelView
{
    protected $pageLength = 20;
    protected $content = array();
    protected $header = array();

    public $sortAjaxAction = null;

    public $pageAjaxAction = null;

    protected $sortAscending = null;
    protected $sortField = null;


    /**
     * @param array         $header     Indexed column header text
     * @param array         $content    Indexed content
     * @param string|array  $name       The name use for the table
     * @param array         $columns    Optional columns classes
     * @param string        $id         The item unique id.
     */
    public function __construct($header = null, $content = null, $name = null, $columns = null, $id = null)
    {
        if (isset($header)) {
            $this->setHeader($header);
        }
        if (isset($content)) {
            $this->setContent($content);
        }

        Widget_TableView::__construct($columns, $id);
        if (isset($name)) {
            $this->setName($name);
        }
    }


    public function setHeader($header)
    {
        assert('is_array($header); /* The "header" parameter must be an array. */');
        $this->header = $header;
        
        return $this;
    }


    public function setContent($content)
    {
        assert('is_array($content); /* The "content" parameter must be an array. */');
        $this->content = $content;
        
        return $this;
    }


    public function setPageLength($pageLength)
    {
        $this->pageLength = $pageLength;
        return $this;
    }



    /**
     * Sets the currently displayed page. Displayed data depends on pageLength.
     *
     * @param integer $pageNumber		First page is 0.
     * @return widget_TableModelView
     */
    public function setCurrentPage($pageNumber)
    {
        $this->currentPage = $pageNumber;
        return $this;
    }


    /**
     * Returns the currently displayed page.
     *
     * @return int		The currently displayed page. First page is 0.
     */
    public function getCurrentPage()
    {
        if (! isset($this->currentPage)) {
            $namePath = $this->getNamepath();
            $name = array_shift($namePath);
            $filter = bab_rp($name, array('filter' => array('pageNumber' => 0)));
            return $filter['filter']['pageNumber'];
        }

        return $this->currentPage;
    }


    /**
     *
     */
    public function computeHeaderSection()
    {
        $columns = $this->header;
        $W = bab_Widgets();
        $layout = $this->getLayout();
        if ($layout instanceof Widget_GridLayout) {
            $layout->addHeaderSection('header', null, 'widget-table-header');
        } else {
            $this->addSection('header', null, 'widget-table-header');
        }
        $this->setCurrentSection('header');

        $j = 0;
        foreach ($columns as $column) {
            if ($column instanceof Widget_Displayable_Interface) {
                $this->addItem($column, 0, $j);
            } else {
                $this->addItem($W->Label($column), 0, $j);
            }
            $j++;
        }
    }


    /**
     *
     */
    public function computeBodySection()
    {
        $contents = $this->content;
        $this->addSection('body', null, 'widget-table-body');
        $this->setCurrentSection('body');

        $contents = array_slice($contents, $this->getCurrentPage()*$this->pageLength, $this->pageLength, true);

		$i = 1;
        foreach ($contents as $id => $contentrow) {
        	$j = 0;
            foreach ($contentrow as $key => $contentcell) {
                $this->addItem($this->computeCellContent($contentrow, $key, $id), $i, $j);
                $j++;
            }
            $i++;
        }
    }


    /**
     * (non-PHPdoc)
     * @see widget_TableModelView::computeCellContent($record, $fieldPath)
     */
    public function computeCellContent($content, $key, $id)
    {
        if ($content[$key] instanceof Widget_Displayable_Interface) {
            return $content[$key];
        }

        $W = bab_Widgets();

        return $W->Label($content[$key]);
    }



    /**
     * Set filter form ajax action.
     *
     * @param Widget_Action $action		A controller method accepting 3 optional parameters.
     * @param String		$reloadItem
     */
    public function setAjaxAction($action = null, $reloadItem = null)
    {

        if (!isset($action)) {
            $W = bab_Widgets();

            $action = $W->Action();
            $action->setMethod('addon/widgets/configurationstorage', 'table', array('key' => $this->getId()));
            $this->setAjaxAction($action, $this);


            $currentPage = $W->getUserConfiguration($this->getId() . '/page', 'widgets', true);
            if (!isset($currentPage)) {
                $currentPage = 0;
            }
            $this->setCurrentPage($currentPage);

            /*$sortField = $W->getUserConfiguration($this->getId() . '/sort', 'widgets', true);
            if (!isset($sortField)) {
                $sortField = '';
            }
            $this->setSortField($sortField);*/


        }

        $this->sortAjaxAction = $action;
        $this->pageAjaxAction = $action;

        $this->sortParameterName = 'sort';

        /*if (!$this->submit) {
            $this->submit = new Widget_SubmitButton();
            $this->submit->setLabel(widget_translate('Filter'));
        }
        $this->submit->setAjaxAction($action, $reloadItem);*/

        return $this;
    }

    public function handlePageSelector()
    {
        $contents = $this->content;
        $W = bab_Widgets();
        $this->addSection('footer', null, 'widget-table-footer');
        $this->setCurrentSection('footer');

        $selector = $W->PageSelector();
        $selector->setPageLength($this->pageLength);
        $selector->setIterator($contents);
        $selector->setCurrentPage($this->getCurrentPage());

        if (isset($this->pageAjaxAction)) {
            $action = clone $this->pageAjaxAction;
            $action->setParameter('sort', null);
            $selector->setAjaxAction($action, $this);
        }

        $namePath = $this->getNamepath();
        $namePath[] = 'filter';
        $namePath[] = 'pageNumber';

        $selector->setPageNamePath($namePath);
        //$selector->setAnchor($this->getAnchor());

        if ($selector->getNbPages() > 1) {
            return $selector;
        }

        return null;
    }

    public function handleContent()
    {
        $this->computeHeaderSection();
        $this->computeBodySection();
    }


    public function display(Widget_Canvas $canvas)
    {
        $id = null;
        $items = array();
        $classes = array();

        $this->handleContent();

        $items[] = Widget_TableView::display($canvas);
        $classes[] = 'widget-table-total-display';

        $selector = $this->handlePageSelector();

        if (isset($selector) || isset($total)) {
            $id = $this->getId();
            // $this->setId('_' . $this->getId());
        }

        if (null !== $selector) {
            $items[] = $selector->display($canvas);
            $classes[] = 'widget-filter-bottom';
        }

        return $canvas->vbox($id, $classes, $items);
    }



    /**
     *
     * @param string $filename
     */
    public function downloadXlsx($filename = null)
    {
        if (!isset($this->content) || empty($this->columns)) {
            die();
        }
        /* @var $ExcelxExport Func_ExcelxExport */
        $ExcelxExport = bab_functionality::get('ExcelxExport');

        if (!$ExcelxExport) {
            $message = widget_translate('The ExcelxExport functionality must be available to export as an Excel spreadsheet.');
            throw new Exception($message);
        }

        if (!isset($filename)) {
            $name = $this->getName();
            if (!empty($name)) {
                $filename = 'export-' . $name . '.xlsx';
            } else {
                $filename = 'export.xlsx';
            }

        }

//         $set = $this->iterator->getSet();
//         $this->initColumns($set);

        $writer = $ExcelxExport->getXlsxWriter();

        bab_setTimeLimit(3600);

        $sheetName = 'Page 1';

        $col = 0;
        $row = 0;

        $columnTypes = array();
        $columnHeaders = array();

        foreach ($this->columns as $columnPath => $column) {
            /* @var $column widget_TableModelViewColumn */
            if (!$column->isExportable()) {
                continue;
            }
            if (!isset($this->columnsDescriptions[$columnPath])) {
                $this->columnsDescriptions[$columnPath] = (string) $column->getDescription();
            }

            $field = $column->getField();
            if ($field instanceof ORM_DateField) {
                $type = 'DD/MM/YYYY';
            } elseif ($field instanceof ORM_DatetimeField) {
                $type = 'DD/MM/YYYY HH:mm';
            } elseif ($field  instanceof ORM_NumericField) {
                $type = '#,##0';
            } else {
                $type = 'string';
            }

            $text = $this->columnsDescriptions[$columnPath];
            $columnHeaderText = bab_convertStringFromDatabase(str_replace(chr(11), '', $text), bab_charset::UTF_8);

            $columnHeaders[$columnHeaderText] = $type;
            $columnTypes[$columnPath] = $type;

            $col++;
        }

        $writer->writeSheetHeader($sheetName, $columnHeaders);

        //$writer->writeSheetRow($sheetName, $row);
        $this->iterator = new ArrayIterator($this->content);

        $this->iterator->seek(0);
        while ($this->iterator->valid()) {
            $record = $this->iterator->current();

            $col = 0;
            $row++;

            $rowData = array();

            foreach ($this->columns as $columnPath => $column) {
                /* @var $column widget_TableModelViewColumn */
                if (!$column->isExportable()) {
                    continue;
                }

                $text = $record[$columnPath];

                switch ($columnTypes[$columnPath]) {

                    case 'DD/MM/YYYY':
                        if ($text === '0000-00-00') {
                            $text = '';
                        }
                        break;

                    case 'DD/MM/YYYY HH:mm':
                        if ($text === '0000-00-00 00:00:00') {
                            $text = '';
                        }
                        break;

                    case 'string':
                    default:
                        break;
                }

                $rowData[] = bab_convertStringFromDatabase(str_replace(chr(11), '', $text), bab_charset::UTF_8);
                $col++;
            }

            $writer->writeSheetRow($sheetName, $rowData);
            $this->iterator->next();
        }

        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $writer->writeToStdOut();
        die;
    }
}
