<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/inputwidget.class.php';


interface Widget_TextEdit_Interface
{
    /**
     * Sets the visible width of the text edit.
     *
     * @param int $nbColumns    The number of visible columns.
     *
     * @return Widget_TextEdit
     */
    public function setColumns($nbColumns);

    /**
     * Returns the visible width (in characters) of the text edit.
     *
     *
     */
    public function getColumns();


    /**
     * Sets the vertical input size (in characters) of the text edit.
     *
     * @param int $nbLines      The number of visible lines.
     *
     * @return Widget_TextEdit
     */
    public function setLines($nbLines);

    /**
     * Returns the vertical input size (in characters) of the text edit.
     *
     * @return int
     */
    public function getLines();


    /**
     * Sets the maximum input size (in characters) of the text edit.
     *
     * @param int $maxSize
     * @return Widget_LineEdit
     */
    public function setMaxSize($maxSize);

    /**
     * Returns the maximum input size (in characters) of the text edit.
     *
     * @return int
     */
    public function getMaxSize();



    /**
     * Sets the maximum input size (in words) of the text edit.
     *
     * @param int $maxWords
     * @return Widget_LineEdit
     */
    public function setMaxWords($maxWords);

    /**
     * Returns the maximum input size (in words) of the text edit.
     *
     * @return int
    */
    public function getMaxWords();
}




/**
 * A Widget_TextEdit is a widget that let the user enter multiple lines of text.
 *
 *
 */
class Widget_TextEdit extends Widget_InputWidget implements Widget_Displayable_Interface, Widget_TextEdit_Interface
{
    /**
     * The number of columns in characters.
     * @var int
     */
    private $nbColumns = 40;

    /**
     * The number of lines.
     * @var int
     */
    private $nbLines = 5;

    /**
     * The minimum input size (in characters) of the text edit.
     * @var int
     */
    private $minSize = null;
    private $submitMinMessage;

    /**
     * The maximum input size (in characters) of the text edit.
     * @var int
     */
    private $maxSize = null;
    private $submitMaxMessage;

    /**
     * The maximum input size (in words) of the text edit.
     * @var int
     */
    private $maxWords = null;
    
    private $autoSize = false;

    /**
     * @param string $id            The item unique id.
     * @return self
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }


    /**
     * Sets the visible width of the text edit.
     *
     * @param int $nbColumns    The number of visible columns.
     *
     * @return self
     */
    public function setColumns($nbColumns)
    {
        assert('is_int($nbColumns); /* The "nbColumns" parameter must be an integer */');
        $this->nbColumns = $nbColumns;
        return $this;
    }

    /**
     * Returns the visible width (in characters) of the text edit.
     */
    public function getColumns()
    {
        return $this->nbColumns;
    }


    /**
     * Sets the vertical input size (in characters) of the text edit.
     *
     * @param int $nbLines      The number of visible lines.
     *
     * @return self
     */
    public function setLines($nbLines)
    {
        assert('is_int($nbLines); /* The "nbLines" parameter must be an integer */');
        $this->nbLines = $nbLines;
        return $this;
    }

    /**
     * Returns the vertical input size (in characters) of the text edit.
     *
     * @return int
     */
    public function getLines()
    {
        return $this->nbLines;
    }


    /**
     * Sets the minimum input size (in characters) of the text edit.
     *
     * @param int $minSize
     * @return self
     */
    public function setMinSize($minSize, $message = '')
    {
        assert('is_int($minSize);  /* The "minSize" parameter must be an integer */');
        $this->minSize = $minSize;
        $this->submitMinMessage = $message;
        return $this;
    }


    /**
     * Returns the minimum input size (in characters) of the text edit.
     *
     * @return int
     */
    public function getMinSize()
    {
        return $this->minSize;
    }


    /**
     * Returns the minimum input size message.
     *
     * @return string
     */
    public function getSubmitMinMessage()
    {
        return $this->submitMinMessage;
    }


    /**
     * Sets the maximum input size (in characters) of the text edit.
     *
     * @param int $maxSize
     * @return self
     */
    public function setMaxSize($maxSize, $message = '')
    {
        assert('is_int($maxSize);  /* The "maxSize" parameter must be an integer */');
        $this->maxSize = $maxSize;
        $this->submitMaxMessage = $message;
        return $this;
    }


    /**
     * Returns the maximum input size (in characters) of the text edit.
     *
     * @return int
     */
    public function getMaxSize()
    {
        return $this->maxSize;
    }


    /**
     * Returns the maximum input size message.
     *
     * @return string
     */
    public function getSubmitMaxMessage()
    {
        return $this->submitMaxMessage;
    }

    /**
     * Sets the maximum input size (in words) of the text edit.
     *
     * @param int $maxWords
     * @return self
     */
    public function setMaxWords($maxWords)
    {
        assert('is_int($maxWords);  /* The "maxWords" parameter must be an integer */');
        $this->maxWords = $maxWords;
        return $this;
    }

    /**
     * Returns the maximum input size (in words) of the text edit.
     *
     * @return int
     */
    public function getMaxWords()
    {
        return $this->maxWords;
    }

    /**
     * (non-PHPdoc)
     * @see programs/widgets/Widget_InputWidget#getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-textedit';
        return $classes;
    }
    
    
    /**
     * @return boolean
     */
    public function isAutoSize()
    {
        return $this->autoSize;
    }
    
    /**
     * @param boolean $autoSize
     */
    public function setAutoSize($autoSize = true)
    {
        $this->autoSize = $autoSize;
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if($this->isAutoSize()){
            $this->addClass('widget-autoresize');
            $this->setLines(1);
        }
        
        if ($this->isDisplayMode()) {

            $classes = $this->getClasses();
            $classes[] = 'widget-displaymode';

            if ($this->getColumns()) {

                $options = $this->getCanvasOptions();
                if (is_null($options)) {
                    $options = $this->Options();
                }
                $options->minWidth(1.4 * $this->getColumns(), 'ex');
                $this->setCanvasOptions($options);
            }
            return $canvas->richtext(
                $this->getId(),
                $classes,
                $this->getValue(),
                BAB_HTML_ALL,
                $this->getCanvasOptions()
            )
            . $canvas->metadata($this->getId(), $this->getMetadata());

        } else {

            if (isset($this->maxSize)) {
                $this->setMetadata('maxSize', $this->getMaxSize());
                $this->setMetadata('submitMaxMessage', $this->getSubmitMaxMessage());
            }
            if (isset($this->minSize)) {
                $this->setMetadata('minSize', $this->getMinSize());
                $this->setMetadata('submitMinMessage', $this->getSubmitMinMessage());
            }
            if (isset($this->maxWords)) {
                $this->setMetadata('maxWords', $this->getMaxWords());
            }

            $placeholder = $this->getPlaceHolder();
            if (isset($placeholder)) {
                $this->addAttribute('placeholder', $placeholder);
            }

            return $canvas->textInput(
                $this->getId(),
                $this->getClasses(),
                $this->getFullName(),
                $this->getValue(),
                $this->getColumns(),
                $this->getLines(),
                $this->isDisabled(),
                $this->getTitle(),
                $this->getAttributes()
            )
            . $canvas->metadata($this->getId(), $this->getMetadata());
        }
    }
}
