<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';




//if (!class_exists('bab_eventPageRefreshed')) {
//	class bab_eventPageRefreshed extends bab_event { }
//}








bab_Widgets()->includePhpClass('Widget_Frame');

/**
 * A Page widget
 *
 */
class Widget_Page extends Widget_Frame
{
	private		$javascript_toolkit	= array();
	protected	$errors			= array();
	public		$title				= '';
	public		$msgerror			= '';
	protected	$scripts			= array();
	protected	$deferedScripts	= array();
	protected	$stylesheets 		= array();
	protected	$alternateLinks	= array();


	/**
	 * @param Widget_Layout $layout	The layout that will manage how widgets are displayed in this container.
	 * @param string $id			The item unique id.
	 */
	public function __construct($id = null, Widget_Layout $layout = null)
	{
		parent::__construct($id, $layout);
	}

	/**
	 * Set the page title
	 * define a protected variable to use in the "title" html tag
	 * @param	string	$title
	 * @return	Widget_Page
	 */
	public function setTitle($title)
	{
	    $babBody = bab_getBody();
		$babBody->setTitle($title);
		//parent::setTitle($title); // ajoute un attribut title sur toute la page
		$this->title = bab_toHtml($title);
		return $this;
	}

	/**
	 * Get the page title
	 * @return string
	 */
	public function getPageTitle()
	{
	    return $this->title;
	}


	/**
	 * Set the page error message
	 * @param	string	$title
	 * @return	Widget_Page
	 */
	public function addError($error)
	{
		$this->errors[] = bab_toHtml($error);
		return $this;
	}

	/**
	 * Test if page contain errors
	 * @return bool
	 */
	public function containErrors()
	{
		return (count($this->errors) > 0);
	}


	/**
	 * @param	string	$toolkitName
	 * @return	Widget_Page
	 */
	public function setJavascriptToolkit($toolkitName)
	{
		$this->javascript_toolkit[$toolkitName] = 1;
	}


	/**
	 * Test if this page implement a particular toolkit
	 * @return boolean
	 */
	public function isCompatibleWith($toolkitName)
	{
		return isset($this->javascript_toolkit[$toolkitName]);
	}


	/**
	 * add an alternate link
	 * populate a protected variable with the url to the javascript file to include in HTML page
	 * @param	string	$url
	 * @return	Widget_Page
	 */
	public function addAlternateLink($url, $id = null, $title = null)
	{
		$this->alternateLinks[$url] = array('id' => $id, 'title' => $title);
		return $this;
	}



	public function getAlternateLinks()
	{
		return $this->alternateLinks;
	}


	/**
	 * add a link to a script
	 * populate a protected variable with the url to the javascript file to include in HTML page
	 * @param	string	$url
	 * @return	Widget_Page
	 */
	public function addJavascriptFile($url)
	{
		$this->deferedScripts[$url] = $url;
		return $this;
	}


	/**
	 * Add a link to a script. This script will be included before all other already added javascript files.
	 * populate a protected variable with the url to the javascript file to include in HTML page.
	 *
	 * @param	string	$url	The url is relative to the site root.
	 * @return	Widget_Page
	 */
	public function prependJavascriptFile($url)
	{
		$this->scripts[$url] = $url;
		return $this;
	}


	/**
	 * Returns an array of javascript file urls in their order of expected inclusion.
	 *
	 * @return array		An array of strings containing the urls of javascript files.
	 */
	public function getJavascriptFiles()
	{
		return $this->scripts;
	}


	/**
	 * Returns an array of javascript file urls in their order of expected inclusion.
	 *
	 * @return array		An array of strings containing the urls of javascript files.
	 */
	public function getDeferedJavascriptFiles()
	{
		return $this->deferedScripts;
	}


	/**
	 * Add a link to a stylesheet
	 * populate a protected variable with the url to the css stylesheets to include in HTML page
	 *
	 * @param	string	$url 	The url can be relative to the site root (prefered) or to the kernel styles directory (deprecated)
	 * @return	Widget_Page
	 */
	public function addStyleSheet($url)
	{

		// fix deprecated

		list($test) = explode('?', $url);

		if (!file_exists($test)) {
			$url = $GLOBALS['babInstallPath'].'styles/'.$url;
		}

		$this->stylesheets[] = $url;
		return $this;
	}

	/**
	 * Returns an array of css file urls in their order of expected inclusion.
	 *
	 * @return array
	 */
	public function getStyleSheets()
	{
		return $this->stylesheets;
	}

	/**
	 * Stop the script and display page
	 * @param	Widget_Canvas	$canvas
	 */
	public function pageEcho(Widget_Canvas $canvas)
	{
		$babBody = bab_getBody();

		// hack to get infos from babBody
		if (!empty($babBody->msgerror)) {
			$this->msgerror = $babBody->msgerror;
		} else {
			$this->msgerror = implode('<br />', $this->errors);
		}

		if (!empty($babBody->title)) {
			$this->title = $babBody->title;
		}

		// scripts or stylesheets canvas dependant
		$canvas->setPageDecorations($this, self::$allClasses);

		die($this->display($canvas));
	}



	public function display(Widget_Canvas $canvas)
	{

		/*
		if ($cacheHeaders = $this->getCacheHeaders())
		{
			$cacheHeaders->send();
		}
		*/

		return parent::display($canvas);
	}

}
