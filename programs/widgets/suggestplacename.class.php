<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/suggestlineedit.class.php';






/**
 * Constructs a Widget_SuggestPlaceName.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SuggestLineEdit
 */
function Widget_SuggestPlaceName($id = null)
{
	return new Widget_SuggestPlaceName($id);
}


/**
 * A Widget_SuggestPlaceName is a widget that let the user enter a city/town/village.
 * Propose postal codes suggestions based on Geonames functionality
 * @link http://wiki.ovidentia.org/index.php/Base_de_donn%C3%A9s_GeoNames
 */
class Widget_SuggestPlaceName extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{


	public function __construct($id = null)
	{
		parent::__construct($id);

		$this->setMetadata('suggestparam', 'search');
	}



	private function getLanguage()
	{
		if (isset($GLOBALS['babLanguage']))
		{
			return $GLOBALS['babLanguage'];
		}

		return bab_rp('language', 'fr');
	}


	/**
	 * Set the widget used for country
	 * postal codes will be suggested according to country if the field is not empty
	 *
	 * @param	Widget_SuggestCountry | Widget_Select	$inputcountry
	 *
	 * @return	Widget_SuggestPlaceName
	 */
	public function setRelatedCountry(Widget_InputWidget $inputcountry) {

		$fields = $this->getMetadata('fields');

		if (null === $fields) {
			$fields = array();
		}

		$fields['country'] = $inputcountry->getId();

		$this->setMetadata('fields', $fields);

		$this->setMetadata('extra_target', $inputcountry->getId());

		return $this;
	}


	/**
	 * @return false | string
	 */
	private function getRelatedCountry() {
		return bab_rp('country', false);
	}



	/**
	 * Set the widget used for postal code
	 * @return 	Widget_SuggestPlaceName
	 */
	public function setRelatedPostalCode(Widget_SuggestPostalCode $suggestpostalcode) {
		$this->setMetadata('info_target', $suggestpostalcode->getId());
		return $this;
	}


	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-suggestcountry';
		return $classes;
	}


	/**
	 * Send suggestions
	 */
	public function suggest() {

		if (false !== $keyword = $this->getSearchKeyword()) {

			/* @var $GeoNames Func_GeoNames */
			if ($GeoNames = @bab_functionality::get('GeoNames')) {

				// set language to query geoname
				if (isset($_GET['language']))
				{
					$GLOBALS['babLanguage'] = $this->getLanguage();
				}

				$country_iso = null;

				if (false !== $country = $this->getRelatedCountry()) {

					$crecord = $GeoNames->getCountryFromName(bab_getStringAccordingToDataBase($country, 'UTF-8'));



					if ($crecord && isset($crecord->iso)) {
						$country_iso = $crecord->iso;
					}
				}



				$piterator = $GeoNames->searchPlaceFromName($keyword, $country_iso);
				$i = 0;

				foreach($piterator as $prec) {
					$i++;
					if ($i > Widget_SuggestLineEdit::MAX) {
						break;
					}

					if (isset($prec->country)) {
						$country = $prec->country;
					} else {
						$country = '';
					}



					parent::addSuggestion('', $prec->placename, $prec->postalcode, $country);
				}
			}

			parent::sendSuggestions();
		}
	}



    /**
     * (non-PHPdoc)
     * @see Widget_SuggestLineEdit::display()
     */
	public function display(Widget_Canvas $canvas)
	{
	    if (function_exists('bab_getDbVersion') && version_compare(bab_getDbVersion(), '7.8.90', '>='))
	    {
	        $this->setMetadata('suggesturl', array('addon' => 'widgets.suggest', 'idx' => 'placename', 'language' => $this->getLanguage(), 'convertCase' => $this->getConvertCase()));
	    } else {
	        $this->setMetadata('suggesturl', array('tg' => 'addon/widgets/suggest', 'idx' => 'placename'));
	    }

		return parent::display($canvas);
	}

}