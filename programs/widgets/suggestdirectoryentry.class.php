<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/suggestlineedit.class.php';






/**
 * Constructs a Widget_SuggestDirectoryEntry.
 *
 * @param string|array	$directory	The the directory id which entities come from.
 * @param string		$id			The item unique id.
 * @return Widget_SuggestDirectoryEntry
 */
function Widget_SuggestDirectoryEntry($directory = 1, $id = null)
{
	return new Widget_SuggestDirectoryEntry($directory, $id);
}


/**
 * A Widget_SuggestDirectoryEntry is a widget that let the user found a user with entry.
 * Propose user suggestions based on database
 */
class Widget_SuggestDirectoryEntry extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{


	/**
	 * @param string		$directory		The directory id the entities come from.
	 * @param string		$id				The item unique id.
	 */
	public function __construct($directory = 1, $id = null)
	{
		parent::__construct($id);

		if (false !== $keyword = $this->getSearchKeyword()) {
			global $babDB;
			$info = bab_getDirInfo($directory);

			if($info['id_group'] == 0){
				$_query = "
					SELECT
						e.id,
						e.sn as lastname,
						e.givenname as firstname,
						e.id_directory,
						db.name as directory

					FROM bab_dbdir_entries as e

					LEFT JOIN bab_db_directories as db
					ON db.id = e.id_directory

					WHERE e.id_directory IN (".$babDB->quote($directory).")

					AND (lastname LIKE '%".$keyword."%' or firstname LIKE '%".$keyword."%')

					LIMIT 0,20";
				$res = $babDB->db_query($_query);

				$i = 0;
				if($babDB->db_num_rows($res) == 0){
					$this->addSuggestion('', widget_translate('No user match'));
				}else{
					while ($user = $babDB->db_fetch_assoc($res)){
						$i++;
						if ($i > Widget_SuggestLineEdit::MAX) {
							break;
						}
						$this->addSuggestion($user['id'], $user['lastname'] . " " . $user['firstname']);
					}
				}
			}else{
				$users = bab_getGroupsMembers($info['id_group']);
				$i = 0;
				foreach($users as $user){
					$dirid = bab_getUserDirEntryId($user['id']);
					if((stripos($user['lastname'], $keyword) !== false || stripos($user['firstname'], $keyword) !== false) && $dirid){
						$i++;
						if ($i > Widget_SuggestLineEdit::MAX) {
							break;
						}

						$this->addSuggestion($dirid, $user['lastname'] . " " . $user['firstname']);
					}
				}
				if($i === 0){
					$this->addSuggestion('', widget_translate('No user match'));
				}
			}

			$this->sendSuggestions();
		}
	}

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-suggestdirectoryentry';
		return $classes;
	}
}
