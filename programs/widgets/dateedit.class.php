<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_DateEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_DateEdit
 */
function Widget_DateEdit($id = null)
{
	return new Widget_DateEdit($id);
}


/**
 * A Widget_DateEdit is a widget that lets the user enter a date.
 */
class Widget_DateEdit extends Widget_InputWidget implements Widget_Displayable_Interface
{

    /**
     * Sets the format of the date.
     * Only usage of %d, %m, %Y are allowed
     *
     * @param string $format	The format string as described in the php strftime function.
     * @return Widget_DatePicker
     */
    public function setFormat($format)
    {
        assert('is_string($format); /* The "format" parameter must be a string */');
        $this->setMetadata('format', $format);
        return $this;
    }

    /**
     * Returns the format of the date.
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->getMetadata('format');
    }


	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-dateedit';
		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Displayable_Interface::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$value = $this->getValue();

		if ($this->isDisplayMode()) {

			$classes = $this->getClasses();
			$classes[] = 'widget-displaymode';

			return $canvas->richtext(
				$this->getId(),
				$classes,
				$this->getValue(),
				BAB_HTML_ALL ^ BAB_HTML_P,
				$this->getCanvasOptions()
			);


		} else {

			return $canvas->lineInput(
				$this->getId(),
				$this->getClasses(),
				$this->getFullName(),
				$value,
				10,
				10,
				$this->isDisabled(),
				false,
				false,
				'date',
			    $this->getPlaceHolder(),
			    $this->getCanvasOptions(),
			    $this->getTitle(),
			    $this->getAttributes()
			)
			.$canvas->metadata($this->getId(), $this->getMetadata());
		}
	}
}
