<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/inputwidget.class.php';
require_once dirname(__FILE__) . '/drilldownmenu.class.php';


/**
 * Constructs a Widget_FunctionPicker.
 *
 * @param string		$id			The item unique id.
 * @return Widget_FunctionPicker
 */
function Widget_FunctionPicker($id = null)
{
    return new Widget_FunctionPicker($id);
}


/**
 * A Widget_FunctionPicker is a drilldown menu that lets the user select organizational chart functions.
 */
class Widget_FunctionPicker extends Widget_DrilldownMenu implements Widget_Displayable_Interface
{

    private $ocid = 1;
    private $relative = false;
    private $user_number = 5;

    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        require_once $GLOBALS['babInstallPath'] . 'utilit/urlincl.php';
        $this->setMetadata('crumbDefaultText', widget_translate('Choose a function'));
    }



    /**
     * (non-PHPdoc)
     * @see Widget_DrilldownMenu::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-grouppicker';
        return $classes;
    }


    /**
     * @param int $id   The org chart id.
     *
     * @return self
     */
    public function setOrgchart($id)
    {
        $this->ocid = $id;

        return $this;
    }


    /**
     * @param bool $relative
     *
     * @return self
     */
    public function setRelative($relative = true)
    {
        $this->relative = $relative;

        return $this;
    }

    /**
     * Set the maximum number of users to display for a single function.
     * After this number, a text will be displayed.
     */
    public function setUserNumber($user_number)
    {
        $this->user_number = $user_number;

        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_DrilldownMenu::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->setDatasource(new bab_url('?tg=addon/widgets/orgchart&idx=functionpicker&ocid='.$this->ocid.'&relative='.$this->relative.'&user_number='.$this->user_number));

        require_once $GLOBALS['babInstallPath'] . 'utilit/artapi.php';
        $value = $this->getValue();
        $title = $this->getTitle();
        if (empty($title)) {
            if ($value) {
                $this->setTitle(bab_getGroupName($value, true) . "\n" . $title);
            } else {
                $this->setTitle(widget_translate('No function selected') . "\n" . $title);
            }
        }

        return parent::display($canvas);
    }
}
