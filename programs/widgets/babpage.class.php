<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
bab_Widgets()->includePhpClass('Widget_Page');


/**
 *
 *
 * @return Widget_BabPage
 */
function Widget_BabPage($id = null, Widget_Layout $layout = null)
{
	return new Widget_BabPage($id, $layout);
}




/**
 * A standard page compatible with ovidentia skin pages.
 */
class Widget_BabPage extends Widget_Page {

	/**
	 * Determines if the page should be displayed inside the current skin (true) or as a full page (false).
	 * @var bool
	 */
	protected $embedded = true;

	/**
	 * Determines if the page should be displayed in a mobile environment
	 * @var bool
	 */
	protected $mobile = false;


	public function __construct($id = null, Widget_Layout $layout = null)
	{
		parent::__construct($id, $layout);

		$addon = bab_getAddonInfosInstance('widgets');

		$this->prependJavascriptFile($GLOBALS['babInstallPath'].'scripts/ovidentia.js');
		$this->addJavascriptFile($GLOBALS['babInstallPath'].'scripts/bab_dialog.js');

//		bab_debug($addon->getRelativePath().'widgets.css');
		$this->addStyleSheet($addon->getStylePath().'widgets.css');
	}


	/**
	 * @param bool	$embedded
	 * @return Widget_BabPage
	 */
	public function setEmbedded($embedded = true)
	{
		$this->embedded = $embedded;
		return $this;
	}

	/**
	 *
	 * @return bool
	 */
	public function isEmbedded()
	{
		return $this->embedded;
	}

	/**
	 * Adds a navigation tab to the page.
	 *
	 * @deprecated Replaced by Widget_Tabs
	 * @see self::setCurrentItemMenu()
	 * @param string                   $itemName 		Tab id that should be also used in setCurrentItemMenu()
	 * @param string                   $text 			Tab label that will be translated.
	 * @param Widget_Action | string   $url 			Tab url
	 *
	 * @return Widget_BabPage
	 */
	public function addItemMenu($itemName, $text, $url)
	{
		$babBody = bab_getBody();

		if ($url instanceof Widget_Action) {
		    $url = $url->url();
		}

		$babBody->addItemMenu($itemName, $text, $url);

		return $this;
	}

	/**
	 * Sets the selected menu item.
	 *
	 * @deprecated Replaced by Widget_Tabs
	 * @see self::addItemMenu()
	 * @param string $itemName 	menu item name
	 *
	 * @return Widget_BabPage
	 */
	public function setCurrentItemMenu($itemName)
	{
		$babBody = bab_getBody();

		$babBody->setCurrentItemMenu($itemName);

		return $this;
	}


	/**
	 * Shortcut to display the page using an HTML canvas.
	 */
	public function displayHtml()
	{
		$W = bab_Widgets();
		$this->pageEcho($W->HtmlCanvas());
	}


	/**
	 * Displays an error message and exit.
	 *
	 * @param	string	$msgerror
	 */
	public function kill($msgerror)
	{
		$this->addError($msgerror);
		$this->displayHtml();
	}



	/**
	 * Set the page error message
	 * @param	string	$title
	 * @return	Widget_Page
	 */
	public function addError($error)
	{
		$this->errors[] = $error;
		return $this;
	}


	/**
	 * Displays the page with skin decorations.
	 *
	 * @param	Widget_Canvas	$canvas
	 */
	public function pageEcho(Widget_Canvas $canvas)
	{
		$babBody = bab_getBody();

		foreach ($this->errors as $error) {
			$babBody->addError($error);
		}

		if (!empty($this->title)) {
			$babBody->title = $this->title;
		}


		/* @var $J Func_jquery */
		$J = bab_functionality::get('jquery');
		if ($J) {
			if ($this->mobile) {
				$this->setJavascriptToolkit('jquery');
				$J->includeCore();
				$J->includeMobile();
				$babBody->addStyleSheet($J->getStyleSheetUrl('mobile'));
			} else {
				$this->setJavascriptToolkit('jquery');
				$J->includeCore();
//				$J->includeUi();
				$babBody->addStyleSheet($J->getStyleSheetUrl());
			}
		}

		$canvas->setPageDecorations($this, self::$allClasses);

		foreach ($this->getJavascriptFiles() as $scriptUrl) {
			$babBody->addJavascriptFile($scriptUrl);
		}

		foreach ($this->getDeferedJavascriptFiles() as $scriptUrl) {
			$babBody->addJavascriptFile($scriptUrl, true);
		}

		foreach ($this->getAlternateLinks() as $alternateLinkUrl => $alternateLinkInfo) {

			$blob = '</script>' . "\n";
			$blob .= '<link rel="alternate" type="application/rss+xml" href="' .  bab_toHtml($alternateLinkUrl) . '" ';
			if (isset($alternateLinkInfo['id'])) {
				$blob .= 'id="' . bab_toHtml($alternateLinkInfo['id']) . '" ';
			}
			if (isset($alternateLinkInfo['title'])) {
				$blob .= 'title="' . bab_toHtml($alternateLinkInfo['title']) . '" ';
			}
			$blob .= ' />' . "\n";
			$blob .= '<script>';

			$babBody->addJavascript($blob);
		}

		$stylesPrefix = $GLOBALS['babInstallPath'].'styles/';
		$stylesPrefixLength = mb_strlen($stylesPrefix);

		foreach ($this->getStyleSheets() as $cssUrl) {

			// css url can be relative to the site root (prefered) or to the kernel styles directory (deprecated)
			if (mb_substr($cssUrl, 0, $stylesPrefixLength) == $stylesPrefix) {
				$cssUrl = mb_substr($cssUrl, $stylesPrefixLength);
			}

			$babBody->addStyleSheet($cssUrl);
		}

		if ($this->isEmbedded()) {
			$babBody->babEcho($this->display($canvas));

		} else {
			$babBody->babPopup($this->display($canvas));
		}


	}
}