<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/image.class.php';



/**
 * Constructs a Widget_ImageZoomer.
 *
 * @param string		$smallImageSrc	The image source, small version (url).
 * @param string		$bigImageSrc	The image source, big version (url).
 * @param string		$labelText		The label text.
 * @param string		$id				The item unique id.
 * @return Widget_ImageZoomer
 */
function Widget_ImageZoomer($smallImageSrc = '', $bigImageSrc = '', $labelText = '', $id = null)
{
	return new Widget_ImageZoomer($smallImageSrc, $bigImageSrc, $labelText, $id);
}



class Widget_ImageZoomer extends Widget_Image implements Widget_Displayable_Interface
{
	private $bigimageurl;


	/**
	 * @var string
	 */
	protected $imageSet;

	/**
	* Constructs a Widget_ImageZoomer.
	*
	* @param string		$smallImageSrc	The image source, small version (url).
	* @param string		$bigImageSrc	The image source, big version (url).
	* @param string		$labelText		The label text.
	* @param string		$id				The item unique id.
	* @return Widget_ImageZoomer
	*/
	public function __construct($smallImageSrc = '', $bigImageSrc = '', $labelText = '', $id = null)
	{
		parent::__construct($id);
		$this->setText($labelText);
		$this->setUrl($smallImageSrc);
		$this->setBigImageUrl($bigImageSrc);
		$this->setImageSet('lightbox');
	}


	/**
	 * Set the image url.
	 *
	 * @param string	$url
	 * @return Widget_Image
	 */
	public function setBigImageUrl($url)
	{
		$this->bigimageurl = $url;
		return $this;
	}


	/**
	 * Set the image set name, use to have multiple lightbox instance.
	 *
	 * @param string	$name
	 * @return Widget_Image
	 */
	public function setImageSet($name)
	{
	    $this->imageSet = $name;
	    return $this;
	}


	/**
	 *
	 * @return string
	 */
	public function getImageSet()
	{
	    return $this->imageSet;
	}


	/**
	 * {@inheritDoc}
	 * @see Widget_Image::getClasses()
	 */
	public function getClasses()
	{
	    $classes = parent::getClasses();
	    $classes[] = 'widget-imagezoomer';
	    return $classes;
	}



	/**
	 * Initializes metadata.
	 */
	protected function initMetadata()
	{
	    $this->setMetadata('imageSet', $this->getImageSet());
	}


    /**
     * {@inheritDoc}
     * @see Widget_Image::display()
     */
	public function display(Widget_Canvas $canvas)
	{
		$smallImage = $canvas->image(
			$this->getId().'_small', // the Widget_ImageZoomerThumbnail use this ID
			array('widget-100pc'),
			$this->getText(),
			$this->getUrl()
		);

		$this->initMetadata();
		$metadata = $this->getMetadata();

		$jquery = bab_jQuery();

		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		return $canvas->linkContainer(
            $this->getId(),
		    $this->getClasses(),
		    array($smallImage),
		    $this->bigimageurl,
		    $this->getCanvasOptions(),
		    $this->getTitle(),
		    $this->getAttributes()
	    )
	    . $canvas->metadata($this->getId(), $metadata)
		. $canvas->loadScript($this->getId(), $jquery->getLightboxJavascriptFile())
		. $canvas->loadStyleSheet($jquery->getLightboxStyleSheet())
		. $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.imagezoomer.jquery.js');
	}
}
