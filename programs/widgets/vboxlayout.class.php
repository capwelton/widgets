<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/boxlayout.class.php';


/**
 * Constructs a Widget_VBoxLayout.
 *
 * @param string $id    The item unique id.
 * @return Widget_VBoxLayout
 */
function Widget_VBoxLayout($id = null)
{
    return new Widget_VBoxLayout($id);
}


/**
 * The Widget_VBoxLayout class provides a way to arrange widget vertically in a container.
 */
class Widget_VBoxLayout extends Widget_BoxLayout implements Widget_Displayable_Interface
{


    /**
     * @param string $id    The item unique id.
     * @return Widget_VBoxLayout
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }


    /**
     * Adds $item to this layout. The position of the item can be specified.
     * If the position is not specified (or null), the item is appended.
     *
     * @param Widget_Displayable_Interface $item
     * @param int $position
     * @return Widget_VBoxLayout
     */
    public function addItem(Widget_Displayable_Interface $item = null, $position = null)
    {
        parent::addItem($item, $position);
        return $this;
    }


    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-layout-vbox';
        return $classes;
    }


    public function display(Widget_Canvas $canvas)
    {
        $items = array();
        foreach ($this->getItems() as $item) {
            $items[] = $item;
        }

        return $canvas->vbox(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        . $canvas->metadata($this->getId(), $this->getMetadata());
    }
}
