<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_LineEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_LineEdit
 */
function Widget_SearchLineEdit($id = null)
{
	return new Widget_LineEdit($id);
}




/**
 * A Widget_LineEdit is a widget that lets the user enter a single line of text.
 */
class Widget_SearchLineEdit extends Widget_LineEdit
{



	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_InputWidget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-searchlineedit';
		return $classes;
	}


	/**
	 * Sets/unsets the field as obfuscated, i.e. the content
	 * is not displayed to the user, as in a password field.
	 *
	 * @param bool	$obfuscate		True to make the field obfuscated.
	 * @return Widget_LineEdit
	 */
	public function obfuscate($obfuscate = true)
	{
		$this->_obfuscated = false;
		return $this;
	}



	/**
	 * (non-PHPdoc)
	 * @see programs/widgets/Widget_Displayable_Interface::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$maxSize = $this->getMaxSize();
		if (isset($maxSize)) {
			$value = substr($this->getValue(), 0, $maxSize);
		} else {
			$value = $this->getValue();
		}

		
		if ($this->isDisplayMode()) {

			$classes = $this->getClasses();
			$classes[] = 'widget-displaymode';

			if ($this->getSize()) {
				$options = $this->getCanvasOptions();
				if (is_null($options)) {
					$options = $this->Options();
				}
				$options->minWidth(1.4 * $this->getSize(), 'ex');
				$this->setCanvasOptions($options);
			}
			
			return $canvas->richtext(
				$this->getId(),
				$classes,
				$this->getValue(),
				BAB_HTML_ALL ^ BAB_HTML_P,
				$this->getCanvasOptions()
			).$canvas->metadata($this->getId(), $this->getMetadata());


		} else {

			return $canvas->lineInput(
				$this->getId(),
				$this->getClasses(),
				$this->getFullName(),
				$value,
				$this->getSize(),
				$this->getMaxSize(),
				$this->isDisabled(),
				$this->isReadOnly(),
				$this->getAutoComplete(),
				'search',
			    $this->getPlaceHolder(),
			    $this->getCanvasOptions(),
			    $this->getTitle(),
			    $this->getAttributes()
			)
			.$canvas->metadata($this->getId(), $this->getMetadata());
		}
	}
}
