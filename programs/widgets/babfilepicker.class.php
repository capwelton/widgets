<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/inputwidget.class.php';
require_once dirname(__FILE__) . '/select.class.php';



/**
 * Constructs a Widget_BabFilePicker.
 *
 * @param string		$id			The item unique id.
 * @return Widget_BabFilePicker
 */
function Widget_BabFilePicker($id = null)
{
	return new Widget_BabFilePicker($id);
}


/**
 * A Widget_BabFilePicker is a widget that let the user select an ovidentia babFolder.
 *
 * It currently opens a popup window displaying a babFolder treeview with clickable nodes.
 */
class Widget_BabFilePicker extends Widget_Select implements Widget_Displayable_Interface
{



	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-babfilepicker';
		$classes[] = 'icon';
		return $classes;
	}



	public function display(Widget_Canvas $canvas)
	{
	    require_once $GLOBALS['babInstallPath'] . 'utilit/tree.php';
        $treeView = new bab_FileTreeView('bab_tv_file', bab_isUserAdministrator());


        $attributes = bab_FileTreeView::SHOW_SUB_FOLDERS /* | bab_FileTreeView::SHOW_FILES */;
        $treeView->setAttributes($attributes);
        $treeView->printTemplate();

        $rootNode = $treeView->getRootNode();

        $this->addOption('', '');
	    $it = $rootNode->createNodeIterator($rootNode);

        $optGroup = self::NOGROUPKEY;
        while ($node = $it->nextNode()) {
            $data = $node->getData();
            if (!$data) {
                continue;
            }

            $path = explode(':', $data->_id);
            array_shift($path);
            if ($data->_type === 'foldercategory') {
                $currentDelegation = str_replace('d', 'DG', $data->_id);
                $optGroup = $data->_title;
                continue;
            }
            if ($data->_type === 'gfile') {
                $path = $currentFolderPath;
                $path[] = $data->_title;
            } else {
                $currentFolderPath = $path;
            }
            array_unshift($path, $currentDelegation);
            $fullpath = implode('/', $path);

            $level = explode('/', $fullpath);
            $padding = str_repeat(bab_nbsp(), 4 * (count($level) - 2));

            $this->addOption($fullpath, $padding . $data->_title, $optGroup);
        }

        return parent::display($canvas);
	}

}
