<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/widget.class.php';



/**
 * Constructs a Widget_Image.
 *
 * @param string		$imageSrc	The image source (url).
 * @param string		$labelText	The label text.
 * @param string		$id			The item unique id.
 * @return Widget_Image
 */
function Widget_Image($imageSrc = '', $labelText = '', $id = null)
{
	return new Widget_Image($imageSrc, $labelText, $id);
}



class Widget_Image extends Widget_Widget implements Widget_Displayable_Interface
{
	private	$text;
	private	$url;

	/**
	* Constructs a Widget_Image.
	*
	* @param string		$imageSrc	The image source (url).
	* @param string		$labelText	The 'alt' label text.
	* @param string		$id			The item unique id.
	* @return Widget_Image
	*/
	public function __construct($imageSrc = '', $labelText = '', $id = null)
	{
		parent::__construct($id);
		$this->setText($labelText);
		$this->setUrl($imageSrc);
	}

	/**
	 * Sets the icon's text label.
	 *
	 * @param string	$labelText
	 * @return Widget_Image
	 */
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}

	/**
	 * Returns the icon's text label.
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}



	/**
	 * Sets the icon's image url.
	 *
	 * @param string	$url
	 * @return Widget_Image
	 */
	public function setUrl($url)
	{
		$this->url = $url;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUrl()
	{
		return $this->url;
	}


	public function setImageUrl($url)
	{
		return $this->setUrl($url);
	}

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-image';
		return $classes;
	}



	public function display(Widget_Canvas $canvas)
	{
		return $canvas->image(
			$this->getId(),
			$this->getClasses(),
			$this->text,
			$this->url,
			$this->getCanvasOptions(),
			$this->getTitle(),
		    $this->getAttributes()
		)
		. $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
