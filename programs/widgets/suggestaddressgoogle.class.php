<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once FUNC_WIDGETS_PHP_PATH . 'suggestaddress.class.php';
require_once FUNC_WIDGETS_PHP_PATH . 'flowlayout.class.php';
require_once FUNC_WIDGETS_PHP_PATH . 'lineedit.class.php';
require_once FUNC_WIDGETS_PHP_PATH . 'hidden.class.php';

/**
 * Constructs a Widget_SuggestAddressGoogle.
 *
 * @param string $id			The item unique id.
 * @param string $restriction	Filter by country. The country must be passed as as a two-character, ISO 3166-1 Alpha-2 compatible country code.
 */
function Widget_SuggestAddressGoogle($id = null,  $restriction = 'fr')
{
    return new Widget_SuggestAddressGoogle($id, $restriction);
}


/**
 * Suggest addresses using the google API
 */
class Widget_SuggestAddressGoogle extends Widget_SuggestAddress implements Widget_Displayable_Interface, Widget_AssociableLabel_Interface
{

    private $suggest = null;
    
    /**
     * Store the location type
     * example values are : route, street_address, street_number, sublocality
     * @url https://developers.google.com/places/supported_types?hl=fr
     * @var Widget_Hidden
     */
    private $loctype = null;
    
    
    private $street = null;
    private $city = null;
    private $postalCode = null;
    private $country = null;
    private $latitude = null;
    private $longitude = null;

    private $restriction = null;

    /**
     * @param string $id			The item unique id.
     * @param string $restriction	Filter by country. The country must be passed as as a two-character, ISO 3166-1 Alpha-2 compatible country code.
     *
     */
    public function __construct($id = null, $restriction = 'fr')
    {
        parent::__construct($id);

        $id = $this->getId();

        $this->suggest     = new Widget_LineEdit($id.'_suggest');
        $this->loctype 	   = new Widget_Hidden($id.'_loctype');
        $this->street 	   = new Widget_Hidden($id.'_street');
        $this->city 	   = new Widget_Hidden($id.'_city');
        $this->postalCode  = new Widget_Hidden($id.'_postalCode');
        $this->country     = new Widget_Hidden($id.'_country');
        $this->latitude    = new Widget_Hidden($id.'_latitude');
        $this->longitude   = new Widget_Hidden($id.'_longitude');




        if(!isset($GLOBALS['babGoogleApiKey'])){
            var_dump('Google API KEY must be define for google suggest to work. You need to add $GLOBALS[\'babGoogleApiKey\'] with it in config.php.');
        } else {
            $this->setMetadata('babGoogleApiKey', $GLOBALS['babGoogleApiKey']);
        }

        if ($restriction) {
            $this->setMetadata('restriction', $restriction);
        }

        $this->setMetadata('notFromSuggest', widget_translate('The address must be selected from one of the suggestions'));
    }


    private function setFieldName($field)
    {
        $name = $this->getNamePath();

        if (!is_array($name)) {
            $name = array($name);
        }

        $name[] = $field;
        $this->$field->setName($name);
    }




    /**
     * set the componentRestrictions option field
     * @return self
     */
    public function setRestriction($restriction) {
        $this->restriction = $restriction;
        return $this;
    }


    /**
     * Set the size
     * @param integer $size
     * @return self
     */
    public function setSize($size) {
        $this->suggest->setSize($size);
        return $this;
    }

    /**
     * Set the labels text for date fields for the start date and the end date of the period
     * @return self
     */
    public function setValue($suggest, $street = '', $city = '', $postalCode = '', $country = '', $latitude = '', $longitude = '', $loctype = '')
    {
        $this->suggest->setValue($suggest);
        $this->loctype->setValue($loctype);
        $this->street->setValue($street);
        $this->city->setValue($city);
        $this->postalCode->setValue($postalCode);
        $this->country->setValue($country);
        $this->latitude->setValue($latitude);
        $this->longitude->setValue($longitude);

        return $this;
    }




    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-suggestgoogle';
        return $classes;
    }

    public function isMandatory()
    {
        if ( $this->suggest->isMandatory() ){
            return true;
        }else{
            return false;
        }
    }

    /**
     * Sets whether the field is mandatory or not.
     *
     * If the field is inside a dialog, the dialog will report an error to
     * the user if he did not fill in the field on submit.
     * This method returns the widget itself so that other methods can be chained.
     *
     * @param boolean $mandatory
     * @param string $message		The message displayed by the dialog if the field is left empty.
     * @return self
     */
    public function setMandatory($mandatory = true, $message = '')
    {
        if (isset($this->suggest)) {
            $this->suggest->setMandatory($mandatory, $message);
        }
        return $this;
    }



    /**
     * @deprecated use setMandatory
     * @param string    $message
     */
    public function setSelectMandatory($mandatory = true, $message = '')
    {
        $this->suggest->setMandatory($mandatory, $message);
        $this->street->setMandatory($mandatory, $message);

        return $this;
    }


    public function getSuggest()
    {
        return $this->suggest;
    }


    /**
     * Sets the label associated to inputwidget.
     *
     * @param Widget_Label $widget
     */
    public function setAssociatedLabel(Widget_Label $widget = null)
    {
        $this->suggest->setAssociatedLabel($widget);
        return $this;
    }


    /**
     * Returns the label associated to widget.
     *
     * @return Widget_Label	The widget or null if no label is associated
     */
    public function getAssociatedLabel()
    {
        return $this->suggest->getAssociatedLabel();
    }




    /**
     * @param Widget_Canvas $canvas
     *
     * @return string
     */
    protected function getScripts(Widget_Canvas $canvas)
    {
        $widgetAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->loadAddonScript($this->getId(), $widgetAddon, 'widgets.suggestaddressgoogle.jquery.js');
    }




    /**
     * (non-PHPdoc)
     * @see Widget_Item::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $this->setFieldName('suggest');
        $this->setFieldName('loctype');
        $this->setFieldName('street');
        $this->setFieldName('city');
        $this->setFieldName('postalCode');
        $this->setFieldName('country');
        $this->setFieldName('latitude');
        $this->setFieldName('longitude');

        // if value not set on suggestion, get it from the from if possible
        if (!$this->suggest->getValue()) {
            if ($form = $this->getForm()) {
                $path = $this->getNamePath();
                $values = $form->getValue($path);
                if (isset($values) && is_array($values)) {
                    foreach ($values as $name => $value) {
                        if (isset($this->$name)) {
                            $this->$name->setValue($value);
                        }
                    }
                }
            }
        }


        return $canvas->div($this->getId(), $this->getClasses(), array(
            $this->suggest->display($canvas),
            $this->loctype->display($canvas),
            $this->street->display($canvas),
            $this->city->display($canvas),
            $this->postalCode->display($canvas),
            $this->country->display($canvas),
            $this->latitude->display($canvas),
            $this->longitude->display($canvas)
        ))
          . $canvas->metadata($this->getId(), $this->getMetadata())
          . $this->getScripts($canvas);

    }
}
