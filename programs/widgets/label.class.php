<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_Label.
 *
 * @param string	$text	The label text.
 * @param string	$id		The item unique id.
 * @return Widget_Label
 */
function Widget_Label($text = '', $id = null)
{
	return new Widget_Label($text, $id);
}



/**
 * Widget_Label
 *
 */
class Widget_Label extends Widget_Widget implements Widget_Displayable_Interface
{
	/**
	 * @var string
	 */
	private $text;

	/**
	 * @var bool
	 */
	private $colon = null;


	/**
	 * @var Widget_InputWidget
	 */
	private $associatedWidget;

	/**
	 * @param string $text	The label text.
	 * @param string $id	The item unique id.
	 * @return Widget_Label
	 */
	public function __construct($text = '', $id = null)
	{
		parent::__construct($id);
		$this->associatedWidget = null;
		$this->setText($text);
	}

	/**
	 * Sets the label text.
	 *
	 * @param string $text
	 * @return Widget_Label
	 */
	public function setText($text)
	{
		$this->text = $text;
		return $this;
	}


	/**
	 * Returns the label's text.
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->text;
	}


	/**
	 * Returns the form to which this label widget is attached.
	 *
	 * @return Widget_Form
	 */
	public function getForm()
	{
		for ($widget = $this; $widget && (!($widget instanceof Widget_Form)); $widget = $widget->getParent()) {
			;
		}

		return $widget;
	}



	/**
	 * display a colon after label text
	 * @return Widget_Label
	 */
	public function colon($colon = true)
	{
		$this->colon = $colon;
		return $this;
	}


	/**
	 * Test if the label is displayed with a colon
	 * @return bool
	 */
	protected function isColon()
	{
		if (null !== $this->colon) {
			return $this->colon;
		}
        if (null === $this->associatedWidget || get_class($this->associatedWidget) === 'Widget_CheckBox') {
            return false;
        }

		$form = $this->getForm();

		if (null !== $form) {
			return $form->isColon();
		}

		return false;
	}


	/**
	 * Sets the widget to which the label is associated.
	 *
	 * @param Widget_Widget $widget
	 * @return Widget_Label
	 */
	public function setAssociatedWidget(Widget_Displayable_Interface $widget = null)
	{
		$this->associatedWidget = $widget;

		if (method_exists($widget, 'getAssociatedLabel') && $this !== $widget->getAssociatedLabel()) {
			$widget->setAssociatedLabel($this);
		}
		return $this;
	}


	/**
	 * Returns the widget to which the label is associated.
	 *
	 * @return Widget_InputWidget	The widget or null if no widget associated
	 */
	public function getAssociatedWidget()
	{
		return $this->associatedWidget;
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Widget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-label';
        if (isset($this->associatedWidget) && method_exists($this->associatedWidget, 'isMandatory') && $this->associatedWidget->isMandatory() && ! $this->isDisplayMode()) {
            $classes[] = 'widget-label-mandatory';
        } elseif ($this->isDisplayMode()) {
            $classes[] = 'widget-label-displaymode';
        }

		return $classes;
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Displayable_Interface::display()
	 */
	public function display(Widget_Canvas $canvas)
	{
		$text = $this->getText();

		if ($this->isColon()) {
		    $text = widget_addColon($text);
		}

		return $canvas->label(
			$this->getId(),
			$this->getClasses(),
			$text,
			$this->getAssociatedWidget() ? $this->getAssociatedWidget()->getId() : null,
			$this->getCanvasOptions(),
			$this->getTitle(),
		    $this->getAttributes()
		)
		. $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
