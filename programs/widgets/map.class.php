<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


require_once dirname(__FILE__) . '/boxlayout.class.php';


/**
 * Constructs a Widget_Map.
 *
 * @param string $id	The item unique id.
 * @return Widget_Map
 */
function Widget_Map($id = null)
{
    return new Widget_Map($id);
}


class Widget_Map extends Widget_Item implements Widget_Displayable_Interface
{
    private $width = 600;
    private $height = 450;
    private $mapType = 'google';//leafletjs @OSM
    private $openerList = array();
    private $marker = array();
    private $lines = array();
    private $linesColors = array();
    private $cluster = true;
    private $attribution = '';
    private $disableControl = false;


    /**
     * @param string $id	The item unique id.
     * @return Widget_VBoxLayout
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
        $this->setMapFrom();
        //$this->setZoom();

        return $this;
    }

    /**
     * Available source :
     * -leafletjs
     * -google
     */
    public function setMapFrom($mapFrom = 'google')
    {
        $this->mapType = $mapFrom;

        return $this;
    }

    /**
     * Available source :
     * -leafletjs
     */
    public function disableControl($status = true)
    {
        $this->disableControl = $status;

        return $this;
    }


    /**
     * @map google
     * @map leafletjs
     */
    public function setCenter($lat, $long)
    {
        $this->setMetadata(
            'center',
            array(
                'lat' => $lat,
                'long' => $long
            )
        );

        return $this;
    }

    /**
     * @map google
     * @map leafletjs
     */
    public function setZoom($zoom = 16)
    {
        $this->setMetadata('zoom', $zoom);

        return $this;
    }

    /**
     * @map google
     * @map leafletjs
     */
    public function addMarker($lat, $long, $name, Widget_Displayable_Interface $widget, $icon = null)
    {
        $this->marker[] = array(
            'lat' => $lat,
            'long' => $long,
            'name' => $name,
            'widget' => $widget,
            'icon' => $icon
        );

        return $this;
    }

    /**
     * [][] : the contained array should be lat,long[,value]
     * The value is use in line color thresold
     *
     * @map google
     * @map leafletjs
     */
    public function setLines($points)
    {
        $this->lines = $points;

        return $this;
    }

    /**
     * [][] : exemple: array('thresold' => '250', 'color' => '#ff0000');
     * It use the value of the point to comparre with thresold
     *
     * @map leafletjs
     */
    public function setLinesColorThresolds($thresolds)
    {
        $this->linesColors = $thresolds;

        return $this;
    }

    /**
     * @map google
     * @map leafletjs
     */
    public function setWidth($width = 600)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @map google
     * @map leafletjs
     */
    public function setHeight($height = 450)
    {
        $this->height = $height;

        return $this;
    }


    /**
     * @map google
     */
    public function addMarkerOpener($id, Widget_Displayable_Interface $widget)
    {
        $this->openerList[] = array(
            'id' => $id,
            'widget' => $widget
        );

        return $this;
    }

    /**
     * Let you disable marker clustering
     *
     * @map google
     *
     * @param string $toggle
     * @return Widget_Map
     */
    public function setCluster($toggle = true)
    {
        $this->cluster = $toggle;

        return $this;
    }

    /**
     * {@inheritDoc}
     * @see Widget_Item::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-map';
        $classes[] = 'widget-map-'.$this->mapType;
        return $classes;
    }

    public function getAllMetadata(Widget_Canvas $canvas)
    {
        $metadata = parent::getMetadata();
        if (!empty($this->marker)) {
            $metadata['marker'] = $this->marker;
            foreach($metadata['marker'] as $id => $marker){
                $metadata['marker'][$id]['id'] = $marker['widget']->getId();
                $metadata['marker'][$id]['widget'] = $marker['widget']->display($canvas);
            }
        }
        if (!empty($this->openerList)) {
            foreach($this->openerList as $opener){
                $metadata['openerList'][$opener['id']][] = $opener['widget']->getId();
            }
        }
        if (!empty($this->lines)) {
            $metadata['lines'] = $this->lines;
        }
        if (!empty($this->linesColors)) {
            $metadata['linesColors'] = $this->linesColors;
        }
        if ($this->attribution) {
            $metadata['attribution'] = $this->attribution;
        }
        $metadata['cluster'] = $this->cluster;
        $metadata['disableControl'] = $this->disableControl;
        $addon = bab_getAddonInfosInstance('widgets');
        $metadata['image_path'] = $addon->getImagesPath().'map/m';
        return $metadata;
    }

    /**
     * Let you personalise copy right section
     *
     * @map leafletjs
     *
     * @param string $attribution
     */
    public function setAttribution($attribution)
    {
        $this->attribution = $attribution;
    }


    /**
     * {@inheritDoc}
     * @see Widget_Item::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        if(isset($GLOBALS['babGoogleApiKey'])){
            $this->setMetadata('babGoogleApiKey', $GLOBALS['babGoogleApiKey']);
        }
        if(isset($GLOBALS['mapTileUrl'])){//Some URL
            $this->setMetadata('mapTileUrl', $GLOBALS['mapTileUrl']);
            if (isset($GLOBALS['mapTileAttribution'])) {
                $this->setAttribution($GLOBALS['mapTileAttribution']);
            }
            if(isset($GLOBALS['mapTileToken'])){
                $this->setMetadata('mapTileToken', $GLOBALS['mapTileToken']);
            }
        } else {
            $this->setMetadata('mapTileUrl', 'http://{s}.tile.osm.org/{z}/{x}/{y}.png');
            $this->setMetadata('mapTileUrl', 'https://maps.wikimedia.org/osm-intl/{z}/{x}/{y}.png');
            $this->setAttribution('&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors');
        }

        $script = "";
        switch ($this->mapType) {
            case 'google':
                $script = $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.map.google.jquery.js');
                break;

            case 'leafletjs':
                $this->setMetadata('leafletUrl', $widgetsAddon->getTemplatePath() . 'leaflet-1-3-4.js?' . $widgetsAddon->getDbVersion());
                $this->setMetadata('leafletPlugin1Url', $widgetsAddon->getTemplatePath() . 'Leaflet.MultiOptionsPolyline.js?' . $widgetsAddon->getDbVersion());
                $script = $canvas->loadAddonStyleSheet($widgetsAddon, 'leaflet-1-3-4.css');
                $script.= $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.map.leafletjs.jquery.js');
                break;
        }

        return $canvas->map(
            $this->getId(),
            $this->getClasses(),
            $this->width,
            $this->height,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ) . $canvas->metadata($this->getId(), $this->getAllMetadata($canvas))
        . $script;
    }
}
