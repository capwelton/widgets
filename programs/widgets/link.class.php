<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/widget.class.php';



/**
 * Constructs a Widget_Link.
 *
 * @param 	string	[$text]	The link text.
 * @param	string	[$url]	The link url
 * @param 	string	[$id]	The item unique id.
 * @return Widget_Link
 */
function Widget_Link($text = '', $url = '', $id = null)
{
    return new Widget_Link($text, $url, $id);
}



/**
 * A Widget_Link.
 *
 */
class Widget_Link extends Widget_Widget implements Widget_Displayable_Interface
{
    private $url = null;

    private $item = null;

    private $action = null;

    const OPEN_PAGE					   = 1;
    const OPEN_POPUP				   = 2;
    const OPEN_DIALOG				   = 3;
    const OPEN_DIALOG_AND_RELOAD	   = 4; // reload widgets on close
    const AJAX_ACTION				   = 5;
    const OPEN_DIALOG_AND_RELOAD_PAGE  = 6; // refresh page on close
    const OPEN_NON_MODAL               = 7;
    const OPEN_MODAL                   = 3;
    const OPEN_DIALOG_EXTENDED         = 8;


    /**
     * @param string | Widget_Displayable_Interface     $item       The link text or widget item.
     * @param string | Widget_Action | bab_url          $action     The link action (or url).
     * @param string                                    $id         The item unique id.
     */
    public function __construct($item = '', $action = '', $id = null)
    {
        parent::__construct($id);
        $this->item = $item;
        if ($action instanceof Widget_Action) {
            $this->setAction($action);
        } elseif ($action instanceof bab_url) {
            $this->setUrl($action->toString());
        } else {
            $this->setUrl($action);
        }

        $this->setOpenMode(self::OPEN_PAGE);
    }


    /**
     * Sets the link text.
     *
     * @param string $text
     * @return self
     */
    public function setText($text)
    {
        $this->item = $text;
        return $this;
    }


    /**
     * Sets the link contained item.
     *
     * @param Widget_Item $item
     * @return self
     */
    public function setItem(Widget_Item $item)
    {
        $this->item = $item;
        return $this;
    }


    /**
     * Get the item or the text contained in the link.
     *
     * @return Widget_Item | string
     */
    public function getItem()
    {
        return $this->item;
    }


    /**
     * Sets the link Url.
     *
     * @param string $url
     * @return self
     */
    public function setUrl($url)
    {
        $this->action = null;
        $this->url = $url;
        return $this;
    }


    /**
     * Returns the link Url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }


    /**
     * Sets the link Url (using an action object).
     *
     * @param Widget_Action $action
     * @return self
     */
    public function setAction(Widget_Action $action)
    {
        $this->action = $action;
        $this->url = $action->url();
        return $this;
    }



    /**
     * Returns the action.
     *
     * @return Widget_Action
     */
    public function getAction()
    {
        return $this->action;
    }


    /**
     * Toggle a classname on item on click or on the first ancestor with class given in the $ancestorClass parameter
     *
     * @param string $className
     * @param string $ancestorClass
     *
     * @return self
     */
    public function setToggleClass($className, $ancestorClass = null)
    {
        $this->setMetadata('toggleClassName', $className);
        if (isset($ancestorClass)) {
            $this->setMetadata('toggleClassAncestor', $ancestorClass);
        }

        return $this;
    }


    /**
     * Set open mode, choose the opening method of the link
     * @param	int	$mode
     * @param	array() $items
     *
     * @return self
     */
    public function setOpenMode($mode, $items = array())
    {
        switch($mode) {
            case self::OPEN_POPUP:
                $this->addClass('widget-popup');
                break;

            case self::OPEN_DIALOG:
            //case self::OPEN_MODAL:            
                $this->addClass('widget-popup-dialog');
                break;

            case self::OPEN_DIALOG_AND_RELOAD:
                $this->addClass('widget-popup-dialog-and-reload');
                if (!empty($items)) {
                    $this->setMetadata('dialogActionReload', $items);
                }
                break;

            case self::AJAX_ACTION:
                $this->addClass('widget-link-ajax-action');
                break;

            case self::OPEN_DIALOG_AND_RELOAD_PAGE:
                $this->addClass('widget-popup-dialog-and-reload-page');
                break;

            case self::OPEN_NON_MODAL:
                $this->addClass('widget-popup-dialog');
                $this->setMetadata('dialogModalMode', false);
                break;
                
            case self::OPEN_DIALOG_EXTENDED:
                $this->addClass('widget-popup-dialog');
                $this->setMetadata('dialogModalMode', false);
                $this->setMetadata('extendedMode', true);
                break;
        }

        return $this;
    }


    /**
     * set a confirmation message displayed in a modal dialog before the page change
     * @param	string	$message
     * @return self
     */
    public function setConfirmationMessage($message)
    {
        $this->setMetadata('confirmationMessage', $message);
        $this->addClass('widget-confirm');

        return $this;
    }


    /**
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-link';

        return $classes;
    }


    /**
     * @see programs/widgets/Widget_Displayable_Interface#display($canvas)
     */
    public function display(Widget_Canvas $canvas)
    {
        if (is_string($this->item)) {
            $item = $canvas->text($this->item);
        } else {
            $item = $this->item;
        }

        if ($this->isDisabled()) {
            $this->addAttribute('disabled', 'disabled');
            $url = null;
        } else {
            $url = $this->url;
        }
        
        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        
        return $canvas->linkContainer(
            $this->getId(),
            $this->getClasses(),
            array($item),
            $url,
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ) . $canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.dialogextend.jquery.js');;
    }
}
