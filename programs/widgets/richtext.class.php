<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_RichText.
 *
 * @param string	$text	The label text.
 * @param string	$id		The item unique id.
 * @return Widget_RichText
 */
function Widget_RichText($text = '', $id = null)
{
	return new Widget_RichText($text, $id);
}



/**
 * Widget_RichText
 * Print text with better rendering like line-breaks or url replacements
 */
class Widget_RichText extends Widget_Widget implements Widget_Displayable_Interface  
{
	private $_text;
	private $_options = BAB_HTML_ALL;

	/**
	 * @param string $text	The label text.
	 * @param string $id	The item unique id.
	 * @return Widget_RichText
	 */
	public function __construct($text = '', $id = null)
	{
		parent::__construct($id);
		$this->setText($text);
	}

	/**
	 * Sets the label text.
	 *
	 * @param string $text
	 */
	public function setText($text)
	{
		$this->_text = $text;
	}


	/**
	 * Returns the label's text.
	 *
	 * @return string
	 */
	public function getText()
	{
		return $this->_text;
	}

	/**
	 * Set a rendering options.
	 *
	 * available options :
	 * <ul>
	 * <li>BAB_HTML_ALL			: a combination of all the options</li>
	 * <li>BAB_HTML_ENTITIES	: special characters will be replaced with html entities</li>
	 * <li>BAB_HTML_AUTO		: the paragraphs tags will be added only if the text contein some text line-breaks</li>
	 * <li>BAB_HTML_P			: double line breaks will be replaced by html paragraphs, if there is no double line breaks, all the text will be in one paragraph</li>
	 * <li>BAB_HTML_BR			: Line-breaks will be replaced by html line breaks</li>
	 * <li>BAB_HTML_LINKS		: url and email adress will be replaced by links</li>
	 * <li>BAB_HTML_JS			: \ and ' and " are encoded for javascript strings, not in BAB_HTML_ALL</li>
	 * <li>BAB_HTML_REPLACE		: Replace ovidentia macro $XXX()</li>
	 * </ul>
	 *
	 * @param int $opt		default option is BAB_HTML_ALL
	 *
	 * @return Widget_RichText
	 */
	public function setRenderingOptions($opt)
	{
		$this->_options = $opt;
		return $this;
	}


	/**
	 * get rendering options.
	 *
	 * @return int
	 */
	public function getRenderingOptions()
	{
		return $this->_options;
	}
	

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-richtext';
		return $classes;
	}


	public function display(Widget_Canvas $canvas)
	{
		return $canvas->richtext($this->getId(),
							  $this->getClasses(),
							  $this->getText(),
							  $this->getRenderingOptions(),
							  $this->getCanvasOptions()
							  );
	}
}
