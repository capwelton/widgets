<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/lineedit.class.php';


/**
 * Constructs a Widget_RatingEdit.
 *
 * @param string		$id			The item unique id.
 * @return Widget_RatingEdit
 */
function Widget_RatingEdit($id = null)
{
	return new Widget_RatingEdit($id);
}


/**
 * A Widget_RatingEdit is a widget that lets the user enter an rating with stars.
 */
class Widget_RatingEdit extends Widget_LineEdit implements Widget_Displayable_Interface
{


	/**
	 * @param string $id			The item unique id.
	 * 
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		$this->setSize(1);

		$this->setMetadata(
		    'hints',
		    array(
				widget_translate('Bad'),
				widget_translate('Poor'),
				widget_translate('Regular'),
				widget_translate('Good'),
				widget_translate('Gorgeous')
            )
        );

		// default image path
		$addon = bab_getAddonInfosInstance('widgets');
		$this->setImagePath($addon->getImagespath().'16x16/raty/');
	}
	
	/**
	 * Allow usage of half stars on rating
	 * @return Widget_RatingEdit
	 */
	public function useHalf()
	{
	    $this->setMetadata('half', true);
	    return $this;
	}
	
	
	/**
	 * Set custom image path for rating
	 * the files used in path are:
	 *  - star-half.png
	 *  - star-off.png
	 *  - star-on.png
	 *  @return Widget_RatingEdit
	 */
	public function setImagePath($path)
	{
	    $this->setMetadata('imagePath', $path);
	    return $this;
	}
	
	/**
	 * Set custom names for images
	 * 
	 * @param int $size Width in pixel (default 16)
	 * 
	 * @return Widget_RatingEdit
	 */
	public function setImages($size, $half = 'star-half.png', $off = 'star-off.png', $on = 'star-on.png')
	{
	    $this->setMetadata('size', $size);
	    $this->setMetadata('starHalf', $half);
	    $this->setMetadata('starOff', $off);
	    $this->setMetadata('starOn', $on);
	    return $this;
	}


	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-ratingedit';
		if ($this->isDisplayMode()) {
		    $classes[] = 'widget-displaymode';
		}
		return $classes;
	}


	public function display(Widget_Canvas $canvas)
	{
		$this->setMetadata('readOnly', $this->isDisplayMode());

		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		$output = $canvas->lineInput(
			$this->getId(),
			$this->getClasses(),
			$this->getFullName(),
			$this->getValue(),
			$this->getSize(),
			$this->getMaxSize(),
			$this->isDisabled(),
			$this->isReadOnly(),
			$this->getAutoComplete(),
		    'text',
		    $this->getPlaceHolder(),
		    $this->getCanvasOptions(),
		    $this->getTitle(),
		    $this->getAttributes()
		)
		. $canvas->metadata($this->getId(), $this->getMetadata())
		. $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.ratingedit.jquery.js');

		return $output;
	}
}
