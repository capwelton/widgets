<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
include_once dirname(__FILE__).'/htmlcanvas.class.php';

/**
 * Constructs a Widget_HtmlPdfCanvas
 *
 * @return Widget_HtmlPdfCanvas
 */
function Widget_HtmlPdfCanvas()
{
    return new Widget_HtmlPdfCanvas;
}

/**
 * This Canvas will generate HTML strings without scripts.
 *
 */
class Widget_HtmlPdfCanvas extends Widget_HtmlCanvas
{
        /**
     * Return a default layout
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects.
     * @param Widget_CanvasOptions	$options				Options for layout items
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function defaultLayout($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<div'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . ' >' ."\n"
            . $this->displayableItemsToHtmlLayout(
                $displayableItems,
                array(),
                $options,
                'div',
                __FUNCTION__
            )
            . '</div>'."\n";
        return $html;
    }



    /**
     * Returns html for a flow.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems	   Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options			   Options for layout items
     * @param string	            $title				   The tooltip title
     * @param string[]              $attributes            An array of name => value attributes.
     * @return string
     */
    public function flow($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<div'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . ' >' ."\n"
            . $this->displayableItemsToHtmlLayout(
                $displayableItems,
                array(),
                $options,
                'div',
                __FUNCTION__
            )
            . '</div>'."\n";
        return $html;
    }

    /**
     * Returns html for a hbox.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options				Options for layout items
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function hbox($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<div'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . ' >' ."\n"
            . $this->displayableItemsToHtmlLayout(
                $displayableItems,
                array(),
                $options,
                'div',
                __FUNCTION__
            )
            . '</div>'."\n";
        return $html;
    }

    /**
     * Returns html for a vbox.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options				Options for layout items.
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function vbox($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<div'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . ' >' ."\n"
            . $this->displayableItemsToHtmlLayout(
                $displayableItems,
                array(),
                $options,
                'div',
                __FUNCTION__
            )
            . '</div>'."\n";
        return $html;
    }

    /**
     * Display items as html with layout options
     * @param 	array					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
     * @param	array					$itemsClasses
     * @param	Widget_CanvasOptions	$itemsOptions
     * @param	string					$itemTagName
     * @param	string					$layout_name
     * @return string
     */
    protected function displayableItemsToHtmlLayout(Array $displayableItems, Array $itemsClasses, Widget_CanvasOptions $itemsOptions = null, $itemTagName, $layout_name)
    {
        $htmlContent = '';

        switch ($layout_name) {
            case 'hbox':
            case 'vbox':
                $position_x = 0;
                $position_y = 0;
                break;
            case 'flow':
            default:
                $position_x = null;
                $position_y = null;
                break;
        }


        foreach ($displayableItems as $item) {

            if ($item instanceof Widget_Displayable_Interface) {
                $htmlItem = $item->display($this);
                $sizePolicy = $item->getSizePolicy();
                $parentAttributes = self::htmlAttributes($item->getParentAttributes());
            } elseif (is_string($item)) {
                $sizePolicy = '';
                $parentAttributes = '';
                $htmlItem = $item;
            } else {
                trigger_error(sprintf('wrong displayable item type : %s', gettype($item)));
                return '';
            }

            if (null !== $itemsClasses || null !== $itemsOptions) {
                $classes = isset($itemsClasses) ? $itemsClasses : array();
                if (!empty($sizePolicy)) {
                    $classes[] = $sizePolicy;
                }
                $classes = trim(implode(' ', $classes));

                $attributes = '';
                if ($classes !== '') {
                    $attributes .= ' class="' . $classes . '"';
                }
                if ($parentAttributes !== '') {
                    $attributes .= ' ' . $parentAttributes;
                }
                $style = $this->getStyle($itemsOptions, $position_x, $position_y);
                if ($style !== '') {
                    $attributes .= ' ' . $style;
                }

                $htmlItem = '<' . $itemTagName . $attributes . '>' . $htmlItem . '</' . $itemTagName . '>';
            }

            switch($layout_name) {
                case 'hbox':
                    $position_x++;
                    break;
                case 'vbox':
                    $position_y++;
                    break;
                case 'flow':
                    break;
            }

            $htmlContent .= $htmlItem;
        }

        return $htmlContent;
    }

    /**
     * Returns canvas grid layout.
     *
     * @see Widget_HtmlCanvas::gridSection()
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
     * @param array					$rows					An array of values returned by gridSection().
     * @param array					$colsOptions			An array of Widget_CanvasOptions that will be applied to corresponding columns.
     * @param Widget_CanvasOptions	$options				CanvasOptions that will be applied to the grid.
     * @param string				$title				    The tooltip title.
     * @param string[]				$attributes				Optional attributes.
     * @return string
     */
    public function grid($id, $classes, $rows = array(), $colsOptions = array(), Widget_CanvasOptions $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);
        $html  = '<table '
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlAttribute('title', $title)
            . self::htmlAttributes($attributes)
            . $style
            . '>';

        foreach ($colsOptions as $colOptions) {
            $colStyle = $this->getStyle($colOptions);
            $html .= '<colgroup span="1" ' . $colStyle . '></colgroup>';
        }

        foreach ($rows as $row) {
            $html .= $row;
        }
        $html .= '</table>';

        return $html;
    }



    /**
     * Display an image
     * @param string				$id				The unique id (for the page).
     * @param string[]				$classes		An array containing the classes as returned by Widget_Item::getClasses().
     * @param string				$text			The alternate text if the image cannot be displayed.
     * @param string				$url			The url of the image.
     * @param Widget_CanvasOptions	$options
     * @param string				$title			The optional tooltip
     * @param string[]              $attributes             An array of name => value attributes.
     * @return 	string
     */
    public function image($id, $classes, $text, $url, $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);
        $html  = '<img '
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . $style
            . ' src="'.bab_toHtml(realpath('.') . '/' . urldecode($url)).'"'
            . ' alt="'.bab_toHtml($text).'" />';


        return $html;
    }

    /**
     * output url for link in the canvas
     * If this is a relative url, add the domain
     * @param string $url
     * @return string
     */
    protected function outputUrl($url)
    {
        if (0 !== mb_strpos($url, 'http')) {
            return bab_getBabUrl().$url;
        }

        return url;
    }



    /**
     * Create metadata container
     *
     * @param	string	$name
     * @param	array	$metadata
     * @return string
     */
    public function metadata($name, $metadata = null)
    {
        return '';
    }


    /**
     * Load script
     * @param	string	$widget_id
     * @param	string	$filename
     * @return string
     */
    public function loadScript($widget_id, $filename) {

        return '';
    }


    /**
     * Load CSS style sheet
     * @param	string	$filename
     * @return string
     */
    public function loadStyleSheet($filename) {

        return '';
    }
}