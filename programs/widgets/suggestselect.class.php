<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/suggestlineedit.class.php';


/**
 * Constructs a Widget_SuggestSelect.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SuggestSelect
 */
function Widget_SuggestSelect($id = null)
{
    return new Widget_SuggestSelect($id);
}


/**
 * A Widget_SuggestSelect is a widget that let the user enter a single line of text.
 * propose suggestions, and a way to confirm the correct suggestion in case of conflict
 */
class Widget_SuggestSelect extends Widget_SuggestLineEdit implements Widget_Displayable_Interface
{
    /**
     * return the ID key of a suggestion
     *
     * return null if the suggestion is not set in request (the form is not submited)
     * return false if the suggestion is set in request but the id of suggestion is not set (the suggestion has not been clicked on)
     *
     * @return string | null | false
     */
    public function getSuggestId()
    {
        $fullname = $this->getFullName();
        $suggest_value = $this->getRequestValueFromName($fullname);

        $fullname[count($fullname) -1] = $this->id_name;
        $suggest_id = $this->getRequestValueFromName($fullname);

        if (null === $suggest_value) {
            return null;
        }

        if (null === $suggest_id) {
            return false;
        }

        return $suggest_id;
    }



    /**
     * {@inheritDoc}
     * @see Widget_SuggestLineEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-suggestselect';
        return $classes;
    }




    /**
     * {@inheritDoc}
     * @see Widget_SuggestLineEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $output = '';

        if (false === $this->getSuggestId()) {
            // a submit is sent but there is no ID, propose a select box
            $options = array();
            foreach($this->suggestlist as $suggestion) {
                $options[$suggestion['id']] = $suggestion['value'];
            }

            if ($options) {
                return $canvas->select(
                    $this->getId(),
                    $this->getClasses(),
                    $this->getFullName(),
                    $this->getValue(),
                    $options,
                    array(),
                    false,
                    6,
                    $this->getCanvasOptions()
                );
            }

            $output .= $canvas->span(
                $this->getId() . '_span',
                array('widget-suggestselect'),
                array($canvas->text(widget_translate('nothing found')))
            );
        }

        $output .= parent::display($canvas);

        return $output;
    }
}
