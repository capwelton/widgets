<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/item.class.php';
require_once dirname(__FILE__) . '/action.class.php';







/**
 * The Widget_Widget class is the base class of all user interface objects.
 */
abstract class Widget_Widget extends Widget_Item
{

    private $name = '';
    private $initScript = '';
    private $cssClasses = array();


    /**
     * This property store a disable/enable state for form fields or one of the parent items
     * @see Widget_Widget::isDisabled()
     * @see Widget_Widget::disable()
     * @see Widget_Widget::enable()
     * @var bool
     */
    private $disabled = null;

    /**
     * This property store a boolean to allow or disallow a form field
     * or multiple form fields with one of the parent items.
     * If form field is not allowed, value will be displayed without field.
     *
     * @see Widget_Widget::isEditMode()
     * @see Widget_Widget::setEditMode()
     * @see Widget_Widget::setDisplayMode()
     * @var bool
     */
    private $formFieldsDisallowed = null;


    /**
     * Set persistent status for display state of the widget
     * if widget is persistent, informations may be stored in cookies or in session to maintain aspect beetween pages
     * data are loaded when widget is initialized AND reloaded
     * @var bool
     */
    private $persistent;
    
    /**
     * Set persistent status for display state of the widget
     * if widget is persistent, informations may be stored in cookies or in session to maintain aspect beetween pages
     * data are loaded only when widget reloaded
     * @var bool
     */
    private $reloadPersistent;

    /**
     * Defines the storage where the persistent data is stored
     * @var int
     */
    private $persistent_storage;

    /**
     * Use cookies.
     */
    const STORAGE_COOKIES       = 0;


    /**
     * Use client (browser) local storage.
     */
    const STORAGE_LOCAL         = 1;

    /**
     * Use client (browser) session storage.
     */
    const STORAGE_SESSION       = 2;


    /**
     * @var string
     */
    protected $ajaxUrl = null;



    /**
     * Sets the name of the wigdet.
     *
     * The name of the widget will be used to generate the name of the corresponding form element.
     * This method returns the widget itself so that other methods can be chained.
     *
     * @param string|array $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


    /**
     * Returns the widget name.
     *
     * @return string|array
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    /**
     * Returns the widget name a string (last element in name if name is an array).
     * @return string
     */
    public function getNameString()
    {
        if (!isset($this->name)) {
            return null;
        }
        
        if (is_array($this->name)) {
            return end($this->name);
        }
        
        return $this->name;
    }


    /**
     * Name path of a widget
     * @see Widget_InputWidget::getFullName()
     * @return array
     */
    protected function getNamePath()
    {
        $namePath = array();
        /* @var $widget Widget_Item */
        for ($widget = $this; $widget; $widget = $widget->getParent()) {
            $name = $widget->getName();

            if (is_array($name)) {
                $namePath = array_merge($name, $namePath);
            } elseif ('' !== $name) {
                array_unshift($namePath, $name);
            }
        }

        return $namePath;
    }


    /**
     * Sets the parent of the widget.
     *
     * @param Widget_Item $parent
     * @return $this
     */
    public function setParent($parent)
    {
        assert(
            'is_null($parent) || $parent instanceof Widget_Item; /* The "parent" parameter must be a Widget_Item. */'
        );
        parent::setParent($parent);
        return $this;
    }


    /**
     * Enables the widget.
     *
     * @return $this
     */
    public function enable()
    {
        $this->disabled = false;
        return $this;
    }


    /**
     * Disables the widget.
     *
     * @return $this
     */
    public function disable()
    {
        $this->disabled = true;
        return $this;
    }


    /**
     * Get disabled status on widget or from parent
     *
     * @return boolean
     */
    public function isDisabled()
    {
        $item = $this;
        while ($item) {
            if (isset($item->disabled)) {
                return $item->disabled;
            }
            $item = $item->getParent();
        }

        return false;
    }





    /**
     * Enables the widget.
     *
     * @return $this
     */
    public function setEditMode()
    {
        $this->formFieldsDisallowed = false;
        return $this;
    }


    /**
     * Disables the widget.
     *
     * @return $this
     */
    public function setDisplayMode()
    {
        $this->formFieldsDisallowed = true;
        return $this;
    }



    /**
     * Get form field disallowed status on widget or from parent
     *
     * @return boolean
     */
    public function isDisplayMode()
    {
        $item = $this;
        while ($item) {
            if (isset($item->formFieldsDisallowed)) {
                return $item->formFieldsDisallowed;
            }
            $item = $item->getParent();
        }

        return false;
    }



    /**
     * The widget will automatically try to save its state (in a cookie for example), so that
     * it will be in the same state the next time the containing page is displayed.
     *
     * @param bool $persistent
     * @param int $mode             storage type
     * @return $this
     */
    public function setPersistent($persistent = true, $mode = null)
    {
        assert('is_bool($persistent); /* The "persistent" parameter must be a boolean. */');

        if ($mode === null) {
            $mode = self::STORAGE_LOCAL;
        }
        //sessionStorage
        $this->persistent = $persistent;
        $this->persistent_storage = $mode;
        return $this;
    }
    
    
    /**
     * The widget will automatically try to save its state (in a cookie for example), so that
     * it will be in the same state the next time the widget is reloaded, for example in an ajax frame.
     *
     * @param bool $persistent
     * @param int $mode             storage type
     * @return $this
     */
    public function setReloadPersistent($persistent = true, $mode = null)
    {
        assert('is_bool($persistent); /* The "persistent" parameter must be a boolean. */');
    
        if ($mode === null) {
            $mode = self::STORAGE_LOCAL;
        }
        //sessionStorage
        $this->reloadPersistent = $persistent;
        $this->persistent_storage = $mode;
        return $this;
    }
    

    /**
     * Returns whether the widget has been set in persistent mode.
     *
     * When in persistent mode a widget will keep its state client side.
     *
     * @return bool
     */
    public function isPersistent()
    {
        return $this->persistent;
    }
    
    
    
    /**
     * Returns whether the widget has been set in persistent mode for reloads only.
     *
     * @return bool
     */
    public function isReloadPersistent()
    {
        return $this->reloadPersistent;
    }
    
    

    /**
     * Sets the css classes of the widget.
     *
     * @param string|array $classes
     *            string containing space separated css class names or an array containing class names.
     * @return $this
     */
    public function setClasses($classes)
    {
        assert(
            'is_array($classes) || is_string($classes); /* The "classes" parameter must be a string or a array. */'
        );
        if ($classes === '') {
            $classes = array();
        } elseif (is_string($classes)) {
            $classes = explode(' ', $classes);
        }
        if (is_array($classes)) {
            $this->cssClasses = $classes;
            array_unique($this->cssClasses);
        }
        return $this;
    }

    /**
     * Appends the css classes to the classes of the widget.
     *
     * @param string|array $classes
     *            string containing space separated css class names or an array containing class names.
     * @return $this
     */
    public function addClasses($classes)
    {
        if (is_string($classes)) {
            $classes = explode(' ', $classes);
        }
        if (is_array($classes)) {
            $this->cssClasses = array_merge($this->cssClasses, $classes);
            array_unique($this->cssClasses);
        }
        return $this;
    }


    /**
     * @see Widget_Item::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        if ($this->isPersistent() || $this->isReloadPersistent()) {
            $classes[] = 'widget-persistent';
            
            if (!$this->isReloadPersistent()) {
                $classes[] = 'widget-persistent-oninit';
            }
            
            if($this->persistent_storage == Widget_Widget::STORAGE_LOCAL) {
                $classes[] = 'widget-local-storage';
            } else if ($this->persistent_storage == Widget_Widget::STORAGE_SESSION) {
                $classes[] = 'widget-session-storage';
            } else if ($this->persistent_storage == Widget_Widget::STORAGE_COOKIES) {
                $classes[] = 'widget-cookies-storage';
            }
        }

        if (isset($this->ajaxUrl)) {
            $classes[] = 'widget-ajax';
        }

        return $classes;
    }



    public function setInitScript($script)
    {
        $this->initScript = $script;
    }




    /**
     * Specifies an action that will be called asynchronously on the server (ajax). If the action
     * succeeds, the closest delayedItem of the page containing $reloadItem will be refreshed
     * (or the whole page if there is none).
     *
     * @param Widget_Action             $action      The action to call in ajax.
     * @param Widget_Item|string|array  $reloadItem  A Widget_Item, id of a Widget_Item, or array of Widget_Items
     *                                               Do not reload if reload item is false or empty string
     * @param string                    $event       The event triggering the ajax action.
     * @param int                       $delayedTime Time in milliseconds after which the event will be triggered
     * @return $this
     */
    public function setAjaxAction(Widget_Action $action = null, $reloadItem = null, $event = 'click', $delayedTime = 0)
    {
        if (!isset($action)) {
            $action = $this->getAction();
        }
        if (!isset($reloadItem)) {
            $reloadItem = $this;
        }

        $this->ajaxUrl = $action->url();
        $this->setMetadata('ajaxAction', $this->ajaxUrl);
        if ('' !== $reloadItem && false !== $reloadItem) {
            if (!is_array($reloadItem)) {
                $reloadItem = array($reloadItem);
            }

            foreach ($reloadItem as $k => $v) {
                if ($v instanceof Widget_Item) {
                    $reloadItem[$k] = $v->getId();
                }
            }
            $this->setMetadata('ajaxActionReload', $reloadItem);
        }
        $this->setMetadata('ajaxActionEvent', $event);
        $this->setMetadata('ajaxDelayedAction', $delayedTime);
        return $this;
    }



    /**
     * Dump widget content as an ascii tree.
     * For debugging purpose.
     *
     * @param string $prefix
     * @return string
     */
    public function dump($prefix = '')
    {
        $dumpString = parent::dump();
        $name = $this->getName();
        if ($name !== '') {
            $dumpString .= ' name="' . $this->getName() . '"';
        }

        return $dumpString;
    }

    public function __toString()
    {
        return $this->dump();
    }
}
