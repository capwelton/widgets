<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/widget.class.php';





class Widget_InputPassword extends Widget_Widget implements Widget_Displayable_Interface
{
    private $labelText1 = '';
    private $labelText2 = '';
    private $label1 = null;
    private $label2 = null;
    private $inputWidget1 = null;
    private $inputWidget2 = null;
    private $rule = array();

    /**
     * @var Widget_Layout
     */
    private $layout = null;


    /**
     *
     * @param string                       	$labelText1
     * @param string                       	$labelText2
     * @param array						 	$rule
     * @param Widget_Layout					$layout
     * @param string                       	$id
     */
    public function __construct($labelText1 = '', $labelText2 = '', $rule = array(), $layout = null, $id = null)
    {
        if (!$labelText1) {
            $labelText1 = widget_translate('Password');
        }
        $this->labelText1 = $labelText1;
        if (!$labelText2) {
            $labelText2 = widget_translate('Retype password');
        }
        $this->labelText2 = $labelText2;
        if (empty($rule)) {
            $func = bab_functionality::get('PwdComplexity');
            /* @var $func Func_PwdComplexity */
            $rule = $func->getRules();
        }
        $this->rule = $rule;

        $this->layout = $layout;

        $this->layout = $this->createFieldLayout();

        $return = parent::__construct($id);

        $this->setSubmitMessage(widget_translate('The password is not valid.'));
        $this->setConfirmMessage(widget_translate('Both passwords have to be identical.'));

        return $return;
    }


    /**
     * Message displayed on form submit if there is a input password widget with an invalid password
     * @return Widget_InputPassword
     */
    public function setSubmitMessage($str)
    {
        $this->setMetadata('submitMessage', $str);
        return $this;
    }


    /**
     * Message displayed on form submit if there is a input password widget with confirm password not identical
     * @return Widget_InputPassword
     */
    public function setConfirmMessage($str)
    {
        $this->setMetadata('confirmMessage', $str);
        return $this;
    }


    /**
     *
     * @param array $string
     * @return self
     */
    public function setRule($rule)
    {
        $this->rule = $rule;
        return $this;
    }


    /**
     *
     * @param bool $mandatory
     * @param string $string
     * @return self
     */
    public function setInputMandatory($mandatory, $string1, $string2)
    {
        $this->inputWidget1->setMandatory($mandatory, $string1);
        $this->inputWidget2->setMandatory($mandatory, $string2);
        return $this;
    }


    /**
     * @return Widget_Layout
     */
    private function createFieldLayout()
    {
        $W = bab_Widgets();

        $this->inputWidget1 = $W->LineEdit()->obfuscate()->setName(array('password', '1'))->addClass('widget-inputpassword-1');
        $this->label1 = $W->LabelledWidget($this->labelText1, $this->inputWidget1);
        $this->inputWidget2 = $W->LineEdit()->obfuscate()->setName(array('password', '2'))->addClass('widget-inputpassword-2');
        $this->label2 = $W->LabelledWidget($this->labelText2, $this->inputWidget2);


        if (!$this->layout) {
            $this->layout = $W->FlowLayout()
                ->setVerticalSpacing(0.2, 'em')
                ->setHorizontalSpacing(2, 'em');
        }
        $this->layout->addItem($this->label1);
        $this->layout->addItem($this->label2);

        return $this->layout;
    }

    /**
     * @return Widget_LabelledWidget
     */
    public function getLabel1()
    {
        return $this->label1;
    }

    /**
     * @return Widget_LabelledWidget
     */
    public function getLabel2()
    {
        return $this->label2;
    }
    

    /**
     * @return self
     */
    public function setParent($parent)
    {
        $this->layout->setParent($parent);
        return $this;
    }

    public function getParent()
    {
        return $this->layout->getParent();
    }

    /**
     *
     * @param mixed $value
     * @return self
     */
    public function setDisplayMode()
    {
        $this->layout->setDisplayMode();
        return $this;
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-inputpassword';

        return $classes;
    }


    public function testMandatory()
    {
        return $this->inputWidget1->testMandatory();
    }


    /**
     * Calls the associated widget setName() method if available.
     *
     * @param string $name
     * @return Widget_InputPassword
     */
    public function setName($name)
    {
        $this->inputWidget1->setName(array($name, '1'));
        $this->inputWidget2->setName(array($name, '2'));

        return $this;
    }



    /**
     * Calls the associated widget getName() method if available.
     *
     * @return string|array
     */
    public function getName()
    {
        if (isset($this->inputWidget)) {
            $methodGetName = array($this->inputWidget, 'getName');
            if (is_callable($methodGetName, true)) {
                return $this->inputWidget->getName();
            }
        }
        return null;
    }


    /**
     *
     * @return Widget_InputPassword
     */
    public function colon($active = true)
    {
        $this->label1->colon($active);
        $this->label2->colon($active);

        return $this;
    }


    /**
     *
     * @param Widget_Canvas $canvas
     * @return string
     */
    public function display(Widget_Canvas $canvas)
    {
        $W = bab_Widgets();

        $this->setMetadata('rule', $this->rule);
        $widgetsAddon = bab_getAddonInfosInstance('widgets');

        return $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array($this->layout->display($canvas)),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        ).$canvas->metadata($this->getId(), $this->getMetadata())
        . $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.inputpassword.jquery.js');;
    }
}
