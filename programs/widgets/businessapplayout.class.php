<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once bab_getAddonInfosInstance('widgets')->getPhpPath() . 'widgets/layout.class.php';



/**
 * 
 *
 * @return Widget_BusinessAppLayout
 */
function Widget_BusinessAppLayout($id = null)
{
	return new Widget_BusinessAppLayout($id);
}


class Widget_BusinessAppLayout extends Widget_Layout implements Widget_Displayable_Interface {


	/**
	 * @param string	$id			The item unique id.
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		
	}
	
	
	public function display(Widget_Canvas $canvas)
	{
		$col1 = array();
		$col2 = array();
		foreach($this->getItems() as $item) {
			
			if ('Widget_BusinessApplicationSection' === get_class($item)) {
				$col2[] = $item;
			} else {
				$col1[] = $item;
			}
		}
		
		$classes = array();
		
		
		return '
		<table id="Widgets_bap_col_table" class="col_table">
			<tr>
				<td class="col">
					'.$canvas->vbox('Widgets_bap_col1', $classes, $col1, $this->getCanvasOptions()).'
				</td>
				
				<td class="col" width="300">
					<iframe id="widget_bap_select" class="closed" width="272" height="0" frameborder="0" allowtransparency="true">
					
					</iframe>
					'.$canvas->vbox('Widgets_bap_col2', $classes, $col2, $this->getCanvasOptions()).'
				</td>
			</tr>
		</table>
		';
	}
}