<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
include_once dirname(__FILE__).'/canvas.class.php';


/**
 * Constructs a Widget_HtmlCanvas
 *
 * @return Widget_HtmlCanvas
 */
function Widget_HtmlCanvas()
{
    return new Widget_HtmlCanvas;
}




/**
 * This Canvas will generate HTML strings.
 *
 */
class Widget_HtmlCanvas extends Widget_Canvas {


    private static function numberFormat($number)
    {
        if (is_float($number)) {
            return number_format($number, 2, '.', '');
        }
        if (empty($number)) {
            return '0';
        }
        return $number;
    }


    /**
     * Returns the html style attribute corresponding to canvas options.
     *
     * @param Widget_CanvasOptions 	$options
     * @param int					$position_x		horizontal postion of item in layout
     * @param int					$position_y		vertical postion of item in layout
     *
     * @return string
     */
    protected function getStyle(Widget_CanvasOptions $options = null, $position_x = null, $position_y = null)
    {
        if ($options === null) {
            return '';
        }
        $styles = array();
        if ($options->textColor() !== null) {
            $styles[] = 'color:' . $this->getColor($options->textColor());
        }
        if ($options->backgroundImage() !== null) {
            $styles[] = 'background-image: url(' . urldecode($options->backgroundImage()) . ')';
        }
        if ($options->backgroundColor() !== null) {
            $styles[] = 'background-color:' . $this->getColor($options->backgroundColor());
        }
        if ($options->left() !== null) {
            $styles[] = 'left:' . self::numberFormat($options->left()) . $options->leftUnit();
        }
        if ($options->top() !== null) {
            $styles[] = 'top:' .  self::numberFormat($options->top()) . $options->topUnit();
        }
        if ($options->width() !== null) {
            $styles[] = 'width:' .  self::numberFormat($options->width()) . $options->widthUnit();
        }

        if ($options->minWidth() !== null) {
            $styles[] = 'min-width:' .  self::numberFormat($options->minWidth()) . $options->minWidthUnit();
        }

        if ($options->minHeight() !== null) {
            $styles[] = 'min-height:' .  self::numberFormat($options->minHeight()) . $options->minHeightUnit();
        }

        if ($options->height() !== null) {
            $styles[] = 'height:' .  self::numberFormat($options->height()) . $options->heightUnit();
        }

        if ($options->horizontalSpacing() !== null) {

            if (null === $position_x) {
                $styles[] = 'padding-right:' .  self::numberFormat($options->horizontalSpacing()) . $options->horizontalSpacingUnit();
            } elseif (0 !== $position_x) {
                $styles[] = 'padding-left:' .  self::numberFormat($options->horizontalSpacing()) . $options->horizontalSpacingUnit();
            }

        }

        if ($options->verticalSpacing() !== null) {

            if (null === $position_y) {
                $styles[] = 'padding-bottom:' .  self::numberFormat($options->verticalSpacing()) . $options->verticalSpacingUnit();
            } elseif (0 !== $position_y) {
                $styles[] = 'padding-top:' .  self::numberFormat($options->verticalSpacing()) . $options->verticalSpacingUnit();
            }
        }

        if ($options->verticalAlign() !== null) {
            $styles[] = 'vertical-align:' . $options->verticalAlign();
        }

        if ($options->horizontalAlign() !== null) {
            $styles[] = 'text-align:' . $options->horizontalAlign();
        }

        if (count($styles) == 0) {
            return '';
        }
        return ' style="' . implode(';', $styles) . '" ';
    }



    /**
     * @param	Widget_Color|string	$color
     * @return string	hexa color code with #
     */
    private function getColor($color)
    {
        return $color instanceof Widget_Color ? '#'.$color->getHexa() : $color;
    }

    /**
     * @return string
     */
    public function getStylesheetUrl()
    {
        $addon = bab_getAddonInfosInstance('widgets');
        return $addon->getStylePath().'widgets.css';
    }

    /**
     * return stylesheet content in a html style tag
     * @return string
     */
    public function getStylesheetContent()
    {
        return "<style type=\"text/css\">\n".file_get_contents($this->getStylesheetUrl())."\n</style>";
    }




    /**
     * Creates a valid html class name from the $classes array.
     *
     * @param array $classes
     * @return string
     */
    protected static function getClassName($classes)
    {
        $className = implode(' ', $classes);
        return $className;
    }

    /**
     * Create ID attribute
     * @return string
     */
    protected static function htmlId($id)
    {
        if (!empty($id)) {
            return ' id="' . bab_toHtml($id) . '"';
        }
        return '';
    }


    /**
     * Create TITLE attribute
     * @param	string	$title
     * @return string
     */
    protected static function htmlTitle($title)
    {
        if (!empty($title)) {
            return ' title="' . bab_toHtml($title) . '"';
        }
        return '';
    }



    /**
     * Create a CLASS attribute
     * @param	string[]	$classes
     * @return string
     */
    protected static function htmlClasses($classes)
    {
        if (!empty($classes)) {
            return ' class="' . bab_toHtml(self::getClassName($classes)) . '"';
        }
        return '';
    }

    /**
     * Create a data-* html attribute
     * @param	array (name => value)
     * @return string
     */
    protected static function htmlAttributes($attributes)
    {
        if (!empty($attributes)) {
            $htmlData = '';
            foreach ($attributes as $name => $value) {
                $htmlData .= ' ' . $name . '="' . bab_toHtml($value) . '"';
            }
            return $htmlData;
        }
        return '';
    }


    /**
     * Create a html attribute.
     *
     * @param	string   $name
     * @param	string   $value
     * @return string
     */
    protected static function htmlAttribute($name, $value)
    {
        if (!isset($value)) {
            return '';
        }
        return ' ' . $name . '="' . bab_toHtml($value) . '" ';
    }


    /**
     * Create a style html attribute
     * @param	array (name => value)
     * @return string
     */
    protected static function htmlStyles($styles)
    {
        if (!empty($styles)) {
            $htmlData = ' style="';
            foreach ($styles as $name => $value) {
                $htmlData .= $name . ':' . bab_toHtml($value) . ';';
            }
            $htmlData.='"';
            return $htmlData;
        }
        return '';
    }


    /**
     * Create a DISABLED attribute
     * @param	boolean	$disabled
     * @return string
     */
    protected static function htmlDisabled($disabled)
    {
        if (true === $disabled) {
            return ' disabled="disabled"';
        }
        return '';
    }

    /**
     * Create a MULTIPLE attribute
     * @param	boolean	$disabled
     * @return string
     */
    protected static function htmlMultiple($multiple)
    {
        if (true === $multiple) {
            return ' multiple';
        }
        return '';
    }


    /**
     * Creates a valid html name from the $fullName array.
     *
     * @param array $fullName
     * @return string
     */
    public static function getHtmlName(array $fullName)
    {
        $htmlName = array_shift($fullName);
        if ($fullName) {
            $htmlName .= '[' . implode('][', $fullName) . ']';
        }
        return $htmlName;
    }


    /**
     * Return the text element.
     *
     * @param string $text
     * @return string
     */
    public function text($text)
    {
        return bab_toHtml($text);
    }



    /**
     * Return a default layout
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects.
     * @param Widget_CanvasOptions	$options				Options for layout items
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function defaultLayout($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<span'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . ' >' ."\n".
            $this->displayableItemsToHtmlLayout($displayableItems, array('widget-layout-item'), $options, 'span', __FUNCTION__)
            . '</span>';
        return $html;
    }



    /**
     * Returns html for a flow.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems	   Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options			   Options for layout items
     * @param string	            $title				   The tooltip title
     * @param string[]              $attributes            An array of name => value attributes.
     * @return string
     */
    public function flow($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<div'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes)
        . ' >' ."\n".
        $this->displayableItemsToHtmlLayout($displayableItems, array('widget-layout-flow-item'), $options, 'div', __FUNCTION__)
        . '</div>';
        return $html;
    }

    /**
     * Returns html for a hbox.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options				Options for layout items
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function hbox($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        if (count($displayableItems) > 0) {
            $displayableItemsToHtmlLayout = $this->displayableItemsToHtmlLayout(
                $displayableItems,
                array('widget-layout-hbox-item'),
                $options,
                'td',
                __FUNCTION__
            );
        } else {
            $displayableItemsToHtmlLayout = '<td></td>';
        }

        $html = '<table'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . '><tr class="widget-layout-hbox-tr">' . "\n"
            . $displayableItemsToHtmlLayout
            . '</tr></table>';


        return $html;
    }

    /**
     * Returns html for a vbox.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options				Options for layout items.
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function vbox($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<div'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . ' >' ."\n"
            . $this->displayableItemsToHtmlLayout(
                $displayableItems,
                array('widget-layout-vbox-item'),
                $options,
                'div',
                __FUNCTION__
            )
            . '</div>'."\n";
        return $html;
    }

    /**
     * Return a div which going to be a map.
     *
     * @param string                $id
     * @param string[]              $classes
     * @param int[string            $width                  Width (in px if no unit specified)
     * @param int|string            $height                 Height  (in px if no unit specified)
     * @param Widget_CanvasOptions  $options                Options for layout items
     * @param string                $title                  The tooltip title
     * @param string[]              $attributes             An array of name => value attributes
     * @return string
     */
    public function map($id, $classes, $width, $height, $options = null, $title = null, $attributes = null)
    {
        if (is_numeric($width)) {
            $width .= 'px';
        }
        if (is_numeric($height)) {
            $height .= 'px';
        }
        $html = '<div'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlAttributes($attributes)
        . self::htmlStyles(array('width' => $width, 'height' => $height))
        . ' >' ."\n"
        . '</div>'."\n";
        return $html;
    }


    /**
     * Returns html for a unordered list.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options				Options for layout items
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function ul($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {

        $html = '<ul'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes)
        . ' >' ."\n".
        $this->displayableItemsToHtmlLayout($displayableItems, array('widget-layout-list-item'), $options, 'li', __FUNCTION__)
        . '</ul>'."\n";
        return $html;
    }


    /**
     * Returns html label element.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param string				$text
     * @param string				$forId
     * @param Widget_CanvasOptions	$options
     * @param string				$title			The (optional) tooltip
     * @param string[]              $attributes     array of optional attributes
     * @return string
     */
    public function label($id, $classes, $text, $forId, $options = null, $title = null, $attributes = null)
    {

        if (empty($forId)) {

            return $this->span($id, $classes, array($this->text($text)), $options, $title, $attributes);

        } else {


            $style = $this->getStyle($options);
            $html = '<label'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . ' for="' . bab_toHtml($forId) . '"'
            . $style
            . ' >' . $this->text($text) . '</label>'."\n";
            return $html;
        }
    }





    /**
     * Returns a canvas richtext element.
     *
     * @param string	$id					The unique id (for the page).
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param string	$text
     * @param int		$renderingOption	rendering options BAB_HTML_*.
     * @param Widget_CanvasOptions	$options
     * @return string
     *
     */
    public function richtext($id, $classes, $text, $renderingOption, $options)
    {
        $text = bab_toHtml($text, $renderingOption);
        if ($text == '' || $text == ' ') {
            $text = '&nbsp;';
        }
        return $this->div($id, $classes, array($text), $options);
    }




    /**
     * Display items as html
     * @param 	array					$displayableItem		Widget_Displayable_Interface object or string.
     * @return string
     */
    private function displayableItemToHtml($item)
    {
        if ($item instanceof Widget_Displayable_Interface) {
            return $item->display($this);
        } elseif (is_string($item)) {
            return $item;
        } elseif (is_int($item) || is_float($item)) {
            return (string)$item;
        } elseif ($item !== null) {
            return $item;
            trigger_error(sprintf('wrong displayable item type : %s', gettype($item)));
        }
        return '';
    }




    /**
     * Display items as html
     * @param 	array					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
     * @return string
     */
    private function displayableItemsToHtml($displayableItems)
    {
        $htmlContent = '';
        if (!is_array($displayableItems)) {
            return $this->displayableItemToHtml($displayableItems);
        }
        foreach ($displayableItems as $item) {
            $htmlContent .= $this->displayableItemToHtml($item);
        }

        return $htmlContent;
    }



    /**
     * Display items as html with layout options
     * @param 	array					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
     * @param	array					$itemsClasses
     * @param	Widget_CanvasOptions	$itemsOptions
     * @param	string					$itemTagName
     * @param	string					$layout_name
     * @return string
     */
    protected function displayableItemsToHtmlLayout(Array $displayableItems, Array $itemsClasses, Widget_CanvasOptions $itemsOptions = null, $itemTagName, $layout_name)
    {
        $htmlContent = '';

        switch ($layout_name) {
            case 'hbox':
            case 'vbox':
                $position_x = 0;
                $position_y = 0;
                break;
            case 'flow':
            default:
                $position_x = null;
                $position_y = null;
                break;
        }


        foreach ($displayableItems as $item) {

            if ($item instanceof Widget_Displayable_Interface) {
                $htmlItem = $item->display($this);
                $sizePolicy = $item->getSizePolicy();
                $parentAttributes = self::htmlAttributes($item->getParentAttributes());
            } elseif (is_string($item)) {
                $sizePolicy = '';
                $parentAttributes = '';
                $htmlItem = $item;
            } else {
                trigger_error(sprintf('wrong displayable item type : %s', gettype($item)));
                return '';
            }


            if (null !== $itemsClasses || null !== $itemsOptions) {

                $htmlItem = '<'.$itemTagName.' class="'.implode(' ', $itemsClasses).' '.$sizePolicy.'" '.$parentAttributes.' '.$this->getStyle($itemsOptions, $position_x, $position_y).'>'.$htmlItem.'</'.$itemTagName.'>';
            }


            switch($layout_name) {
                case 'hbox':
                    $position_x++;
                    break;
                case 'vbox':
                    $position_y++;
                    break;
                case 'flow':
                    break;
            }


            $htmlContent .= $htmlItem;
        }

        return $htmlContent;
    }



    /**
     * Display some html
     * @param string	$id
     * @param string[]	$classes
     * @param string	$html
     * @return string
     */
    public function html($id, $classes, $html, $options = null, $title = null, $attributes = null)
    {
        return $this->div($id, $classes, array($html), $options, $title, $attributes);
    }


    /**
     * Returns html for a div.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
     * @param Widget_CanvasOptions	$options				Options for layout items.
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function div($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);

        $html = '<div'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . $style
            . ' >' . $this->displayableItemsToHtml($displayableItems) . '</div>';
        return $html;
    }




    /**
     * Returns html for a header.
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or strings
     * @param int					$level					integer from 1 to 6, represent the importance of the header
     * @param Widget_CanvasOptions	$options				Options for layout items.
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function h($id, $classes, $displayableItems, $level, $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);

        $html = '<h'.$level
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . $style
            . ' >' . $this->displayableItemsToHtml($displayableItems) . '</h'.$level.'>'."\n";
        return $html;
    }




    /**
     * Returns html for a span.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
     * @param Widget_CanvasOptions	$options				Options for layout items.
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function span($id, $classes, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);

        $html = '<span'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . $style
            . ' >' . $this->displayableItemsToHtml($displayableItems) . '</span>'."\n";
        return $html;
    }


    /**
     * output url for link in the canvas
     * @param string $url
     * @return string
     */
    protected function outputUrl($url)
    {
        return $url;
    }



    /**
     * Returns html for a link.
     *
     * @param string				$id
     * @param string[]				$classes
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or strings.
     * @param string				$url
     * @param Widget_CanvasOptions	$options
     * @param string				$title
     * @param string[]				$attributes
     * @return string
     */
    public function linkContainer($id, $classes, $displayableItems, $url, $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);

        $html = '<a'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlAttributes($attributes)
        . self::htmlTitle($title);
        if (isset($url)) {
           $html .= ' href="' . bab_toHtml($this->outputUrl($url)) . '"';
        }
        $html .= $style
            . ' >' . $this->displayableItemsToHtml($displayableItems) . '</a>'."\n";
        return $html;
    }






    /**
     * Returns an HTML checkbox input field.
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		$fullName			An array containing the full name as returned by Widget_Item::getFullName()
     * @param bool		$value				The initial value.
     * @param string	$checked_value
     * @param string	$unchecked_value	If not null, this value will be submitted if the checkbox is not checked. If null, no value is submitted if the checkbox is not checked.
     * @param bool		$disabled
     * @param string	$title				The tooltip title
     * @param string[]  $attributes         optional attributes
     * @return string
     */
    public function checkBoxInput($id, $classes, $fullName, $value, $checked_value, $unchecked_value, $disabled, $title, $attributes = null)
    {
        if (isset($unchecked_value)) {
            $html = '<input type="hidden"'
            . ' class="widget-checkbox-hidden"'
            . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"'
            . ' value="'. bab_toHtml($unchecked_value).'"'
            . self::htmlDisabled($disabled)
            . ' />';
        } else {
            $html = '';
        }
        $html .= '<input type="checkbox"'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlDisabled($disabled)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes)
        . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"';

        if ($checked_value != 'on') {
            $html .= ' value="' . bab_toHtml($checked_value) . '"';
        }

        $html .= (isset($value) && ((string) $value == (string) $checked_value) ? ' checked="checked"' : '')
        . ' />'."\n";
        return $html;
    }

    /**
     * Returns an HTML button
     *
     * @param string				$id					The unique (for the page) html id.
     * @param string[]				$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param bool					$disabled
     * @param array					$displayableItems	Array of Widget_Displayable_Interface objects.
     * @param Widget_CanvasOptions	$options			Canvas options
     * @param string				$title				button title
     * @param array					$attributes         Optional attributes
     * @return string
     */
    public function button($id, $classes, $disabled, $displayableItems, $options = null, $title = null, $attributes = null)
    {
        $html = '<button '
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlDisabled($disabled)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes)
        . $this->getStyle($options)
        . ' >' ."\n". $this->displayableItemsToHtml($displayableItems) . '</button>'."\n";
        return $html;
    }


    /**
     * Returns an HTML submit button
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		$fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
     * @param string	$label				button text.
     * @param bool		$disabled
     * @param string	$title				button title
     * @param array     $attributes
     * @return string
     */
    public function submitInput($id, $classes, $fullName, $label, $disabled, $title, $attributes = null)
    {

        $html = '<input type="submit"'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlDisabled($disabled)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes);

        if (null !== $label) {
            $html .= ' value="' . bab_toHtml($label) . '"';
        }

        if ($fullName) {
            $html .= ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"';
        }

        $html .= ' />'."\n";

        return $html;
    }


    /**
     * Returns an HTML reset button
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param string	$label				button text.
     * @param bool		$disabled
     * @param string	$title				button title
     * @param array     $attributes
     * @return string
     */
    public function resetInput($id, $classes, $label, $disabled, $title, $attributes = null)
    {

        $html = '<input type="reset"'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlDisabled($disabled)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes);

        if (null !== $label) {
            $html .= ' value="' . bab_toHtml($label) . '"';
        }

        $html .= ' />'."\n";

        return $html;
    }


    /**
     * Returns an HTML hidden input field.
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		$fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
     * @param string	$value				The initial value.
     * @param array     $attributes
     * @return string
     */
    public function hidden($id, $classes, $fullName, $value, $attributes = null)
    {
        $html = '<input type="hidden"'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . ' value="' . bab_toHtml($value) . '"'
        . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"'
        . self::htmlAttributes($attributes)
        . ' />'."\n";
        return $html;
    }


    /**
     * Returns an HTML single line text input field.
     *
     * @param string	            $id					The unique (for the page) html id.
     * @param string[]	            $classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		            $fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
     * @param string	            $value				The initial value.
     * @param int		            $size				The size in character or NULL.
     * @param int		            $maxSize			The maximum size in characters or NULL.
     * @param bool		            $disabled
     * @param bool		            $readOnly			The value of the readonly parameter.
     * @param bool|int|string       $autoComplete		The value of the autocomplete parameter.
     * @param string	            $type				Html input type.
     * @param string	            $placeholder		The placeholder text.
     * @param Widget_CanvasOptions	$options			Options for layout items.
     * @param string	            $title				The tooltip title
     * @param string[]              $attributes         An array of name => value attributes.
     * @return string
     */
    public function lineInput(
        $id,
        $classes,
        $fullName,
        $value,
        $size,
        $maxSize,
        $disabled,
        $readOnly = false,
        $autoComplete = true,
        $type = 'text',
        $placeholder = null,
        $options = null,
        $title = null,
        $attributes = null
    ) {
        if ($autoComplete === null || $autoComplete === true || $autoComplete === '1' || $autoComplete === 1) {
            $autoComplete = null;
        } elseif ($autoComplete === false || $autoComplete === '0' || $autoComplete === 0) {
            $autoComplete = 'off';
        }
        $html = '<input type="' . bab_toHtml($type) . '"'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . self::htmlDisabled($disabled)
            . ($readOnly ? ' readonly="readonly"' : '')
            . self::htmlAttribute('value', $value)
            . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"'
            . self::htmlAttribute('size', $size)
            . self::htmlAttribute('maxlength', $maxSize)
            . self::htmlAttribute('placeholder', $placeholder)
            . self::htmlAttribute('autocomplete', $autoComplete)
            . ' />'."\n";
        return $html;
    }


    /**
     * Returns an jQuery Slider.
     *
     * @param string	            $id					The unique (for the page) html id.
     * @param string[]	            $classes			An array containing the classes as returned by Widget_Item::getClasses()
	 * @param string[]	            $inputClasses       An array containing the classes for the input item as returned by Widget_Slider::getInputClasses()
     * @param array		            $fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
     * @param string	            $value				The initial value.
     * @param int		            $size				The size in character or NULL.
     * @param bool		            $disabled
     * @param Widget_CanvasOptions	$options			Options for layout items.
     * @param string	            $title				The tooltip title
     * @param string[]              $attributes         An array of name => value attributes.
     * @return string
     */
    public function slider(
        $id,
        $classes,
        $inputClasses,
        $fullName,
        $value,
        $size,
        $disabled,
        $options = null,
        $title = null,
        $attributes = null
    ) {
        $html = '<div'
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . self::htmlTitle($title)
            . self::htmlAttributes($attributes)
            . self::htmlDisabled($disabled)
            . self::htmlAttribute('size', $size)
            .'>'."\n".
                '<input type="text" style="display: none;"'
                . self::htmlDisabled($disabled)
                . self::htmlClasses($inputClasses)
                . self::htmlAttribute('value', $value)
                . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"'
                . ' autocomplete="off"'
                . ' />'."\n"
            .'</div>'."\n";
        return $html;
    }


    /**
     * Returns an HTML single line password input field.
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		$fullName			An array containing the full name as returned by Widget_InputWidget::getFullName()
     * @param string	$value				The initial value.
     * @param int		$size				The size in character or NULL.
     * @param int		$maxSize			The maximum size in characters or NULL.
     * @param bool		$disabled
     * @param string	$title
     * @param bool		$readOnly			The value of the readonly parameter.
     * @param bool      $autoComplete       The value of the autocomplete parameter.
     * @param string    $placeholder        The placeholder text.
     * @return string
     */
    public function passwordInput($id, $classes, $fullName, $value, $size, $maxSize, $disabled, $title, $readOnly = 0, $autoComplete = 1, $placeholder = null)
    {
        if ($autoComplete === null || $autoComplete === true || $autoComplete === '1' || $autoComplete === 1) {
            $autoComplete = null;
        } elseif ($autoComplete === false || $autoComplete === '0' || $autoComplete === 0) {
            $autoComplete = 'off';
        }
        $html = '<input type="password"'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlDisabled($disabled)
        . self::htmlTitle($title)
        . ($readOnly ? ' readonly="readonly"' : '')
        . self::htmlAttribute('value', $value)
        . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"'
        . self::htmlAttribute('size', $size)
        . self::htmlAttribute('autocomplete', $autoComplete)
        . self::htmlAttribute('maxlength', $maxSize)
        . self::htmlAttribute('placeholder', $placeholder)
        . ' />'."\n";
        return $html;
    }


    /**
     * Returns an HTML multi-line text input field.
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		$fullName			An array containing the full name as returned by Widget_Item::getFullName()
     * @param string	$value				The initial value.
     * @param int		$width				The width in character or NULL.
     * @param int		$height				The height in characters or NULL.
     * @param bool		$disabled
     * @param string    [$title]            Optional title
     * @param array     [$attributes]       Optional attributes
     * @return string
     */
    public function textInput($id, $classes, $fullName, $value, $width, $height, $disabled, $title = null, $attributes = null)
    {
        $html = '<textarea'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlDisabled($disabled)
        . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"'
        . self::htmlAttribute('cols', $width)
        . self::htmlAttribute('rows', $height)
        . self::htmlAttribute('title', $title)
        . self::htmlAttributes($attributes)
        . '>' . bab_toHtml($value) . '</textarea>'."\n";
        return $html;
    }


    /**
     * Returns a single line text input field.
     *
     * @param string	$id					The unique id (for the page).
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		$fullName			An array containing the full name as returned by Widget_Item::getFullName()
     * @param array		$acceptedMimeType   The mime type.
     * @param int		$size				The size of uploader.
     * @param bool		$multiple			Multiple file upload, default false
     * @param string    [$title]            Optional title
     * @param array     [$attributes]       Optional attributes
     * @return string
     */
    public function fileUpload($id, $classes, $fullName, $acceptedMimeType, $size, $multiple = false, $title = null, $attributes = null)
    {
        $name_suffix = $multiple ? '[]' : '';

        $html = '<input'
        . self::htmlClasses($classes)
        . self::htmlId($id)
        . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . $name_suffix. '"'
        . ' type="file"';
        if ($size != null) {
            $html .= ' size ="' . bab_toHtml($size) . '"';
        }
        if (count($acceptedMimeType) > 0) {
            $html .= ' accept="' . bab_toHtml(implode(',', $acceptedMimeType)) . '"';
        }

        if ($multiple) {
            $html .= ' multiple=""';
        }
        $html .= self::htmlAttribute('title', $title)
          . self::htmlAttributes($attributes);

        $html .= ' />';

        return $html;
    }





    private function selectOptions($options, $selectedValue)
    {
        $html = '';

        foreach ($options as $optkey => $optvalue) {
            if ($optvalue instanceof Widget_SelectOption) {
                if (is_array($selectedValue)) {
                    $optvalue->selected(in_array($optkey, $selectedValue));
                } else {
                    $optvalue->selected(((string) $optkey) === ((string) $selectedValue));
                }
                $html .= $optvalue->display($this);
            } else {
                if (is_array($selectedValue)) {

                    $html .= $this->selectOption($optkey, $optvalue, array(), in_array($optkey, $selectedValue));
                } else {
                    $html .= $this->selectOption($optkey, $optvalue, array(), ((string) $optkey) === ((string) $selectedValue));
                }
            }
        }

        return $html;
    }


    /**
     * @param	string					$value
     * @param	string					$text
     * @param	string[]				$classes
     * @param	bool					$selected
     * @param	string					$title
     * @param	bool					$disabled
     * @param	Widget_CanvasOptions	$canvasOptions
     */
    public function selectOption($value, $text, array $classes, $selected, $title = null, $disabled = false, Widget_CanvasOptions $canvasOptions = null)
    {
        $html = '<option value="'.bab_toHtml($value).'"'
                . self::htmlClasses($classes)
                . self::htmlDisabled($disabled)
                . $this->getStyle($canvasOptions);

        if ($selected) {
            $html .= ' selected="selected" ';
        }

        $html .= '>'.str_replace(array(' ','&lt;b&gt;', '&lt;/b&gt;'), array('&nbsp;','<b>', '</b>'), bab_toHtml($text)).'</option>'."\n";

        return $html;
    }



    /**
     * Returns an HTML select.
     *
     * @param string				$id					The unique (for the page) html id.
     * @param string[]				$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array					$fullName			An array containing the full name as returned by Widget_Item::getFullName()
     * @param mixed					$value				The initial value.
     * @param array					$selectOptions			key values for select
     * @param array					$optgroups			options in groups
     * @param bool					$disabled
     * @param int					$size				number of lines to display
     * @param array					$classesOptions		Options classes
     * @param array					$classesOptGroups	Options groups classes
     * @param bool					$muliple			Multiple select
     * @param Widget_CanvasOptions	$options				Options for layout items.
     * @param string	            $title				    The tooltip title
     * @param string[]              $attributes             An array of name => value attributes.
     * @return string
     */
    public function select(
        $id,
        $classes,
        $fullName,
        $value,
        $selectOptions = null,
        $optgroups = null,
        $disabled = false,
        $size = null,
        $classesOptions = null,
        $classesOptGroups = null,
        $multiple = false,
        $options = null,
        $title = null,
        $attributes = null
    ) {


        $html = '<select'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes)
        . self::htmlDisabled($disabled)
        . self::htmlMultiple($multiple)
        . $this->getStyle($options)
        . ' name="' . bab_toHtml(self::getHtmlName($fullName)) . ($multiple?'[]':''). '"'
        . (isset($size) ? ' size="' . $size . '"' : "")
        . '>'."\n";

        // regular options

        $html .= $this->selectOptions($selectOptions, $value);


        // optgroups

        if (isset($optgroups)) {
            foreach ($optgroups as $optgroup => $selectOptions) {
                $html .= '<optgroup label="'.str_replace(array(' ','&lt;b&gt;', '&lt;/b&gt;'), array('&nbsp;','<b>', '</b>'), bab_toHtml($optgroup)).'">';
                $html .= $this->selectOptions($selectOptions, $value);
                $html .= '</optgroup>';
            }
        }

        $html .= '</select>'."\n";

        return $html;
    }


    /**
     * Returns an HTML radio button.
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param array		$fullName			An array containing the full name as returned by Widget_Item::getFullName()
     * @param string	$value
     * @param bool		$checked			The initial value.
     * @param bool		$disabled
     * @return string
     */
    public function radioInput($id, $classes, $fullName, $value, $checked, $disabled)
    {
        $html = '<input type="radio" '
        . self::htmlClasses($classes)
        . self::htmlId($id)
        .' name="'. bab_toHtml(self::getHtmlName($fullName)).'" '
        .' value="'.bab_toHtml($value). '" ';

        if ($checked) {
            $html .= 'checked="checked" ';
        }

        $html.= self::htmlDisabled($disabled);

        $html.= '/>';
        return $html;
    }




    /**
     * Display column layout for business application page
     * @param	Widget_BusinessApplicationPage	$obj
     * @return string
     */
    public function BusinessApplicationPage(Widget_BusinessApplicationPage $obj)
    {
        return $obj->getHtml('businessapplicationpage.html', 'page');
    }


    /**
     * Display a business application list
     * @param	Widget_BusinessApplicationList	$obj
     * @return string
     */
    public function BusinessApplicationList(Widget_BusinessApplicationList $obj)
    {
        return $obj->getHtml($this);
    }


    /**
     * Display a list of pair with a title
     *
     * @param string	$id					The unique (for the page) html id.
     * @param string[]	$classes			An array containing the classes as returned by Widget_Item::getClasses()
     * @param string	$title				Title of list
     * @param array		$displayableItems	Array of Widget_Displayable_Interface objects.
     * @param string[]  $attributes         optional attributes
     * @return string
     */
    public function PairVBox($id, $classes, $title, $displayableItems, $attributes = null)
    {
        $html = '<table'
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlAttribute('title', $title)
        . self::htmlAttributes($attributes)
        . ">\n";

        if (!empty($title)) {
            $html .= '<caption>' . bab_toHtml($title) . '</caption>'."\n";
        }

        $html .= '<tbody>'."\n";

        foreach ($displayableItems as $item) {

            $html .= '<tr'.self::htmlClasses($item->getClasses());

            if ($item instanceof Widget_Pair) {
                $html .= '><th>'.$item->getFirst()->display($this).'</th><td>'.$item->getSecond()->display($this).'</td></tr>'."\n";
            } else {
                $html .= '><td colspan="2">'.$item->display($this).'</td></tr>'."\n";
            }
        }


        $html .= '</tbody></table>'."\n";

        return $html;
    }



    /**
     * Updates the content of hiddenFields array with values from _GET and _POST matching the fields of the form $id.
     *
     * @param string $id
     * @param array $hiddenFields
     */
    private function getSelfPageHiddenFields($id, &$hiddenFields)
    {
        $context = array_keys($_POST + $_GET);
        $form = Widget_Item::getById($id);

        $fnames = array();
        foreach ($form->getFields() as $f) {
            $htmlname = self::getHtmlName($f->getFullName());
            if (!empty($htmlname)) {
                $fnames[] = $htmlname;
            }
        }

        $context = array_diff($context, $fnames);

        foreach ($context as $fieldname) {
            $value = bab_rp($fieldname);
            if (!is_array($value) && !isset($hiddenFields[$fieldname])) {
                $hiddenFields[$fieldname] = $value;
            }
        }
    }



    /**
     * Display a form
     *
     * @param string				$id						The unique (for the page) html id.
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses()
      * @param array					$fullName				An array containing the full name as returned by Widget_Item::getFullName(
     * @param boolean				$readonly				Form generate a modification of data on server if true
     * @param array					$hiddenFields			Hidden fields
     * @param array					$displayableItems
     * @param boolean				$selfPageHiddenFields	Add necessary hidden fields in form to submit on the same page
     * @param string				$anchor					Set anchor in destination page
     * @param Widget_CanvasOptions	$options				Canvas options
     * @param string				$title					The optional tooltip
     * @param string[]				$attributes             Array of optional attributes
     * @return string
     */
    public function form($id, $classes, $fullName, $readonly, $hiddenFields, $displayableItems, $selfPageHiddenFields, $anchor = null, $options = null, $title = null, $attributes = null)
    {
        $method = $readonly ? 'get' : 'post';


        if (true === $selfPageHiddenFields) {
            $this->getSelfPageHiddenFields($id, $hiddenFields);
        }

        $style = $this->getStyle($options);

        $name = '';
        if(bab_toHtml(self::getHtmlName($fullName))){
            $name = ' name="' . bab_toHtml(self::getHtmlName($fullName)) . '"';
        }

        $html  = '<form '
            . self::htmlId($id)
            . $name
            . self::htmlClasses($classes)
            . self::htmlAttributes($attributes)
            . self::htmlTitle($title)
            . $style;

        if (false === $readonly) {
            $html .= ' enctype="multipart/form-data" ';
        }

        if (isset($GLOBALS['babPhpSelf'])) {
            $action = $GLOBALS['babPhpSelf'];
        } else {
            $action = bab_getSelf();
        }

        if (null !== $anchor) {
            $action .= '#'.urlencode($anchor);
        }

        $html .= ' method="' . bab_toHtml($method) .'"';
        $html .= ' action="' . bab_toHtml($action) . '">';
        $html .= "\n";

        foreach ($hiddenFields as $name => $value) {
            $html .= '<input type="hidden" name="' . bab_toHtml($name) . '" value="' . bab_toHtml($value) . '" />'."\n";
        }

        $html .= $this->displayableItemsToHtml($displayableItems);

        // This is a fix for Internet Explorer
        $html .=  '<input type="text" style="visibility:hidden;display:none;" />';

        $html .= '</form>'."\n";

        return $html;
    }






    /**
	 * Display an image
	 * @param string				$id				The unique id (for the page).
	 * @param string[]				$classes		An array containing the classes as returned by Widget_Item::getClasses().
	 * @param string				$text			The alternate text if the image cannot be displayed.
	 * @param string				$url			The url of the image.
	 * @param Widget_CanvasOptions	$options
	 * @param string				$title			The optional tooltip
	 * @param string[]              $attributes             An array of name => value attributes.
	 * @return 	string
     */
    public function image($id, $classes, $text, $url, $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);
        $html  = '<img '
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlTitle($title)
        . self::htmlAttributes($attributes)
        . $style
        . ' src="'.bab_toHtml($url).'"'
        . ' alt="'.bab_toHtml($text).'" />';


        return $html;
    }



    /**
     * Display a bab_TreeView object
     * @param 	string			$id				The unique (for the page) html id.
     * @param 	string[]		$classes		An array containing the classes as returned by Widget_Item::getClasses()
     * @param	bab_TreeView	$tree
     */
    public function simpletreeview($id, $classes, $tree)
    {
        return $this->div(null, $classes, array($tree->printTemplate()));
    }



    /**
     * Treeview Node
     *
     * @param string	              $id
     * @param string[]	              $classes
     * @param array		              $displayableItems
     * @param array		              $childNodes				An array of Widget_TreeViewNode
     * @param Widget_CanvasOptions	  $options
     * @return string
     */
    public function treeviewnode($id, Array $classes, Array $displayableItems, Array $childNodes, Widget_CanvasOptions $options = null)
    {
        $style = $this->getStyle($options);

        $html = '<li '
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . $style
        . '>';

        $html .= '<div class="line">';
        $html .= $this->displayableItemsToHtmlLayout($displayableItems, array('widget-layout-flow-item'), $options, 'span', 'flow');
        $html .= '</div>';

        if (count($childNodes) > 0) {
            $html .= '<ul>';

            foreach ($childNodes as $childNode) {
                $html .= $childNode->display($this);
            }

            $html .= '</ul>';
        }

        $html .= '</li>';

        return $html;
    }



    /**
     * Returns canvas grid layout.
     *
     * @see Widget_HtmlCanvas::gridSection()
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
     * @param array					$rows					An array of values returned by gridSection().
     * @param array					$colsOptions			An array of Widget_CanvasOptions that will be applied to corresponding columns.
     * @param Widget_CanvasOptions	$options				CanvasOptions that will be applied to the grid.
     * @param string				$title				    The tooltip title.
     * @param string[]				$attributes				Optional attributes.
     * @return string
     */
    public function grid($id, $classes, $rows = array(), $colsOptions = array(), Widget_CanvasOptions $options = null, $title = null, $attributes = null)
    {
        $style = $this->getStyle($options);

        if (isset($options)
          && ($options->horizontalSpacingUnit() === 'px')
          && ($options->verticalSpacingUnit() === 'px')
          && ($options->verticalSpacing() === $options->horizontalSpacing())
          && ($options->verticalSpacing() > 0)) {
            $cellspacing = $options->verticalSpacing();
        } else {
            $cellspacing = 0;
        }
        $html  = '<table border="0" cellspacing="' . $cellspacing . '" cellpadding="0" '
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlAttribute('title', $title)
        . self::htmlAttributes($attributes)
        . $style
        . '>';

        foreach ($colsOptions as $colOptions) {
            $colStyle = $this->getStyle($colOptions);
            $html .= '<colgroup span="1" ' . $colStyle . '></colgroup>';
        }

        foreach ($rows as $row) {
            $html .= $row;
        }
        $html .= '</table>';

        return $html;
    }


    private function gridSectionTag($tag, $id, $classes, $rows = array(), $options = null)
    {
        $style = $this->getStyle($options);

        $html  = '<'.$tag.' '
            . self::htmlId($id)
            . self::htmlClasses($classes)
            . $style
            . '>';

            foreach ($rows as $row) {
                $html .= $row;
            }

            $html .= '</'.$tag.'>';

            return $html;
    }


    /**
     * Returns a header grid section for the canvas grid layout.
     *
     * @see Widget_HtmlCanvas::gridRow()
     *
     * @param string                $id         The unique id (for the page).
     * @param string[]              $classes    An array containing the classes as returned by Widget_Item::getClasses().
     * @param array                 $rows       Array of values returned by gridRow().
     * @param Widget_CanvasOptions  $options    CanvasOptions that will be applied to the section.
     */
    public function gridHeaderSection($id, $classes, $rows = array(), $options = null)
    {
        return $this->gridSectionTag('thead', $id, $classes, $rows, $options);
    }


    /**
     * Returns a grid section for the canvas grid layout.
     *
     * @see Widget_HtmlCanvas::gridRow()
     *
     * @param string                $id         The unique id (for the page).
     * @param string[]              $classes    An array containing the classes as returned by Widget_Item::getClasses().
     * @param array                 $rows       Array of values returned by gridRow().
     * @param Widget_CanvasOptions  $options    CanvasOptions that will be applied to the section.
     */
    public function gridSection($id, $classes, $rows = array(), $options = null)
    {
        return $this->gridSectionTag('tbody', $id, $classes, $rows, $options);
    }


    /**
     * Returns a grid row for the canvas grid layout.
     *
     * @see Widget_HtmlCanvas::gridRowCell()
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
     * @param array					$cells					Array of values returned by gridRowCell().
     * @param Widget_CanvasOptions	$options				CanvasOptions that will be applied to the section.
     * @return string
     */
    public function gridRow($id, $classes, $cells = array(), $options = null)
    {
        $style = $this->getStyle($options);

        $html  = '<tr '
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . $style
        . '>';

        foreach ($cells as $cell) {
            $html .= $cell;
        }

        $html .= '</tr>';

        return $html;
    }


    /**
     * Returns a grid cell for grid row the canvas grid layout.
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses().
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects or string.
     * @param Widget_CanvasOptions	$options				Options for layout items
     * @return string
     */
    public function gridRowCell($id, $classes, $displayableItems = array(), $options = null, $rowSpan = null, $colSpan = null, $colDesc = null)
    {
        $style = $this->getStyle($options);

        $parentAttributes = '';
        if ($displayableItems instanceof Widget_Displayable_Interface) {
            $classes[] = $displayableItems->getSizePolicy();
            $parentAttributes = self::htmlAttributes($displayableItems->getParentAttributes());
        } elseif (count($displayableItems) == 1) {
            $item = $displayableItems[0];
            if (isset($item) && $item instanceof Widget_Item) {
                $classes[] = $item->getSizePolicy();
                $parentAttributes.= self::htmlAttributes($item->getParentAttributes()) .' ';
            }
        }

        $html  = '<td '
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . self::htmlAttribute('data-th', $colDesc)
        . $parentAttributes
        . (isset($rowSpan) ? ' rowspan="' . $rowSpan . '"' : '')
        . (isset($colSpan) ? ' colspan="' . $colSpan . '"' : '')
        . $style
        . '>';


        $html .= $this->displayableItemsToHtml($displayableItems);

        $html .= '</td>';

        return $html;
    }



    /**
     * Display a menu with tab items
     *
     * @param string				$id						The unique id (for the page).
     * @param string[]				$classes				An array containing the classes as returned by Widget_Item::getClasses()
     * @param array					$displayableItems		Array of Widget_Displayable_Interface objects.
     * @param Widget_CanvasOptions	$options
     *
     * @return string
     */
    public function tabs($id, $classes, $displayableItems, $options)
    {
        $html  = '<div '
        . self::htmlId($id)
        . self::htmlClasses($classes)
        . $this->getStyle($options)
        . "><ul>\n";

        $content = '';

        foreach ($displayableItems as $tab) {

            $content .= $tab->display($this);

            if (null !== $tab->getAction()) {
                $url = $tab->getAction()->url();
            } else {
                $url = '#' . $this->text($tab->getId());
            }

            $html  .= '<li '
            . self::htmlClasses($tab->getClasses())
            . '>'.$this->linkContainer('', array(), array($this->span('', '', array($tab->getLabel()))), $url, $tab->getCanvasOptions())
            . "</li>\n";
        }

        $html .= "</ul>".$content."</div>\n";

        return $html;
    }


    /**
     * Create anchor
     * @param	string	$name
     * @return string
     */
    public function anchor($name)
    {
        return '<a name="' . $this->text($name).'"></a>';
    }


    /**
     * @param	mixed	$value
     * @return	string
     */
    public static function json_encode($value)
    {
        require_once $GLOBALS['babInstallPath'] . 'utilit/json.php';
        return bab_json_encode($value);
    }


    /**
     * Create metadata container.
     *
     * @param	string	$name
     * @param	mixed	$metadata
     * @return string
     */
    public function metadata($name, $metadata = null)
    {
        if (empty($metadata)) {
            return '';
        }

        $html = '<script type="text/javascript">'."\n"
            . '// <![CDATA['."\n"
            . 'window.babAddonWidgets.addMetadata("'.bab_toHtml($name, BAB_HTML_ENTITIES | BAB_HTML_JS).'", '.self::json_encode($metadata).");\n"
            . '// ]]>'."\n"
            . '</script>'."\n";

        return $html;
    }


    /**
     * Load script
     * @param	string	$widget_id
     * @param	string	$filename
     * @return string
     */
    public function loadScript($widget_id, $filename)
    {
        if (empty($filename)) {
            return '';
        }

        $html = '<script type="text/javascript">'."\n"
            . '// <![CDATA['."\n"
            . 'window.babAddonWidgets.needScript("'.bab_toHtml($widget_id).'", "'.bab_toHtml($filename).'");'."\n"
            . '// ]]>'."\n"
            . '</script>'."\n";

        return $html;
    }


    /**
     * Includes the specified script from the template path ot the specified addon.
     * The addon version is appended to the script url to ensure that cached script will be
     * reloaded on addon upgrade.
     *
     * @since 1.0.91
     *
     * @param string $widgetId
     * @param bab_addonInfos $addon
     * @param string $filename
     * @return string
     */
    public function loadAddonScript($widgetId, bab_addonInfos $addon, $filename)
    {
        if (!empty($filename)) {
            $filename = $addon->getTemplatePath() . $filename . '?' . $addon->getDbVersion();
        }
        return $this->loadScript($widgetId, $filename);
    }


    /**
     * Load CSS style sheet
     * @param	string	$filename
     * @return string
     */
    public function loadStyleSheet($filename)
    {
        if (empty($filename)) {
            return '';
        }

        $html = '<script type="text/javascript">'."\n"
            . '// <![CDATA['."\n"
            . 'window.babAddonWidgets.stylesheet("' . bab_toHtml($filename) . '");'."\n"
            . '// ]]>'."\n"
            . '</script>'."\n";

        return $html;
    }


    /**
     * Includes the specified CSS style sheet from the style path of the specified addon.
     * The addon version is appended to the css url to ensure that cached css will be
     * reloaded on addon upgrade.
     *
     * @since 1.0.91
     *
     * @param bab_addonInfos $addon
     * @param string $filename
     * @return string
     */
    public function loadAddonStyleSheet(bab_addonInfos $addon, $filename)
    {
        if (!empty($filename)) {
            $filename = $addon->getStylePath() . $filename . '?' . $addon->getDbVersion();
        }
        return $this->loadStyleSheet($filename);
    }


    /**
     * Returns an iframe element.
     *
     * @param string				$id			The unique id (for the page).
     * @param string				$url		url of page to load in iframe
     * @param Widget_CanvasOptions	$options
     * @return string
     *
     */
    public function iframe($id, $url, $options)
    {
        return '<iframe '
        . self::htmlId($id)
        . $this->getStyle($options)
        . ' src="'.bab_toHtml($url).'">'

        . '</iframe>';
    }



    /**
     * Add optional decorations on page.
     *
     * @param	Widget_Page	$page
     * @param	array		$classes	class names used in widgets of the page
     */
    public function setPageDecorations(Widget_Page $page, $classes)
    {
        $addon = bab_getAddonInfosInstance('widgets');

        if ($page->isCompatibleWith('jquery')) {

            if ($jquery = bab_functionality::get('jquery')) {

                if (isset($classes['Widget_Timeline'])) {
                    $page->prependJavascriptFile($addon->getTemplatePath().'timeline/timeline-init.js');
                    $page->prependJavascriptFile($addon->getTemplatePath().'timeline/timeline_js/timeline-api.js');
                    $page->addJavascriptFile($addon->getTemplatePath().'widgets.timeline.jquery.js');
                }

                $page->addStyleSheet($jquery->getStyleSheetUrl());

                $page->prependJavascriptFile($addon->getTemplatePath().'widgets.js');
            }
        }
    }


    /**
     * Sends page title through custom http header.
     *
     * Use during ajax interaction.
     *
     * As the http header must be encoded in ISO-8859-1 (in http 1.1 see https://dzone.com/articles/utf-8-in-http-headers),
     * it is converted using rawurlencode and will be decoded on the js side into the correct charset.
     *
     * @param string $title
     * @return string
     */
    public function sendPageTitle($title)
    {
        header('X-Ovi-PageTitle: ' . rawurlencode(bab_convertStringFromDatabase($title, bab_Charset::UTF_8)));
    }
}
