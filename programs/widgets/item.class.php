<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
// require_once dirname(__FILE__) . '/assert.php';



require_once dirname(__FILE__) . '/canvas.class.php';







/**
 * The Widget_Displayable_Interface denotes objects that can be displayed on a Widget_Canvas.
 *
 * @package GUI
 */
interface Widget_Displayable_Interface
{
    /**
     * @param Widget_Canvas	$canvas		The canvas
     * @return	string		The rendered string corresponding to the object on the specified canvas.
     */
    public function display(Widget_Canvas $canvas);
}



/**
 * Insert an array at a specified position in another array.
 * if $lengthToReplace is specified and greater than zero this number
 * of elements will be removed from the orginal array.
 *
 * Requires PHP 5.0.2 + (to preserve array keys)
 *
 * @param array		$destArray
 * @param array		$arrayToInsert
 * @param int		$insertPosition
 * @param int		$lengthToReplace
 * @return array
 */
function Widget_arrayInsert($destArray, $arrayToInsert, $insertPosition, $lengthToReplace = 0)
{
    $arrayBeginning = array_slice(
        $destArray,
        0,
        $insertPosition,
        true
    );
    $arrayEnding = array_slice(
        $destArray,
        $insertPosition + $lengthToReplace,
        count($destArray) - ($insertPosition + $lengthToReplace),
        true
    );

    $destArray = $arrayBeginning + $arrayToInsert + $arrayEnding;

    return $destArray;
}



/**
 * Appends an array to another array.
 *
 * Preserves array keys.
 *
 * @param array		$destArray
 * @param array		$arrayToAppend
 * @return array
 */
function Widget_arrayAppend($destArray, $arrayToAppend)
{
    $destArray = $destArray + $arrayToAppend;

    return $destArray;
}



class Widget_SizePolicy
{
    const IGNORED =	'ignored';
    const MINIMUM =	'minimum';
    const MAXIMUM =	'maximum';
    const FIXED =	'fixed';
}


/**
 * @package GUI
 * @return Widget_Item
 */
function Widget_Item($id)
{
    return Widget_Item::getById($id);
}


/**
 * Widget_Item is the base class for {@link Widget_Widget} and {@link Widget_Layout}.
 * They represent the objects that can be placed in a layout.
 *
 * Items have a unique id (on a specific page) and can be retrieved from any
 * place in the code using the static method Widget_Item::{@link getById}.
 *
 * @package GUI
 */
abstract class Widget_Item implements Widget_Displayable_Interface
{
    private $id;

    /**
     * @var string[] $classes		Additional (html) classes.
     */
    private $classes;

    /**
     * @var Widget_Item $parent
     */
    private $parent;

    private $title;

    /**
     * @var array
     */
    private $metadata;

    protected $attributes = array();

    protected $sizePolicy = '';
    protected $parentAttributes = array();

    /**
     * The itemCounter is used to generate unique ids.
     * @var int $itemCounter
     */
    protected static $itemCounter = 1;

    /**
     * @var Widget_CanvasOptions $canvasOptions
     */
    protected $canvasOptions = null;


    /**
     *
     * @var Widget_CacheHeaders	$cacheHeaders
     */
    private $cacheHeaders = null;


    /**
     * All items ID of objects inherited from Widget_Item
     */
    private static $allItems = array();

    /**
     * All class name of objects inherited from Widget_Item
     */
    protected static $allClasses = array();


    /**
     * @var string The url of the action if the item may be reloaded by an ajax call.
     */
    protected $delayedAction = null;



    /**
     * The specified id must be unique for all items created on the page.
     * If id is not specified, a unique id is automatically generated.
     *
     * @param string $id	The item unique id.
     * @return Widget_Item
     */
    public function __construct($id = null)
    {
        if (is_null($id)) {
            $id = $this->createId();
        }
        $this->setId($id);
        $this->setParent(null);
        $this->_initialized = false;
        $this->title = null;
        $this->classes = array();
        $this->metadata = array();

        $this->addAllClasses();
    }


    /**
     * Generates and return a unique id for the current page.
     */
    protected function createId()
    {
        include_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = bab_getInstance('bab_Session');
        if (!isset($session->widget_itemCounter)) {
           $session->widget_itemCounter = 1;
        }
        return strtolower(str_replace('\\', '_', get_class($this))) . $session->widget_itemCounter++;
      }

    /**
     */
    public function __clone()
    {
        $this->parent = null;
        $this->id = $this->createId();
    }



    /**
     *
     * @param string $html should be simple html with no style, no class...
     * @return Widget_Item
     */
    public function setContextualHelp($html)
    {
        $this->setMetadata('contextualHelp', $html);
        $this->addClass('widget-hasHelp');
        return $this;
    }

    /**
     * Updates the static $allClasses array.
     */
    protected function addAllClasses()
    {
        $classname = get_class($this);
        if (array_key_exists($classname, self::$allClasses)) {
            return;
        }

        self::$allClasses[$classname] = $classname;

        while ($classname = get_parent_class($classname)) {
            if (array_key_exists($classname, self::$allClasses)) {
                return;
            }
            self::$allClasses[$classname] = $classname;
        }


    }


    /**
     *
     * @return Widget_CanvasOptions
     */
    public static function options()
    {
        require_once dirname(__FILE__).'/canvasoptions.class.php';
        return new Widget_CanvasOptions();
    }

    /**
     * @return Widget_CacheHeaders
     */
    public static function cacheHeaders()
    {
        require_once dirname(__FILE__).'/cacheheaders.class.php';
        return new Widget_CacheHeaders();
    }

    /**
     * Sets the canvas options of the item.
     *
     * @param Widget_CanvasOptions $canvasOptions
     * @return $this
     */
    public function setCanvasOptions(Widget_CanvasOptions $canvasOptions)
    {
        $this->canvasOptions = $canvasOptions;
        return $this;
    }

    /**
     * @return Widget_CanvasOptions|null
     */
    public function getCanvasOptions()
    {
        return $this->canvasOptions;
    }



    /**
     * Returns the page to which this widget is attached.
     *
     * @return Widget_Page|null
     */
    public function getPage()
    {
        for ($widget = $this; $widget && (!($widget instanceof Widget_Page)); $widget = $widget->getParent()) {
            ;
        }

        return $widget;
    }

    /**
     *
     * @param Widget_CacheHeaders $cacheHeaders
     * @return $this
     */
    public function setCacheHeaders(Widget_CacheHeaders $cacheHeaders)
    {
        if ($page = $this->getPage()) {
            $page->cacheHeaders = $cacheHeaders;
            return $this;
        }

        $this->cacheHeaders = $cacheHeaders;
        return $this;
    }

    /**
     *
     * @return Widget_CacheHeaders | null
     */
    public function getCacheHeaders()
    {
        if ($page = $this->getPage()) {
            return $page->cacheHeaders;
        }

        return $this->cacheHeaders;
    }



    /**
     * Sets the size policy of the item.
     * Technically, the size policy $plicy is a classname applied on the
     * layout item containing this item.
     *
     * @see Widget_SizePolicy
     *
     * @param string $policy
     * @return $this
     */
    public function setSizePolicy($policy)
    {
        $this->sizePolicy = $policy;
        return $this;
    }

    /**
     * Returns the size policy of the item.
     *
     * @see Widget_SizePolicy
     * @return string | null
     */
    public function getSizePolicy()
    {
        return $this->sizePolicy;
    }

    /**
     * Sets the parent of the item.
     *
     * @param Widget_Item $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        if (null !== $parent && null !== $this->cacheHeaders) {
            if (null !== $cacheHeaders = $parent->getCacheHeaders()) {
                $cacheHeaders->combineWith($this->cacheHeaders);
            } else {
                $parent->setCacheHeaders($this->cacheHeaders);
            }

            $this->cacheHeaders = null;
        }
    }


    /**
     * Returns the parent of the item.
     *
     * @return Widget_Item A reference to the parent item or null.
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Returns all the items that have been instanciated.
     *
     * @return Widget_Item[]
     */
    public static function getAllItems()
    {
        return Widget_Item::$allItems;
    }

    /**
     * Returns the item with the specified id or null.
     *
     * @param string $id
     *            of the item to retrieve
     * @return Widget_Item
     */
    public static function getById($id)
    {
        assert('is_string($id); /* The "id" parameter must be a string */');
        if (isset(Widget_Item::$allItems[$id])) {
            return Widget_Item::$allItems[$id];
        }
        return null;
    }

    /**
     * Returns the item with the specified id or null.
     *
     * @param string $id
     *            of the item to retrieve
     * @return Widget_Item
     */
    public static function _($id)
    {
        return Widget_Item::getById($id);
    }


    /**
     * Sets the id of the item.
     *
     * The id must be unique for all items created on the page.
     *
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        assert('is_string($id); /* The "id" parameter must be a string */');
        assert('!isset(Widget_Item::$allItems[$id]); /* An item with the id "' . $id . '" already exists */');
        if (isset($this->id)) {
            unset(Widget_Item::$allItems[$this->id]);
        }
        Widget_Item::$allItems[$id] =& $this;
        $this->id = $id;

        return $this;
    }


    /**
     *
     */
    public function destroy()
    {
        unset(Widget_Item::$allItems[$this->id]);
    }

    /**
     * Returns the item id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add the specified class names to the item.
     *
     * @param string|array $className,... One or more class names.
     * @return $this
     */
    public function addClass($className /*,... */)
    {
        $args = func_get_args();
        $numArgs = func_num_args();
        for ($i = 0; $i < $numArgs; $i++) {
            if (!is_array($args[$i])) {
                $args[$i] = array($args[$i]);
            }
            foreach ($args[$i] as $class) {
                $this->classes[] = $class;
            }
        }
        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addAttribute($name, $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    /**
     * @return Widget_Item
     */
    public function removeAttribute($name)
    {
        unset($this->attributes[$name]);
        return $this;
    }


    /**
     *
     * @return string[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
    
    /**
     * Returns the value of the specified attribute, or null if the attribute is not set
     * @param string $attribute
     * @return NULL|mixed
     */
    public function getAttribute($attribute)
    {
        return isset($this->attributes[$attribute]) ? $this->attributes[$attribute] : null;    
    }



    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addParentAttribute($name, $value)
    {
        $this->parentAttributes[$name] = $value;
        return $this;
    }

    /**
     * @return Widget_Item
     */
    public function removeParentAttribute($name)
    {
        unset($this->parentAttributes[$name]);
        return $this;
    }


    /**
     *
     * @return string[]
     */
    public function getParentAttributes()
    {
        return $this->parentAttributes;
    }


    /**
     * Returns the (style) classes associated to this item.
     *
     * @return string[]
     */
    public function getClasses()
    {
        return $this->classes;
    }


    /**
     *
     * @param int $size
     * @param string $labelPosition		'top' or 'left'
     *
     * @return $this
     */
    public function setIconFormat($size, $labelPosition)
    {
        $this->addClass(
            'icon-' . $labelPosition . '-' . $size,
            'icon-' . $labelPosition,
            'icon-' . $size . 'x' . $size
        );
        return $this;
    }


    /**
     * Sets the title of the item.
     *
     * The title of the widget will be displayed in a tooltip.
     * This method returns the widget itself so that other methods can be chained.
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        //		assert('is_string($title); /* The "title" parameter must be a string. */');
        $this->title = $title;
        return $this;
    }

    /**
     * Returns the item title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }





    /**
     * Returns the structure of the item in an html ul / li tree.
     * Used for debugging purpose.
     *
     * @return string
     */
    public function dump()
    {
        $dumpString = '<span id="' . $this->getId() . '">' . get_class($this) . '("<b>' . $this->getId() . '</b>") </span>';
        $parent = $this->getParent();

        if (null === $parent) {
            $dumpString .= ' <small>parent=null</small>';
        } else {
            $dumpString .= ' <small>parent=<a href="#' . $parent->getId() . '">' . $parent->getId() . '</a></small>';
        }

        return $dumpString;
    }


    /**
     * Returns a string containing the html class of the item.
     *
     * The html class is obtained by concatenating the class name of the item
     * with its ancestors class names. (eg. "Widget_Frame Widget_ContainerWidget Widget_Widget Widget_Item").
     *
     * @return string 	The html class of the item.
     */
    protected function getHtmlClass()
    {
        $htmlclass = $classname = get_class($this);
        while ($classname = get_parent_class($classname)) {
            $htmlclass .= ' ' . $classname;
        }
        if (count($this->classes) > 0) {
            $htmlclass .= ' ' . implode(' ', $this->classes);
        }
        return $htmlclass;
    }


    /**
     * Returns the html id of the item.
     *
     * @return string	The html id of the item.
     */
    protected function getHtmlId()
    {
        return htmlspecialchars($this->id);
    }

    /**
     * Add a metadata to the item.
     *
     * @param string   $key
     * @param mixed    $value
     * @return $this
     */
    public function setMetadata($key, $value)
    {
        if (!isset($key)) {
            $this->metadata = $value;
        } else {
            $this->metadata[$key] = $value;
        }
        return $this;
    }

    /**
     * get metadata
     * key is optional, without key all metadata are returned as array
     * @param string	$key
     *
     * @return mixed
     */
    public function getMetadata($key = null)
    {
        if (null === $key) {
            return $this->metadata;
        } elseif (isset($this->metadata[$key])) {
            return $this->metadata[$key];
        } else {
            return null;
        }

    }

    /**
     * Set a confirmation message usable in canvas for the click event
     *
     * @param string $message
     * @return $this
     */
    public function setConfirmationMessage($message)
    {
        $this->setMetadata('confirmationMessage', $message);
        $this->addClass('widget-confirm');
        return $this;
    }


    /**
     * Sets the Widget_Action that will be called when the item has
     * to be refreshed.
     *
     * @param Widget_Action $reloadAction
     *
     * @return $this
     */
    public function setReloadAction(Widget_Action $reloadAction)
    {
        $this->delayedAction = $reloadAction;
        $this->setMetadata('delayedAction', $reloadAction->url());
        return $this;
    }


    /**
     * Returns the action that will be called when the delayed item has
     * to be refreshed.
     *
     * @return Widget_Action
     */
    public function getReloadAction()
    {
        return $this->delayedAction;
    }

    /**
     * @param string $funcIconClassname
     *
     * @return self
     */
    public function setIcon($funcIconClassname)
    {
        $this->addClass('icon', $funcIconClassname);
        return $this;
    }

    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas) {
        return '';
    }
}
