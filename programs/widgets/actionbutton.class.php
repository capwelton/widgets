<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

require_once dirname(__FILE__) . '/link.class.php';



/**
 * Constructs a Widget_ActionButton.
 *
 * @param 	string	[$text]	The link text.
 * @param	string	[$url]	The link url
 * @param 	string	[$id]	The item unique id.
 * @return Widget_ActionButton
 */
function Widget_ActionButton($text = '', $url = '', $id = null)
{
    return new Widget_ActionButton($text, $url, $id);
}

/**
 * A Widget_ActionButton.
 *
 */
class Widget_ActionButton extends Widget_Link
{

    /**
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-actionbutton';

        return $classes;
    }
}
