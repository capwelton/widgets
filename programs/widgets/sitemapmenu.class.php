<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__).'/widget.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/sitemap.php';

class Widget_SitemapMenu extends Widget_Widget implements Widget_Displayable_Interface
{
	/**
	 * Number of loaded levels
	 * @var int
	 */
	private $loaded_levels = 1;


	/**
	 * @var bab_siteMap
	 */
	private $sitemap;

	/**
	 * @var string
	 */
	private $sitemap_uid;

	/**
	 *
	 * @var string
	 */
	private $rootNode = null;


	/**
	 */
	public function __construct($id = null)
	{

		parent::__construct($id);

		// force site sitemap
		$this->sitemap = bab_siteMap::getSiteSitemap();

		require_once $GLOBALS['babInstallPath'].'utilit/settings.class.php';

		$settings = bab_getInstance('bab_Settings');
		/*@var $settings bab_Settings */
		$site = $settings->getSiteSettings();
		$this->sitemap_uid = $site['sitemap'];

		$this->setMetadata('maxdepth', 10);
		$this->setMetadata('loading', widget_translate('Loading...'));

	}

	/**
	 * Set the number of loaded levels
	 * @param	int	$num
	 * @return Widget_SitemapMenu
	 */
	public function setLoadedLevels($num)
	{
		$this->loaded_levels = $num;
		return $this;
	}


	/**
	 *
	 * @param int $depth
	 * @return Widget_SitemapMenu
	 */
	public function setMaxDepth($depth)
	{
		$this->setMetadata('maxdepth', $depth);
		return $this;
	}


	/**
	 * @param	string	$nodeId
	 */
	public function setRootNode($nodeId)
	{
		$this->rootNode = $nodeId;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getRootNode()
	{
		if (isset($this->rootNode))
		{
			return $this->rootNode;
		}

		return $this->sitemap->getVisibleRootNode();
	}





	/**
	 * (non-PHPdoc)
	 * @see Widget_InputWidget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-sitemap-menu';
		return $classes;
	}


	/**
	 *
	 * @param string 	$nodeId
	 * @param int 		$level		Number of level to load with this call
	 * @param array		$ulclasses
	 * @param bool		$dynamic_childnodes
	 * @return string
	 */
	private function levelHtml($nodeId, $level, $ulclasses = null, $dynamic_childnodes = true, $id = null)
	{
		// hack pour les noeuds des sous-arbres dynamiques (lien vers le noeud et ses enfants)
		list($nodeId) = explode('#', $nodeId);

		$rootnode = $this->sitemap->getLevelRootNode($nodeId);

		if ('core' === $this->sitemap_uid)
		{
			// the core sitemap cannot be sorted with a configuration
			$rootnode->sortChildNodes();
		}

		$parent = $rootnode->getNodeById($nodeId);
		if (!isset($parent))
		{
			$parent = $rootnode;
		}
		$node = $parent->firstChild();


		$html = '<ul';

		if (isset($id))
		{
			$html .= ' id="'.$this->getId().'"';
		}

		if (isset($ulclasses))
		{
			$html .= ' class="sitemap-menu-root '.bab_toHtml(implode(' ', $ulclasses)).'"';
		}

		$html .= '>';


		if (isset($node))
		{
			do {

				$sitemapItem = $node->getData();
				/*@var $sitemapItem bab_sitemapItem */

				$ul = null;
				$classes = array();

				if (($level - 1) > 0 && $sitemapItem->haschildnodes)
				{
					$ul = $this->levelHtml($sitemapItem->id_function, ($level - 1));

				} else if ($sitemapItem->haschildnodes && $dynamic_childnodes) {

					$classes[] = 'sitemap-ajax-'.$sitemapItem->id_function;
				}


				$html .= $sitemapItem->getHtmlListItem($ul, $classes);


			} while ($node = $node->nextSibling());
		}

		$html .= '</ul>';


		return $html;
	}



	/**
	 *
	 */
	public static function ajax($nodeId, $depth, $maxdepth)
	{
		$menu = new Widget_SitemapMenu;

		echo bab_convertStringFromDatabase(
			$menu->levelHtml($nodeId, 1, null, ($depth < $maxdepth)),
			'UTF-8'
		);

		die();
	}


    /**
     * {@inheritDoc}
     * @see Widget_Item::display()
     */
	public function display(Widget_Canvas $canvas)
	{
		$widgetsAddon = bab_getAddonInfosInstance('widgets');

		if (!$widgetsAddon) {
			return '';
		}

		return $this->levelHtml(
		    $this->getRootNode(),
		    $this->loaded_levels,
		    $this->getClasses(),
		    true,
		    $this->getId()
		) . $canvas->metadata($this->getId(), $this->getMetadata())
		. $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.sitemapmenu.jquery.js');
	}
}






bab_functionality::includefile('Ovml/Function');

/**
 *
 * <OFSitemapMenu [basenode="parentNode"] [maxdepth="depth"] [loadedlevels="1"]>
 *
 */
class Func_Ovml_Function_WidgetSitemapMenu extends Func_Ovml_Function
{
	/**
	 * @return string
	 */
	public function toString()
	{
		$W = bab_Widgets();
		$args = $this->args;
		$widget = new Widget_SitemapMenu;


		foreach( $args as $p => $v)
		{
			switch(mb_strtolower(trim($p)))
			{
				case 'basenode':
					$widget->setRootNode($v);
					break;

				case 'maxdepth':
					$widget->setMaxDepth((int) $v);
					break;

				case 'loadedlevels':
					$widget->setLoadedLevels((int) $v);
					break;
			}
		}

		return $widget->display($W->HtmlCanvas());
	}
}
