<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

require_once dirname(__FILE__) . '/containerwidget.class.php';
require_once dirname(__FILE__) . '/vboxlayout.class.php';


/**
 * Constructs a Widget_Accordions.
 *
 * @param string		$id			The item unique id.
 * @return Widget_Accordions
 */
function Widget_Accordions($id = null)
{
	return new Widget_Accordions($id);
}



/**
 * A Widget_Accordions.
 *
 */
class Widget_Accordions extends Widget_ContainerWidget implements Widget_Displayable_Interface
{

	private $labels = array();
	private $openPanel = false;
	
	private $collapsible = true;
	private $animated = false;
	
	/**
	 *
	 * @param string $id
	 */
	public function __construct($id = null)
	{
		parent::__construct($id, new Widget_VBoxLayout());
	}


	/**
	 * Adds a collapsable panel to the accordion widget
	 *
	 * @param string		$label			label part of the panel
	 * @param Widget_Item	$item			collapsabled part of the panel
	 * @param int			$position
	 * @return $this
	 */
	public function addPanel($label, Widget_Displayable_Interface $item, $position = null)
	{
		$this->addItem($item, $position);
		$this->labels[$item->getId()] = array('label' => $label, 'item' => $item);
		return $this;
	}


	/**
	 * Sets which panel is open.
	 *
	 * @param int          $index	The zero-based index of the panel that is active (open). A negative value selects panels going backward from the last panel.
	 * @return $this
	 */
	public function setOpenPanel($index)
	{
	    $this->openPanel = $index;
		return $this;
	}


	/**
	 * Returns which panel is open.
	 *
	 * @return string
	 */
	public function getOpenPanel()
	{
		return $this->openPanel;
	}

	
	/**
	 * Sets animated property of the accordion.
	 * if true, an animation is displayed when a panel is opened.
	 *
	 * @param bool	$active		Default: true
	 * @return $this
	 */
	public function setAnimated($active = true)
	{
		$this->animated = $active;
		return $this;
	}
	
	
	/**
	 * Returns the animated property of the accordion.
	 * if true, an animation is displayed when a panel is opened.
	 *
	 * @return bool
	 */
	public function isAnimated()
	{
		return $this->animated;
	}

	
	/**
	 * @see Widget_Accordions::isAnimated()
	 * @deprecated
	 * @return bool
	 */
	public function getAnimated()
	{
	    return $this->animated;
	}
	
	
	/**
	 * Sets or collapsible property of the accordion.
	 * if true, all the accordion panels can be closed at the same time.
	 * if false, one panel is always open.
	 *
	 * @param bool	$active		Default: true
	 * @return $this
	 */
	public function setCollapsible($active = true)
	{
		$this->collapsible = $active;
		return $this;
	}
	
	/**
	 * Returns the collapsible property of the accordion.
	 * if true, all the accordion panels can be closed at the same time.
	 * if false, one panel is always open.
	 *
	 * @return bool
	 */
	public function isCollapsible()
	{
		return $this->collapsible;
	}
	
	
	/**
	 * @see Widget_Accordions::isCollapsible()
	 * @deprecated
	 * @return bool
	 */
	public function getCollapsible()
	{
		return $this->isCollapsible();
	}


	/**
	 * (non-PHPdoc)
	 * @see Widget_Widget::getClasses()
	 */
	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-accordions';
		$classes[] = 'ui-accordion-container';
		return $classes;
	}



	public function display(Widget_Canvas $canvas)
	{
		$items = array();
		foreach ($this->labels as $labelItem) {
			$label = $labelItem['label'];
			$item = $labelItem['item'];
			$classes = array('widget-accordion-header');
			$items[] = $canvas->div(
				'',
			    array('widget-accordion-panel'),
				array(
					$canvas->span(
					    '',
					    $classes,
					    array(
					        $canvas->linkContainer(
					            '',
					            array(),
					            array($label),
					            '#'
					        )
					    )
					),
					$item
				)
			);
		}

		if ($this->isAnimated()) {
			$this->setMetadata('animated', true);
		}
		if (!$this->isCollapsible()) {
		    $this->setMetadata('collapsible', false);
		}
		if ($this->getOpenPanel() !== false) {
		    $this->setMetadata('openPanel', $this->getOpenPanel());
		}
        return $canvas->div(
            $this->getId(),
            $this->getClasses(),
            $items,
            $this->getCanvasOptions()
        ) . $canvas->metadata($this->getId(), $this->getMetadata());
	}
}
