<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/uploader.class.php';
include_once $GLOBALS['babInstallPath'].'utilit/path.class.php';
require_once $GLOBALS['babInstallPath'].'utilit/uploadincl.php';
require_once $GLOBALS['babInstallPath'].'utilit/inifileincl.php';
/**
 * Constructs a Widget_FilePicker.
 *
 * @param string		$id			The item unique id.
 * @return Widget_FilePicker
 */
function Widget_FilePicker($id = null)
{
    return new Widget_FilePicker($id);
}


/**
 * A Widget_FilePicker is a widget that let the user upload files.
 * by default, upload are commited to a temporary folder
 *
 * value of this input widget is an array containing the list of files in the folder
 *
 */
class Widget_FilePicker extends Widget_Uploader implements Widget_Displayable_Interface
{
    const PREFIX_LENGTH = 6;
    const BASE64 =  'BASE64';
    const QPRINT =  'QPRINT';
    const URLENC =  'URLENC';
    const NONE =    '______';

    /**
     * Folder used to save files
     * @var bab_Path
     */
    private $folder = null;


    /**
     * state of folder
     * true if the folder is a temporary directory
     * false if the folder is a custom directory
     *
     * @var bool
     */
    private $temporary = null;


    /**
     * Encoding method used to write files on filesystem
     * null value used to disable encoding
     *
     * @var string 		Widget_FilePicker::BASE64 | Widget_FilePicker::QPRINT | Widget_FilePicker::NONE
     */
    private $encoding = null;


    /**
     * An optional drop target associated to the file picker.
     * (Files dropped on this item will be uploaded as if they
     * had been selected with the filepicker).
     * @var Widget_Displayable_Interface
     */
    private $associatedDropTarget = null;


    /**
     * True to allow the selection of only one file.
     * @var bool
     */
    private $oneFileMode = false;

    protected $thumbWidth = 48;
    protected $thumbHeight = 48;

    /**
     * url to default image
     * @see Widget_ImagePicker::setDefaultImage()
     * @var string
     */
    protected $defaultImage = null;


    /**
     * max size of a file in bytes
     * @var int
     */
    private $maxsize = null;


    /**
     * options for filename of each file
     * @see self::setFileNameWidth()
     * @var Widget_CanvasOptions
     */
    private $fileNameCanvasOptions = null;


    /**
     * Delete temporary folder on load only if the value is a bab_Path
     * @var bool
     */
    private $clearTemporary = true;


    /**
     * Name generated from full path name, used in temporary path name
     * @var string
     */
    private $uid;


    /**
     * ImportFile used
     * @var bool
     */
    private $usedImportFile = false;


    /**
     * Disable clean up folder (if too much user it could triger memory limit or beeing too long, it should be done with kron table)
     * @var bool
     */
    private $disableCleanUp = false;





    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);

        // set temporary folder
        $addon = bab_getAddonInfosInstance('widgets');
        $this->folder = new bab_Path($addon->getUploadPath());
        $this->folder->push(__CLASS__);
        $this->folder->push(session_id());
        $this->temporary = true;

        // autodetect best encoding method
        $this->encoding = self::URLENC;
//         if (function_exists('quoted_printable_encode') || function_exists('imap_8bit')) {
//             $this->encoding = self::QPRINT;
//         } else {
//             $this->encoding = self::BASE64;
//         }

        $uploadMaxFilesize = bab_inifile_requirements::return_bytes(ini_get('upload_max_filesize'));
        $postMaxSize = bab_inifile_requirements::return_bytes(ini_get('post_max_size'));

        $this->setMaxSize(min(array($postMaxSize, $uploadMaxFilesize)));

        $this->setMetadata('selfpage', bab_getSelf());
        $this->setMetadata('title', widget_translate('Add file'));
        $this->setMetadata('loading', widget_translate('Loading...'));
        $this->setMetadata('msgerror', widget_translate('Error while uploading file'));
        $this->setMetadata('msgerror_delete', widget_translate('Error while deleting file'));
        $this->setMetadata('msgerror_duplicate', widget_translate('The file %s already exists, replace?'));
        $this->setMetadata('msgerror_toobig', widget_translate('The file is too big (%1$s kB). The maximum size is %2$s kB.'));
        $this->setMetadata('msgdelete', widget_translate('Do you really want to delete this file?'));
        $this->setMetadata('one_file_mode', false);
        $this->setMetadata('display_existing_files', true);
        $this->setMetadata('uploadOrDeleteUid', uniqid('uploadOrDelete', true));
        $this->setMetadata('thumbnailUid', uniqid('thumbnail', true));
        if (class_exists('bab_CsrfProtect')) {
            $this->setMetadata('babCsrfProtect', bab_getInstance('bab_CsrfProtect')->getToken());
        } else {
            $this->setMetadata('babCsrfProtect', '0');
        }

        $this->setMetadata('fileclasses', array('widget-filepicker-file'));

        if (bab_functionality::includefile('Icons')) {
            $this->addClass(Func_Icons::ICON_LEFT_16);
            $this->setMetadata('deleteclasses', array(Func_Icons::ACTIONS_EDIT_DELETE, 'icon', 'widget-filepicker-file-delete'));
            $this->setMetadata('loadingclasses', array('widget-layout-vbox-item', Func_Icons::STATUS_CONTENT_LOADING, 'icon', 'widget-filepicker-loading'));
        }
    }

    /**
     * Set widget name and use it as temporary folder
     *
     * @param	string|array	$name
     *
     * @see programs/widgets/Widget_Widget#setName($name)
     *
     * @return self
     */
    public function setName($name)
    {
        if ($this->temporary) {
            if (is_array($name)) {
                $this->folder->push($name[count($name)-1]);
            } else {
                $this->folder->push($name);
            }


        }

        parent::setName($name);

        return $this;
    }



    /**
     * Set Button label as metadata for javascript, the title is used as button label
     * @param	string	$title
     *
     * @return self
     */
    public function setButtonLabel($title)
    {
        $this->setMetadata('title', $title);
        return $this;
    }



    /**
     * Set title only
     * @param	string	$title
     *
     * @return self
     */
    public function setButtonTitle($title)
    {
        return parent::setTitle($title);
    }



    /**
     * Set title as metadata for javascript, the title is used as button label
     * @param	string	$title
     * @see programs/widgets/Widget_Item#setTitle($title)
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->setButtonLabel($title);
        return $this->setButtonTitle($title);
    }


    /**
     * Empty temporary folder on initialization
     * @return self
     */
    public function clearTemporary($value = true)
    {
        $this->clearTemporary = $value;
        return $this;
    }


    /**
     * Disable clean up folder (if too much user it could triger memory limit or beeing too long, it should be done with kron table)
     * @return self
     */
    public function disableCleanUp($value = true)
    {
        $this->disableCleanUp = $value;
        return $this;
    }




    /**
     * Do not display already uploaded files in widget
     *
     * @return self
     */
    public function hideFiles()
    {
        $this->setMetadata('display_existing_files', false);
        return $this;
    }

    /**
     * Set/unset the one-file mode.
     * If active, all files currently in the associated folder are deleted
     * before the new file is uploaded.
     *
     * @param $active		True to activate one-file mode, false to deactivate.
     * @return self
     */
    public function oneFileMode($active = true)
    {
        $this->oneFileMode = $active;
        $this->setMetadata('one_file_mode', $active);
        if ($active) {
            $this->setMetadata('title', widget_translate('Select file'));
        } else {
            $this->setMetadata('title', widget_translate('Add files'));
        }

        return $this;
    }


    /**
     *
     * @param int $width
     * @param string $unit
     * @return self
     */
    public function setFileNameWidth($width, $unit = 'px')
    {
        $this->setMetadata('filenamewidth', $width.$unit);

        $options = $this->Options();
        $this->fileNameCanvasOptions = $options;
        $options->width($width, $unit);

        $this->addClass('widget-filepicker-limitedwidth');

        return $this;
    }



    /**
     * set the path of folder where the files will be stored
     * the folder must be created before
     *
     * @see bab_Path::createDir()
     *
     * @param bab_Path $path
     * @return self
     */
    public function setFolder(bab_Path $path)
    {
        assert('$path->isAbsolute(); /* The "path" parameter must be an absolute path to folder */');

        $this->folder = $path;
        $this->temporary = false;

        return $this;
    }

    /**
     * Get the folder where the files are uploaded
     *
     * @return bab_Path
     */
    public function getFolder()
    {
        return $this->folder;
    }


    /**
     * Force encoding method or disable encoding
     *
     * <ul>
     * 	<li>Widget_FilePicker::QPRINT : quoted printable, require php 5.3.0 or the imap extension</li>
     *  <li>Widget_FilePicker::BASE64 : base 64, the most compatible</li>
     *  <li>null : disable encoding</li>
     * </ul>
     *
     * @param string $method		Widget_FilePicker::QPRINT | Widget_FilePicker::BASE64 | null
     * @return $this
     */
    public function setEncodingMethod($method)
    {
        $this->encoding = $method;
        return $this;
    }




    /**
     * Returns the value, try to autodetect value from folder.
     * If the value of the field is a bab_Path, the corresponding file is imported in the temporary folder.
     *
     * @return array
     */
    public function getValue()
    {
        $val = parent::getValue();

        $destPath = $this->getFolder();

        if ($this->clearTemporary && $this->temporary && !$this->usedImportFile) {

            $basePath = $this->getTemporaryBasePath($this->getNameString());
            if ($basePath->toString() !== $destPath->toString()) {
                try {
                    $destPath->deleteDir();
                } catch (bab_FolderAccessRightsException $e) { }
                $destPath->createDir();
            }
        }

        if ($val instanceof bab_Path && $val->isFile()) {

            $this->importFile($val, 'UTF-8');
        }
        $path = $this->folder->toString();


        if (!file_exists($path) || !is_dir($path)) {
            return array();
        }

        $returnvalue = array();
        $d = dir($path);
        while (false !== ($entry = $d->read())) {
            if (is_file($path.'/'.$entry)) {
                $returnvalue[] = self::decode($entry);
            }
        }

        $d->close();

        return $returnvalue;
    }



    /**
     * Unescape a string from filesystem and return readable value in database charset
     *
     * @param string $str
     * @return string
     */
    public static function decode($str)
    {
        // prefix is ascii on filesystem, mb_string is not needed
        $prefix = substr($str, 0, self::PREFIX_LENGTH);
        $value = substr($str, self::PREFIX_LENGTH);

        switch ($prefix) {
            case self::BASE64:
                $iPos = mb_strrpos($value, '.');
                if (false !== $iPos) {
                    $root = base64_decode(mb_substr($value, 0, $iPos));
                    $ext = mb_substr($value,$iPos);
                    return $root.$ext;
                }
                return base64_decode($value);

            case self::QPRINT:
                return quoted_printable_decode($value);

            case self::URLENC:
                return urldecode($value);

            case self::NONE:
                return $value;
        }

        return $str;
    }



    /**
     * Escape a string before writing into filesystem
     *
     * @param string $str
     * @param string $encoding		Widget_FilePicker::QPRINT | Widget_FilePicker::BASE64 | null
     *
     * @return string
     */
    private static function encode($str, $encoding)
    {
        // try to not encode extension

        switch($encoding) {
            case self::BASE64:

                $iPos = mb_strrpos($str, ".");
                if (false !== $iPos)
                {

                    $root = mb_substr($str, 0, $iPos);
                    $ext = mb_substr($str,$iPos);

                    if (preg_match('/[a-zA-Z0-9]+/', $ext)) {
                        return self::BASE64.base64_encode($root).$ext;
                    }
                }

                return self::BASE64.base64_encode($str);


            case self::QPRINT:

                if (function_exists('quoted_printable_encode')) {
                    $str = quoted_printable_encode($str);

                } elseif (function_exists('imap_8bit')) {

                    $str = imap_8bit($str);
                } else {
                    throw new Exception('the quoted printable encoding type need the php imap extension or php 5.3.0');
                }

                // imap_8bit add line break every 76 chars, we don't need that for files names

                $aLines = explode('='.chr(13).chr(10), $str);
                $str = implode('', $aLines);

                return self::QPRINT.$str;

            case self::URLENC:
                $str = urlencode($str);
                return self::URLENC . $str;

            case self::NONE:
                return self::NONE.$str;
        }

        return $str;
    }


    /**
     * Push a folder name into the associated folder and encode it
     * @param	string	$name
     * @return self
     */
    public function push($name)
    {
        $this->folder->push(self::encode($name, $this->encoding));
        return $this;
    }


    /**
     * Set maximum size for one uploaded file
     * @param	int		$size		bytes
     * @return self
     */
    public function setMaxSize($size)
    {
        $this->maxsize = $size;
        $this->setMetadata('maxsize', $this->maxsize);
        return $this;
    }




    /**
     * Defines an optional drop target associated to the file picker.
     * (Files dropped on this item will be uploaded as if they
     * had been selected with the filepicker).
     *
     * @param widget_Item $item
     * @return self
     */
    public function setAssociatedDropTarget(Widget_Displayable_Interface $item)
    {
        $this->associatedDropTarget = $item;

        $this->setMetadata('droptarget', $this->associatedDropTarget->getId());
        $this->associatedDropTarget->addClass('widget-filepicker-drop-target');
        $this->associatedDropTarget->addAttribute('data-droptext', widget_translate('Drop files here...'));
        $this->associatedDropTarget->setMetadata('filepicker', $this->getId());

        return $this;
    }


    /**
     * @see Widget_Uploader::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-filepicker';

        if ($this->oneFileMode) {
            $classes[] = 'widget-filepicker-onefilemode';
        }

        return $classes;
    }




    /**
     * Set informations in session to upload file(s) in next page
     *
     * @see Func_Widgets::onBeforePageCreated()
     *
     * @return void
     */
    private function addNextPageJob()
    {
        include_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = bab_getInstance('bab_Session');

        if (isset($session->addon_widgets)) {
            $session_addon_widgets = $session->addon_widgets;
        } else {
            $session_addon_widgets = array('__CLASS__' => array());
        }

        // remove previous jobs
        unset($session_addon_widgets[__CLASS__][$this->getMetadata('uploadOrDeleteUid')]);
        unset($session_addon_widgets[__CLASS__][$this->getMetadata('thumbnailUid')]);


        $session_addon_widgets[__CLASS__][$this->getMetadata('uploadOrDeleteUid')] = array(

            'method' => 'uploadOrDelete',
            'params' => array(
                $this->getId(),
                $this->folder->toString(),
                $this->encoding,
                $this->getMetadata('one_file_mode'),
                $this->getAcceptedMimeTypes(),
                $this->maxsize
            ),
            'name' => $this->getName()
        );


        $session_addon_widgets[__CLASS__][$this->getMetadata('thumbnailUid')] = array(

            'method' => 'getThumbnail',
            'params' => array(
                $this->folder->toString(),
                $this->encoding,
                $this->thumbWidth,
                $this->thumbHeight
            )
        );


        $serName = $this->getName();
        if (is_array($serName)) {
            $serName = implode('/', $serName);
        }

        $session_addon_widgets[__CLASS__]['folderByName'][$serName] = $this->folder->toString();
        $session->addon_widgets = $session_addon_widgets;
    }


    /**
     * Get folder by name if available in session
     * @param string $name
     * @return bab_Path
     */
    protected function getFolderByName($name)
    {


        include_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = bab_getInstance('bab_Session');

        if (!isset($session->addon_widgets)) {
            return null;
        }

        $session_addon_widgets = $session->addon_widgets;

        if (!isset($session_addon_widgets[__CLASS__]['folderByName'][$name])) {
            return null;
        }

        $path = $session_addon_widgets[__CLASS__]['folderByName'][$name];
        return new bab_Path($path);
    }


    /**
     * Upload files to folder; delete file from folder, do not call directly
     *
     * @param	string		$widget_id
     * @param 	string	 	$path
     * @param	string		$encoding			encoding method
     * @param	bool		$one_file_mode
     * @param	array		$acceptedMimeTypes
     * @param	int			$maxsize
     *
     * @return void
     */
    public static function uploadOrDelete($widget_id, $path, $encoding, $one_file_mode, $acceptedMimeTypes, $maxsize)
    {
        $folder = new bab_Path($path);


        // bug vu sur IIS :
        // <b>Warning</b>:  Unknown: 1 result set(s) not freed. Use mysql_free_result to free result sets which were requested using mysql_query() in <b>Unknown</b> on line <b>0</b><br />

        ini_set('mysql.trace_mode', 'Off');

        try {
            if (!isset($_FILES[$widget_id])) {
                // no file uploaded

                if ($delete = bab_rp('deletefile')) {
                    // remove file if exists

                    $delete = bab_getStringAccordingToDataBase($delete, 'UTF-8');

                    $iterator = new Widget_FilePickerIterator($folder);
                    foreach($iterator as $file) {

                        if ($file->toString() === $delete) {
                            try {
                                //$file->delete(); // need 7.8.92
                                unlink($file->getFilePath()->toString());
                                die('OK');

                            } catch(Exception $e) {
                                die($e->getMessage());
                            }
                        }
                    }

                    die(widget_translate('The file does not exists'));

                } else if($fileurl = bab_rp('addFileFromUrl')) {

                    $filename = basename($fileurl);

                    // try to get extension
                    $ext = '';
                    $iPos = mb_strrpos($filename, '.');
                    if (false !== $iPos) {
                        $ext = mb_substr($filename, $iPos);
                        if (strlen($ext) > 5) {
                            $ext = '';
                        }
                    }

                    // hack for long filenames and unsignificant url
                    if (strlen($filename) > 25) {
                        $filename = parse_url($fileurl, PHP_URL_HOST);
                        $iPos1 = mb_strrpos($filename, '.');
                        if (false !== $iPos1) {
                            $filename = mb_substr($filename, 0, ($iPos1));
                            if (false === $iPos2 = mb_strrpos($filename, '.')) {
                                $iPos2 = 0;
                            }
                            $filename = mb_substr($filename, $iPos2+1);
                        }

                        $filename .= $ext;
                    }


                    if ($folder->isDir()) {
                        if ($one_file_mode) {
                            // We remove the folder content if in one file mode
                            foreach ($folder as $file) {
                                //$file->delete(); // need 7.8.92
                                unlink($file->toString());
                            }
                        } else {

                            // Check if the filename is already present in destination
                            // folder and change the name if it is the case.
                            $arr = array();
                            $folder->createDir();
                            $I = new Widget_FilePickerIterator($folder);
                            foreach ($I as $currentfile) {
                                $arr[$currentfile->toString()] = true;
                            }

                            while (isset($arr[$filename])) {
                                $filename = '1-'.$filename;
                            }

                        }
                    } else {
                        if ($folder->createDir() === false) {
                            $errorMessage = sprintf(widget_translate('Error while creating destination folder %s'), $path);
                            die($errorMessage);
                        }
                    }



                    $dest = clone $folder;

                    $dest->push(self::encode($filename, $encoding));

                    // set user_agent of php the same as user, hack for facebook or similar sites filtered by browsers
                    ini_set('user_agent', $_SERVER['HTTP_USER_AGENT']);

                    if (!@copy($fileurl, $dest->toString())) {
                        die(widget_translate("The file can't be downloaded from the internet to the web server"));
                    }

                    die('OK'.$filename);

                } else {
                    return;
                }
            }


            $testencoding = bab_pp('testencoding');
            if ($folder->isDir()) {
                if ($one_file_mode) {
                    // We remove the folder content if in one file mode
                    foreach ($folder as $file) {
                        /*@var $file bab_Path */
                        //$file->delete(); // need 7.8.92
                        unlink($file->toString());
                    }
                }
            } else {
                if ($folder->createDir() === false) {
                    $errorMessage = sprintf(widget_translate('Error while creating destination folder %s'), $folder->toString());
                    throw new Exception($errorMessage);
                }
            }

            if (isset($_FILES[$widget_id]['name']) && is_array($_FILES[$widget_id]['name'])) {

                $files = array();
                foreach($_FILES[$widget_id] as $key => $property) {
                    foreach($property as $filenumber => $value) {

                        if ('ajax' === bab_rp('request_mode')) {
                            if($testencoding != '&#9820;'){
                                $value = bab_getStringAccordingToDataBase($value, 'UTF-8');
                            }
                        }

                        $files[$filenumber][$key] = $value;
                    }
                }

                foreach($files as $uploadinfo) {
                    self::upload($folder, $uploadinfo, $encoding, $acceptedMimeTypes, $maxsize);
                }

            } else {

                if ('ajax' === bab_rp('request_mode')) {
                    if($testencoding != '&#9820;'){
                        $_FILES[$widget_id] = bab_getStringAccordingToDataBase($_FILES[$widget_id], 'UTF-8');
                    }
                }

                self::upload($folder, $_FILES[$widget_id], $encoding, $acceptedMimeTypes, $maxsize);
            }

        } catch(Exception $e) {

            if ('ajax' === bab_rp('request_mode')) {
                die($e->getMessage());
            }

            // trigger_error($e->getMessage());
        }

        if ('ajax' === bab_rp('request_mode')) {
            die('OK');
        }
    }




    /**
     * Upload one file
     * @param 	bab_Path 	$folder
     * @param 	array 		$uploadinfo
     * @param	string		$encoding
     * @param	array		$acceptedMimeTypes
     * @param	int			$maxsize			Kb for one file
     * @return void
     */
    private static function upload($folder, $uploadinfo, $encoding, $acceptedMimeTypes, $maxsize)
    {
        require_once $GLOBALS['babInstallPath'].'utilit/uploadincl.php';
        $upload = bab_fileHandler::upload($uploadinfo);

        if ($upload->error && $upload->code !== UPLOAD_ERR_NO_FILE) {
            throw new Exception($upload->error);
        }

        if (0 !== count($acceptedMimeTypes)) {
            if (!in_array($upload->mime, $acceptedMimeTypes)) {
                $message = widget_translate('This type of file is not allowed') . ' (' . $upload->mime . ')';
                throw new Exception($message);
            }
        }

        if (null !== $maxsize) {
            if ($maxsize < $upload->size)
            {
                throw new Exception(sprintf(widget_translate('The file %s (%01.2f Kb) is bigger than the maximum allowed to upload (%d Kb)'), $upload->filename, $upload->size / 1024, round($maxsize / 1024)));
            }
        }


        $destination = clone $folder;

        $destination->push(self::encode($upload->filename, $encoding));
        $upload->import($destination->toString());

        unset($destination);
    }




    public static function getThumbnail($path, $encoding, $width, $height)
    {

        // bug vu sur IIS :
        // <b>Warning</b>:  Unknown: 1 result set(s) not freed. Use mysql_free_result to free result sets which were requested using mysql_query() in <b>Unknown</b> on line <b>0</b><br />

        ini_set('mysql.trace_mode', 'Off');




        if (!isset($_GET['thumbfile']) ) {

            return null;
        }


        $url = self::getImageUrl(new bab_Path($path), bab_rp('thumbfile'), $encoding, $width, $height);
        if (null === $url) {
            die('OK');
        }

        die($url);
    }




    /**
     * Remove expired temporary files
     * @return void
     */
    private function cleanupExpired()
    {
        if (!$this->disableCleanUp) {
            $sessions = array();
            foreach (bab_getActiveSessions() as $arr) {
                $sessions[$arr['session_id']] = 1;
            }

            $addon = bab_getAddonInfosInstance('widgets');
            $folder = new bab_Path($addon->getUploadPath());
            $folder->push(__CLASS__);
            $path = $folder->toString();

            if (!is_dir($path)) {
                return;
            }

            $d = dir($path);
            while (false !== ($entry = $d->read())) {
                if (!isset($sessions[$entry]) && '.' !== $entry && '..' !== $entry) {
                    $expired = new bab_Path($path, $entry);

                    if (!method_exists($expired, 'deleteDir')) {
                        bab_debug('ovidentia is too old, no cleanup');
                        return;
                    }

                    try {
                        $expired->deleteDir();
                    } catch(Exception $e) { }
                }
            }
        }
    }



    /**
     * Remove temporary directory for session.
     * @return void
     */
    public static function cleanup()
    {
        $addon = bab_getAddonInfosInstance('widgets');
        $folder = new bab_Path($addon->getUploadPath());
        $folder->push(__CLASS__);
        $folder->push(session_id());

        try {
            $folder->deleteDir();
        } catch(Exception $e) {
            // already clean
        }

        include_once $GLOBALS['babInstallPath'].'utilit/session.class.php';
        $session = bab_getInstance('bab_Session');

        if (isset($session->addon_widgets) && isset($session->addon_widgets[__CLASS__])) {
            $session_addon_widgets = $session->addon_widgets;
            unset($session_addon_widgets[__CLASS__]);
            $session->addon_widgets = $session_addon_widgets;
        }
    }


    public function getTemporaryBasePath($name)
    {
        $addon = bab_getAddonInfosInstance('widgets');
        $folder = new bab_Path($addon->getUploadPath());
        $folder->push(__CLASS__);
        $folder->push(session_id());

        $folder->push($name);
        return $folder;
    }


    /**
     * Get temporary files folder full path
     *
     * @param	string	$name		form field name used in widget
     *
     * @return bab_Path
     */
    public function getTemporaryPath($name = null)
    {
        if (!isset($name)) {
            $name = $this->getNameString();
        }

        if ($tmp = $this->getFolderByName($name)) {
            return $tmp;
        }

        return $this->getTemporaryBasePath($name);
    }

    /**
     * Get temporary files full path by widget name
     *
     * @param	string	$name		form field name used in widget
     *
     * @return Widget_FilePickerIterator
     */
    public function getTemporaryFiles($name = null)
    {
        $folder = $this->getTemporaryPath($name);
        return $this->getFolderFiles($folder);
    }


    /**
     * Get files in path
     * @param bab_Path $path
     * @return Widget_FilePickerIterator | null
     */
    public function getFolderFiles(bab_Path $path)
    {
        if (!is_dir($path->toString())) {
            // bab_debug('no folder : '.$path->toString(), DBG_TRACE, __CLASS__);
            return null;
        }

        return new Widget_FilePickerIterator($path);
    }


    /**
     * Set a custom folder UID for temporary folder
     * The method can be used to maintain a same upload temporary folder accross multiples file pickers
     *
     *
     * @param string $uid   An unique ID must be generated for this parameter
     */
    public function setFolderUid($uid)
    {
        $this->uid = $uid;
        return this;
    }


    /**
     * Import and encode a copy or an upload of a file into current folder
     * @param	bab_Path 	$path				full path name of file to import
     * @param	string		$source_encoding	filename encoding
     * @param	string		$filename			overwrite filename of target, default is set from source path (set in charset $source_encoding)
     * @return	bool
     */
    public function importFile(bab_Path $path, $source_encoding, $filename = null)
    {
        if (null === $filename) {
            $filename = basename($path->toString());
        } else {
            $filename = self::encode(bab_getStringAccordingToDataBase($filename, $source_encoding), $this->encoding);
        }

        // before importFile, the widget must be in the widget tree a the form
        // because the uid added to temporary path will be generated from the full path name
        if ($this->temporary) {
            $this->addFolderUid();
        }

        if ($this->oneFileMode) {
            try {
                $this->folder->deleteDir();
            } catch (Exception $e) { }
        }
        $this->folder->createDir();
        $dest = clone $this->folder;
        $dest->push($filename);

        $this->usedImportFile = true;

        if (is_uploaded_file($path->toString())) {
            return move_uploaded_file($path->toString(), $dest->toString());
        }

        $cp = copy($path->toString(), $dest->toString());

        return $cp;
    }


    /**
     * Import and encode a copy or an upload of a file into current folder
     *
     * @since 1.0.79
     * @param  string   $data               the binary data
     * @param  string   $filename           overwrite filename of target, default is set from source path (set in charset $source_encoding)
     * @return	bool
     */
    public function importFileData($data, $filename)
    {
        // before importFile, the widget must be in the widget tree a the form
        // because the uid added to temporary path will be generated from the full path name
        if ($this->temporary) {
            $this->addFolderUid();
        }

        if ($this->oneFileMode) {
            try {
                $this->folder->deleteDir();
            } catch (Exception $e) { }
        }
        $this->folder->createDir();
        $dest = clone $this->folder;
        $dest->push($filename);

        $this->usedImportFile = true;

        $cp = file_put_contents($dest->toString(), $data);

        return $cp;
    }


    /**
     * Import and encode a copy of all files and folders into current folder
     * @param bab_Path 	$src					source folder path
     * @param string 	$source_encoding		filenames and foldernames encoding
     * @return void
     */
    public function importPath(bab_Path $src, $source_encoding)
    {
        foreach($src as $tmpFile)
        {

            if ($tmpFile->isDir())
            {
                $fp = clone $this;
                $fp->folder = clone $this->getFolder();

                $fp->folder->push(self::encode(bab_getStringAccordingToDataBase($tmpFile->getBasename(), $source_encoding), $this->encoding));
                $fp->importPath($tmpFile, $source_encoding);

            } else {
                $this->importFile($tmpFile, $source_encoding);
            }
        }
    }




    /**
     * open a file in the filepicker folder with fopen
     *
     * @param string $filename
     * @param string $source_encoding	filename encoding
     * @param string $mode				fopen mode 'r' | 'w' | 'a' | 'rb'
     *
     * @return resource				a file pointer resource on success, or false on error.
     */
    public function openFile($filename, $source_encoding, $mode)
    {
        $this->folder->createDir();

        $dest = clone $this->folder;
        $dest->push(self::encode(bab_getStringAccordingToDataBase($filename, $source_encoding), $this->encoding));

        return fopen($dest->toString(), $mode);
    }



    /**
     * Get by encoded filename
     *
     * @param string $encoded_filename
     * @return Widget_FilePickerItem
     */
    public function getByName(bab_Path $path, $encoded_filename) {

        return new Widget_FilePickerItem($path, $encoded_filename);
    }

    /**
     * get image url
     * try to use thumbnailer if available or use default icon
     *
     * @param	bab_Path	$folder		folder
     * @param 	string 		$filename	not encoded filename
     * @param	string		$encoding
     * @param	int			$width
     * @param	int			$height
     *
     * @return string
     */
    private static function getImageUrl(bab_Path $folder, $filename, $encoding, $width, $height)
    {
        $T = @bab_functionality::get('Thumbnailer', false);

        if ($T) {


            /*@var $T Func_Thumbnailer */

            $path = clone $folder;
            $path->push(self::encode($filename, $encoding));
            if (!$path->isFile()) {//FIX CHANGING OF DEFAULT ENCODING METHOD TO URL
                $path->pop();
                $path->push(self::encode($filename, Widget_FilePicker::QPRINT));
            }
            $filepath = $path->toString();

            $icon = $T->getIcon($filepath, $width, $height);

            if (is_object($icon)) {
                return $icon->__toString().'?'.time();
            }

            return $icon.'?'.time();
        }

        $addon = bab_getAddonInfosInstance('widgets');

        if ($height >= 48)
        {
            return $addon->getImagesPath().'48x48/application-octet-stream.png';
        }

        if ($height >= 32)
        {
            return $addon->getImagesPath().'32x32/application-octet-stream.png';
        }

        return $addon->getImagesPath().'16x16/application-octet-stream.png';
    }

    /**
     * @param	string	$filename
     * @return string
     */
    private static function getFileDescription($filename)
    {
        $F = bab_functionality::get('FileInfos');

        if ($F) {
            $mimetype = $F->getMimeTypeFromExtension($filename);
            return $F->getFileTypeFromMimeType($mimetype);
        }

        return '';

    }


    /**
     * got to this location after upload
     * @param	Widget_Action	$action
     * @return	self
     */
    public function onUploadAction(Widget_Action $action)
    {
        $this->setMetadata('uploadAction', $action->url());
        $this->setMetadata('uploadActionAjax', $action->isAjax());
        return $this;
    }



    /**
     * Specifies an action that will be called asynchronously on the server (ajax). If the action
     * succeeds, the closest delayedItem of the page containing $reloadItem will be refreshed
     * (or the whole page if there is none).
     *
     * @deprecated use setAjaxAction() instead.
     *
     * @param Widget_Action $action
     * @param mixed   $reloadItem
     * @return self
     */
    public function onUploadAjaxAction(Widget_Action $action, $reloadItem = null)
    {
        return $this->setAjaxAction($action, $reloadItem);
    }


    /**
     * Execute this javascript function after upload
     *
     * @param  string   $jsFunction     The name of the javascript method to call.
     * @param  string   $domain         The javascript object on which the method is called.
     * @return	self
     */
    public function onUpload($jsFunction, $domain = 'window.babAddonWidgets')
    {
        $arr = $this->getMetadata('uploadJs');
        $arr[] = $domain.'.'.$jsFunction;

        $this->setMetadata('uploadJs', $arr);
        return $this;
    }



    /**
     * Add a unique id genereated from full path name to the temporaary folder
     * do nothing if allready done
     */
    protected function addFolderUid()
    {
        if (!isset($this->uid)) {

            $this->uid = 'tmp'.md5(implode('/', $this->getFullName()));

            // formSubmitUid is a hidden field in form,
            // the value is created on each form creation
            // if the form is persistent, the hidden field value should be persisted

            if ($form = $this->getForm()) {
                if ($formSubmitUid = $form->getHiddenValue('_formSubmitUid')) {
                    $this->uid .= '.'.$formSubmitUid;
                }
            }

            if ($formSubmitUid = bab_rp('formSubmitUid')) {
                // no form in ancestors, we are in an ajax frame, the UID is given in url
                $this->uid .= '.'.$formSubmitUid;
            }

            $this->getFolder()->push($this->uid);
        }
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Uploader::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        if (!$widgetsAddon || !$widgetsAddon->isAccessValid()) {
            throw new Exception('The widget addon need to be installed to perform an upload with the file picker widget');
        }

        $arrNames = $this->getFullName();
        $this->setMetadata('fullname', $canvas->getHtmlName($arrNames));

        $this->setMetadata('waitUploadErrorMessage', widget_translate('Please, wait for all files to finish uploading.'));

        $this->setMetadata('thumbWidth', $this->thumbWidth);
        $this->setMetadata('thumbHeight', $this->thumbHeight);

        // create a frame with hidden fields for the values to be in form submit
        $d = array();
        $h = array();
        //$h[] = $canvas->hidden(null, null, $arrNames, '');

        $tmpUidField = null;


        if ($this->temporary) {
            $this->addFolderUid();
            /*
            bab_debug('Temporary folder: '.bab_toHtml($this->folder->tostring()).'<br />'
                .'Full name: /'.bab_toHtml(implode('/', $this->getFullname())), DBG_TRACE, __CLASS__);
            */
            $arrNames = $this->getFullName();
            $uidNames = $arrNames;
            $uidNames[] = 'uid';

            $tmpUidField = $canvas->hidden(null, null, $uidNames, $this->uid);
        }

        $this->addNextPageJob();
        $this->cleanupExpired();



        if (isset($this->defaultImage)) {
            $d[] = $canvas->image(null, array('widget-filepicker-default-image'), '', $this->defaultImage);
        }

        foreach ($this->getValue() as $filename) {

            $hArrName = $arrNames;
            $hArrName[] = 'files';
            $hArrName[] = '';



            if ($this->getMetaData('display_existing_files')) {

                $items = array();

                $downloadUrl = '?tg=addon/widgets/files&idx=download&uid=' . $this->getMetadata('uploadOrDeleteUid') . '&file=' . bab_toHtml($filename);

                if ($imageurl = self::getImageUrl($this->folder, $filename, $this->encoding, $this->thumbWidth, $this->thumbHeight)) {
                    $canvasImage = $canvas->image('', array(), self::getFileDescription($filename), $imageurl);
                    $imageDownloadLink = $canvas->linkContainer('', array(), array($canvasImage), $downloadUrl);
                    $items[] = $canvas->div(
                        null,
                        array('widget-layout-vbox-item'),
                        array($imageDownloadLink)
                    );
                }

                $labelDownloadLink = $canvas->linkContainer('', array(), array($filename), $downloadUrl);
                $items[] = $canvas->span('', array(), array($labelDownloadLink), $this->fileNameCanvasOptions, $filename);
                if (!$this->isDisplayMode()) {
                    $items[] = $canvas->span('', $this->getMetadata('deleteclasses'), array(''), $this->fileNameCanvasOptions, $filename);
                }

                $d[] = $canvas->div('', $this->getMetadata('fileclasses'), $items);
            }

            $h[] = $canvas->hidden('', array(), $hArrName, $filename);
        }




        $displayframe = $canvas->div('', array('widget-filepicker-files'), $d);
        $valuesframe = $canvas->div('', array('widget-filepicker-values'), $h);

        $name = $this->getName();
        if (is_array($name)) {
            $name = array_pop($name);
        }
        $multiple = !$this->oneFileMode && '[]' === mb_substr($name, -2);

        //MOVE to the setAssociated... method
        /*if (isset($this->associatedDropTarget)) {
            $this->setMetadata('droptarget', $this->associatedDropTarget->getId());
            $this->associatedDropTarget->addClass('widget-filepicker-drop-target');
            $this->associatedDropTarget->setMetadata('filepicker', $this->getId());
        }*/

        // default uploader if no javascript

        $uploader = $canvas->fileUpload('', array(), array($this->getId()), $this->getAcceptedMimeTypes(),$this->getSize(), $multiple);

        if ($this->isDisplayMode()) {
            return $canvas->div(
                $this->getId(),
                $this->getClasses(),
                array(
                    $displayframe
                )
            )
            .$canvas->metadata($this->getId(), $this->getMetadata());
        }



        return $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array(
                $tmpUidField,
                $valuesframe,
                $uploader,
                $displayframe
            ),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
        )
        .$canvas->hidden('', array(), array('widget_filepicker_job_uid', ''), $this->getMetadata('uploadOrDeleteUid'))
        .$canvas->metadata($this->getId(), $this->getMetadata())
        .$canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.filepicker.jquery.js');
    }
}




/**
 * File uploaded with a FilePicker widget
 *
 */
class Widget_FilePickerItem
{
    private $path = null;
    private $encoded_file_name = null;


    /**
     * @param bab_Path $path
     * @param string $encoded_file_name
     */
    public function __construct(bab_Path $path, $encoded_file_name)
    {
        $this->path = $path;
        $this->encoded_file_name = $encoded_file_name;
    }

    /**
     * Get original filename
     *
     * @return string
     */
    public function toString()
    {
        return Widget_FilePicker::decode($this->encoded_file_name);
    }

    /**
     *
     * @return bab_Path
     */
    public function getFilePath()
    {
        $filepath = clone $this->path;
        $filepath->push($this->encoded_file_name);

        return $filepath;
    }

    /**
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->encoded_file_name;
    }

    /**
     *
     * @return bool
     */
    public function isDir()
    {
        $sFullPathName = $this->getFilePath()->toString();
        return @is_dir($sFullPathName);
    }

    /**
     * Download the file to user
     *
     * @param	bool	$inline
     *
     * @return mixed
     */
    public function download($inline = true)
    {
        $sFullPathName = $this->getFilePath()->toString();

        $fp = fopen($sFullPathName, 'rb');
        if ($fp)
        {
            bab_setTimeLimit(3600);

            if (mb_strtolower(bab_browserAgent()) == 'msie') {
                header('Cache-Control: public');
            }

            if ($inline) {
                header('Content-Disposition: inline; filename="'.$this->toString().'"'."\n");
            } else {
                header('Content-Disposition: attachment; filename="'.$this->toString().'"'."\n");
            }

            $mime = bab_getFileMimeType($sFullPathName);
            $fsize = filesize($sFullPathName);
            header('Content-Type: '.$mime."\n");
            header('Content-Length: '.$fsize."\n");
            header('Content-transfert-encoding: binary'."\n");

            while (!feof($fp)) {
                print fread($fp, 8192);
            }
            fclose($fp);
            exit;
        }
    }

    /**
     * Delete file if exists
     * @throw Exception
     *
     * @return bool
     */
    public function delete()
    {
        $sFullPathName = $this->getFilePath()->toString();

        if (!file_exists($sFullPathName)) {
            $message = widget_translate('The file does not exists');
            throw new Exception($message);
        }

        if (!is_writable($sFullPathName)) {
            $message = widget_translate('The file is not writable');
            throw new Exception($message);
        }

        if (!@unlink($sFullPathName)) {
            $message = widget_translate('Error while deleting the file');
            throw new Exception($message);
        }

        return true;
    }
}






/**
 * CSV result iterator
 * to browse the content of the uploaded CSV file
 */
class Widget_FilePickerIterator implements Iterator
{
    private $path 				= null;
    private $res				= null;
    private $key				= null;

    /**
     * Constructor
     *
     * @param	bab_Path	$path			folder to read
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    public function rewind()
    {
        $this->res = dir($this->path->toString());
        $this->next();
    }


    /**
     *
     * @return Widget_FilePickerItem
     */
    public function current()
    {
        return new Widget_FilePickerItem($this->path, $this->key);
    }

    public function key()
    {
        return $this->key;
    }

    public function next()
    {
        do {
            $entry = $this->res->read();
        } while (false !== $entry && ('.' === $entry || '..' === $entry));

        $this->key = $entry;
    }

    /**
     *
     * @return bool
     */
    public function valid()
    {
        return (isset($this->key) && false !== $this->key);
    }
}
