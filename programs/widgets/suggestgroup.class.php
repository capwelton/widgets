<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';
require_once dirname(__FILE__) . '/suggestlineedit.class.php';






/**
 * Constructs a Widget_SuggestGroup.
 *
 * @param string		$id			The item unique id.
 * @return Widget_SuggestGroup
 */
function Widget_SuggestGroup($id = null)
{
	return new Widget_SuggestGroup($id);
}


/**
 * A Widget_SuggestGroup is a widget that let the group found a group with entry. 
 * Propose group suggestions based on database
 */
class Widget_SuggestGroup extends Widget_SuggestLineEdit implements Widget_Displayable_Interface 
{

	private $listGroup;

	/**
	 * @param string $id			The item unique id.
	 * @return Widget_LineEdit
	 */
	public function __construct($id = null)
	{
		parent::__construct($id);
		$listGroup = bab_getGroups();

		if (false !== $keyword = $this->getSearchKeyword()) {
			
			$i = 0;
			foreach($listGroup['id'] as $groupID) {
				$groupName = bab_getGroupName($groupID,false);
				if ( stripos($groupName,$keyword) !== false ){
					$i++;
					if ($i > Widget_SuggestLineEdit::MAX) {
						break;
					}
					$groupPathName = bab_getGroupName($groupID);
					if ( $groupPathName != $groupName ){
						$groupPathName = substr( ( $groupPathName ), 0 , -(strlen($groupName)) );
						$this->addSuggestion($groupID, $groupName." - ".$groupID, $groupPathName);
					}else{
						$this->addSuggestion($groupID, $groupName." - ".$groupID);
					}
				}
			}

			$this->sendSuggestions();
		}
	}

	public function getClasses()
	{
		$classes = parent::getClasses();
		$classes[] = 'widget-suggestgroup';
		return $classes;
	}

}