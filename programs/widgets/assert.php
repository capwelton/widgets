<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
// 
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';


assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_BAIL, 0);
assert_options(ASSERT_QUIET_EVAL, 1);
assert_options(ASSERT_CALLBACK, 'Widget_assertHandler');

/**
 * Assertion handler function.
 * This function is called whenever an assertion fails.
 */
function Widget_assertHandler($file, $line, $code)
{
	// If there is a php comment in the assert code, we use it as an assert message.
	$matches = array();
	preg_match('/\* (.*) \*/', $code, $matches);
	if (isset($matches[1])) {
		$errorMessage = $matches[1];
	} else {
		$errorMessage = '';
	} 
	echo("Assertion Failed: <strong>$errorMessage</strong>");
	//echo("<ul><li>File '$file'</li><li>Line '$line'</li><li>Code '$code'</li></ul>");
	echo '<pre>' . Widget_getBacktrace(true, 3) . '</pre>';
}



function Widget_getParentsClasses($obj, $str = '') {
	$parent = get_parent_class($obj);
	if (false !== $parent) {
		if (empty($str)) {
			$str = $parent;
		} else {
			$str = $parent.' -> '.$str;
		}
		$str = Widget_getParentsClasses($parent, $str);
	}
	
	return $str;
}



/**
 * Returns a formatted backtrace
 * Need error_reporting set with E_NOTICE
 * @param	boolean		$echo		Display or send to Widget_debug
 * @param	int			$nbShifts	Number of element to skip at top of backtrace
 */
function Widget_getBacktrace($echo = false, $nbShifts = 1)
{
    
    // Get backtrace
    $backtrace = debug_backtrace();

    // Unset call to debug_print_backtrace
	while ($nbShifts--) {
    	array_shift($backtrace);
	}
    
    // Iterate backtrace
    $calls = array();
    foreach ($backtrace as $i => $call) {
    	if (isset($call['file']) && isset($call['line'])) {
    		$filePath = explode('/', $call['file']);
    		$phpFile = array_pop($filePath);
        	$location = implode('/', $filePath) . '/<b>' . $phpFile . '</b> : line ' . $call['line'];
    	} else {
        	$location = '';
        }
        $function = (isset($call['class'])) ?
            '<b>' . $call['class'] . '</b>::<b>' . $call['function'] . '</b>':
            '<b>' . $call['function'] . '</b>';
       
        $params = '';
        if (isset($call['args'])) {
			$nbParam = 0;
			foreach ($call['args'] as $param)
			{
				if (is_string($param) || is_numeric($param)) {
					$param_str = (string) $param;
				} elseif (is_array($param)) {
					$param_str = sprintf('%d element(s)', (string) count($param));
				} elseif (is_object($param)) {
					$vars = get_object_vars($param);
					$param_str = 'public properties :'."\n";
					foreach($vars as $key => $val) {
						$param_str .= $key.' = '.((string) $val)."\n";
					}
					$parent = Widget_getParentsClasses($param);
					if ('' !== $parent) {
						$param_str .= "\nparent class : \n".$parent;
					}
				}


				if ($nbParam > 0) {
					$params .= ', ';
				}
				$spanId = 'babParam_' . rand() . '_' . $i . '_' . $nbParam;

				if (is_object($param)) {
					$param_type = get_class($param);
					$param = get_class($param);
				} else {
					$param_type = gettype($param);
				}

				$params .= '<span title="' . htmlEntities('[' . $param . ']') . '" style="cursor: pointer" onclick="s=document.getElementById(\'' . $spanId . '\'); s.style.display==\'none\'?s.style.display=\'\':s.style.display=\'none\'">+[' . htmlEntities($param_type) . ']</span>'
						.  '<div style="display: none; background-color: #EEEECC" id="' . $spanId . '">' . htmlEntities( $param_str ) . '</div>';
				$nbParam++;
			}
        }

        $calls[] = ($i + 1) . ' ' . $function . '(' . $params . ') ==> ' . $location;
    }

	$display = implode("\n", $calls);

	return $display;	
}

