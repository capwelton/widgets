<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
//include_once 'base.php';

//require_once $GLOBALS['babInstallPath'] . 'utilit/devtools.php';
require_once dirname(__FILE__) . '/containerwidget.class.php';



/**
 * Constructs a Widget_reCAPTCHA.
 *
 * @param string	$id		The item unique id.
 * @return Widget_reCAPTCHA
 */
function Widget_reCAPTCHA($id = null)
{
    return new Widget_reCAPTCHA($id);
}



/**
 * Widget_reCAPTCHA
 *
 */
class Widget_reCAPTCHA extends Widget_Widget implements Widget_Displayable_Interface
{

    /**
     * @param string $text	The label text.
     * @param string $id	The item unique id.
     */
    public function __construct($id = null)
    {
        parent::__construct($id);
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Widget::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        $classes[] = 'widget-reCAPTCHA';

        return $classes;
    }


    /**
     *
     * @param string $size     allowed value are 'normal' (default) and 'compact'
     */
    public function setSize($size)
    {
        $this->setMetadata('size', $size);

        return $this;
    }


    static public function checkReCaptcha()
    {
        if (!isset($GLOBALS['babGoogleReCaptchaPrivate']) || !$GLOBALS['babGoogleReCaptchaPrivate']) {
            return false;
        }
        $secret = $GLOBALS['babGoogleReCaptchaPrivate'];
        $response = $_POST['g-recaptcha-response'];
        $remoteip = $_SERVER['REMOTE_ADDR'];

        $api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
            . $secret
            . "&response=" . $response
            . "&remoteip=" . $remoteip ;

            $decode = json_decode(file_get_contents($api_url), true);

            return $decode['success'];
    }


    /**
     * (non-PHPdoc)
     * @see Widget_Displayable_Interface::display()
     */
    public function display(Widget_Canvas $canvas)
    {

        if(isset($GLOBALS['babGoogleReCaptchaPublic'])){
            $this->setMetadata('babGoogleReCaptchaPublic', $GLOBALS['babGoogleReCaptchaPublic']);
        } else {
            throw new ErrorException('reCAPTCHA need babGoogleReCaptchaPublic to be defined to be used.');
        }

        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        $script = $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.recaptcha.jquery.js');

        return $canvas->div(
            $this->getId(),
            $this->getClasses(),
            array(),
            $this->getCanvasOptions(),
            $this->getTitle(),
            $this->getAttributes()
            )
            . $canvas->metadata($this->getId(), $this->getMetadata())
            . $script;
    }
}
