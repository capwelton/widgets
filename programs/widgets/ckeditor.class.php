<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2019 by CapWelton ({@link http://www.capwelton.com})
 */
require_once dirname(__FILE__) . '/textedit.class.php';


class Widget_CKEditor extends Widget_TextEdit implements Widget_Displayable_Interface
{
    
    private $options;
    
    /**
     * @param string $id			The item unique id.
     */
    public function __construct($id = null)
    {
        $this->options = array(); 
        parent::__construct($id);
        $this->initializeOptions();
    }
    
    private function initializeOptions()
    {
        $this->setDefaultLanguage();
        $this->setDefaultFontColor();
        $this->setDefaultHeading();
        $this->setDefaultToolbar();
    }
    
    public function getDefaultLanguage()
    {
        return bab_getLanguage();
    }
    
    public function setDefaultLanguage()
    {
        return $this->setLanguage($this->getDefaultLanguage());
    }
    
    public function setLanguage($language)
    {
        return $this->setOption('language', $language);
    }
    
    public function getLanguage()
    {
        return $this->getOption('language');
    }
    
    public function getDefaultToolbar()
    {
        return array(
            'heading',
            'alignment:left',
            'alignment:center',
            'alignment:right',
            'alignment:justify',
            '|',
            'bold',
            'italic',
            'fontSize',
            'fontColor',
            '|',
            'link',
            'bulletedList',
            'numberedList',
            '|',
            'indent',
            'outdent',
            '|',
            'imageUpload',
            'blockQuote',
            'insertTable',
            'mediaEmbed',
            'undo',
            'redo'
        );
    }
    
    public function setDefaultToolbar()
    {
        return $this->setToolbar($this->getDefaultToolbar());
    }
    
    public function setToolbar($toolbar)
    {
        return $this->setOption('toolbar', $toolbar);
    }
    
    public function getToolbar()
    {
        return $this->getOption('toolbar');
    }
    
    public function getDefaultHeading()
    {
        /**
         * An option has the following structure :
         * - model : the name in the dropdown. Must be unique
         * - view : (optional) specify the dom node and the class
         *      ex: 'view' => 'p' - Will add the p tag arround the text with this heading
         *          'view' => array('name' => 'p', 'classes' => 'myCustomClass')  - Will add the p tag with the myCustomClass arround the text with this heading
         * - title : the title in the dropdown
         * - class : the class of the button in the dropdown
         * 
         * 'options' => array(
                array('model' => 'paragraph', 'title' => 'Paragraph', 'class' => 'heading_paragraph'),
                array('model' => 'heading1', 'view' => 'h1', 'title' => 'Heading 1', 'class' => 'ck-heading_heading1'),
                array('model' => 'heading2', 'view' => 'h2', 'title' => 'Heading 2', 'class' => 'ck-heading_heading2'),
                array('model' => 'heading3', 'view' => array('name' => 'h3', 'classes' => 'myCustomHeader3'), 'title' => 'Heading 3', 'class' => 'custom_heading3'),
            )
         */
        return array(
            'options' => array(
                array('model' => 'paragraph', 'title' => widget_translate('Paragraph'), 'class' => 'heading_paragraph'),
                array('model' => 'heading1', 'view' => 'h1', 'title' => widget_translate('Heading 1'), 'class' => 'ck-heading_heading1'),
                array('model' => 'heading2', 'view' => 'h2', 'title' => widget_translate('Heading 2'), 'class' => 'ck-heading_heading2')
            )
        );
    }
    
    public function setDefaultHeading()
    {
        return $this->setHeading($this->getDefaultHeading());
    }
    
    public function setHeading($heading)
    {
        return $this->setOption('heading', $heading);
    }
    
    public function getHeading()
    {
        return $this->getOption('heading');
    }
    
    public function getDefaultFontColor()
    {
        return array(
            array(
                'color' => 'rgb(0, 0, 0)',
                'label' => 'Black'
            ),
            array(
                'color' => 'rgb(77, 77, 77)',
                'label' => 'Dim grey'
            ),
            array(
                'color' => 'rgb(153, 153, 153)',
                'label' => 'Grey'
            ),
            array(
                'color' => 'rgb(230, 230, 230)',
                'label' => 'Light grey'
            ),
            array(
                'color' => 'rgb(255, 255, 255)',
                'label' => 'White',
                'hasBorder' => true
            ),
            array(
                'color' => 'rgb(230, 77, 77)',
                'label' => 'Red'
            ),
            array(
                'color' => 'rgb(230, 153, 77)',
                'label' => 'Orange'
            ),
            array(
                'color' => 'rgb(230, 230, 77)',
                'label' => 'Yellow'
            ),
            array(
                'color' => 'rgb(153, 230, 77)',
                'label' => 'Light green'
            ),
            array(
                'color' => 'rgb(77, 230, 77)',
                'label' => 'Green'
            ),
            array(
                'color' => 'rgb(77, 230, 153)',
                'label' => 'Aquamarine'
            ),
            array(
                'color' => 'rgb(77, 230, 230)',
                'label' => 'Turquoise'
            ),
            array(
                'color' => 'rgb(77, 153, 230)',
                'label' => 'Light blue'
            ),
            array(
                'color' => 'rgb(77, 77, 230)',
                'label' => 'Blue'
            ),
            array(
                'color' => 'rgb(153, 77, 230)',
                'label' => 'Purple'
            )
        );
    }
    
    public function setDefaultFontColor()
    {
        return $this->setFontColor($this->getDefaultFontColor());
    }
    
    /**
     * @param array $fontColor An array of colors with the following structure : 
     * array(
     *  array('color' => '#ff0000', 'label' => 'Red'),
     *  array('color' => '#00ff00', 'label' => 'Green'),
     *  array('color' => '#0000ff', 'label' => 'Blue')
     * )
     * 
     * Optionaly, a "hasBorder" key can be specified for each color, with a boolean value (default is false)
     * 
     * The color attribute can be any css attribute :
     * - Hexadecimal : #000000
     * - Rgb : rgb(0,0,0)
     * - Rgba : rgba(0,0,0,1)
     * - Hsl : hsl(0, 0%, 0%)
     * - Hsla : hsl(0, 0%, 0%, 1)
     * - Color name : black
     * 
     * Note : depending on the css attribute given, it might not be displayed depending on the browser or the pdf library used
     */
    public function setFontColor($fontColor)
    {
        return $this->setOption('fontColor', array('colors' => $fontColor));
    }
    
    public function getFontColor()
    {
        return $this->getOption('fontColor');
    }
    
    /**
     * @param array $options
     * @return self
     */
    public function setOptions($options = array())
    {
        $this->options = $options;
        return $this;
    }
    
    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }
    
    /**
     *
     * @param string $optionKey
     * @param mixed $optionValue
     * @return self
     */
    public function setOption($optionKey, $optionValue)
    {
        $this->options[$optionKey] = $optionValue;
        return $this;
    }
    
    /**
     * @param string $optionKey
     * @return mixed
     */
    public function getOption($optionKey)
    {
        if (!isset($this->options[$optionKey])) {
            return null;
        }
        return $this->options[$optionKey];
    }
    
    
    /**
     * (non-PHPdoc)
     * @see Widget_TextEdit::getClasses()
     */
    public function getClasses()
    {
        $classes = parent::getClasses();
        
        $classes[] = 'widget-ckeditor';
        return $classes;
    }
    
    /**
     * Set the minheight css property of the input area
     * @param string $minHeight ex: 300px
     * @return Widget_CKEditor
     */
    public function setMinHeight($minHeight)
    {
        return $this->setOption('minHeight', $minHeight);
    }
    
    /**
     * (non-PHPdoc)
     * @see Widget_TextEdit::display()
     */
    public function display(Widget_Canvas $canvas)
    {
        if ($this->getMetadata('css') === null) {
            $this->setMetadata('css', bab_getCssUrl());
        }
        
        $widgetsAddon = bab_getAddonInfosInstance('widgets');
        
        $this->setMetadata('options', $this->options);
        
        $display = parent::display($canvas);
        $display .= $canvas->loadAddonScript($this->getId(), $widgetsAddon, 'widgets.ckeditor.jquery.js');
        $display .= $canvas->loadScript($this->getId(), $widgetsAddon->getTemplatePath().'ckeditor/translations/'.$this->getOption('language').'.js');
        
        return $display;
    }
}
