<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2011 by CANTICO ({@link http://www.cantico.fr})
 */

require_once $GLOBALS['babInstallPath'].'utilit/utilit.php';
require_once $GLOBALS['babInstallPath'].'utilit/json.php';


/* @var $W Func_Widgets */
$W = bab_functionality::get('Widgets');

$idx = bab_rp('idx', null);

switch ($idx) {

    case 'table':
        $key = bab_rp('key', null);
        if (isset($key)) {
            $addon = bab_rp('addon', 'widgets');
            $sessionOnly = (bool)bab_rp('insession', true);
            foreach ($_REQUEST as $name => $value) {
                if (is_array($value) && isset($value['filter'])) {
                    $W->setUserConfiguration($key . '/filter', $value['filter'], $addon, $sessionOnly);
                }
                if ($name === 'filter') {
                    $W->setUserConfiguration($key . '/filter', $value, $addon, $sessionOnly);
                }
                if ($name === 'page') {
                    $W->setUserConfiguration($key . '/page', $value, $addon, $sessionOnly);
                }
                if ($name === 'pageLength') {
                    $W->setUserConfiguration($key . '/pageLength', $value, $addon, $sessionOnly);
                }
                if ($name === 'sort') {
                    $W->setUserConfiguration($key . '/sort', $value, $addon, $sessionOnly);
                }
                if ($name === 'columns') {
                    $W->setUserConfiguration($key . '/columns', $value, $addon, $sessionOnly);
                }
                if ($name === 'displaySubTotalRow') {
                    $W->setUserConfiguration($key . '/displaySubTotalRow', $value, $addon, $sessionOnly);
                }
                if ($name === 'displayTotalRow') {
                    $W->setUserConfiguration($key . '/displayTotalRow', $value, $addon, $sessionOnly);
                }
            }
            $json = array(
                'reloadSelector' => '.depends-' . $key
            );
            die(bab_json_encode($json));
        }
        break;

    case 'addFilter':
        $key = bab_rp('key', null);
        if (isset($key)) {
            $addon = bab_rp('addon', 'widgets');
            $sessionOnly = (bool)bab_rp('insession', true);

            $filter = $W->getUserConfiguration($key . '/filter', $addon, $sessionOnly);
            if (!isset($filter)) {
                $filter = array();
            }
            $name = bab_rp('name');
            $value = bab_rp('value');
            $filter[$name] = $value;

            $W->setUserConfiguration($key . '/filter', $filter, $addon, $sessionOnly);

            $json = array(
                'reloadSelector' => '.depends-' . $key
            );
            die(bab_json_encode($json));
        }
        break;

    case 'removeFilter':
        $key = bab_rp('key', null);
        if (isset($key)) {
            $addon = bab_rp('addon', 'widgets');
            $sessionOnly = (bool)bab_rp('insession', true);

            $filter = $W->getUserConfiguration($key . '/filter', $addon, $sessionOnly);
            if (!isset($filter)) {
                die;
            }
            $name = bab_rp('name');
            if (!isset($filter[$name])) {
                die;
            }
            unset($filter[$name]);
            unset($filter[$name . '_ID']);

            $W->setUserConfiguration($key . '/filter', $filter, $addon, $sessionOnly);

            $json = array(
                'reloadSelector' => '.depends-' . $key
            );
            die(bab_json_encode($json));
        }
        break;

    case 'selectRow':
        $key = bab_rp('key', null);
        if (isset($key)) {
            $addon = bab_rp('addon', 'widgets');
            $sessionOnly = (bool)bab_rp('insession', true);

            $selectedRows = $W->getUserConfiguration($key . '/selectedRows', $addon, $sessionOnly);
            if (!isset($selectedRows)) {
                $selectedRows = array();
            }
            $ids = bab_rp('id');
            foreach ($ids as $id => $selected) {
                if ($selected == '1') {
                    $selectedRows[$id] = $id;
                } else {
                    unset($selectedRows[$id]);
                }
            }

            $W->setUserConfiguration($key . '/selectedRows', $selectedRows, $addon, $sessionOnly);

            $reloadSelector = '.depends-' . $key . '-selection';
            if (count($ids) > 1) {
                $reloadSelector .= ',.depends-' . $key;
            }
            $json = array(
                'reloadSelector' => $reloadSelector
            );
            die(bab_json_encode($json));

//             header('Content-Type: application/json');
//             die(bab_convertStringFromDatabase(bab_json_encode($json), 'UTF-8'));
        }

        break;


    case 'delete':
        $path = bab_rp('path', null);
        $addon = bab_rp('addon', 'widgets');
        if (isset($path)) {
            $W->deleteUserConfiguration($path, $addon);
        }
        break;

    case 'set':
    default:
        $key = bab_rp('key', null);
        $value = bab_rp('value', null);
        $addon = bab_rp('addon', 'widgets');
        if (isset($key) && isset($value)) {
            $W->setUserConfiguration($key, $value, $addon);
        }

        list($key) = explode('/', $key);
        $json = array(
            'reloadSelector' => '.depends-' . $key
        );
        die(bab_json_encode($json));

        break;
}

die;
