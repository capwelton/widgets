<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
require_once dirname(__FILE__).'/../controller.class.php';


/**
 *
 */
class widget_CtrlConfiguration extends widget_Controller
{
    /**
     * Saves one key/value pair for the current user only.
     *
     * @param string $key
     * @param string $value
     * @return boolean
     */
    public function setValue($key = null, $value = null)
    {
        $W = bab_Widgets();

        $addon = 'widgets';

        if (isset($key) && isset($value)) {
            $W->setUserConfiguration($key, $value, $addon);
            $this->addMessage(widget_translate('Configuration saved'));
        }

        return true;
    }


    /**
     * Saves multiple key/values at once for the current user only.
     *
     * @param array $values
     * @return boolean
     */
    public function setValues($path = '', $values = null)
    {
        $W = bab_Widgets();

        $addon = 'widgets';

        if (isset($values) && is_array($values)) {
            foreach ($values as $key => $value) {
                $W->setUserConfiguration($path . $key, $value, $addon);
            }
        }

        return true;
    }




    /**
     *
     * @param string $path
     * @return boolean
     */
    public function delete($path = '')
    {
        $W = bab_Widgets();

        $addon = 'widgets';
        if (isset($path) && $path !== '') {
            $W->deleteUserConfiguration($path, $addon);
        }

        return true;
    }



    /**
     * Saves one key/value pair for the current user only.
     *
     * @param string $key
     * @param string $value
     * @return boolean
     */
    public function setGlobalValue($key = null, $value = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException();
        }
        $W = bab_Widgets();

        $addon = 'widgets';

        if (isset($key) && isset($value)) {
            $W->setDefaultConfiguration($key, $value, $addon);
            $this->addMessage(widget_translate('Configuration saved'));
        }

        return true;
    }


    /**
     * Saves multiple key/values at once, default for all users.
     *
     * @param array $values
     * @return boolean
     */
    public function setGlobalValues($path = '', $values = null)
    {
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException();
        }
        $W = bab_Widgets();

        $addon = 'widgets';

        if (isset($values) && is_array($values)) {
            foreach ($values as $key => $value) {
                $W->setDefaultConfiguration($path . $key, $value, $addon);
            }
        }

        return true;
    }


    /**
     *
     * @param string $path
     * @return boolean
     */
    public function deleteGlobal($path = '')
    {
        if (!bab_isUserAdministrator()) {
            throw new bab_AccessException();
        }
        $W = bab_Widgets();

        $addon = 'widgets';
        if (isset($path) && $path !== '') {
            $W->deleteDefaultConfiguration($path, $addon);
        }

        return true;
    }


    public function cancel()
    {
        return true;
    }
}
