<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
 ************************************************************************/
include "base.php";


function widgets_onDeleteAddon()
{
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    bab_removeAddonEventListeners('widgets');

    $functionalities = new bab_functionalities();
    $functionalities->unregister('Ovml/Function/WidgetSitemapMenu');
    $functionalities->unregister('Widgets');

    return true;
}



function widgets_upgrade($version_base, $version_ini)
{
    include_once $GLOBALS['babInstallPath'].'utilit/eventincl.php';
    include_once $GLOBALS['babInstallPath'].'utilit/install.class.php';
    require_once dirname(__FILE__) . '/widgets.php';
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';

    bab_installWindow::message(bab_toHtml(sprintf(widget_translate('Upgrading widgets addon form version %s to version %s'), $version_base, $version_ini)));

    $addon = bab_getAddonInfosInstance('widgets');

    $functionalities = new bab_functionalities();
    $functionalities->register('Widgets', $addon->getPhpPath().'widgets.php');
    $functionalities->register('Ovml/Function/WidgetSitemapMenu', $addon->getPhpPath().'widgets/sitemapmenu.class.php');

    bab_removeAddonEventListeners('widgets');

    $addon->addEventListener('bab_eventBeforePageCreated', 'widgets_onBeforePageCreated', 'init.php');

    return true;
}


function widgets_onBeforePageCreated(bab_eventBeforePageCreated $event)
{
    $babBody = bab_getBody();

    require_once dirname(__FILE__) . '/widgets.php';
    Func_Widgets::onBeforePageCreated();

    /* @var $jquery Func_Jquery */
    if ($jquery = bab_functionality::get('jquery')) {
        $jquery->includeCore();
        $jquery->includeUi();
    }

    $addon = bab_getAddonInfosInstance('widgets');
    $babBody->addJavascriptFile($addon->getTemplatePath().'widgets.js', false);
    if ($jquery) {
        $babBody->addJavascriptFile($addon->getTemplatePath().'widgets.jquery.js', true);
    }

    $babBody->addStyleSheet($addon->getStylePath().'widgets.css');

    if (bab_Registry::get('/widgets/includeDefaultTheme', true)) {
        $babBody->addStyleSheet($addon->getStylePath().'default/theme.css');
    }
}
