<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/widgetTest.php';


class Widget_LabelledWidgetTest extends Widget_WidgetTest
{
    protected $itemClass = 'Widget_LabelledWidget';
    

    /**
     * For a LabelledWidget with non associated widget, getName()
     * must return null.
     */
    public function testSetName()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
    
        $item->setName('myName');
    
        $this->assertNull(
            $item->getName()
        );
    }
    
    
    /**
     * For a LabelledWidget with non associated widget, getName()
     * must return null.
     */
    public function testSetMultipleNames()
    {
        // Creates a Mock_Widget_Item.
        $item = $this->construct();
        /*@var $item Widget_LabelledWidget */
    
        $item->setName(array('myContact', 'name'));
    
        $this->assertNull(
            $item->getName()
        );
    }
    
   
    
//     public function testSetNameOnLabelledWidgetIsAppliedToAssociatedWidget()
//     {
//         // creates a mock_widget_item.
//         $item = $this->construct();

        
//         $item->setname(array('mycontact', 'name'));
        
//         $this->assertequals(
//             array('mycontact', 'name'),
//             $item->getname()
//         );
//     }
}
