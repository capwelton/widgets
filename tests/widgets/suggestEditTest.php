<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';
require_once dirname(__FILE__) . '/inputWidgetTest.php';

abstract class Widget_SuggestEditTest extends Widget_InputWidgetTest
{
    protected $itemClass = 'Widget_SuggestEdit';
}
