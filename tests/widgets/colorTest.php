<?php

require_once dirname(__FILE__) . '/../mock/MockOvidentia.php';

class Widget_ColorTest extends PHPUnit_Framework_TestCase
{
    
    protected $itemClass = 'Widget_Color';
    
    /**
     * @return Widget_Item
     */
    protected function construct()
    {
        $W = bab_Widgets();
        $W->includePhpClass($this->itemClass);
        $args = func_get_args();
        $item = $this->getMockForAbstractClass($this->itemClass, $args);
        return $item;
    }

    
    public function hsl2rgbProvider()
    {
        return array(
            array(array(0.0, 0.0, 0.0), array(0.0, 0.0, 0.0)),
            
            array(array(0.0, 1.0, 0.5), array(1.0, 0.0, 0.0)),
            array(array(1/6, 1.0, 0.5), array(1.0, 1.0, 0.0)),
            array(array(2/6, 1.0, 0.5), array(0.0, 1.0, 0.0)),
            array(array(3/6, 1.0, 0.5), array(0.0, 1.0, 1.0)),
            array(array(4/6, 1.0, 0.5), array(0.0, 0.0, 1.0)),
            array(array(5/6, 1.0, 0.5), array(1.0, 0.0, 1.0)),
        );
    }

    public function hexa2rgbProvider()
    {
        return array(
            array('000000', array(0.0, 0.0, 0.0)),
    
            array('FF0000', array(1.0, 0.0, 0.0)),
            array('FFFF00', array(1.0, 1.0, 0.0)),
            array('00FF00', array(0.0, 1.0, 0.0)),
            array('00FFFF', array(0.0, 1.0, 1.0)),
            array('0000FF', array(0.0, 0.0, 1.0)),
            array('FF00FF', array(1.0, 0.0, 1.0)),
        );
    }

    public function yuv2rgbProvider()
    {
        return array(
            array(array(0.0, 0.0, 0.0),             array(0.0, 0.0, 0.0)),
            array(array(0.299, -0.147, 0.615),      array(1.0, 0.0, 0.0)),
            array(array(0.886, -0.436, 0.100),      array(1.0, 1.0, 0.0)),
            array(array(0.587, -0.289, -0.515),     array(0.0, 1.0, 0.0)),
            array(array(0.701, 0.148, -0.615),      array(0.0, 1.0, 1.0)),
            array(array(0.114, 0.436, -0.100),      array(0.0, 0.0, 1.0)),
            array(array(0.413, 0.290, 0.515),       array(1.0, 0.0, 1.0)),
        );
    }
    
    
    
    
    /**
     * @dataProvider hsl2rgbProvider
     */
    public function testHsl2RgbConversions($hsl, $expextedRgb)
    {
        // Creates a Mock_Widget_Color.
        $color = $this->construct();

        $color->setHSL($hsl[0], $hsl[1], $hsl[2]);
        $rgb = $color->getRGB();
        
        $this->assertEquals(
            $rgb,
            $expextedRgb,
            '',
            0.001
        );
    }


    /**
     * @dataProvider hsl2rgbProvider
     */
    public function testRgb2HslConversions($expectedHsl, $rgb)
    {
        // Creates a Mock_Widget_Color.
        $color = $this->construct();

        $color->setRGB($rgb[0], $rgb[1], $rgb[2]);
        $hsl = $color->getHSL();
        
        $this->assertEquals(
            $hsl,
            $expectedHsl,
            '',
            0.001
        );
    }
    

    /**
     * @dataProvider yuv2rgbProvider
     */
    public function testYuv2RgbConversions($yuv, $expextedRgb)
    {
        // Creates a Mock_Widget_Color.
        $color = $this->construct();
    
        $color->setYUV($yuv[0], $yuv[1], $yuv[2]);
        $rgb = $color->getRGB();
    
        $this->assertEquals(
            $rgb,
            $expextedRgb,
            '',
            0.001
        );
    }
    
    
    /**
     * @dataProvider yuv2rgbProvider
     */
    public function testRgb2YuvConversions($expectedYuv, $rgb)
    {
        // Creates a Mock_Widget_Color.
        $color = $this->construct();
    
        $color->setRGB($rgb[0], $rgb[1], $rgb[2]);
        $yuv = $color->getYUV();
    
        $this->assertEquals(
            $yuv,
            $expectedYuv,
            '',
            0.002
        );
    }
    
    
    /**
     * @dataProvider hexa2rgbProvider
     */
    public function testHexa2RgbConversions($hexa, $expextedRgb)
    {
        // Creates a Mock_Widget_Color.
        $color = $this->construct();

        $color->setHexa($hexa);
        $rgb = $color->getRGB();
        
        $this->assertEquals(
            $rgb,
            $expextedRgb,
            '',
            0.002
        );
    }


    /**
     * @dataProvider hexa2rgbProvider
     */
    public function testRgb2HexaConversions($expectedHexa, $rgb)
    {
        // Creates a Mock_Widget_Color.
        $color = $this->construct();

        $color->setRGB($rgb[0], $rgb[1], $rgb[2]);
        $hexa = $color->getHexa();
        
        $this->assertEquals(
            $hexa,
            $expectedHexa,
            '',
            0.001
        );
    }
}
