/**
 * Extending jQuery with autocomplete
 * Version: 1.4.2
 * Author: Yanik Gleyzer (clonyara)
 */
(function(jQuery) {

    // some key codes
    var RETURN = 13;
    var TAB = 9;
    var ESC = 27;
    var ARRLEFT = 37;
    var ARRUP = 38;
    var ARRRIGHT = 39;
    var ARRDOWN = 40;
    var BACKSPACE = 8;
    var DELETE = 46;

function debug(s) {
    jQuery('#info').append(htmlspecialchars(s)+'<br>');
}

// getting caret position obj: {start,end}
function getCaretPosition(obj) {
    var start = -1;
    var end = -1;
    if (typeof obj.selectionStart != "undefined") {
        start = obj.selectionStart;
        end = obj.selectionEnd;
    } else if (document.selection&&document.selection.createRange) {
        var M = document.selection.createRange();
        var Lp;
        try {
            Lp = M.duplicate();
            Lp.moveToElementText(obj);
        } catch(e) {
            Lp = obj.createTextRange();
        }
        Lp.setEndPoint("EndToStart", M);
        start = Lp.text.length;
        if (start>obj.value.length) {
            start = -1;
        }

        Lp.setEndPoint("EndToStart", M);
        end = Lp.text.length;
        if (end>obj.value.length) {
            end = -1;
        }
    }
    return {'start':start,'end':end};
}

// set caret to
function setCaret(obj, l) {
    obj.focus();
    if (obj.setSelectionRange){
        obj.setSelectionRange(l,l);
    } else if(obj.createTextRange) {
        m = obj.createTextRange();
        m.moveStart('character', l);
        m.collapse();
        m.select();
    }
}

// prepare array with velued objects
// required properties are id and value
// rest of properties remaines
function prepareArray(jsondata) {
    var new_arr = [];
    for (var i = 0; i < jsondata.length; i++) {
        if (jsondata[i] && jsondata[i].id != undefined && jsondata[i].value != undefined) {
            jsondata[i].id = jsondata[i].id + "";
            jsondata[i].value = jsondata[i].value + "";
            if (jsondata[i].info != undefined) {
                jsondata[i].info = jsondata[i].info + "";
            }
            new_arr.push(jsondata[i]);
        }
    }
    return new_arr;
}

// php analogs
function escapearg(s) {
    if (s == undefined || !s) {
        return '';
    }
    return s.replace('\\','\\\\').
        replace('*','\\*').
        replace('.','\\.').
        replace('/','\\/');
}

function htmlspecialchars(s) {
    if (s == undefined || !s) {
        return '';
    }
    return s.replace('&','&amp;').
        replace('<','&lt;').
        replace('>','&gt;');
}

function ltrim(s) {
    if(s == undefined || !s) {
        return '';
    }
    return s.replace(/^\s+/g,'');
}


// extending jQuery
jQuery.fn.autocomplete = function(options){ return this.each(function(){
    // take me
    var me = jQuery(this);
    var me_this = jQuery(this).get(0);

    // test for supported text elements
    if (!me.is('input:text,input:password,textarea')) {
        return;
    }

    // get or ajax_get required!
    if (!options && (!jQuery.isFunction(options.get) || !options.ajax_get)) {
        return;
    }
    // check plugin enabled
    if (me.attr('jqac') == 'on') {
        return;
    }

    // plugin on!
    me.attr('jqac','on');

    // no browser's autocomplete!
    me.attr('autocomplete','off');

    // default options
    options = jQuery.extend({
            delay         : 500 ,
            timeout       : 50000 ,
            minchars      : 3 ,
            multi         : false ,
            cache         : false ,
            height        : 200 ,
            autowidth     : false ,
            noresults     : 'No results',
            separator     : ',',
            trigger       : false,
            trigger_start : '',
            trigger_end   : ''
        },
        options
    );




    // bind key events
    // handle special keys here
    me.keydown(function(ev) {
        switch (ev.which) {
            // return choose highlighted item or default propogate
            case RETURN:
                if (!suggestions_menu) {
                    return true;
                } else {
                    setHighlightedValue();
                }
                return false;
            // up changes highlight
            case ARRUP:
//                if (!suggestions_menu) {
//                    return true;
//                }
                changeHighlight(ev.keyCode);
                return suggestions_menu === false;

            // down changes highlight or open new menu
            case ARRDOWN:
                if (!suggestions_menu) {
                    getSuggestions(getUserInput());
                } else {
                    changeHighlight(ev.keyCode);
                }
                return suggestions_menu === false;

            // escape clears menu
            case ESC:
                clearSuggestions();
                return false;
        }
        return true;
    });

    me.keypress(function(ev) {
        // ev.which doesn't work here - it always returns 0
        switch (ev.keyCode) {
            case ESC:
                return false;
        }
        return true;
    });

    // handle normal characters here
    me.keyup(function(ev) {
        switch (ev.which) {
            case RETURN:
            case ESC:
            case ARRLEFT:
            case ARRRIGHT:
            case ARRUP:
            case ARRDOWN:
                return false;
            default:
                getSuggestions(getUserInput());
        }
        return true;
    });



    me.blur(function() {
        setTimeout(function() {
            if(me.is(':focus')){
                return;
            }
            var metadata = window.babAddonWidgets.getMetadata(me.attr('id'));
            if (metadata.id_target) {
            	var selectedInputElement = document.getElementById(metadata.id_target);
            	if(selectedInputElement){
                    var selectedTextValue = selectedInputElement.suggestValue;
                    var currentTextValue = me.val();
                    if (selectedTextValue != currentTextValue) {
                        jQuery('#'+metadata.id_target).val('');
                        document.getElementById(metadata.id_target).suggestValue = '';
                        me.removeClass('selected');
                    } else {
                        me.addClass('selected');
                    }
            	}
            }
            if(suggestions_menu && !suggestions_menu_button_down) {
                jQuery(suggestions_menu).remove();
            }
            hide_loading();
            killTimeout();
        },100);
    });


    me.click(function() {
        getSuggestions(getUserInput());
        return true;
    });

    // init variables
    var user_input = "";
    var input_chars_size  = 0;
    var suggestions = [];
    var current_highlight = 0;
    var suggestions_menu = false;
    var suggestions_menu_button_down = false;
    var suggestions_list = false;
    var loading_indicator = false;
    var clearSuggestionsTimer = false;
    var getSuggestionsTimer = false;
    var showLoadingTimer = false;
    var zIndex = me.css('z-index');

    // get user input
    function getUserInput() {
        var val = me.val();
        if (options.multi) {
            var pos = getCaretPosition(me_this);
            var start = pos.start >= 0 ? pos.start : orig.length;
            for ( ; start > 0 && val.charAt(start-1) != options.separator ; start--) {
                ;
            }
            var end = pos.start >= 0 ? pos.start : orig.length;
            for( ; end < val.length && val.charAt(end) != options.separator ; end++) {
                ;
            }
            var val = val.substr(start, end-start);
        } else if (options.trigger) {
            var pos = getCaretPosition(me_this);
            var triggerStart = options.trigger_start;
            var triggerStartLength = triggerStart.length;
            var triggerEnd = options.trigger_end;
            var triggerEndLength = triggerEnd.length;
            var start = pos.start >= 0 ? pos.start : val.length - triggerStartLength;
            for ( ; start > 0 && val.substr(start, triggerStartLength) != triggerStart ; start--) {
                if (val.substr(start, triggerEndLength) == triggerEnd) {
                    return '';
                }
            }
            var end = pos.start >= 0 ? pos.start : val.length - triggerEndLength;
            for( ; end < val.length - triggerEndLength && val.substr(end, triggerEndLength) != triggerEnd && val.substr(end, 1) != ' ' && val.substr(end, 1) != '\n'; end++) {
                ;
            }
            val = val.substr(start, end - start);
        }
        return ltrim(val);
    }

    // set suggestion
    function setSuggestion(val, separator) {
        user_input = val;
        if (options.multi) {
            var orig = me.val();
            var pos = getCaretPosition(me_this);
            var start = pos.start >= 0 ? pos.start : orig.length;
            for ( ; start>0 && orig.charAt(start-1) != options.separator ; start--) {
                ;
            }
            var end = pos.start >= 0 ? pos.start : orig.length;
            for( ; end<orig.length && orig.charAt(end) != options.separator ; end++) {
                ;
            }
            var new_val = orig.substr(0, start) + (start > 0 ? ' ' : '') + options.trigger_start + val + options.trigger_end + orig.substr(end);
            me.val(new_val + separator);
            setCaret(me_this, start + val.length + separator.length + options.trigger_start.length + options.trigger_end.length + (start > 0 ? 1 : 0));
        } else if (options.trigger) {
            var orig = me.val();
            var pos = getCaretPosition(me_this);
            var triggerStart = options.trigger_start;
            var triggerStartLength = triggerStart.length;
            var triggerEnd = options.trigger_end;
            var triggerEndLength = triggerEnd.length;
            var start = pos.start >= 0 ? pos.start : orig.length - triggerStartLength;
            for ( ; start > 0 && orig.substr(start, triggerStartLength) != triggerStart ; start--) {
                ;
            }
            var end = pos.start >= 0 ? pos.start : orig.length - triggerStartLength;
            for( ; end < orig.length - triggerEndLength && orig.substr(end, triggerEndLength) != triggerEnd && orig.substr(end, 1) != ' ' && orig.substr(end, 1) != '\n'; end++) {
                ;
            }
            if (orig.substr(end, triggerEndLength) == triggerEnd) {
                end += triggerEndLength;
            }
            var new_val = orig.substr(0, start) + options.trigger_start + val + options.trigger_end + orig.substr(end);
            me.val(new_val + separator);
            setCaret(me_this, start + val.length + options.trigger_start.length + options.trigger_end.length);            
        } else {
            me_this.focus();
            me.val(val);
        }

        window.babAddonWidgets.suggestedit_setValue(me);
    }

    // get suggestions
    function getSuggestions(val) {

        if (options.trigger) {
            var tiggerStart = options.trigger_start;
            var tiggerStartLength = options.trigger_start.length;
            if (val.substr(0, tiggerStartLength) != tiggerStart) {
                clearSuggestions();
                return false;
            }
        }
        // input length is less than the min required to trigger a request
        // reset input string
        // do nothing

        if (val.length < options.minchars){
            clearSuggestions();
            return false;
        }
        // if caching enabled, and user is typing (ie. length of input is increasing)
        // filter results out of suggestions from last request
        if (options.cache && val.length > input_chars_size && suggestions.length) {
            var arr = [];
            for (var i = 0; i < suggestions.length; i++) {
                var re = new RegExp("(" + escapearg(val) + ")", 'ig');
                if (re.exec(suggestions[i].value)) {
                    arr.push( suggestions[i] );
                }
            }

            user_input = val;
            input_chars_size = val.length;
            suggestions = arr;

            createList(suggestions);
            return false;
        } else { // do new request
            clearTimeout(getSuggestionsTimer);
            user_input = val;
            input_chars_size = val.length;
            
            var key = val.substr(options.trigger_start.length);

            getSuggestionsTimer = setTimeout(
                function() {
                    suggestions = [];
                    // call pre callback, if exists
                    if (jQuery.isFunction(options.pre_callback)) {
                        options.pre_callback();
                    }
                    // call get
                    if (jQuery.isFunction(options.get)) {
                        suggestions = prepareArray(options.get(key));
                        createList(suggestions);
                    } else if(jQuery.isFunction(options.ajax_get)) { // call AJAX get
                        clearSuggestions();
                        showLoadingTimer = setTimeout(show_loading,options.delay);
                        options.ajax_get(key, ajax_continuation, window.babAddonWidgets.getMetadata(me.attr('id')));
                    }
                },
                options.delay
            );
        }
        return false;
    };

    // AJAX continuation
    function ajax_continuation(jsondata) {
        hide_loading();
        suggestions = prepareArray(jsondata);
        createList(suggestions);
    }

    // shows loading indicator
    function show_loading() {
        if (!loading_indicator) {
            loading_indicator = jQuery('<div class="widget-suggestlineedit-menu"><div class="widget-suggestlineedit-loading">Loading</div></div>').get(0);
            jQuery(loading_indicator).css('position','absolute');
            var pos = me.offset();
            jQuery(loading_indicator).css('left', pos.left + "px");
            jQuery(loading_indicator).css('top', ( pos.top + me.height() + 2 ) + "px");
            if (!options.autowidth){
                jQuery(loading_indicator).width(me.width());
            }
            jQuery('body').append(loading_indicator);
        }
        jQuery(loading_indicator).show();
        setTimeout(hide_loading, 10000);
    }

    // hides loading indicator
    function hide_loading() {
        if (loading_indicator) {
            jQuery(loading_indicator).hide();
        }
        clearTimeout(showLoadingTimer);
    }

    // create suggestions list
    function createList(arr) {
        if (suggestions_menu) {
            jQuery(suggestions_menu).remove();
        }
        hide_loading();
        killTimeout();

        if (!arr.length) {
            // do not display empty menu
            return;
        }

        if(!me.is(':focus')){
            return;
        }

        // create holding div
        suggestions_menu = jQuery('<div class="widget-suggestlineedit-menu"></div>').get(0);

        // ovveride some necessary CSS properties
        jQuery(suggestions_menu).css({
            'position':'absolute',
            'z-index':zIndex,
            'max-height':options.height+'px',
            'overflow-y':'auto'
        });

        // create and populate ul
        suggestions_list = jQuery('<ul></ul>').get(0);
        // set some CSS's
        jQuery(suggestions_list).
              css('list-style','none').
              css('margin','0px').
              css('padding','0px').
              css('overflow','hidden');
        // regexp for replace
        var re = new RegExp("("+escapearg(htmlspecialchars(user_input))+")",'ig');

        // loop throught arr of suggestions creating an LI element for each suggestion
        for (var i = 0; i < arr.length; i++) {
            var val = new String(arr[i].value);
            // using RE

            if (user_input.length > 2) {
                var output = htmlspecialchars(val).replace(re,'<em>$1</em>');
            } else {
                var output = val;
            }

            // using substr
            //var st = val.toLowerCase().indexOf( user_input.toLowerCase() );
            //var len = user_input.length;
            //var output = val.substring(0,st)+"<em>"+val.substring(st,st+len)+"</em>"+val.substring(st+len);

            var span = jQuery('<span class="widget-suggestlineedit-link">'+output+'</span>').get(0);
            if (arr[i].info != undefined && arr[i].info != "") {
                jQuery(span).append(jQuery('<div class="widget-suggestlineedit-info">'+arr[i].info+'</div>'));
            }
            if (arr[i].cssclass != undefined && arr[i].cssclass != "") {
                jQuery(span).addClass(arr[i].cssclass);
            }

            jQuery(span).attr('name',i+1);
            jQuery(span).click(function () { setHighlightedValue(); });
            jQuery(span).mouseover(function () { setHighlight(jQuery(this).attr('name'),true); });

            var li = jQuery('<li class="ui-state-default"></li>').get(0);
            jQuery(li).append(span);

            jQuery(suggestions_list).append(li);
        }

        // no results
        //if (arr.length == 0){
        //  jQuery(suggestions_list).append('<li class="widget-suggestlineedit-warning">'+options.noresults+'</li>');
        //}

        jQuery(suggestions_menu).append(suggestions_list);

        // get position of target textfield
        // position holding div below it
        // set width of holding div to width of field
        var pos = me.offset();

        jQuery(suggestions_menu).css('left', pos.left + "px");
        jQuery(suggestions_menu).css('top', ( pos.top + me.height() + 2 ) + "px");
        jQuery(suggestions_menu).css('minWidth', ( me.width() ) + "px");
        if (!options.autowidth) {
            jQuery(suggestions_menu).width(me.width());
        }

        // set mouseover functions for div
        // when mouse pointer leaves div, set a timeout to remove the list after an interval
        // when mouse enters div, kill the timeout so the list won't be removed
        jQuery(suggestions_menu).mouseover(function() { killTimeout(); });
        jQuery(suggestions_menu).mouseout(function() { resetTimeout(); });


        jQuery(suggestions_menu).mousedown(function() {
            setTimeout(function() {
                me.focus();
            },1);
        });
        //jQuery(suggestions_menu).mouseup(function() { console.log('mouseup');suggestions_menu_button_down = false; me.focus(); });
        //jQuery(suggestions_menu).scroll(function() { console.log('scroll');me.focus(); });


        // add DIV to document
        jQuery('body').append(suggestions_menu);

        // bgIFRAME support
        if (jQuery.fn.bgiframe) {
            jQuery(suggestions_menu).bgiframe({height: suggestions_menu.scrollHeight});
        }


        // adjust height: add +20 for scrollbar
        if (suggestions_menu.scrollHeight > options.height) {
            jQuery(suggestions_menu).height(options.height);
            jQuery(suggestions_menu).width(jQuery(suggestions_menu).width() + 20);
        }

        // currently no item is highlighted
        current_highlight = 0;

        // remove list after an interval
        clearSuggestionsTimer = setTimeout(function () { clearSuggestions(); }, options.timeout);
    };

    // set highlighted value
    function setHighlightedValue() {
        var metadata = window.babAddonWidgets.getMetadata(me.attr('id'));
        if (current_highlight && suggestions[current_highlight-1]) {
              var sugg = suggestions[ current_highlight-1 ];

              if (sugg.affected_value != undefined && sugg.affected_value != '') {
                setSuggestion(sugg.affected_value, metadata.multiple_separator);
              } else {
                setSuggestion(sugg.value, metadata.multiple_separator);
              }
              // pass selected object to callback function, if exists
              if (jQuery.isFunction(options.callback)) {
                  options.callback(suggestions[current_highlight-1]);
              }


              if (metadata.info_target) {
                  jQuery('#'+metadata.info_target).val(sugg.info);
                  jQuery('#'+metadata.info_target).change();
              }

              if (metadata.id_target) {
                  jQuery('#'+metadata.id_target).val(sugg.id);
                  jQuery('#'+metadata.id_target).change();
                  document.getElementById(metadata.id_target).suggestValue = sugg.value;
              }

              if (metadata.extra_target) {
                  var extra_target = jQuery('#'+metadata.extra_target);
                  if (extra_target.is('select')) {
                      extra_target.find('option').filter(function(index) { return (jQuery(this).text() == sugg.extra); }).prop('selected', true);
                  } else {
                      extra_target.val(sugg.extra);
                  }
              }

              clearSuggestions();
        }
        if(metadata.displayable){
        	me.trigger('change');
        }
    };

    // change highlight according to key
    function changeHighlight(key) {
        if (!suggestions_list || suggestions.length == 0) {
            return false;
        }
        var n;
        if (key == ARRDOWN) {
            n = current_highlight + 1;
        } else if (key == ARRUP) {
            n = current_highlight - 1;
        }

        if (n > jQuery(suggestions_list).children().size()) {
            n = 1;
        }
        if (n < 1) {
            n = jQuery(suggestions_list).children().size();
        }
        return setHighlight(n);
    };


    // change highlight
    function setHighlight(n,mouse_mode) {
        if (!suggestions_list) {
            return false;
        }
        if (current_highlight > 0) {
            clearHighlight();
        }
        current_highlight = Number(n);
        var li = jQuery(suggestions_list).children().get(current_highlight-1);
        li.className = 'ui-state-hover';
        // for mouse mode don't adjust scroll! prevent scrolling jumps
        if (!mouse_mode) {
            adjustScroll(li);
        }
        killTimeout();
        return true;
    };

    // clear highlight
    function clearHighlight() {
        if (!suggestions_list) {
            return false;
        }
        if (current_highlight > 0) {
            jQuery(suggestions_list).children().get(current_highlight-1).className = 'ui-state-default';
            current_highlight = 0;
        }
    };

    // clear suggestions list
    function clearSuggestions() {
        killTimeout();
        if (suggestions_menu) {
            jQuery(suggestions_menu).remove();
            suggestions_menu = false;
            suggestions_list = false;
            current_highlight = 0;
        }
    };

    // set scroll
    function adjustScroll(el) {
        if (!suggestions_menu) {
            return false;
        }
        var viewportHeight = suggestions_menu.clientHeight;
        var wholeHeight = suggestions_menu.scrollHeight;
        var scrolled = suggestions_menu.scrollTop;
        var elTop = el.offsetTop;
        var elBottom = elTop + el.offsetHeight;
        if (elBottom > scrolled + viewportHeight) {
            suggestions_menu.scrollTop = elBottom - viewportHeight;
        } else if (elTop < scrolled) {
            suggestions_menu.scrollTop = elTop;
        }
        return true;
    }

    // timeout funcs
    function killTimeout() {
        clearTimeout(clearSuggestionsTimer);
    };

    function resetTimeout() {
        clearTimeout(clearSuggestionsTimer);
        clearSuggestionsTimer = setTimeout(function () { clearSuggestions(); }, 30000);
    };
});};

})(jQuery);












/**
 * serialize multi dimmentional array to query string
 */
window.babAddonWidgets.suggestlineedit_serialize = function(obj, name) {

    if (typeof obj == 'object') {

        // serialize

        var arr = new Array();

        for (var i in obj) {

            if (null != name) {

                var value = window.babAddonWidgets.suggestlineedit_serialize(obj[i], encodeURIComponent(name)+'['+encodeURIComponent(i)+']');
                var param = name+'['+encodeURIComponent(i)+']='+value;
            } else {

                var value = window.babAddonWidgets.suggestlineedit_serialize(obj[i], i);

                if (typeof obj[i] != 'object') {
                    var param = encodeURIComponent(i)+'='+value;
                } else {
                    var param = value;
                }
            }

            arr.push(param);
        }

        return arr.join('&');

    } else {
        return encodeURIComponent(obj);
    }

};






/**
 * called if value modified by keyboard or by suggestion
 *
 */
window.babAddonWidgets.suggestedit_setValue = function(field) {
    var metadata = window.babAddonWidgets.getMetadata(field.attr('id'));

    if (metadata.keyup) {
        for(var i =0; i < metadata.keyup.length; i++) {
            var func = metadata.keyup[i];

            eval(func+'(field)');
        }
    }
};






window.babAddonWidgets.suggestedit_init = function(domNode) {

    jQuery(domNode).find('.widget-suggestedit').each(function () {

        var metadata = window.babAddonWidgets.getMetadata(this.id);

        if (!document.getElementById(metadata.id_target)) {
            return 0;
        }

        document.getElementById(metadata.id_target).suggestValue = jQuery(this).val();
        var minChars = (typeof(metadata.minChars) !== 'undefined') ? metadata.minChars : 3;

        if (!jQuery(this).autocomplete)
        {
            return 0;
        }

        var options = {

            autowidth: true,

            minchars : minChars,

            ajax_get : function(v, cont, metadata) {
                params = metadata.suggesturl;
                params[metadata.suggestparam] = v;

                if (metadata.fields) {
                    for (var key in metadata.fields) {
                        if (field = jQuery('#'+metadata.fields[key])) {

                            if (field.is('select')) {
                                params[key] = field.find(':selected').text();
                            } else {
                                params[key] = field.val();
                            }
                        }
                    }
                }

                params = window.babAddonWidgets.suggestlineedit_serialize(params);

                jQuery.ajax({
                    type	: "GET",
                    url		: "?",
                    data	: params,
                    async	: true,
                    success	: function(returnvalue){
                        if (typeof returnvalue == "object") {
                            var r = returnvalue;
                        } else {
                            eval('var r = '+returnvalue+';');
                        }
                        cont(r);
                    }
                });
            }
        };

        if (typeof(metadata.multiple_separator) !== 'undefined' && metadata.multiple_separator.length > 0) {
            options.multi = true;
            options.separator = metadata.multiple_separator;
        }
        
        if (typeof(metadata.trigger_start) !== 'undefined' && metadata.trigger_start.length > 0) {
            options.trigger = true;
            options.multi = false;
            options.trigger_start = metadata.trigger_start;
            if (typeof(metadata.trigger_end) !== 'undefined' && metadata.trigger_end.length > 0) {
                options.trigger_end = metadata.trigger_end;
            }
        }


        jQuery(this).autocomplete(options);
    });

    jQuery(domNode).find('.widget-suggestedit').keyup(function(event){
        window.babAddonWidgets.suggestedit_setValue(jQuery(event.target));
    });
};






window.bab.addInitFunction(window.babAddonWidgets.suggestedit_init);

