

function __widget_bap_markField(obj, message, nb_alert) {

    if (-1 != obj.className.indexOf('widget_bap_errorobject'))
    {
        return;
    }

    var oldclass = obj.className;
    obj.className = oldclass+' widget_bap_errorobject';
    obj.onclick = function() {
        this.className=oldclass;
        if (this.nextSibling && 'widget_bap_errormessage' == this.nextSibling.className )
        {
            this.parentNode.removeChild(this.nextSibling);
        }
    };

    var span = document.createElement('span');
    span.className = 'widget_bap_errormessage';

    if (null == (notice = widget_bap_getString(message))) {
        notice = 'customize this message with addJsString(\''+message+'\')';
    }

    span.innerHTML = notice;

    if (obj.nextSibling)
    {
        obj.parentNode.insertBefore(span, obj.nextSibling);
    } else {
        obj.parentNode.appendChild(span);
    }
}


function __widget_bap_fieldSet(formobj, name, test) {
    for (var i =0 ; i < formobj.elements.length ; i++ )
    {
        objCtrl = formobj.elements[i];
        if (name == objCtrl.name && test(objCtrl)) {
            return true;
        }
    }
    return false;
}





/**
 * fonction pour valider un formulaire de saisie
 * for onsubmit form event
 */
function widget_bap_validateForm(formobj) {

    var objCtrl = null;
    var nb_alert = 0;

    for (var i =0 ; i < formobj.elements.length ; i++ )
    {
        objCtrl = formobj.elements[i];
        if (typeof objCtrl.className != 'undefined' && '' != objCtrl.className)
        {
            arr = objCtrl.className.split(' ');
            for (var j in arr)
            {

                switch(arr[j]) {

                    case 'widget_bap_hours_min':
                        if ('' != objCtrl.value && !/[0-9]{1,2}:[0-9]{1,2}/.test(objCtrl.value)) {
                            __widget_bap_markField(objCtrl, arr[j], nb_alert);
                            nb_alert++;
                        }
                        break;


                    case 'widget_bap_numeric':

                        if (isNaN(objCtrl.value)) {
                            __widget_bap_markField(objCtrl, arr[j], nb_alert);
                            nb_alert++;
                        }

                        break;

                    case 'widget_bap_notempty':
                        if ('' == objCtrl.value) {
                            __widget_bap_markField(objCtrl, arr[j], nb_alert);
                            nb_alert++;
                        }

                        break;

                    case 'widget_bap_fieldsetchecked':

                        if (!__widget_bap_fieldSet(formobj, objCtrl.name,
                                function(objCtrl) {
                                        return objCtrl.checked;
                                    }
                                )
                            )

                            {
                            if (!widget_bap_fieldsetchecked) {
                                __widget_bap_markField(objCtrl, arr[j], nb_alert);
                                nb_alert++;
                                var widget_bap_fieldsetchecked = 1;
                                }

                        }
                        break;
                }
            }
        }
    }


    return 0 == nb_alert;
}



/**
 * Initialize form validator
 */
function widget_bap_validateAllForms() {

    jQuery('form').submit(function(e) {
        if (false === widget_bap_validateForm(e.target)) {

            e.preventDefault();

            if (null != (notice = widget_bap_getString('widget_bap_invalid_form'))) {
                alert(notice);
            }
        }
    });


    // title on submit button are confirmation messages

    jQuery('input[type=\'submit\'][title]').click(function(e) {
        if (false === window.confirm(e.target.title)) {
            e.preventDefault();
        }
    });
}




// INIT


jQuery(document).ready(function() {
    widget_bap_validateAllForms();
});

