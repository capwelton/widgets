
function widget_selectInit(domNode) {
	
	jQuery(domNode).find('.widget-select').not('.widget-init-done').each(function() {
		var select = jQuery(this);
		select.addClass('widget-init-done');

		
		var metadata = window.babAddonWidgets.getMetadata(select.attr('id'));
		if (metadata.optgroupselect)
		{
			var val = select.find('option:selected').attr('value');
			var optgroupselect = jQuery('#'+metadata.optgroupselect);
			
			// dump optgroups in memory
			window.babAddonWidgetSelect = new Object();
			 
			
			
			select.find('option').each(function() {
				var option = jQuery(this);
				var optgroup = option.parent('optgroup').attr('label');
				
				if (!optgroup)
				{
					return;
				}
				
				if (!window.babAddonWidgetSelect[optgroup])
				{
					window.babAddonWidgetSelect[optgroup] = new Array();
				}
				
				if ('' != option.attr('value'))
				{
					window.babAddonWidgetSelect[optgroup].push([option.attr('value'), option.text()]);
				}
			});
			
			
			
			optgroupselect.change(function() {
				
				var optgroup = jQuery(this).find('option:selected').text();
				
				select.empty();
				select.append('<option value=""></option>');
				
				if (!window.babAddonWidgetSelect[optgroup])
				{
					// build all optgroups
					for (var optgroup in window.babAddonWidgetSelect)
					{
						var oo = select.append('<optgroup label="'+optgroup+'"></optgroup>');
						for(var i =0; i < window.babAddonWidgetSelect[optgroup].length; i++)
						{
							var pair = window.babAddonWidgetSelect[optgroup][i];
							oo.append('<option value="'+pair[0]+'">'+pair[1]+'</option>');
						}
					}
					
					select.val(val);
					return 0;
				}
				
				
				
				
				for(var i =0; i < window.babAddonWidgetSelect[optgroup].length; i++)
				{
					pair = window.babAddonWidgetSelect[optgroup][i];
					option = jQuery('<option value="'+pair[0]+'">'+pair[1]+'</option>');
					if (pair[0] == val)
					{
						option.prop('selected', true);
					}
					select.append(option);
				}
			});
			
			
			optgroupselect.change();
			
			
		}
		
		
	});

}


window.bab.addInitFunction(widget_selectInit);


