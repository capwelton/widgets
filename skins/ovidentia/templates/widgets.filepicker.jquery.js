/**
* AJAX Upload ( http://valums.com/ajax-upload/ )
* Copyright (c) Andris Valums
* Licensed under the MIT license ( http://valums.com/mit-license/ )
* Thanks to Gary Haran, David Mark, Corey Burns and others for contributions
*/
(function () {
    /* global window */
    /* jslint browser: true, devel: true, undef: true, nomen: true, bitwise: true, regexp: true, newcap: true, immed: true */

    /**
* Wrapper for FireBug's console.log
*/
    function log(){
        if (typeof(console) != 'undefined' && typeof(console.log) == 'function'){
            Array.prototype.unshift.call(arguments, '[Ajax Upload]');
            console.log( Array.prototype.join.call(arguments, ' '));
        }
    }

    /**
* Attaches event to a dom element.
* @param {Element} el
* @param type event name
* @param fn callback This refers to the passed element
*/
    function addEvent(el, type, fn){
        if (el.addEventListener) {
            el.addEventListener(type, fn, false);
        } else if (el.attachEvent) {
            el.attachEvent('on' + type, function(){
                fn.call(el);
});
} else {
            throw new Error('not supported or DOM not loaded');
        }
    }

    /**
* Attaches resize event to a window, limiting
* number of event fired. Fires only when encounteres
* delay of 100 after series of events.
*
* Some browsers fire event multiple times when resizing
* http://www.quirksmode.org/dom/events/resize.html
*
* @param fn callback This refers to the passed element
*/
    function addResizeEvent(fn){
        var timeout;

addEvent(window, 'resize', function(){
            if (timeout){
                clearTimeout(timeout);
            }
            timeout = setTimeout(fn, 100);
        });
    }

    // Needs more testing, will be rewriten for next version
    // getOffset function copied from jQuery lib (http://jquery.com/)
    if (document.documentElement.getBoundingClientRect){
        // Get Offset using getBoundingClientRect
        // http://ejohn.org/blog/getboundingclientrect-is-awesome/
        var getOffset = function(el){
            var box = el.getBoundingClientRect();
            var doc = el.ownerDocument;
            var body = doc.body;
            var docElem = doc.documentElement; // for ie
            var clientTop = docElem.clientTop || body.clientTop || 0;
            var clientLeft = docElem.clientLeft || body.clientLeft || 0;

            // In Internet Explorer 7 getBoundingClientRect property is treated as physical,
            // while others are logical. Make all logical, like in IE8.
            var zoom = 1;
            if (body.getBoundingClientRect) {
                var bound = body.getBoundingClientRect();
                zoom = (bound.right - bound.left) / body.clientWidth;
            }

            if (zoom > 1) {
                clientTop = 0;
                clientLeft = 0;
            }

            var top = box.top / zoom + (window.pageYOffset || docElem && docElem.scrollTop / zoom || body.scrollTop / zoom) - clientTop, left = box.left / zoom + (window.pageXOffset || docElem && docElem.scrollLeft / zoom || body.scrollLeft / zoom) - clientLeft;

            return {
                top: top,
                left: left
            };
        };
    } else {
        // Get offset adding all offsets
        var getOffset = function(el){
            var top = 0, left = 0;
            do {
                top += el.offsetTop || 0;
                left += el.offsetLeft || 0;
                el = el.offsetParent;
            } while (el);

            return {
                left: left,
                top: top
            };
        };
    }

    /**
* Returns left, top, right and bottom properties describing the border-box,
* in pixels, with the top-left relative to the body
* @param {Element} el
* @return {Object} Contains left, top, right,bottom
*/
    function getBox(el){
        var left, right, top, bottom;
        var offset = getOffset(el);
        left = offset.left;
        top = offset.top;

        right = left + el.offsetWidth;
        bottom = top + el.offsetHeight;

        return {
            left: left,
            right: right,
            top: top,
            bottom: bottom
        };
    }

    /**
* Helper that takes object literal
* and add all properties to element.style
* @param {Element} el
* @param {Object} styles
*/
    function addStyles(el, styles){
        for (var name in styles) {
            if (styles.hasOwnProperty(name)) {
                el.style[name] = styles[name];
            }
        }
    }

    /**
* Function places an absolutely positioned
* element on top of the specified element
* copying position and dimentions.
* @param {Element} from
* @param {Element} to
*/
    function copyLayout(from, to){
var box = getBox(from);

        addStyles(to, {
position: 'absolute',
left : box.left + 'px',
top : box.top + 'px',
width : from.offsetWidth + 'px',
height : from.offsetHeight + 'px'
});
    }

    /**
* Creates and returns element from html chunk
* Uses innerHTML to create an element
*/
    var toElement = (function(){
        var div = document.createElement('div');
        return function(html){
            div.innerHTML = html;
            var el = div.firstChild;
            return div.removeChild(el);
        };
    })();

    /**
* Function generates unique id
* @return unique id
*/
    var getUID = (function(){
        var id = 0;
        return function(){
            return 'ValumsAjaxUpload' + id++;
        };
    })();

    /**
* Get file name from path
* @param {String|Array} file path to file
* @return filename
*/
    function fileFromPath(file){

        if (typeof file == 'string')
        {
            return file.replace(/.*(\/|\\)/, "");
        }

        for (var i = 0; i < file.length; i++) {
            file[i].name.replace(/.*(\/|\\)/, "");
        }

        return file;
    }

    /**
* Get file extension lowercase
* @param {String} file name
* @return file extenstion
*/
    function getExt(file){

        if (typeof file == 'string')
        {
            return (-1 !== file.indexOf('.')) ? file.replace(/.*[.]/, '') : '';
        }

        return '';
    }

    function hasClass(el, name){
        var re = new RegExp('\\b' + name + '\\b');
        return re.test(el.className);
    }
    function addClass(el, name){
        if ( ! hasClass(el, name)){
            el.className += ' ' + name;
        }
    }
    function removeClass(el, name){
        var re = new RegExp('\\b' + name + '\\b');
        el.className = el.className.replace(re, '');
    }

    function removeNode(el){
        el.parentNode.removeChild(el);
    }

    /**
* Easy styling and uploading
* @constructor
* @param button An element you want convert to
* upload button. Tested dimentions up to 500x500px
* @param {Object} options See defaults below.
*/
    window.AjaxUpload = function(button, options){
        this._settings = {
            multiple: false,
            // Location of the server-side upload script
            action: 'upload.php',
            // File upload name
            name: 'userfile',
            // Additional data to send
            data: {},
            // Submit file as soon as it's selected
            autoSubmit: true,
            // The type of data that you're expecting back from the server.
            // html and xml are detected automatically.
            // Only useful when you are using json data as a response.
            // Set to "json" in that case.
            responseType: false,
            // Class applied to button when mouse is hovered
            hoverClass: 'hover',
            // Class applied to button when AU is disabled
            disabledClass: 'disabled',
            // When user selects a file, useful with autoSubmit disabled
            // You can return false to cancel upload
            onChange: function(file, extension){
            },
            // Callback to fire before file is uploaded
            // You can return false to cancel upload
            onSubmit: function(file, extension){
            },
            // Fired when file upload is completed
            // WARNING! DO NOT USE "FALSE" STRING AS A RESPONSE!
            onComplete: function(file, response){
            }
        };

        // Merge the users options with our defaults
        for (var i in options) {
            if (options.hasOwnProperty(i)){
                this._settings[i] = options[i];
            }
        }

        // button isn't necessary a dom element
        if (button.jquery){
            // jQuery object was passed
            button = button[0];
        } else if (typeof button == "string") {
            if (/^#.*/.test(button)){
                // If jQuery user passes #elementId don't break it
                button = button.slice(1);
            }

            button = document.getElementById(button);
        }

        if ( ! button || button.nodeType !== 1){
            throw new Error("Please make sure that you're passing a valid element");
        }

        if ( button.nodeName.toUpperCase() == 'A'){
            // disable link
            addEvent(button, 'click', function(e){
                if (e && e.preventDefault){
                    e.preventDefault();
                } else if (window.event){
                    window.event.returnValue = false;
                }
            });
        }

        // DOM element
        this._button = button;
        // DOM element
        this._input = null;
        // If disabled clicking on button won't do anything
        this._disabled = false;

        // if the button was disabled before refresh if will remain
        // disabled in FireFox, let's fix it
        this.enable();

        this._rerouteClicks();
    };

    // assigning methods to our class
    AjaxUpload.prototype = {
        setData: function(data){
            this._settings.data = data;
        },
        disable: function(){
            addClass(this._button, this._settings.disabledClass);
            this._disabled = true;

            var nodeName = this._button.nodeName.toUpperCase();
            if (nodeName == 'INPUT' || nodeName == 'BUTTON'){
                this._button.setAttribute('disabled', 'disabled');
            }

            // hide input
            if (this._input){
                // We use visibility instead of display to fix problem with Safari 4
                // The problem is that the value of input doesn't change if it
                // has display none when user selects a file
                this._input.parentNode.style.visibility = 'hidden';
            }
        },
        enable: function(){
            removeClass(this._button, this._settings.disabledClass);
            this._button.removeAttribute('disabled');
            this._disabled = false;

        },
        /**
* Creates invisible file input
* that will hover above the button
* <div><input type='file' /></div>
*/
        _createInput: function(){
            var self = this;

            var input = document.createElement("input");
            input.setAttribute('type', 'file');

            if (self._settings.multiple){
                input.setAttribute('name', this._settings.name + '[]');
                input.setAttribute('multiple', 'multiple');
            } else {
                input.setAttribute('name', this._settings.name);
            }

            addStyles(input, {
                'position' : 'absolute',
                // in Opera only 'browse' button
                // is clickable and it is located at
                // the right side of the input
                'right' : 0,
                'margin' : 0,
                'padding' : 0,
                'fontSize' : '480px',
                'cursor' : 'pointer',
                'zIndex' : 2000
            });

            var div = document.createElement("div");
            addStyles(div, {
                'display' : 'block',
                'position' : 'absolute',
                'overflow' : 'hidden',
                'margin' : 0,
                'padding' : 0,
                'opacity' : 0,
                // Make sure browse button is in the right side
                // in Internet Explorer
                'direction' : 'ltr',
                //Max zIndex supported by Opera 9.0-9.2
                'zIndex': 2147483583
            });

            // Make sure that element opacity exists.
            // Otherwise use IE filter
            if ( div.style.opacity !== "0") {
                if (typeof(div.filters) == 'undefined'){
                    throw new Error('Opacity not supported by the browser');
                }
                div.style.filter = "alpha(opacity=0)";
            }

            addEvent(input, 'change', function(){

                if ( ! input || input.value === ''){
                    return;
                }

                // Get filename from input, required
                // as some browsers have path instead of it

                if (input.files)
                {
                    var file = fileFromPath(input.files);
                } else {
                    var file = fileFromPath(input.value);
                }

                if (false === self._settings.onChange.call(self, file, getExt(file))){
                    self._clearInput();
                    return;
                }

                // Submit form when value is changed
                if (self._settings.autoSubmit) {
                    self.submit();
                }
            });

            addEvent(input, 'mouseover', function(){
                addClass(self._button, self._settings.hoverClass);
            });

            addEvent(input, 'mouseout', function(){
                removeClass(self._button, self._settings.hoverClass);

                // We use visibility instead of display to fix problem with Safari 4
                // The problem is that the value of input doesn't change if it
                // has display none when user selects a file
                input.parentNode.style.visibility = 'hidden';

            });

            div.appendChild(input);
            document.body.appendChild(div);

            this._input = input;
        },
        _clearInput : function(){
            if (!this._input){
                return;
            }

            // this._input.value = ''; Doesn't work in IE6
            removeNode(this._input.parentNode);
            this._input = null;
            this._createInput();

            removeClass(this._button, this._settings.hoverClass);
        },
        /**
         * Function makes sure that when user clicks upload button,
         * the this._input is clicked instead
         */
        _rerouteClicks: function(){
            var self = this;

            // IE will later display 'access denied' error
            // if you use using self._input.click()
            // other browsers just ignore click()

            self.touchStart = false; 
            addEvent(self._button, 'mouseover', function(){
            	if (!self.touchStart) {
	                if (self._disabled){
	                    return;
	                }
	
	                if ( ! self._input){
	                    self._createInput();
	                }
	
	                var div = self._input.parentNode;
	                copyLayout(self._button, div);
	                div.style.visibility = 'visible';
            	}

            });
            //FIX ISSUE WITH SOME MOBILE BROWSER THAT DOES NOT SEND MOUSEOVER EVENT
            addEvent(self._button, 'touchstart', function(){
            	self.touchStart = true;
            	if (self._disabled){
                    return;
                }

                if ( ! self._input){
                    self._createInput();
                }

                var div = self._input.parentNode;
                copyLayout(self._button, div);
                div.style.visibility = 'visible';
                
                //In case we are in a dialog, we need to set the focus on it
                //Otherwise, if the currentFocus is not in the visible page (scrolled on top or bottom)
                //clicking on the button will autoscroll on the focused element and will not open the
                //file selector
            	var dialog = self._button.closest('.ui-dialog');
            	if(dialog){
            		//We are in a dialog, set the focus on it
            		dialog.focus();
            	}
            });


            // commented because we now hide input on mouseleave
            /**
             * When the window is resized the elements
             * can be misaligned if button position depends
             * on window size
             */
            //addResizeEvent(function(){
            // if (self._input){
            // copyLayout(self._button, self._input.parentNode);
            // }
            //});

        },
        /**
         * Creates iframe with unique name
         * @return {Element} iframe
         */
        _createIframe: function(){
            // We can't use getTime, because it sometimes return
            // same value in safari :(
            var id = getUID();

            // We can't use following code as the name attribute
            // won't be properly registered in IE6, and new window
            // on form submit will open
            // var iframe = document.createElement('iframe');
            // iframe.setAttribute('name', id);

            var iframe = toElement('<iframe src="javascript:false;" name="' + id + '" />');
            // src="javascript:false; was added
            // because it possibly removes ie6 prompt
            // "This page contains both secure and nonsecure items"
            // Anyway, it doesn't do any harm.
            iframe.setAttribute('id', id);

            iframe.style.display = 'none';
            document.body.appendChild(iframe);

            return iframe;
        },
        /**
         * Creates form, that will be submitted to iframe
         * @param {Element} iframe Where to submit
         * @return {Element} form
         */
        _createForm: function(iframe){
            var settings = this._settings;

            // We can't use the following code in IE6
            // var form = document.createElement('form');
            // form.setAttribute('method', 'post');
            // form.setAttribute('enctype', 'multipart/form-data');
            // Because in this case file won't be attached to request
            var form = toElement('<form method="post" enctype="multipart/form-data"></form>');

            form.setAttribute('action', settings.action);
            form.setAttribute('target', iframe.name);
            form.style.display = 'none';
            document.body.appendChild(form);

            // Create hidden input element for each data key
            for (var prop in settings.data) {
                if (settings.data.hasOwnProperty(prop)){
                    var el = document.createElement("input");
                    el.setAttribute('type', 'hidden');
                    el.setAttribute('name', prop);
                    el.setAttribute('value', settings.data[prop]);
                    form.appendChild(el);
                }
            }
            var el = document.createElement("input");
            el.setAttribute('type', 'hidden');
            el.setAttribute('name', 'testencoding');
            el.setAttribute('value', '&#9820;');
            form.appendChild(el);
            return form;
        },
        /**
         * Gets response from iframe and fires onComplete event when ready
         * @param iframe
         * @param file Filename to use in onComplete callback
         */
        _getResponse : function(iframe, file){
            // getting response
            var toDeleteFlag = false, self = this, settings = this._settings;

            addEvent(iframe, 'load', function(){

                if (// For Safari
                    iframe.src == "javascript:'%3Chtml%3E%3C/html%3E';" ||
                    // For FF, IE
                    iframe.src == "javascript:'<html></html>';"){
                        // First time around, do not delete.
                        // We reload to blank page, so that reloading main page
                        // does not re-submit the post.

                        if (toDeleteFlag) {
                            // Fix busy state in FF3
                            setTimeout(function(){
                                removeNode(iframe);
                            }, 0);
                        }

                        return;
                }

                var doc = iframe.contentDocument ? iframe.contentDocument : window.frames[iframe.id].document;

                // fixing Opera 9.26,10.00
                if (doc.readyState && doc.readyState != 'complete') {
                   // Opera fires load event multiple times
                   // Even when the DOM is not ready yet
                   // this fix should not affect other browsers
                   return;
                }

                // fixing Opera 9.64
                if (doc.body && doc.body.innerHTML == "false") {
                    // In Opera 9.64 event was fired second time
                    // when body.innerHTML changed from false
                    // to server response approx. after 1 sec
                    return;
                }

                var response;

                if (doc.XMLDocument) {
                    // response is a xml document Internet Explorer property
                    response = doc.XMLDocument;
                } else if (doc.body){
                    // response is html document or plain text
                    response = doc.body.innerHTML;

                    if (settings.responseType && settings.responseType.toLowerCase() == 'json') {
                        // If the document was sent as 'application/javascript' or
                        // 'text/javascript', then the browser wraps the text in a <pre>
                        // tag and performs html encoding on the contents. In this case,
                        // we need to pull the original text content from the text node's
                        // nodeValue property to retrieve the unmangled content.
                        // Note that IE6 only understands text/html
                        if (doc.body.firstChild && doc.body.firstChild.nodeName.toUpperCase() == 'PRE') {
                            doc.normalize();
                            response = doc.body.firstChild.firstChild.nodeValue;
                        }

                        if (response) {
                            response = eval("(" + response + ")");
                        } else {
                            response = {};
                        }
                    }
                } else {
                    // response is a xml document
                    response = doc;
                }



                // Reload blank page, so that reloading main page
                // does not re-submit the post. Also, remember to
                // delete the frame
                toDeleteFlag = true;

                // Fix IE mixed content issue
                iframe.src = "javascript:'<html></html>';";

                settings.onComplete(file, response);
            });
        },
        /**
         * Upload file contained in this._input
         */
        submit: function(){
            var self = this, settings = this._settings;

            if ( ! this._input || this._input.value === ''){
                return;
            }

            if (this._input.files)
            {
                var file = fileFromPath(this._input.files);


            } else {
                var file = fileFromPath(this._input.value);
            }

            // user returned false to cancel upload
            if (false === settings.onSubmit.call(this, file, getExt(file))){
                this._clearInput();
                return;
            }

            if (typeof(window.FormData) !== 'undefined' && this._input.files) {

                // try uploading with FormData
                var inputname = this._input.name;
                if ('[]' == inputname.substr((inputname.length - 2),2))
                {
                    inputname = inputname.substr(0,(inputname.length - 2)); // this is the widget ID
                }

                window.babAddonWidgets.filepicker_uploadWithFormData(inputname, this._input.files);
                return;
            }

            // sending request
            var iframe = this._createIframe();
            var form = this._createForm(iframe);

            // assuming following structure
            // div -> input type='file'
            removeNode(this._input.parentNode);
            removeClass(self._button, self._settings.hoverClass);

            form.appendChild(this._input);

            form.submit();

            // request set, clean up
            removeNode(form); form = null;
            removeNode(this._input); this._input = null;

            // Get response from iframe and fire onComplete event when ready
            this._getResponse(iframe, file);

            // get ready for next request
            this._createInput();
        }
    };
})();



/**
 * This is the event handler for drop event for browser supporting file.getAsBinary() (FF3.6).
 * @deprecated use widget_filePickerManageDropWithFormData instead
 */
function widget_filePickerManageDropWithGetAsBinary(event)
{
    event.preventDefault();

    var wId = window.babAddonWidgets.getMetadata(this.id).filepicker;

    var data = event.dataTransfer;

    var boundary = '------multipartformboundary' + (new Date).getTime();
    var dashdash = '--';
    var crlf     = '\r\n';

    // Build RFC2388 string.
    var builder = '';

    builder += dashdash;
    builder += boundary;
    builder += crlf;

    var meta = window.babAddonWidgets.getMetadata(wId);

    var xhr = new XMLHttpRequest();

    var fields = [
         { name: 'request_mode', value: 'ajax' },
         { name: 'widget_filepicker_job_uid', value: meta.uploadOrDeleteUid },
         { name: 'babCsrfProtect', value: meta.babCsrfProtect }
    ];

    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];

        builder += "Content-Disposition: form-data; name=\"" + field.name + "\"\r\n\r\n" + field.value + "\r\n"

        // Write boundary.
        builder += dashdash;
        builder += boundary;
        builder += crlf;
    }

    // For each dropped file.
    for (var i = 0; i < data.files.length; i++) {
        var file = data.files[i];

        // Generate headers.
        builder += 'Content-Disposition: form-data; name="' + wId + '[]"';
        if (file.fileName) {
            builder += '; filename="' + file.fileName + '"';
        }
        builder += crlf;

        builder += 'Content-Type: application/octet-stream';
        builder += crlf;
        builder += crlf;

        // Append binary data.
        builder += file.getAsBinary();
        builder += crlf;

        // Write boundary.
        builder += dashdash;
        builder += boundary;
        builder += crlf;
    }

    // Mark end of the request.
    builder += dashdash;
    builder += boundary;
    builder += dashdash;
    builder += crlf;


    var action = meta.selfpage;

    xhr.open("POST", action, true);

    xhr.setRequestHeader('content-type', 'multipart/form-data; boundary='
                + boundary);
    xhr.sendAsBinary(builder);

    xhr.onload = function(event) {
        var response = xhr.responseText;

        if ('OK' === response) {

            var meta = window.babAddonWidgets.getMetadata(wId);
            var uploadJs = meta.uploadJs;
            var uploadAction = meta.uploadAction;
            var ajaxAction = meta.ajaxAction;

            if (uploadJs) {
                for(var i =0; i < uploadJs.length; i++) {
                    var func = uploadJs[i];

                    eval(func+'(wId)');
                }
            }

            if (ajaxAction) {
                jQuery.ajax({
                    async: false,
                    url: ajaxAction,
                    dataType: 'json',
                    success: function (response) {
                        if (meta.ajaxActionReload) {
                            var nbAjaxActionReload = meta.ajaxActionReload.length;
                            for (var i = 0; i < nbAjaxActionReload; i++) {
                                var reloadedElement = document.getElementById(meta.ajaxActionReload[i]);
                                if (reloadedElement) {
                                    window.babAddonWidgets.reload(reloadedElement);
                                }
                            }
                        }
                    }
                });

            }

            else if (uploadAction) {
                jQuery.ajax({
                    async: false,
                    url: uploadAction,
                    dataType: 'json',
                    success: function (response) {
                        jQuery.each(response, function(key, html) {
                            jQuery('#' + key).html(html);
                            window.bab.init(jQuery('#' + key));
                        });
                    }
                });

            }

        } else if(response.length < 2048) {
              alert(response);
        } else {
              alert(window.babAddonWidgets.getMetadata(wId).msgerror);
        }
    };

    // Prevent FireFox opening the dragged file.
    event.stopPropagation();
}








window.babAddonWidgets.filepicker_uploadWithFormData = function(wId, files) {

    var count = files.length;
    form = new FormData();
    for (var i= 0; i < count; i++) {
        form.append(wId + "[]", files[i]);
    }

    var meta = window.babAddonWidgets.getMetadata(wId);

    form.append('request_mode', 'ajax');
    form.append('widget_filepicker_job_uid', meta.uploadOrDeleteUid);
    form.append('babCsrfProtect',  meta.babCsrfProtect);

    var xhr = new XMLHttpRequest();
    xhr.addEventListener("load", function() {
        window.babAddonWidgets.filepicker_complete(wId, files, xhr.responseText);
    }, false);

    xhr.addEventListener("error",  function() {
        alert('Failed');
    }, false);

    xhr.addEventListener('progress', function(e) {
        var done = e.position || e.loaded, total = e.totalSize || e.total;
        var progress = (Math.floor(done/total*1000)/10);

        if (progress == Number.POSITIVE_INFINITY || progress == Number.NEGATIVE_INFINITY)
        {
            //bug : "Infinity" in chrome
            progress = 100;
        }

        jQuery('#'+wId+' .progresscontainer div').css('width', progress+'%');

        // console.log('xhr.progress '+progress);

    }, false);

    if ( xhr.upload ) {
        xhr.upload.onprogress = function(e) {
            var done = e.position || e.loaded, total = e.totalSize || e.total;
            var progress = (Math.floor(done/total*1000)/10);

            jQuery('#'+wId+' .progresscontainer div').css('width', progress+'%');
            // console.log('xhr.upload.progress '+progress);
        };


    }


    var action = meta.selfpage;

    xhr.open("POST", action);
    xhr.send(form);

};




/**
 * This is the event handler for drop event for browser supporting FormData object.
 */
function widget_filePickerManageDropWithFormData(event)
{
    event.preventDefault();
    var files = event.dataTransfer.files;

    var meta = window.babAddonWidgets.getMetadata(this.id);
    var wId = this.id;
    if (meta.filepicker) {
        wId = meta.filepicker;
    }
    window.babAddonWidgets.filepicker_uploadWithFormData(wId, files);
    window.babAddonWidgets.filepicker_submit(wId, files);

    event.stopPropagation();
}



function widget_filePickerInit(domNode)
{
    //
    // Drag and drop
    //
    jQuery('.widget-filepicker-drop-target').not('.widget-filepicker-drop-target-init-done').each(function() {

        jQuery(this).addClass('.widget-filepicker-drop-target-init-done');
        if (typeof(this.addEventListener) !== "function") {
            return;
        }

        document.addEventListener(
            "dragover",
            function(event) {
                event.preventDefault();
                jQuery('.widget-filepicker-drop-target').addClass('widget-filepicker-drag');
            },
            true
        );
        document.addEventListener(
            "dragleave",
            function(event) {
                event.preventDefault();
                jQuery('.widget-filepicker-drop-target').removeClass('widget-filepicker-drag');
            },
            true
        );
        this.addEventListener(
            "dragover",
            function(event) {
                event.preventDefault();
                //jQuery(this).css('outline', '1px solid orange');
                jQuery(this).addClass('widget-filepicker-dragover');
            },
            true
        );
        this.addEventListener(
            "dragleave",
            function(event) {
                event.preventDefault();
                //jQuery(this).css('outline', '');
                jQuery(this).removeClass('widget-filepicker-dragover');
            },
            true
        );

        if (typeof(window.FormData) !== 'undefined') {
            this.addEventListener("drop", widget_filePickerManageDropWithFormData, true);
        } else {
            this.addEventListener("drop", widget_filePickerManageDropWithGetAsBinary, true);
        }

    });




    jQuery(domNode).find('.widget-filepicker input[type=file]').each(function() {


        var filepicker = jQuery(this);
        var wId = filepicker.get(0).parentNode.id;


        if (0 != jQuery('#'+wId+' .widget-filepicker-file').length) {
             // hide default image
             jQuery('#'+wId+' .widget-filepicker-default-image').hide();
        }


        var meta = window.babAddonWidgets.getMetadata(wId);
        var searchCombo = meta.googleSearchWidgets;
        var prefix = meta.googleSearchPrefix;
        var gravatar = meta.gravatarEmailId;

        if (null != searchCombo) {
            var suggest = jQuery('<span class="widget-filepicker-suggest" title="Google"></span>').click(function(event) {
                window.babAddonWidgets.filepicker_googleImageSuggest(wId, searchCombo, prefix);
            });
            filepicker.after(suggest);
        }

        if (null != gravatar) {
            var suggest = jQuery('<span class="widget-filepicker-gravatar" style="display:none" title="Gravatar"></span>').click(function(event) {
                window.babAddonWidgets.filepicker_setGravatar(wId, jQuery('#'+gravatar).val());
            });
            filepicker.after(suggest);

            // monitor email field

            var updatevisibility = function() {
                var f = jQuery('#'+gravatar);
                if (f.val() != '' && f.hasClass('widget-emaillineedit-valid'))
                {
                    if (suggest.is(':visible'))
                    {
                        suggest.css('opacity', 0.2);
                    }
                    window.babAddonWidgets.filepicker_GravatarShow(f, suggest);
                } else {
                    suggest.hide();
                }
            };


            jQuery('#'+gravatar).keyup(updatevisibility).change(updatevisibility);
            setTimeout(updatevisibility, 500);
        }

        // try to replace input uploader with a button
        var text = meta.title;
        filepicker.after('<span id="'+wId+'JsButton" class="widget-filepicker-add ui-state-default">'+text+'</span>');



        filepicker.remove();

        // add delete actions on existing files
        jQuery('#'+wId+' .widget-filepicker-file-delete').click(function() {
            window.babAddonWidgets.filepicker_click(wId, this);
        });

//		jQuery('#'+wId+'JsButton').on("click mousedown mouseup focus blur keydown change dblclick mousemove mouseover mouseout mousewheel keydown keyup keypress textInput touchstart touchmove touchend touchcancel resize scroll zoom select change submit reset",function(e){
//		     console.log(e);
//		}); 
        
        new AjaxUpload('#'+wId+'JsButton', {
          // Boolean to specify if the flepicker should allow selection
          // of multiple files.
          multiple: !window.babAddonWidgets.getMetadata(wId).one_file_mode,
          // Location of the server-side upload script
          // NOTE: You are not allowed to upload files to another domain
          action: window.babAddonWidgets.getMetadata(wId).selfpage,
          // File upload name
          name: wId,
          // Additional data to send
          data: {
              request_mode : 'ajax',
              widget_filepicker_job_uid: meta.uploadOrDeleteUid,
              babCsrfProtect: meta.babCsrfProtect
          },
          // Submit file after selection
          autoSubmit: true,
          // The type of data that you're expecting back from the server.
          // HTML (text) and XML are detected automatically.
          // Useful when you are using JSON data as a response, set to "json" in that case.
          // Also set server response type to text/html, otherwise it will not work in IE6
          responseType: false,
          // Fired after the file is selected
          // Useful when autoSubmit is disabled
          // You can return false to cancel upload
          // @param file basename of uploaded file
          // @param extension of that file
          onChange: function(file, extension) {
              var inputfile = jQuery('[name="'+wId+'"]');
              if (meta.imageCropper && inputfile[0].files && inputfile[0].files[0]) {
                  var fileup = inputfile[0].files[0];
                  var fr = new FileReader(); // FileReader instance
                  fr.onload = function () {
                      var cropper = jQuery('#'+meta.imageCropper).find('.widget-imagecropper');
                      cropper.attr("src", fr.result);
                      cropper.cropper('replace', fr.result);
                  };
                    //fr.readAsText( file );
                  fr.readAsDataURL(fileup);
              }
          },
          // Fired before the file is uploaded
          // You can return false to cancel upload
          // @param file basename of uploaded file
          // @param extension of that file
          onSubmit: function(file, extension) {
              return window.babAddonWidgets.filepicker_submit(wId, file);
              // this.disable();
          },
          // Fired when file upload is completed
          // WARNING! DO NOT USE "FALSE" STRING AS A RESPONSE!
          // @param file basename of uploaded file
          // @param response server response
          onComplete: function(file, response) {
              // this.enable();
              window.babAddonWidgets.filepicker_complete(wId, file, response);
          }

       });

    });

}




window.babAddonWidgets.filepicker_submit = function(wId, file) {

    var meta = window.babAddonWidgets.getMetadata(wId);
    var duplicate = false;

    if (window.File && window.FileReader && window.FileList && window.Blob)  {
        var fileSize = file[0].size;
        
        var maxSize = meta.maxsize;

        if (fileSize > maxSize) {
            alert(meta.msgerror_toobig.replace(/%1\$s/, Math.floor(fileSize / 1024)).replace(/%2\$s/, Math.floor(maxSize / 1024)));
            return false;
        }
    }

    if (false == meta.one_file_mode) {
        jQuery('#'+wId+' .widget-filepicker-values input').each(function() {
            if (typeof file == 'string') {
                if (jQuery(this).attr('value') == file) {
                    duplicate = !window.confirm(meta.msgerror_duplicate.replace(/%s/, file));
                }
            } else {
                for (var i = 0; i < file.length; i++) {
                    if (jQuery(this).attr('value') == file[i].name) {
                        duplicate = !window.confirm(meta.msgerror_duplicate.replace(/%s/, file[i].name));
                    }
                }
            }
        });
    }

    if (duplicate) {
        return false;
    }

    jQuery('#'+wId).addClass('widget-filepicker-uploading');

    if (typeof(window.FormData) !== 'undefined') {
        jQuery('#'+wId).append(
            '<div id="'+wId+'loading" class="widget-filepicker-loadingprogress"><div class="'+meta.loadingclasses.join(' ')+'">'+meta.loading+'</div><div class="progresscontainer"><div></div></div></div>'
        );
    }

    return true;
};



window.babAddonWidgets.filepicker_complete = function(wId, file, response) {

      jQuery('#'+wId).removeClass('widget-filepicker-uploading');

      // remove loading item if exists
      setTimeout(function() {
          jQuery('#'+wId+'loading').fadeOut(1000, function() {
              jQuery('#'+wId+'loading').remove();
          });
      }, 500);


      if ('OK' === response) {

          var meta = window.babAddonWidgets.getMetadata(wId);
          var uploadJs = meta.uploadJs;
          var uploadAction = meta.uploadAction;
          var uploadActionAjax = meta.uploadActionAjax;


          var display_existing_files = window.babAddonWidgets.getMetadata(wId).display_existing_files;

          if (uploadJs) {
                for(var i =0; i < uploadJs.length; i++) {
                    var func = uploadJs[i];

                    eval(func+'(wId)');
                }
          }

          if (meta.ajaxAction) {
              var item = document.getElementById('#'+wId);
              window.babAddonWidgets.ajax(meta.ajaxAction, item, meta.ajaxActionReload);
          }

          else if (uploadAction) {
                  if (uploadActionAjax) {
                    jQuery.ajax({
                        async: false,
                        url: uploadAction,
                        success: function (response) {
                            jQuery.each(response, function(key, html) {
                                jQuery('#' + key).html(html);
                                window.bab.init(jQuery('#' + key));
                            });
                        }
                    });
                  } else {
                      window.location.href = uploadAction;
                  }

          } else if (display_existing_files) {

              if (typeof file == 'string')
              {
                  window.babAddonWidgets.filepicker_addFile(wId, file);
              } else {
                  for (var i = 0; i < file.length; i++)
                  {
                      window.babAddonWidgets.filepicker_addFile(wId, file[i].name);
                  }
              }

              // if file come from a drag & drop
              jQuery('.widget-filepicker-drop-target').css('outline', '');
          }

      } else if(response.length < 2048) {
          alert(response);
      } else {
          alert(window.babAddonWidgets.getMetadata(wId).msgerror);
      }

};





window.babAddonWidgets.filepicker_addFile = function(wId, file) {

      var meta = window.babAddonWidgets.getMetadata(wId);

      // remove old file if one_file_mode

      if (meta.one_file_mode) {

          jQuery('#'+wId+' .widget-filepicker-files div').remove();
          jQuery('#'+wId+' .widget-filepicker-values input').remove();
      }

      // hide default image

      jQuery('#'+wId+' .widget-filepicker-default-image').hide();


      // add file as value

      newContener = jQuery('<div class="'+window.babAddonWidgets.getMetadata(wId).fileclasses.join(' ')+'"></div>');
      newfilelabel = jQuery('<a class="widget-filepicker-file-label" href="?tg=addon/widgets/files&idx=download&uid=' + window.babAddonWidgets.getMetadata(wId).uploadOrDeleteUid + '&file='+file+'" title="'+file+'">'+file+'</a>');
      newfiledeletebutton = jQuery('<span class="'+window.babAddonWidgets.getMetadata(wId).deleteclasses.join(' ')+'" title="'+file+'"></span>');
      if (meta.filenamewidth) {
          newfilelabel.css('width', meta.filenamewidth);
      }

      newContener.prepend(newfiledeletebutton);
      newContener.prepend(newfilelabel);

      newfiledeletebutton.click(function() {
          window.babAddonWidgets.filepicker_click(wId, this);
      });



      jQuery('#'+wId+' .widget-filepicker-files').append(newContener);

      jQuery('#'+wId+' .widget-filepicker-values').append(
          '<input type="hidden" name="'+window.babAddonWidgets.getMetadata(wId).fullname+'[files][]" value="'+file+'" />'
      );


      // get thumbnail

      window.babAddonWidgets.filepicker_getThumbnail(wId, file, newContener);
};







window.babAddonWidgets.filepicker_getThumbnail = function(wId, file, newContener) {

    var meta = window.babAddonWidgets.getMetadata(wId);
    jQuery.ajax({
       url: meta.selfpage,
       data: 'thumbfile=' + encodeURIComponent(file)
           + '&widget_filepicker_job_uid=' + meta.thumbnailUid,
       success: function(response){
            if ('OK' == response) {
                return false;
            } else {
                if (response.length < 2048) {
                     // the #Date force reload of the thumbnail il the filename is the same as the previous one
                    var imgUrl = response + '#' + new Date().getTime();
                    newContener.prepend(jQuery('<div class="widget-layout-vbox-item" style="margin: auto; width: ' + meta.thumbWidth + 'px; height: ' + meta.thumbHeight + 'px; background: url(\'' + imgUrl + '\') no-repeat center;"> </div>'));
                } else {
                    alert('thumbnail error');
                }
            }
       }
    });

};




/**
 * delete
 */
window.babAddonWidgets.filepicker_click = function(wId, fileDom) {

    if (window.confirm(window.babAddonWidgets.getMetadata(wId).msgdelete)) {

        //var filename = jQuery.trim(jQuery(fileDom).text());
        var filename = fileDom.title;
        console.debug(fileDom);

        jQuery.ajax({
           url: window.babAddonWidgets.getMetadata(wId).selfpage,
           data: 'deletefile='+encodeURIComponent(filename)+'&widget_filepicker_job_uid='+window.babAddonWidgets.getMetadata(wId).uploadOrDeleteUid,
           success: function(response){
                if ('OK' == response) {

                    jQuery('#'+wId+' input[type=hidden]').each(function() {

                        if (filename == jQuery(this).attr('value')) {
                            jQuery(this).remove();

                            jQuery('#'+wId+' .widget-filepicker-file').each(function() {

                                //if (filename == jQuery.trim(jQuery(this).find('.widget-filepicker-file-delete').text())) {
                                if (filename == jQuery.trim(jQuery(this).find('.widget-filepicker-file-delete').attr('title'))) {
                                    jQuery(this).remove();
                                }
                            });
                        }
                    });



                    if (0 == jQuery('#'+wId+' .widget-filepicker-file').length) {
                         // display default image
                         jQuery('#'+wId+' .widget-filepicker-default-image').show();
                    }


                } else {
                    if(response.length < 2048) {
                        alert(response);
                    } else {
                        alert(window.babAddonWidgets.getMetadata(wId).msgerror_delete);
                    }
                }
           }
        });

    }
};


window.babAddonWidgets.filepicker_getGravatarUrl = function(email) {

    var MD5=function(s){
        function L(k,d){
            return(k<<d)|(k>>>(32-d));
        }
        function K(G,k){
            var I,d,F,H,x;
            F=(G&2147483648);
            H=(k&2147483648);
            I=(G&1073741824);
            d=(k&1073741824);
            x=(G&1073741823)+(k&1073741823);
            if(I&d){
                return(x^2147483648^F^H);
            }
            if(I|d){
                if(x&1073741824){
                    return(x^3221225472^F^H);
                }else{
                    return(x^1073741824^F^H);
                }
            }else{
                return(x^F^H);
            }
        }
        function r(d,F,k){
            return(d&F)|((~d)&k);
        }
        function q(d,F,k){
            return(d&k)|(F&(~k));
        }
        function p(d,F,k){
            return(d^F^k);
        }
        function n(d,F,k){
            return(F^(d|(~k)));
        }
        function u(G,F,aa,Z,k,H,I){
            G=K(G,K(K(r(F,aa,Z),k),I));
            return K(L(G,H),F);
        }
        function f(G,F,aa,Z,k,H,I){
            G=K(G,K(K(q(F,aa,Z),k),I));
            return K(L(G,H),F);
        }
        function D(G,F,aa,Z,k,H,I){
            G=K(G,K(K(p(F,aa,Z),k),I));
            return K(L(G,H),F);
        }
        function t(G,F,aa,Z,k,H,I){
            G=K(G,K(K(n(F,aa,Z),k),I));
            return K(L(G,H),F);
        }
        function e(G){
            var Z;
            var F=G.length;
            var x=F+8;
            var k=(x-(x%64))/64;
            var I=(k+1)*16;
            var aa=Array(I-1);
            var d=0;
            var H=0;
            while(H<F){
                Z=(H-(H%4))/4;d=(H%4)*8;
                aa[Z]=(aa[Z]|(G.charCodeAt(H)<<d));
                H++;
            }
            Z=(H-(H%4))/4;
            d=(H%4)*8;
            aa[Z]=aa[Z]|(128<<d);
            aa[I-2]=F<<3;
            aa[I-1]=F>>>29;
            return aa;
        }
        function B(x){
            var k="",F="",G,d;
            for(d=0;d<=3;d++){
                G=(x>>>(d*8))&255;
                F="0"+G.toString(16);
                k=k+F.substr(F.length-2,2);
            }
            return k;
        }
        function J(k){
            k=k.replace(/rn/g,"n");
            var d="";
            for(var F=0;F<k.length;F++){
                var x=k.charCodeAt(F);
                if(x<128){
                    d+=String.fromCharCode(x);
                }else{
                    if((x>127)&&(x<2048)){
                        d+=String.fromCharCode((x>>6)|192);
                        d+=String.fromCharCode((x&63)|128);
                    }else{
                        d+=String.fromCharCode((x>>12)|224);
                        d+=String.fromCharCode(((x>>6)&63)|128);
                        d+=String.fromCharCode((x&63)|128);
                    }
                }
            }
            return d;
        }
        var C=Array();
        var P,h,E,v,g,Y,X,W,V;
        var S=7,Q=12,N=17,M=22;
        var A=5,z=9,y=14,w=20;
        var o=4,m=11,l=16,j=23;
        var U=6,T=10,R=15,O=21;
        s=J(s);
        C=e(s);
        Y=1732584193;
        X=4023233417;
        W=2562383102;
        V=271733878;
        for(P=0;P<C.length;P+=16){
            h=Y;
            E=X;
            v=W;
            g=V;
            Y=u(Y,X,W,V,C[P+0],S,3614090360);
            V=u(V,Y,X,W,C[P+1],Q,3905402710);
            W=u(W,V,Y,X,C[P+2],N,606105819);
            X=u(X,W,V,Y,C[P+3],M,3250441966);
            Y=u(Y,X,W,V,C[P+4],S,4118548399);
            V=u(V,Y,X,W,C[P+5],Q,1200080426);
            W=u(W,V,Y,X,C[P+6],N,2821735955);
            X=u(X,W,V,Y,C[P+7],M,4249261313);
            Y=u(Y,X,W,V,C[P+8],S,1770035416);
            V=u(V,Y,X,W,C[P+9],Q,2336552879);
            W=u(W,V,Y,X,C[P+10],N,4294925233);
            X=u(X,W,V,Y,C[P+11],M,2304563134);
            Y=u(Y,X,W,V,C[P+12],S,1804603682);
            V=u(V,Y,X,W,C[P+13],Q,4254626195);
            W=u(W,V,Y,X,C[P+14],N,2792965006);
            X=u(X,W,V,Y,C[P+15],M,1236535329);
            Y=f(Y,X,W,V,C[P+1],A,4129170786);
            V=f(V,Y,X,W,C[P+6],z,3225465664);
            W=f(W,V,Y,X,C[P+11],y,643717713);
            X=f(X,W,V,Y,C[P+0],w,3921069994);
            Y=f(Y,X,W,V,C[P+5],A,3593408605);
            V=f(V,Y,X,W,C[P+10],z,38016083);
            W=f(W,V,Y,X,C[P+15],y,3634488961);
            X=f(X,W,V,Y,C[P+4],w,3889429448);
            Y=f(Y,X,W,V,C[P+9],A,568446438);
            V=f(V,Y,X,W,C[P+14],z,3275163606);
            W=f(W,V,Y,X,C[P+3],y,4107603335);
            X=f(X,W,V,Y,C[P+8],w,1163531501);
            Y=f(Y,X,W,V,C[P+13],A,2850285829);
            V=f(V,Y,X,W,C[P+2],z,4243563512);
            W=f(W,V,Y,X,C[P+7],y,1735328473);
            X=f(X,W,V,Y,C[P+12],w,2368359562);
            Y=D(Y,X,W,V,C[P+5],o,4294588738);
            V=D(V,Y,X,W,C[P+8],m,2272392833);
            W=D(W,V,Y,X,C[P+11],l,1839030562);
            X=D(X,W,V,Y,C[P+14],j,4259657740);
            Y=D(Y,X,W,V,C[P+1],o,2763975236);
            V=D(V,Y,X,W,C[P+4],m,1272893353);
            W=D(W,V,Y,X,C[P+7],l,4139469664);
            X=D(X,W,V,Y,C[P+10],j,3200236656);
            Y=D(Y,X,W,V,C[P+13],o,681279174);
            V=D(V,Y,X,W,C[P+0],m,3936430074);
            W=D(W,V,Y,X,C[P+3],l,3572445317);
            X=D(X,W,V,Y,C[P+6],j,76029189);
            Y=D(Y,X,W,V,C[P+9],o,3654602809);
            V=D(V,Y,X,W,C[P+12],m,3873151461);
            W=D(W,V,Y,X,C[P+15],l,530742520);
            X=D(X,W,V,Y,C[P+2],j,3299628645);
            Y=t(Y,X,W,V,C[P+0],U,4096336452);
            V=t(V,Y,X,W,C[P+7],T,1126891415);
            W=t(W,V,Y,X,C[P+14],R,2878612391);
            X=t(X,W,V,Y,C[P+5],O,4237533241);
            Y=t(Y,X,W,V,C[P+12],U,1700485571);
            V=t(V,Y,X,W,C[P+3],T,2399980690);
            W=t(W,V,Y,X,C[P+10],R,4293915773);
            X=t(X,W,V,Y,C[P+1],O,2240044497);
            Y=t(Y,X,W,V,C[P+8],U,1873313359);
            V=t(V,Y,X,W,C[P+15],T,4264355552);
            W=t(W,V,Y,X,C[P+6],R,2734768916);
            X=t(X,W,V,Y,C[P+13],O,1309151649);
            Y=t(Y,X,W,V,C[P+4],U,4149444226);
            V=t(V,Y,X,W,C[P+11],T,3174756917);
            W=t(W,V,Y,X,C[P+2],R,718787259);
            X=t(X,W,V,Y,C[P+9],O,3951481745);
            Y=K(Y,h);
            X=K(X,E);
            W=K(W,v);
            V=K(V,g);
        }
        var i=B(Y)+B(X)+B(W)+B(V);
        return i.toLowerCase();
    };
    return 'http://www.gravatar.com/avatar/' + MD5(email) + '.jpg?s=256&default=404';
};


window.babAddonWidgets.filepicker_setGravatar = function(wId, email) {
    var imageurl = window.babAddonWidgets.filepicker_getGravatarUrl(email);
    window.babAddonWidgets.filepicker_setFromUrl(wId, imageurl);
};


window.babAddonWidgets.filepicker_GravatarTimer = null;

window.babAddonWidgets.filepicker_GravatarShow = function(emailField, suggest)
{
    if (window.babAddonWidgets.filepicker_GravatarTimer !== null)
    {
        clearTimeout(window.babAddonWidgets.filepicker_GravatarTimer);
    }

    window.babAddonWidgets.filepicker_GravatarTimer = setTimeout(function() {

        jQuery.ajax({
            url: window.babAddonWidgets.filepicker_getGravatarUrl(emailField.val()),
            type:'HEAD',
            error: function()
            {
                suggest.hide();
            },
            success: function()
            {
                suggest.show();
                suggest.css('opacity', 1);
            }
        });
    }, 1000);


};



/**
 * Google search suggest images
 *
 *
 */
window.babAddonWidgets.filepicker_googleImageSuggest = function(wId, searchCombo, prefix) {

    // get keywords


    var keywords = '';

    for (var i = 0; i < searchCombo.length; i++) {
        var field = jQuery('#'+searchCombo[i]);

        var search = null;

        if (null == field || field.length == 0) {
            // the widget does not exists, we use the string as an additional keyword
            search = searchCombo[i];
        } else {
            search = field.attr('value');

            if (null == search) {
                // the value attribute probably not exits on the widget, we try with the inner text
                search = field.text();
            }
        }


        if (null != search && '' != search) {
            if ('' != keywords) {
                keywords += ' ';
            }
            if (prefix) {
                keywords += prefix;
            }
            keywords += search;
        }
    }

    if ('' == keywords) {
        alert(window.babAddonWidgets.getMetadata(wId).suggestMissingKeywordsError);
        return;
    }

    keywords = '"'+keywords+'"';


    // load google API if not loaded


    var e = document.createElement("script");
    e.src = 'http://www.google.com/jsapi?key='+window.babAddonWidgets.getMetadata(wId).googleSearchApiKey;
    e.type = "text/javascript";
    document.getElementsByTagName("head")[0].appendChild(e);

    window.babAddonWidgets.filepicker_waitGoogle(wId, keywords);

};


/**
 * Wait will google API js is loading
 */
window.babAddonWidgets.filepicker_waitGoogle = function(wId, keywords) {

    if (typeof google != 'undefined' && typeof google.load != 'undefined') {
        window.babAddonWidgets.filepicker_googleImageSuggestOpen(wId, keywords);
        return;
    }

    setTimeout(function() { window.babAddonWidgets.filepicker_waitGoogle(wId, keywords); }, 200);
};



/**
 * Add file to file picker folder from url
 */
window.babAddonWidgets.filepicker_setFromUrl = function(wId, url)
{
    var metadata = window.babAddonWidgets.getMetadata(wId);

    jQuery.ajax({
       url: metadata.selfpage,
       data: 'addFileFromUrl='+encodeURIComponent(url)+'&widget_filepicker_job_uid='+metadata.uploadOrDeleteUid,
       success: function(response){
            if ('OK' == response.substring(0,2)) {

                filename = response.substring(2);
                window.babAddonWidgets.filepicker_addFile(wId, filename);
            } else {
                if(response.length < 2048) {
                    alert(response);
                } else {
                    alert('error, the widget library is probably not accessible or not installed');
                }
            }
       }
    });
};



/**
 * search on google and open result dialog
 */
window.babAddonWidgets.filepicker_googleImageSuggestOpen = function(wId, keywords) {

    google.load("search", '1', {'callback' : function() {

        var metadata = window.babAddonWidgets.getMetadata(wId);

        // Our ImageSearch instance.
          imageSearch = new google.search.ImageSearch();

          imageSearch.setResultSetSize(google.search.Search.LARGE_RESULTSET);

          if (size = metadata.googleSearchSize) {
              var sizeRestriction = eval('google.search.ImageSearch.'+size);
              imageSearch.setRestriction(google.search.ImageSearch.RESTRICT_IMAGESIZE, sizeRestriction);
          }

          if (type = metadata.googleSearchType) {
              var typeRestriction = eval('google.search.ImageSearch.'+type);
              imageSearch.setRestriction(google.search.ImageSearch.RESTRICT_IMAGETYPE, typeRestriction);
          }

          imageSearch.setSearchCompleteCallback(this, function(searchControl, searcher) {


              var filepicker = jQuery('#'+wId);
              var dialog = jQuery('<div title="'+metadata.suggestTitle+'" class="widget-filepicker-googlesuggestions"></div>');
              filepicker.append(dialog);
              dialog.dialog();

            // Check that we got results
              if (imageSearch.results && imageSearch.results.length > 0) {


                  var results = imageSearch.results;

                  for (var i = 0; i < results.length; i++) {
                          var result = results[i];

                          var link = jQuery('<a href="'+result.url+'" title="'+result.contentNoFormatting+'"><img height="64" src="'+result.tbUrl+'" /></a>');
                          dialog.append(link);

                          link.click(function(event) {
                              event.preventDefault();
                              window.babAddonWidgets.filepicker_setFromUrl(wId, this.href);
                              dialog.dialog('close');
                          });
                  }
              } else {
                  dialog.append('<div>' + metadata.suggestNoResultsError+' '+keywords + '</div>');
              }

              if (metadata.suggestExternalLinkLabel) {
                  dialog.append(jQuery('<div style="margin-top: 1em"><a class="widget-actionbutton" target="images" href="http://images.google.com/images?q=' + encodeURIComponent(keywords) + '">' + metadata.suggestExternalLinkLabel + '</a><div>'));
              }

          }, null);

          imageSearch.execute(keywords);

    }});
};




window.bab.addInitFunction(widget_filePickerInit);
