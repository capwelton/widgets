/**
 * Set values of color
 */
window.babAddonWidgets.colorSet = function(colorPicker, color, arr, slider)
{
	var colorspace = window.babAddonWidgets.getMetadata(colorPicker.attr('id')).colorspace; 
	
	var rules = window.babAddonWidgets.getMetadata(colorPicker.attr('id')).rules; 
	var sliderpos = rules[2];
	
	arr[sliderpos] = slider;
	v1 = arr[0];
	v2 = arr[1];
	v3 = arr[2];
	
	
	switch(colorspace)
	{
	case 'YUV':
		color.setYUV(v1, v2, v3);
		break;
	
	case 'RGB':
		color.setRGB(v1, v2, v3);
		break;
		
	case 'HSL':
		color.setHSL(v1, v2, v3);
		break;
	}
};


/**
 * get color array
 * @return Array
 */
window.babAddonWidgets.colorGet = function(colorPicker, color)
{
	var colorspace = window.babAddonWidgets.getMetadata(colorPicker.attr('id')).colorspace; 
	
	switch(colorspace)
	{
	case 'YUV':
		return color.getYUV();

	case 'RGB':
		return color.getRGB();

	case 'HSL':
		return color.getHSL();
	}
};



/**
 * 
 */
window.babAddonWidgets.getColorArr = function(colorPicker, X, Y)
{
	var rules = window.babAddonWidgets.getMetadata(colorPicker.attr('id')).rules; 
	
	var xpos = rules[0];
	var ypos = rules[1];
	var sliderpos = rules[2];
	
	var arr = new Array(3);
	
	arr[xpos] = X;
	arr[ypos] = Y;
	arr[sliderpos] = colorPicker.data('colorPickerSlider');
	
	return arr;
}



window.babAddonWidgets.colorPickerUpdateFrame = function(colorPicker, colorFrame, previousvalue)
{
	var color = new Widget_Color();
	
	var newvalue = colorPicker.data('colorPickerSlider');
	var foundvalue = null;
	
	if (newvalue !== previousvalue)
	{
		colorFrame.find('.cell').each(function() {
			
			if (newvalue !== colorPicker.data('colorPickerSlider'))
			{
				return false;
			}
			
			var cell = jQuery(this);
			var arr = cell.data('Widget_color');
			window.babAddonWidgets.colorSet(colorPicker, color, arr, newvalue);
			cell.css('background-color', '#'+color.getHexa());
			cell.data('Widget_color', window.babAddonWidgets.colorGet(colorPicker, color));
			
			if (color.getHexa() == colorPicker.val() && null == foundvalue)
				{
				cell.css('outline', '#fff 1px solid');
				foundvalue = true;
				}
			else {
				cell.css('outline', 'none');
			}
		});
	}
	
	setTimeout(function() { window.babAddonWidgets.colorPickerUpdateFrame(colorPicker, colorFrame, newvalue); }, 100);
};










/**
 * @return jQuery
 */
window.babAddonWidgets.colorPickerFrame = function(colorPicker, dialog, input, selectedHexa)
{
	var color = new Widget_Color();
	
	steps = 25;
	width = 350;

	stepWidth = (width / steps);
	mainframe = jQuery('<div></div>');
	table = jQuery('<table class="color-picker-popup"></table>');
	//table.css('border-collapse', 'collapse');
	
	var slider = jQuery('<div id="slider" class="widget-colorvalueslider" style="margin:2px auto 8px auto;width:100%;"></div>').slider({
		orientation: "horizontal",
		range: "min",
		min: 0,
		max: 100,
		step: 5,
		value: colorPicker.data('colorPickerSlider') * 100,
		slide: function(event, ui) {
		colorPicker.data('colorPickerSlider', (ui.value/100));
		}
	});
	
	if (window.babAddonWidgets.getMetadata(colorPicker.attr('id')).slider)
	{
		mainframe.append(slider);
	}
	
	
	mainframe.append(table);
	
	
	for (y = 0; y < 1; y += 1/steps) {
		var tr = jQuery('<tr></tr>');
		table.append(tr);
		for (x = 0; x < 1; x += 1/steps) {
			
			var cell = jQuery('<td class="cell"></td>');
			tr.append(cell);
			cell.css('width', stepWidth+'px');
			cell.css('height', stepWidth+'px');
			cell.data('Widget_color', window.babAddonWidgets.getColorArr(colorPicker, x, y));
			
			
			
			cell.hover(function() {
				jQuery(this).css('outline', '#000 1px solid');
			}, function() {
				jQuery(this).css('outline', 'none');
			});
			
			cell.click(function() {
				
				var arr = jQuery(this).data('Widget_color');
				
				window.babAddonWidgets.colorSet(colorPicker, color, arr, colorPicker.data('colorPickerSlider'));
				input.val(color.getHexa());
				input.css('background-color', '#'+color.getHexa());
				input.css('color', '#'+color.getHexa());
				
				dialog.dialog('close');
			});
		}
	}
	
	
	
	
	return mainframe;
};












function widget_colorPickerInit(domNode) {
	
	
	jQuery(domNode).find('.widget-colorpicker').not('[type="color"]').not('.widget-init-done').each(function() {
		
		var colorPicker = jQuery(this);
		colorPicker.addClass('widget-init-done');
		
		
		colorPicker.click(function() {
			
			var sliderValue = window.babAddonWidgets.getMetadata(jQuery(this).attr('id')).sliderValue;
			
			color = new Widget_Color;
			color.setHexa(jQuery(this).val());
			
			
			if (jQuery(this).val())
			{
				var arr_color = window.babAddonWidgets.colorGet(jQuery(this), color);
				
				var rules = window.babAddonWidgets.getMetadata(jQuery(this).attr('id')).rules; 
				var sliderpos = rules[2];
				
				sliderValue = arr_color[sliderpos];
			}
			
			colorPicker.data('colorPickerSlider', sliderValue);
			
			var pos = jQuery(this).offset();
			var title = window.babAddonWidgets.getMetadata(jQuery(this).attr('id')).title; 
			//  title="'+title+'"
			dialog = jQuery('<div></div>').dialog({ 
				position:[pos.left, pos.top + 20],
				width: 480,
				height: 500,
				draggable:true,
				resizable:true
			});
			
			var colorFrame = window.babAddonWidgets.colorPickerFrame(colorPicker, dialog, jQuery(this), color.getHexa());
			dialog.append(colorFrame);
			
			window.babAddonWidgets.colorPickerUpdateFrame(jQuery(this), colorFrame);
		});
		
		var lineEdit = window.babAddonWidgets.getMetadata(colorPicker.attr('id')).AssociatedLineEdit;
		if (lineEdit)
		{
			jQuery('#'+lineEdit).keyup(function() {
				color = new Widget_Color;
				color.setHueFromString(jQuery(this).val(), 1, 0.7);
				
				colorPicker.val(color.getHexa());
				colorPicker.css('background-color', '#'+color.getHexa());
				colorPicker.css('color', '#'+color.getHexa());
			});
		}

		colorPicker.css('background-color', '#'+colorPicker.val());
		colorPicker.css('color', '#'+colorPicker.val());

	});
	
	
}



window.bab.addInitFunction(widget_colorPickerInit);
