

function widgets_mapleafletloadPlugins(urls){
    var nbUrl = urls.length;
    var loadingElements = 0
    
    for (i = 0; i < nbUrl; i++) {
	    var script = document.createElement('script');
	    script.setAttribute('type', 'text/javascript');
	    script.setAttribute('src', urls[i]);
	    script.setAttribute('defer', 'defer');
	    document.body.appendChild(script);
	
	    if ((nbUrl-1) == i) {
		    script.onreadystatechange = function () {
		        if (this.readyState == 'complete' || this.readyState == 'loaded') {
		        	loadingElements+=1;
		        	if (loadingElements>=nbUrl) {
		        		widgets_mapLeafletInitialize();
		        	}
		        }
		    };
		
		    script.onload = function() {
		    	loadingElements+=1;
		    	if (loadingElements>=nbUrl) {
		    		widgets_mapLeafletInitialize();
		    	}
		    };
	    }
    }
}

function widgets_mapleafletloadScript(url, plugins){
	var scripts = document.scripts;
    var nbScripts = scripts.length;
    var i = 0;
    for (i = 0; i < nbScripts; i++) {
        if (url == scripts[i].getAttribute('src')) {
            return true;
        }
    }
    
    var script = document.createElement('script');
    script.setAttribute('type', 'text/javascript');
    script.setAttribute('src', url);
    script.setAttribute('defer', 'defer');
    document.body.appendChild(script);

    script.onreadystatechange = function () {
        if (this.readyState == 'complete' || this.readyState == 'loaded') {
        	widgets_mapleafletloadPlugins(plugins)
        }
    };

    script.onload = function() {
    	widgets_mapleafletloadPlugins(plugins)
    };
}


function widgets_mapLeafletjsInit(domNode)
{
    if(!window.bab.mapLeaflet){
        widget = jQuery('.widget-map.widget-map-leafletjs');
        var widget_meta = window.babAddonWidgets.getMetadata(widget.attr('id'));
        widgets_mapleafletloadScript(widget_meta.leafletUrl, [widget_meta.leafletPlugin1Url]);
    }else{
        if(window.bab.mapLeaflet == 'loaded'){
        	widgets_mapLeafletInitialize();
        }
    }
    //domNode = document.getElementByTagName('body');
}

function widgets_mapLeafletInitialize()
{
	window.bab.mapLeaflet = 'loaded';
    jQuery('.widget-map.widget-map-leafletjs').not('.widget-init-done').each(function () {

        widget = jQuery(this);
        var widget_meta = window.babAddonWidgets.getMetadata(widget.attr('id'));

        jQuery(this).addClass('widget-init-done');
        
        var center_meta = widget_meta.center;
        var zoom_meta = widget_meta.zoom;
        var marker_meta = widget_meta.marker;
        var openerList_meta = widget_meta.openerList;
        var lines_meta = widget_meta.lines;
        var color_values = widget_meta.linesColors
        var cluster_meta = widget_meta.cluster;
        var disableControl = widget_meta.disableControl;
        var tileurl_meta = widget_meta.mapTileUrl;
        var tiletoken_meta = widget_meta.mapTileToken;
        var attribution_meta = widget_meta.attribution;
        
        disableOption = {};
        if (disableControl) {
        	var disableOption = {
    			dragging: false,
    		    touchZoom: false,
    		    scrollWheelZoom: false,
    		    doubleClickZoom: false,
    		    boxZoom: false,
    		    tap: false,
    		    keyboard: false,
    		    zoomControl: false,
    		    attributionControl: false,
    		    prefix: false
        	};
        }
        
        var myMap = L.map(widget.attr('id'), disableOption);

        if (center_meta && 'lat' in center_meta && 'long' in center_meta) {
        	myMap.panTo([center_meta.lat, center_meta.long]);
        }
        if (zoom_meta) {
        	myMap.setZoom(zoom_meta);
        }
        
        var tileOption = {
        	maxZoom: 18 
        };
        if (tiletoken_meta) {
        	tileOption['accessToken'] = tiletoken_meta
        }
        if (attribution_meta) {
        	tileOption['attribution'] = attribution_meta
        }
        
        L.tileLayer(
    		tileurl_meta,
    		tileOption
		).addTo(myMap);

        if (lines_meta && lines_meta.length != 0) {
        	var colors = [{color: '#FF0000'}];
        	var thresolds = false;
        	if (color_values && color_values.length != 0) {
        		thresolds = [];
        		colors = [];
        		
        		var colorLen = color_values.length;
        		for (var i=0; i<colorLen; i++) {
        			thresolds.push(color_values[i].thresold);
        			colors.push({ color: color_values[i].color });
        		}
        	}
        	
        	var polyline = new L.multiOptionsPolyline(
        		lines_meta,
        		{
        			multiOptions: {
        				optionIdxFn: function (latLng) {
        					if (thresolds === false || typeof(latLng[2]) == undefined) {
        						return 0
        					}
        		            var i

        		            for (i = 0; i < thresolds.length; ++i) {
        		                if (latLng[2] <= thresolds[i]) {
        		                    return i;
        		                }
        		            }
        		            return thresolds.length;
        		        },
        		        options: colors
        			},
        			weight: 2
        		}
	        ).addTo(myMap);
        	
        	setTimeout(function() {//HACK TO TRY TO GET IT WORK in all ajax scenarios.
    			myMap.fitBounds(polyline.getBounds());
			}, 500);
        	
        }

        // Creation du marker
        var markers = new Array();
        if (marker_meta) {
            for (var i = 0,len = marker_meta.length; i < len; i++) {
            	
            	var markerOptions = {
            		title: marker_meta[i].name
            	};
            	if (marker_meta[i].icon) {
            		markerOptions.icon = L.icon({
	            	    iconUrl: marker_meta[i].icon
	            	});
            	}
            	
            	var marker = L.marker(
        			[marker_meta[i].lat, marker_meta[i].long],
        			markerOptions
            	).addTo(myMap);
            	markers[marker_meta[i].id] = marker;

				if (!disableControl) {
	            	marker.bindPopup(marker_meta[i].widget);
	            	jQuery(marker).on('popupopen', function(event) {
	            		window.bab.init(event.currentTarget._popup._container);
	            	})
	            	
	            	if(openerList_meta && openerList_meta[marker_meta[i].id]){//NOT TESTED
						for(var k=0, length=openerList_meta[marker_meta[i].id].length; k < length; k++){
							jQuery("#"+openerList_meta[marker_meta[i].id][k]).click({oMarker: marker_meta[i].id}, function(event) {
								myMap.closePopup();
								myMap.setZoom(12);
								myMap.panTo(markers[event.data.oMarker].getLatLng());
								markers[event.data.oMarker].openPopup();
							});
						}
	            	}
				}
            }
        }
    });
}




window.bab.addInitFunction(widgets_mapLeafletjsInit);
