

"use strict";



/**
 * Change visibility of columns with a css class
 * 
 * @param {DOMElement}  tableview	Widget dom element
 * @param {int} 		cellIndex
 * @param {String} 		visibility	visible : will show the olumn
 * 									hidden : will hide the column
 * 									toggle : will change the column visibility status
 * 
 * @return {bool} true if column is visible after call.
 */
window.babAddonWidgets.tableview_setColumnVisible = function(tableview, cellIndex, visibility) {

    var tab = tableview.getElementsByTagName('TABLE')[0];

    var colStart = 0;
    for (var c = 0; c < cellIndex; c++) {
        colStart += tab.rows[0].cells[c].colSpan;
    }

    var nbRows = tab.rows.length;
    var tdColSpan = tab.rows[0].cells[cellIndex].colSpan;
    var headerCell = jQuery(tab.rows[0].cells[colStart]);
    

    var visible;
    if (visibility === 'visible') {
        visible = true;
    } else if (visibility === 'hidden') {
        visible = false;
    } else {
        visible = headerCell.hasClass('widget-hidden-column');
    }
    
    for (var r = 0; r < nbRows; r++) {

        var row = tab.rows[r];
        for (c = 0; c < tdColSpan; c += row.cells[colStart + c].colSpan) {
        	var cell = jQuery(row.cells[colStart + c]);

            if (visible) {
            	cell.removeClass('widget-hidden-column');
            } else {
            	cell.addClass('widget-hidden-column');
            }
        }
    }
    return visible;
}

window.babAddonWidgets.tableview_pieChartDataLabel = function(lgtxt, lgtxt2, datavalue, pctvalue)
{
    if (pctvalue < 5) {
        return '';
    }
    lgtxt = lgtxt.length > 15 ? lgtxt.substr(0, 15) + '...' : lgtxt;
    return lgtxt + " " + lgtxt2 + "\n" + datavalue + " (" + (Math.round(pctvalue * 10) / 10) + "%)" ;
}

window.babAddonWidgets.tableview_barChartDataLabel = function(lgtxt, lgtxt2, datavalue, pctvalue, yPosBottom, yPosTop)
{
    if (yPosBottom - yPosTop < 10) {
        return '';
    }
    return datavalue;
}


window.babAddonWidgets.tableview_chartDataAnnotation = function(lgtxt, lgtxt2, datavalue, pctvalue)
{
    return lgtxt + " " + lgtxt2 + "\n" + datavalue + " (" + (Math.round(pctvalue * 10) / 10) + "%)" ;
}
	

window.babAddonWidgets.tableview_init = function(domNode) {
    
    // Graphs
    jQuery(domNode).find(".widget-tableview.widget-view-as-chart").not('.widget-view-as-chart-init-done').each(function() {
        jQuery(this).addClass('widget-view-as-chart-init-done');
        
        var tableview = jQuery(this);
        var canvasId = this.id + 'chart';
        var getColor = window.babAddonWidgets.tableview.getColor;
        var makeDatasetMultipleColor = window.babAddonWidgets.tableview.makeDatasetMultipleColor;
        var makeTabColor = window.babAddonWidgets.tableview.makeTabColor;
        var table = tableview.find('table');
        var fontFamilies = window.getComputedStyle(window.document.body, null).getPropertyValue('font-family').split(',');
        var fontFamily = "'" + fontFamilies[0].replace(/"/g, '') + "'";
        var options = {	
    		animation: {
				animateRotate: false,
				animateScale: false
			},
        	legend: {
        		display: true
        	}
        };
        
        var meta = window.babAddonWidgets.getMetadata(this.id);
    	
    	if (meta.options) {
    		for(var key in meta.options){
    			options[key] = meta.options[key];
    		}
    	}
        
        var size = tableview.width();
        var width = size;
        var height = size * 0.5;

        tableview.append('<canvas id="' + canvasId + '" height="' + height + '" width="' + width + '"></canvas>');
        
        var ctx = document.getElementById(canvasId).getContext("2d");
        
        var generateChartDataFromTable = window.babAddonWidgets.tableview.generateChartDataFromTable;
        
        var generateChartDatasetFromTable;
        var generateReverseChartDatasetFromTable;
        if (tableview.hasClass('widget-chart-data-series-in-columns')) {
            generateChartDatasetFromTable = window.babAddonWidgets.tableview.generateReverseChartDatasetFromTable;
            generateReverseChartDatasetFromTable = window.babAddonWidgets.tableview.generateChartDatasetFromTable;
        } else {
            generateChartDatasetFromTable = window.babAddonWidgets.tableview.generateChartDatasetFromTable;
            generateReverseChartDatasetFromTable = window.babAddonWidgets.tableview.generateReverseChartDatasetFromTable;
        }
        
        var type;
        if (tableview.hasClass('widget-chart-pie')) {
            ctx.links = table.links;
            var val = generateReverseChartDatasetFromTable(table);
            val.datasets[0].backgroundColor = makeTabColor(val.labels.length,0.5);
            type = 'pie';
        } else if (tableview.hasClass('widget-chart-doughnut')) {
            ctx.links = table.links;
            var val = generateReverseChartDatasetFromTable(table);
            val.datasets[0].backgroundColor = makeTabColor(val.labels.length,0.5);
            type= 'doughnut';
        } else if (tableview.hasClass('widget-chart-polararea')) {
            ctx.links = table.links;
            var val = generateReverseChartDatasetFromTable(table);
            val.datasets[0].backgroundColor = makeTabColor(val.labels.length,0.5);
            type= 'polarArea';
        } else if (tableview.hasClass('widget-chart-radar')) {
            ctx.links = table.links;
            var val = generateChartDatasetFromTable(table);
            val = makeDatasetMultipleColor(val);
            type = 'radar'
        } else if (tableview.hasClass('widget-chart-bar')) {
            ctx.links = table.links;
            var val = generateChartDatasetFromTable(table);
            val = makeDatasetMultipleColor(val);
            var type = 'bar';
        }  else if (tableview.hasClass('widget-chart-line')) {
            ctx.links = table.links;
            var val = generateChartDatasetFromTable(table);
            val = makeDatasetMultipleColor(val);
            type='line';
        }else if (tableview.hasClass('widget-chart-horizontalbar')) {
	          ctx.links = table.links;
	          var val = generateChartDatasetFromTable(table);
	          val = makeDatasetMultipleColor(val);
	          type='horizontalBar';
        }else if (tableview.hasClass('widget-chart-stackedbar')){ 
        	ctx.links = table.links;
        	var val = generateChartDatasetFromTable(table);
            val = makeDatasetMultipleColor(val);
        	type = "bar";
        	options.scales = {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true
                }]
            };
        } else { 
            ctx.links = table.links;
            var val = generateChartDatasetFromTable(table);
            val = makeDatasetMultipleColor(val);
            
            type = 'bar';
        }
        
        if(tableview.hasClass("widget-chart-nolegend")){
        	options.legend.display = false;
        }
        
        var entry = {
    	    type: type,
    	    data: val,
    	    options: options
        }

        var chart = new Chart(ctx,entry);
        Chart.defaults.global.defaultFontFamily = fontFamily;
        table.hide();
	});
    
    //Tables
    jQuery(domNode).find('.widget-tableview').not('.widget-tableview-init-done').each(function() {
    	jQuery(this).addClass('widget-tableview-init-done');
        
        var tableview = jQuery(this);
        var table = tableview.find('table:first');
        var tableBox = table.parent(); //Get the direct parent to avoid adding the pagination system into the sticky positionned element
        
        var meta = window.babAddonWidgets.getMetadata(this.id);
        var tableOptions = {};
    	
    	if(meta.options){
    		for(var key in meta.options){
    			tableOptions[key] = meta.options[key];
    		}
    	}
    	
    	
    	if(tableOptions.fixedHeader){
    		var fixedHeaderTableHeight = '40em'; //Default value, can be overwritted by setting the option fixedHeaderTableHeight
    		if(tableOptions.fixedHeaderTableHeight){
    			if(tableOptions.fixedHeaderTableHeight == 'auto'){
    				fixedHeaderTableHeight = 'auto';
    				
    				var body = document.body;
    			    var html = document.documentElement;

    			    var originalPageHeight = Math.max( body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
    			    var windowHeight = html.clientHeight;
    			    var windowWidth = html.clientWidth;
    			    
			    	var tablePosition = tableBox.offset();
			    	tablePosition = tablePosition.top;
			    	var tableWidth = tableBox.outerWidth();
			    	
			    	var tdId = 0;
			    	tableview.find('.widget-table-header td').each(function(){
			    		jQuery(this).attr('data-tdId', tdId);
			    		tdId++;
			    	});
			    	
			    	
			    	var cloneview = tableview.clone(true,true);//Clone table, with data and events
			    	cloneview.attr('id', cloneview.attr('id')+'_clone');
			    	cloneview.addClass('widget-tableview-clone');
			    	cloneview.find('.widget-table-header').siblings().empty();//Remove row data
			    	cloneview.children().not(':first-child').remove();//Only keep header (there might be the pagination widget we don't want to keep)
			    	tableview.parent().append(cloneview);//Add clone after original table
			    	
			    	//Initialize cloned table css
			    	cloneview.css('position', 'fixed');
			    	cloneview.css('z-index', '1000');
			    	cloneview.css('max-width', tableview.outerWidth()+'px');
			    	cloneview.css('top', '0');
			    	cloneview.css('overflow', 'hidden');
			    	cloneview.find('table').css('visibility', 'hidden');
			    	
			    	//Make the table row clikable behind the cloned element, but not on the cloned header
			    	cloneview.css('pointer-events', 'none');
			    	cloneview.find('.widget-table-header').css('pointer-events', 'initial');
			    	
			    	manageAutoTableHeader(tableview, cloneview);
			    	
    			    jQuery(window).scroll(function(){
    			    	manageAutoTableHeader(tableview, cloneview);
    			    });
    			    
    			    jQuery(window).resize(function(){
    			    	manageAutoTableHeader(tableview, cloneview);
    			    });
    			}
    			else{
    				fixedHeaderTableHeight = tableOptions.fixedHeaderTableHeight;
    				tableBox.addClass('widget-tableview-fixed-header');
    	    		tableBox.css('max-height', fixedHeaderTableHeight);
    			}
    		}
    	}
    });
};

function manageAutoTableHeader(tableview, cloneview)
{
	var hasBeenResized = false;
	if(isElementVisible(tableview)){
		if(!hasBeenResized){
			hasBeenResized = initHeaderWidth(tableview, cloneview);
    	}
		cloneview.show();
	}
	else{
		cloneview.hide();
	}
	
	if(isElementTopVisible(tableview)){
		cloneview.find('table').css('visibility', 'hidden');
	}
	else{
		cloneview.find('table').css('visibility', 'visible');
	}
	
	manageTableScroller(tableview, cloneview);
	
	if(isElementBottomVisible(tableview)){
		cloneview.find('.tableview-autoscroller').css('overflow-x', 'hidden');
	}
	else{
		cloneview.find('.tableview-autoscroller').css('overflow-x', 'scroll');
	}
}

function manageTableScroller(tableview, cloneview)
{
	cloneview.css('overflow-x', 'hidden');
	
	var scroller = cloneview.find('.tableview-autoscroller');
	if(!scroller.length){
		var scroller = jQuery('<div class="tableview-autoscroller"><div></div></div>').appendTo(cloneview);
	}
	scroller.css('overflow-x', 'scroll');
	scroller.css('width', cloneview.width()+'px');
	scroller.css('position', 'fixed');
	scroller.css('bottom', '0');
	scroller.css('pointer-events', 'initial');
	scroller.find('div').css('width', tableview.find('.widget-table-header').outerWidth()+'px');
	scroller.find('div').css('height', '1px');
    
    //Update table scroll bar
    tableview.scroll(function(){
    	scroller.scrollLeft(tableview.scrollLeft());
    	cloneview.scrollLeft(tableview.scrollLeft());
    });
    scroller.scroll(function(){
    	cloneview.scrollLeft(scroller.scrollLeft());
    	tableview.scrollLeft(scroller.scrollLeft());
    });
}

function initHeaderWidth(original, clone)
{
	//Resize the cloned thead to match the original 
	var windowHeight = document.documentElement.clientHeight;
	var hasBeenResized = false;
	
	clone.find('.widget-table-header td').each(function(){
		var tdId = jQuery(this).attr('data-tdId');
		var originalTd = original.find('.widget-table-header td[data-tdId="'+tdId+'"]');
		
		if(originalTd.outerWidth() != jQuery(this).outerWidth()){
			hasBeenResized = true;
		}
		jQuery(this).css('min-width', originalTd.outerWidth()+'px');
	});
	clone.css('max-width', original.outerWidth()+'px');
	clone.css('min-height', windowHeight + 'px');
	
	return hasBeenResized;
}

function isElementHidden(element)
{
	var a = element.offset().top;
	var b = element.height();
	var c = jQuery(window).height();
	var d = jQuery(window).scrollTop();
	return (a+b) < d || (c+d) < a;
}

function isElementVisible(element)
{
	return !isElementHidden(element); 
}

function isElementBottomVisible(element)
{
	var a = element.offset().top;
	var b = element.height();
	var c = jQuery(window).height();
	var d = jQuery(window).scrollTop();
	return (a+b) >= d && (a+b) <= (c+d);
}

function isElementTopVisible(element)
{
	var a = element.offset().top;
	var c = jQuery(window).height();
	var d = jQuery(window).scrollTop();
	return a >= d && a <= (c+d);
}

window.babAddonWidgets.tableview = {
    
    getColorHue: function(max, index, opacity) {
        return 'hsla(' + ((360 / max) * index) + ', 30%, 70%, ' + opacity + ')';
    },
    
    
    getColorSaturation: function(max, index, opacity) {
        return 'hsla(0, 0%, ' + (10 + (90 / max) * index) + '%, ' + opacity + ')';
    },
    
    
    makeTabColor: function(max,opacity) {
    	var retour = [];
    	var getColorHue = window.babAddonWidgets.tableview.getColorHue;
    	for(let i = 0; i < max; i++){
    		retour.push(getColorHue(max,i,opacity));
    	}
    	
    	return retour;
    },
    
    
    makeDatasetMultipleColor: function(val) {
    	var dataset = val.datasets;
        var getColor = window.babAddonWidgets.tableview.getColor;
        dataset.forEach(function(innerobj, index){
        	if (!innerobj.borderColor) {
        		innerobj.borderColor = getColor(dataset.length, index, 1);
        	}
        	if (!innerobj.backgroundColor) {
        		innerobj.backgroundColor = getColor(dataset.length, index, 0.5);
        	}
        	
        });
        return val;
    },


    generateChartLabelsFromTable: function(table) {
        
        var rows = jQuery(table).find('tr');
        var nbRows = rows.length ;
        
        if (typeof(table.links) === 'undefined') {
            table.links = {};
        }
        
        var labels = [];
        jQuery(table).find('tr').first().find('td,th').each(function (index) {
            if (index != 0) {
                labels.push(jQuery(this).text().trim());                           
            }
        });
        
        return labels;
    },


    generateChartDataFromTable: function (table) {
        
        var colors = [];
    
        var rows = jQuery(table).find('tr');
        var nbRows = rows.length - 1;
        
        if (typeof(table.links) === 'undefined') {
            table.links = {};
        }
        
        var chartData = [];
        rows.each(function(index) {
            if (index != 0) {
                var cols = $(this).find('td');  
                var value;
            	var label;
            	var link;
                cols.each(function(innerIndex) {
                    if (innerIndex == 0) {
                    	label = jQuery(this).text().trim();
                	} else {
                        value = jQuery(this).text();
                        link = jQuery(this).find('a').get(0);
                        if (link) {
                            table.links[label] = link;
                        }
                    }
                });
                
                var data = {
                    label : label,
                    title : label,
                    value : value
                }
    
                chartData.push(data);
            }
        });
    
        return chartData;
    },


    generateChartDatasetFromTable: function(table) {
        var rows = jQuery(table).find('tr');
        var nbRows = rows.length;
        
        if (typeof(table.links) === 'undefined') {
            table.links = {};
        }
        
        var labels = [];
        jQuery(table).find('tr').first().find('td,th').each(function (index) {
            if (index != 0) {
                labels.push(jQuery(this).text().trim());                           
            }
        });
    
        var datasets = [];		
        rows.each(function(index) {
            if (index != 0) {
                let cols = jQuery(this).find('td');  
                let data = [];
                let label;
                let link;
                cols.each(function(innerIndex) {

                    if (innerIndex == 0) {
                        label = jQuery(this).text().trim();
                    } else {
                    	let celldata = jQuery(this).text().trim();
                        if (celldata === "") {
                            celldata = NaN;    
                        }
                        data.push(celldata);
                        link = jQuery(this).find('a').get(0);
                        if (link) {
                            table.links[labels[innerIndex - 1] + '.' + label] = link;
                        }
                    }
                });
    
                var dataset = {
                    label: label,
                    data: data
                }
                
                window.babAddonWidgets.tableview.updateDataset(jQuery(this), dataset);
                
                datasets.push(dataset);
            }
        });
    
        var chartData = {
            labels: labels,
            datasets: datasets
        };
        
        return chartData;
    },
    
    
    generateReverseChartDatasetFromTable: function(table) {

        if (typeof(table.links) === 'undefined') {
            table.links = {};
        }
        
        var labels = [];
        jQuery(table).find('tr').find('td:nth-child(1),th:nth-child(1)').each(function (index) {
            if (index != 0) {
                labels.push(jQuery(this).text().trim());
            }
        });
        
        var datasets = [];
        var getColor = window.babAddonWidgets.tableview.getColor;
        var cols = jQuery(table).find('tr').first().find('td,th');
        var nbCols = cols.length ;
        cols.each(function(index) {
            if (index === 0) {
                return;
            }
        	
            let cellIndex = this.cellIndex;
            var data = [];
            var label;
            var link;
            let rows = jQuery(this).closest('table').find('tr > td:nth-child(' + (cellIndex + 1) + '),tr > th:nth-child(' + (cellIndex + 1) + ')');
            rows.each(function(innerIndex) {
                if (innerIndex == 0) {
                    label = jQuery(this).text().trim();
                } else {
                    data.push(jQuery(this).text());
                    link = jQuery(this).find('a').get(0);
                    if (link) {
                        table.links[labels[innerIndex - 1] + '.' + label] = link;
                    }
                }
            });
            var dataset = {
                label : label,
                data : data,
                lineTension: 0.2
            }
            
            window.babAddonWidgets.tableview.updateDataset(jQuery(this), dataset);

            datasets.push(dataset);
        });
        var chartData = {
            labels: labels,
            datasets: datasets
        };
        
        return chartData;
    },
    
    
    updateDataset: function(element, dataset) {
   
    	if (element.hasClass('widget-chart-nofill')) {
        	dataset.fill = false;
        }
        if (element.hasClass('widget-chart-dashed')) {
        	dataset.borderDash = [8, 8];
        } 
        if (element.hasClass('widget-chart-dotted')) {
        	dataset.borderDash = [3, 6];
        } 
        
    	const style = getComputedStyle(document.body);
    	
    	let classname = element.attr('class');
    	
    	let matches = classname.match(/widget-chart-color-([a-z]+)/);
    	if (matches) {
    		let colorName = matches[1];
    		dataset.borderColor = style.getPropertyValue('--color-' + colorName);
    		
    		let color = new Widget_Color();
    		color.setHexa(dataset.borderColor.replace('#', ''));
    		
    		let hsl = color.getHSL();
    		hsl[2] *= 1.5;
    		hsl[1] /= 1.2;
    		color.setHSL(hsl[0], hsl[1], hsl[2]);
//    		console.debug(color.getHexa());
        	dataset.backgroundColor = '#' + color.getHexa();
        	
    	}
    }
}

window.babAddonWidgets.tableview_delayedinit = function(domNode) {
    if (window.Chart) {
        window.babAddonWidgets.tableview_init(domNode);
    } else {
        setTimeout(function() { window.babAddonWidgets.tableview_delayedinit(domNode) }, 50);
    }
}

// We select the colorscheme
window.babAddonWidgets.tableview.getColor = window.babAddonWidgets.tableview.getColorHue;
//window.babAddonWidgets.tableview.getColor = window.babAddonWidgets.tableview.getColorSaturation;


window.bab.addInitFunction(window.babAddonWidgets.tableview_delayedinit);
