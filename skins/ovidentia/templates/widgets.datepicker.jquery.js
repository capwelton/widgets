


function widget_datePickerInit(domNode)
{
	function getMinDate(meta)
	{
		if (meta.mindatey != null) {
	        return new Date(meta.mindatey, meta.mindatem -1, meta.mindated, 0, 0, 0, 0);
	    }
		return null;
	}
	
	function getMaxDate(meta)
	{
	    if (meta.maxdatey != null) {
	        maxdate = new Date(meta.maxdatey, meta.maxdatem -1, meta.maxdated, 0, 0, 0, 0);
	    }
	    return null;
	}
	
	
	
	
	
    highlightDateMax = new Date();
    highlightDateMin = new Date(highlightDateMax);
    highlightDateMin.setDate(highlightDateMin.getDate() - 1);
    if (jQuery.datepicker) {
        // Prepare datepicker widgets
        jQuery(domNode).find(".widget-datepicker").not('.widget-init-done').each(function() {
            var meta = window.babAddonWidgets.getMetadata(this.id);

            var highlightedDateMin = highlightDateMin;
            var highlightedDateMax = highlightDateMax;

            if (meta.highlightedDateMin) {
                highlightedDateMin = new Date(meta.highlightedDateMin + 'T00:00:00');
                highlightedDateMax = new Date(highlightedDateMin);
                highlightedDateMax.setDate(highlightedDateMax.getDate() + 1);
            }
            if (meta.highlightedDateMax) {
                highlightedDateMax = new Date(meta.highlightedDateMax + 'T00:00:00');
            }

            if (typeof meta.format == 'undefined') {
                meta.format = "%d-%m-%Y";
            }

            if (typeof meta.numberOfMonths == 'undefined') {
                meta.numberOfMonths = [1, 1];
            }

            var selectableDate = false 
            if (typeof meta.selectableDate != 'undefined') {
            	selectableDate = {};
            	style = '';
            	sameColor = [];
            	jQuery.each(meta.selectableDate, function(index, date) {
            		var className = 'wdiget-daycolor-'+date.textcolor.substring(1, 7)+date.backgroundcolor.substring(1, 7)
            		if (typeof sameColor[className] == 'undefined') {
            			sameColor[className] = true;
	            		style+= '.ui-datepicker-calendar .'+className+' a { color: '+date.textcolor+'; background-color: '+date.backgroundcolor+';}'
            		}
            		date.className = className;
            		selectableDate[date.date] = date;
            		
                });
            	jQuery('html > head').append(jQuery('<style>'+style+'</style>'));
            }

            var datePickerOptions = {
                beforeShowDay: function(date) {
                    if (date >= highlightedDateMin && date < highlightedDateMax) {
                        return [true, 'widget-highlight', ''];
                    }
                    if (selectableDate) {
                    	var isoDate = new Date(date.getTime()-(date.getTimezoneOffset()*60000)).toJSON().substring(0, 10);//IGNORE TZ
	                    if (typeof selectableDate[isoDate] !== 'undefined') {
	                    	return [true, selectableDate[isoDate].className, selectableDate[isoDate].title];
	                    } else {
	                    	return [false, '', ''];
	                    }
                    }
                    
                    return [true, '', ''];
                },
                yearRange: '1900:2100',
                firstDay: 1,
                hideIfNoPrevNext: true,
                constrainInput: true,
                numberOfMonths: meta.numberOfMonths,
                dateFormat:
                    meta.format
                    .replace('%Y', 'yy')
                    .replace('%m', 'mm')
                    .replace('%d', 'dd'),
                duration: 0,
                onSelect: function() {
                    var meta = window.babAddonWidgets.getMetadata(this.id);

                    if (meta.to && jQuery('#'+meta.to).datepicker("getDate")) {
                        //jQuery('#'+meta.to).datepicker("setDate", )
                        todate = jQuery('#'+meta.to).datepicker("getDate");
                        var nextdate = jQuery(this).datepicker("getDate");
                        var previousdate = this.beforedate;
                        if (previousdate != null) {
                            timedif = nextdate.getTime() - previousdate.getTime();

                            Math.trunc = Math.trunc || function(x) {
                                return x < 0 ? Math.ceil(x) : Math.floor(x);
                            };

                            nbdays = Math.trunc(timedif/(1000*60*60*24));

                            todate.setDate(todate.getDate()+nbdays);

                            var meta = window.babAddonWidgets.getMetadata(this.id);
                            var mindate = getMinDate(meta);
                            var maxdate = getMaxDate(meta);

                            jQuery('#'+meta.to).datepicker("option", {
                                minDate: mindate==null ? meta.from	? jQuery('#'+meta.from).datepicker("getDate") 	: null : mindate,
                                maxDate: maxdate==null ? meta.to	? null/*jQuery('#'+meta.to).datepicker("getDate")*/ 	: null : maxdate
                            });

                            jQuery('#'+meta.to).datepicker("setDate", todate);
                        }
                    }

                    jQuery(this).change();
                },
                beforeShow: function() {
                    this.beforedate = jQuery(this).datepicker("getDate");
                    var meta = window.babAddonWidgets.getMetadata(this.id);
                    var mindate = getMinDate(meta);
                    var maxdate = getMaxDate(meta);
                    
                    return {
                        minDate: mindate==null ? meta.from	? jQuery('#'+meta.from).datepicker("getDate") 	: null : mindate,
                        maxDate: maxdate==null ? meta.to	? null/*jQuery('#'+meta.to).datepicker("getDate")*/ 	: null : maxdate
                    };

                }
            };

            if (typeof meta.defaultDate != 'undefined') {
                datePickerOptions.defaultDate = meta.defaultDate;
            }

            if (typeof meta.showWeek != 'undefined') {
                datePickerOptions.showWeek = meta.showWeek;
            }
            if (typeof meta.dateClickUrl != 'undefined' && meta.dateClickUrl != '') {
                datePickerOptions.onSelect = function(dateText, inst) {
                    var dateClickUrl = meta.dateClickUrl.replace('__date__', dateText);
                    if (meta.dateClickUrlAjax) {
                    	window.babAddonWidgets.ajax(dateClickUrl, false, {});
                    } else {
                    	window.location.href = dateClickUrl;
                    }
                };
            }
            if (jQuery(this).hasClass('widget-datepicker-changeyear')) {
                datePickerOptions.changeYear = true;
            }
            if (jQuery(this).hasClass('widget-datepicker-changemonth')) {
                datePickerOptions.changeMonth = true;
            }
        	if (jQuery(this).hasClass('widget-datepicker-todaybutton')) {
                $.datepicker._gotoToday = function(id) { 
                	$(id).datepicker('setDate', new Date()).datepicker('hide').blur().change(); 
                };
                datePickerOptions.showButtonPanel = true;
        	}
            
            jQuery(this)
                .addClass('widget-init-done')
                .datepicker(datePickerOptions)
                .attr('autocomplete', 'off');
        });
    }
}

window.bab.addInitFunction(widget_datePickerInit);
