



window.babAddonWidgets.telLineEditEvent = function() {


    var enable = function(input) {

        input.addClass('widget-tellineedit-valid');
        input.removeClass('widget-tellineedit-invalid');
    };

    var disable = function(input) {

        input.removeClass('widget-tellineedit-valid');
        input.addClass('widget-tellineedit-invalid');
    };




    var input = jQuery(this);

    if ('' == input.val())
    {
        enable(input);
        return;
    }


    if (input.val().match(/[^0-9 +().]+/))
    {
        disable(input);
        return;
    }

    enable(input);
    return;
};



function widget_telLineEditInit(domNode) {

    jQuery(domNode).find('.widget-tellineedit').each(function() {
        var input = jQuery(this);

        if (input.data('widgetevent'))
        {
            return;
        }

        input.data('widgetevent', true);


        // lock form validation if not a valid tel address

        input.blur(window.babAddonWidgets.telLineEditEvent);
        input.keyup(window.babAddonWidgets.telLineEditEvent);

        input.blur();


        // form event

        if (form = input.closest('form')) {
            if (form.data('widget-tellineedit-event')) {
                // if mutiple field of same type, prevent mutiple submit event
                return;
            }

            form.data('widget-tellineedit-event', true);

            form.on('validate.widgets', function() {
                var input = jQuery(this).find('.widget-tellineedit-invalid')[0];

                if (input) {
                    input.focus();
                    var message = window.babAddonWidgets.getMetadata(input.id).submitMessage;

                    if (window.babAddonWidgets.validatemandatory) {
                        form.addClass('widget-invalid');
                        alert(message);
                        return false;
                    }

                    if (window.babAddonWidgets.validate) {
                        var confirm = window.confirm(message);
                        if (!confirm) {
                            form.addClass('widget-invalid');
                        }
                        return confirm;
                    }
                }

                return true;
            });
        }

    });

}



window.bab.addInitFunction(widget_telLineEditInit);

