
window.babAddonWidgets.aclButtonClicked = function() {
	var input = jQuery(this).closest('.widget-acl-frame').prev('input');
	
	// open dialog
	
	var page = jQuery('<div class="widget-acl-dialog"><div class="toolbar"></div><ul class="root"></ul></div>');
	
	page.attr('title', input.attr('title'));
	
	page.dialog({
		'width': jQuery(window).width() - 80,
		'height': jQuery(window).height() - 80,
		'modal': true, 
		'buttons': 
			[{
				text: "Ok",
	           	click: function() {
					// serialize checked groups
				
					window.babAddonWidgets.updateGroups(input, jQuery(this));
					jQuery(this).dialog("close"); 
				}
			}]
	});
	
	
	window.babAddonWidgets.fillGroups(input, page.find('ul.root'), '', 2);
	window.babAddonWidgets.addToolbar(input, page);
	
	return false;
}


window.babAddonWidgets.addGroupSelection = function(domNode)
{
	jQuery(domNode).find('.widget-acl').each(function(index, widget) { 
		widget = jQuery(widget);
		var meta = window.babAddonWidgets.getMetadata(widget.attr('id'));
		
		var frame = jQuery('<div class="widget-acl-frame"><strong></strong></div>');
		frame.find('strong').text(widget.attr('title'));
	
		var button = jQuery('<button><img src="'+meta.setGroupsImage+'" /></button>');
		button.attr('title', meta.buttonText);
		frame.append(button);
		
		var list = jQuery('<ul></ul>');
		if (meta.maxHeight)
		{
			list.css('max-height', meta.maxHeight+meta.maxHeightUnit);
		}
		frame.append(list);
		
		frame.insertAfter(widget);
	
		// window.babAddonWidgets.updateList(widget, true);
	
		button.click(window.babAddonWidgets.aclButtonClicked);
	});
}

/**
 * OPen group, the group must be visible
 * 
 * @param input
 * @param page
 */
window.babAddonWidgets.openGroup = function(input, page, id_group)
{
	
	
	 var plus = page.find('input[name="group_tree[]"][value="'+id_group+'"]').closest('li').find('strong');
	 if (plus.hasClass('plus'))
	 {
		 window.babAddonWidgets.deployGroup(plus, input, false);
	 }
}


/**
 * Toolbar
 * 
 * @param input
 * @param page
 */
window.babAddonWidgets.addToolbar = function(input, page)
{
	// groups stored in the input
	var val = input.attr('value').replace(/^[,0]+/g,'').replace(/[,0]+$/g,'');
	
	
	
	if ('' != val)
	{
		var toolbar = page.find('.toolbar');
		
		var meta = window.babAddonWidgets.getMetadata(input.attr('id'));
		var button = jQuery('<button></button>');
		toolbar.append(button);
		
		button.text(meta.deployButton);
		
		button.click(function() {
			
			// add the groups checked by user but not allready saved in the input
			page.find('input:checked').each(function(index, checkbox) {
				val += ','+jQuery(checkbox).attr('value');
			});

       		jQuery.ajax({
       		   url: meta.selfpage,
       		   data: 'tg=addon/widgets/groups&idx=checked&groups='+encodeURIComponent(val)+'&uid='+encodeURIComponent(meta.uid),
       		   success: function(response) {
       			   eval('var lists = '+response+';');
       			   var id_group = null;
       			   
       			   // make sure the first node is open
       			   window.babAddonWidgets.openGroup(input, page, 0);
       			   
       			   for(var l = 0; l < lists.length; l++)
   				   {
       				   for(var g = 0; g < lists[l].length; g++)
   					   {
       					   id_group = lists[l][g];
       					   window.babAddonWidgets.openGroup(input, page, id_group);
   					   }
   				   }
       		   }
       		});
		});
	}
}




/**
 * 
 * @param widget
 * @param groups
 */
window.babAddonWidgets.processGroups = function(widget, groups, real)
{
	var ul = widget.next('div').find('ul');
	var meta = window.babAddonWidgets.getMetadata(widget.attr('id'));
	
	
	
	for(var i = 0; i < groups.length; i++)
	{
		if (groups[i].withChildNodes)
		{
			g = groups[i].id+'a';
		} else {
			g = groups[i].id+'b';
		}
		
		

		if (ul.find('li.widget-acl-group-'+g).length > 0)
		{
			// allready inserted
			ul.find('li.widget-acl-group-'+g).removeClass('deleted');
			continue;
		}
		
		
		
		var li = jQuery('<li><div class="group"><span></span><br /><em></em><br /></div><div class="delete"></div></li>');
		li.data('group', groups[i]);
		li.addClass('widget-acl-group-'+g);
		li.find('span').text(groups[i].name);
		if (groups[i].withChildNodes)
		{
			li.find('em').text(meta.withChildNodes);
		}
		
		if (groups[i].setOfGroups)
		{
			li.find('em').text(meta.setOfGroups);
		}
		
		if (!real)
		{
			li.addClass('inserted');
		}
		
		ul.append(li);
		
		
		li.find('.delete').attr('title', meta.deleteTitle);
		
		li.find('.delete').click(function() {
			
			var id_group = jQuery(this).closest('li').data('group').id;
			var input = jQuery(this).closest('.widget-acl-frame').prev('input');
			
			var value = input.attr('value').split(',');
			var newvalue = new Array;
			for(var i = 0; i < value.length; i++)
			{
				if (value[i] != id_group && value[i] != id_group+'+')
				{
					newvalue.push(value[i]);
				}
			}
			
			input.attr('value', newvalue.join(','));
			
			// jQuery(this).closest('li').remove();
			jQuery(this).closest('li').addClass('deleted');
		});
	}
}






/**
 * Update the main list of groups
 * @param jQuery 	widgets
 * @param bool		real		real update or update with additional CSS classes to show the future changes
 * 								for real=false, classes are deleted, inserted
 * @returns {String}
 */
window.babAddonWidgets.updateList = function(widgets, real)
{
	var url_uid = '';
	
	widgets.each(function(index, widget) { 
		
		widget = jQuery(widget);
		
		var groups = widget.attr('value');
		
		if ('' == groups)
		{
			return;
		}
		
		var meta = window.babAddonWidgets.getMetadata(widget.attr('id'));
		url_uid += '&groups['+encodeURIComponent(meta.uid)+']='+encodeURIComponent(groups);
		
		var ul = widget.next('div').find('ul');
		ul.addClass('loading');
	});
	
	
	
	
	
	if ('' == url_uid)
	{
		return;
	}
	
	
	var meta = window.babAddonWidgets.getMetadata(jQuery(widgets[0]).attr('id'));
	var controller = 'tg=addon/widgets/groups';
	// var controller = 'addon=widgets.groups';

	jQuery.ajax({
	   url: meta.selfpage,
	   data: controller+'&idx=infos'+url_uid,
	   success: function(response) {
			
			
			if ('{' == response.substr(0,1)) {

				eval('var items = '+response+';');
				
				for(var id in items)
				{
					var groups = items[id];
					var widget = jQuery('#'+id);
					var ul = widget.next('div').find('ul');
					var g = null;
					
					ul.removeClass('loading');
					
					ul.find('li').map(function() 
					{
						var li = jQuery(this);
						remove = true;
						for(var i = 0; i < groups.length; i++)
						{
							if (groups[i].id == li.data('group').id && groups[i].withChildNodes == li.data('group').withChildNodes)
							{
								// li found in response, do not remove li
								remove = false;
								break;
							}
						}
						
						if (remove)
						{
							if (real)
							{
								li.remove();
							} else {
								li.addClass('deleted');
							}
						}
					});
					
					
	
					window.babAddonWidgets.processGroups(widget, groups, real);
					
				}
				
			} else {
				
				if (response.length != 0)
				{
				
					if(response.length < 2048) {
						alert(response);
					} else {
						alert('error, you should check the installation status of Widget library');
					}
				}
			}
	   }
	});
	
}




window.babAddonWidgets.isChecked = function(input, id_group, withChildNodes)
{
	if (withChildNodes)
	{
		id_group = id_group+'+';
	}
	
	var groups = input.attr('value').split(',');
	for(var i=0; i<groups.length; i++)
	{
		if (groups[i] == id_group)
		{
			return true;
		}
	}
	
	return false;
}




window.babAddonWidgets.updateGroups = function(input, dialog)
{
	// do not remove from input the id not processed in dialog
	
	var groups = input.attr('value').split(',');
	var indialog = new Array();
	var checked = new Array();
	var groupid = null;
	dialog.find('input').each(function(index, input) {
		
		input = jQuery(input);
		
		if (input.attr('name').indexOf('tree') != -1)
		{
			groupid = input.attr('value')+'+';
		} else {
			groupid = input.attr('value');
		}
		
		indialog.push(groupid);
		
		if (input.is(':checked') && !input.is(':disabled'))
		{
			checked.push(groupid);
		}
	});
	
	// remove processed item
	groups = jQuery.map(groups, function(g) {
		
		if ('' == g)
		{
			return null;
		}
		
		for(var i=0; i<indialog.length; i++)
		{
			if (indialog[i] == g)
			{
				return null;
			}
		}
		
		return g;
	});
	
	
	// add checked items
	for(var i=0; i<checked.length; i++)
	{
		groups.push(checked[i]);
	}

	
	if (groups.length == 0)
	{
		input.attr('value', '');
	} else {
		input.attr('value', groups.join(','));
	}
	
	window.babAddonWidgets.updateList(input, false);
}







/**
 * get group ID from LI
 */
window.babAddonWidgets.getGroupId = function(li)
{
	var id = jQuery(li.find('input')[0]).attr('value');
	
	return id;
}



/**
 * deploy group
 * @returns
 */
window.babAddonWidgets.deployGroup = function(plus, input, async) {
	
	var li = plus.closest('li');
	
	if (plus.hasClass('plus'))
	{
		if (!li.next('ul,li').is('ul'))
		{
			plus.attr('class','loading');
			var ul = jQuery('<ul></ul>');
			ul.insertAfter(li);
			window.babAddonWidgets.fillGroups(input, ul, window.babAddonWidgets.getGroupId(li), 1, async);
		} else {
			li.next('ul').show();
			plus.attr('class','minus');
		}
		
		
	} else {
		
		li.next('ul').hide();
		plus.attr('class','plus');
	}
	
}



window.babAddonWidgets.fillGroupsLevel = function(input, ul, id_parent, groups, parent)
{
	for(var i=0; i<groups.length; i++)
	{
		
		
		var li = jQuery('<li><strong></strong><div class="list-item"><input type="checkbox" name="group_tree[]" value="'+groups[i].id+'" /><span></span></div> <div class="eol"><input type="checkbox" name="group[]" value="'+groups[i].id+'" /> </div></li>');
		li.find('span').text(groups[i].name);
		var group_tree = li.find('[name="group_tree[]"]'); 	// left checkbox
		var group = li.find('[name="group[]"]'); 			// right checkbox
		
		if (null != groups[i].groupTreeDisabled && groups[i].groupTreeDisabled)
		{
			group_tree.prop('disabled', true);
		}
		
		if (null != groups[i].groupDisabled && groups[i].groupDisabled)
		{
			group.prop('disabled', true);
		}
		
		if (window.babAddonWidgets.isChecked(input, groups[i].id, true))
		{
			group_tree.prop('checked',true);
			group.prop('checked',true);
			group.prop('disabled',true);
		}
		
		if (window.babAddonWidgets.isChecked(input, groups[i].id, false))
		{
			group.prop('checked',true);
		}
		
		if (parent.is(':checked'))
		{
			group_tree.prop('checked',true);
			group.prop('checked',true);
			group_tree.prop('disabled',true);
			group.prop('disabled',true);
		}
		
		if (groups[i].setOfGroups)
		{
			group_tree.css({"visibility":"hidden"});
		}
		
		
		ul.append(li);
		
		group_tree.click(function() {
			
			var c = jQuery(this).closest('li').next('ul').find('input[type=checkbox]');
			c.push(jQuery(this).closest('div').nextAll('.eol').find('input[type=checkbox]')[0]);
			
			if (jQuery(this).is(':checked'))
			{
				c.prop('disabled', true);
				c.prop('checked', true);
			} else {
				c.prop('disabled', false);
				c.prop('checked', false);
			}
		});
		
		
		// add a + if the group have child nodes
		
		if (groups[i].hasChildNodes)
		{
			var plus = li.find('strong');
			plus.attr('class','plus');
			plus.click(function() {
				window.babAddonWidgets.deployGroup(jQuery(this), input, true);
			});
		}
	}
	
}




/**
 * @param	jQuery	input			input text field
 * @param	jQuery 	ul				target ul to insert nodes
 * @param	int		id_parent		parent group
 * @param	int		levels			number of levels to get in request
 */
window.babAddonWidgets.fillGroups = function(input, ul, id_parent, levels, async)
{
	// get groups from id_parent
	
	var meta = window.babAddonWidgets.getMetadata(input.attr('id'));
	
	if (null == async)
	{
		async = true;
	}
	
	jQuery.ajax({
	   async: async,
	   url: meta.selfpage,
	   data: 'tg=addon/widgets/groups&idx=get&id_parent='+id_parent+'&uid='+meta.uid+'&levels='+levels+'&id_delegation='+meta.id_delegation,
	   success: function(response){
			if ('[' == response.substr(0,1)) {
				
				eval('var result = '+response+';');
				
				for(var item = 0; item < result.length; item++)
				{
					var level_id_parent = result[item].id_parent;
					var level_groups = result[item].items;
					
					// apply filter
					
					
					
					for(var i = 0; i < level_groups.length; i++)
					{
						if (meta.disabledGroups && meta.disabledGroups[level_groups[i]['id']])
						{
							level_groups[i].groupDisabled = true;
							level_groups[i].groupTreeDisabled = true;
						}
					}
					
					var parent = ul.parent().find('[name="group_tree[]"][value="'+level_id_parent+'"]');
					
					if (parent.length != 0)
					{
						ul = parent.closest('li').next('ul');
						if (ul.length == 0)
						{
							ul = jQuery('<ul></ul>');
							ul.insertAfter(parent.closest('li'));
						}
					}
					
					//console.debug(level_id_parent);
					//console.debug(level_groups);
					
					window.babAddonWidgets.fillGroupsLevel(input, ul, level_id_parent, level_groups, parent);
					
					// set the minus class on parent checkbox
					
					if (!ul.hasClass('root'))
					{
						ul.prev('li').find('strong').attr('class', 'minus');
					}
				}
				
				
				

			} else {
				if(response.length < 2048) {
					alert(response);
				} else {
					alert('error');
				}
			}
	   }
	});
}



window.babAddonWidgets.initForm = function(form)
{
	widgets = form.find('.widget-acl');
	
	domNode = form.get(0);
	
	var acl = widgets;
	
	// hide default field
	
	acl.hide();
	
	// add group selection

	window.babAddonWidgets.addGroupSelection(domNode);
	window.babAddonWidgets.updateList(widgets, true);
	
}



window.babAddonWidgets.initAcl = function(domNode)
{
	var widgets = jQuery(domNode).find('.widget-acl').not('.widget-init-done');
	
	if (widgets.length > 0) {
		
		widgets.addClass('widget-init-done');
		
		// process full form only
		
		var form = widgets.closest('form').not('.widget-init-acl-done');
		
		if (form.length > 0) {
			form.addClass('widget-init-acl-done');
			
			form.ready(function() {
				window.babAddonWidgets.initForm(form);
			});
		
		}
		
	}
	
}



window.bab.addInitFunction(window.babAddonWidgets.initAcl);


