var timeline_data = {  // save as a global variable
'dateTimeFormat': 'iso8601',

'events' : [
]
}



var tl;
var resizeTimerID = null;



function widget_timelineInit(domNode) {

    jQuery(domNode).find('.widget-timeline').each(function() {
        
    	try {
    	    var timelineWidget = this;
    	    
    	   
    	    if (!timelineWidget) {
    	    	return;
    	    }
    		
    		var theme = Timeline.ClassicTheme.create();
    		theme.firstDayOfWeek = 1;


    		var metadata = window.babAddonWidgets.getMetadata(timelineWidget.id);
    	
    		theme.timeline_start = metadata.start;
    		theme.timeline_stop = metadata.stop;
    		// Set the Timeline's "width" automatically.
    		theme.autoWidth = metadata.autoWidth;
    	    // Set autoWidth on the Timeline's first band's theme,
    	    // will affect all bands.
    	
    		var todayStart = new Date();
    		todayStart.setUTCHours(0, 0, 0, 0);
    	
    		var todayEnd = new Date();
    		todayEnd.setUTCHours(0, 0, 0, 0);
    		todayEnd.setDate(todayStart.getDate() + 1);
    	
    		var unitMapping = {
    			millisecond: Timeline.DateTime.MILLISECOND,
    			second: Timeline.DateTime.SECOND,
    			minute: Timeline.DateTime.MINUTE,
    			hour: Timeline.DateTime.HOUR,
    			day: Timeline.DateTime.DAY,
    			week: Timeline.DateTime.WEEK,
    			month: Timeline.DateTime.MONTH,
    			year: Timeline.DateTime.YEAR,
    			decade: Timeline.DateTime.DECADE,
    			century: Timeline.DateTime.CENTURY,
    			millenium: Timeline.DateTime.MILLENIUM
    		};
    		
    	    var eventSource = new Timeline.DefaultEventSource();
    	    
    	    var spanHighlights = metadata.spanHighlights;
    	    
    	    var bands = metadata.bands;
    	    var bandInfos = [];
    	    
    	    for (var i = 0; i < bands.length; i++) {
    	    	var band = bands[i];
    	    	var bandInfo = Timeline.createBandInfo({
    	    		width:	band.width,
    	    		intervalUnit : unitMapping[band.intervalUnit],
    	    		intervalPixels: band.intervalPixels,
    	    		layout: band.type,
    	    		eventSource: eventSource,
    	    		theme: theme,
    	    		classes: band.classes
    	    	});
    	
    	    	// Highlight current day.
    	        var todaySpan = new Timeline.SpanHighlightDecorator({
    	            startDate:  todayStart,
    	            endDate:    todayEnd,
    	            color:      "#ec8",
    	            opacity:    25,
    	            inFront:    false
    	        });
    	    	bandInfo.decorators = [ todaySpan ];
    	
    	
    	    	for (var j = 0; j < spanHighlights.length; j++) {
    	    		bandInfo.decorators.push(new Timeline.SpanHighlightDecorator(spanHighlights[j]));
    	    	}
    	
    	    	if (i > 0) {
    	    		bandInfo.syncWith = 0;
    	    		bandInfo.highlight = true;
    	    	}
    	    	bandInfos.push(bandInfo);
    	    }
    	    
    	    tl = Timeline.create(timelineWidget, bandInfos, Timeline.HORIZONTAL);
    	
    	    var url = '.';	// The base url for image, icon and background image
    	    				// references in the data
    	 
    	    timeline_data.events = metadata.periods;
    	    
    	    eventSource.loadJSON(timeline_data, url); 
    	
    	    
    	
    	    
    	    tl.layout(); // display the Timeline
    	
    	
    	    jQuery('body').resize(function () {
    	        if (resizeTimerID == null) {
    	            resizeTimerID = window.setTimeout(function() {
    	                resizeTimerID = null;
    	                tl.layout();
    	            }, 500);
    	        }    	
    	    });
    	    
    	    
    	    // timeline
    	    
    	    Timeline.DefaultEventSource.Event.prototype.fillInfoBubble = function(elmt, theme, labeller) {
    	        var doc = elmt.ownerDocument;
    	
    	        var divBody = doc.createElement("div");
    	        this.fillDescription(divBody);
    	        theme.event.bubble.bodyStyler(divBody);
    	        elmt.appendChild(divBody);
    	
    	    };
    	} catch (e) {
    		window.setTimeout(widget_timelineInit, 500, domNode);
    	}
    });
}


window.bab.addInitFunction(widget_timelineInit);


