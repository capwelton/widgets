/**
 * Add a button next to last field of a multifield container. The button create a copy of the last field and insert it under
 * The new field will have a new name.
 * 
 * @see Widget_MultiField
 */



window.babAddonWidgets.multiField_setBtn = function(vboxitem) {
	// create add button next to field
	container = jQuery(vboxitem).parents('.widget-multifield');

	if (container) {
		
		container.css('white-space', 'nowrap');

		// add button
		
		meta = window.babAddonWidgets.getMetadata(container.attr('id'));
		btn = jQuery('<small class="ui-default-state">'+meta.addlabel+'</small>').css('cursor', 'pointer');
		
		vboxitem.appendChild(btn.get(0));


		btn.click(function() {
			vboxitem = jQuery(this).parent();
			
			jQuery(this).next().remove();
			jQuery(this).remove();
			
			vbox = vboxitem.parent().get(0);
			newvboxitem = vboxitem.clone();
			newvboxitem.find('script').remove();
			newvboxitem.hide();
			vbox.appendChild(newvboxitem.get(0));
			
			// remove value

			newvboxitem.find('input').attr('value', '');
			newvboxitem.find('textarea').children().remove();
			newvboxitem.find('select option[selected]').prop('selected', false);
			
			
			
			newvboxitem.slideDown('fast', function() {
				window.babAddonWidgets.multiField_setBtn(this);
			});

			currentname = newvboxitem.find('input, select, textarea').attr('name');
			
			if (undefined === currentname) {
				throw 'Failed to get name attribute from input field';
			}
			
			result = /^(.*?)\[(\d+?)\]$/.exec(currentname);

			if (!result) {
				throw 'unexpected matching error on name : '+currentname;
			}
			
			
			nameindex = (1+parseInt(result[2]));
			
			
			newvboxitem.find('input, select, textarea').each(function() {
				var input = jQuery(this);
				
				var name = input.attr('name');
				var currentid = input.attr('id');
				
				var nameMatch = /^(.*?)\[(\d+?)\]$/.exec(name);
				
				var newId = currentid+'-'+nameindex;
				
				input.attr('name', nameMatch[1]+'['+nameindex+']');
				input.attr('id', newId);
				
				// add metadata
				
				currentmeta = window.babAddonWidgets.getMetadata(currentid);
				window.babAddonWidgets.addMetadata(newId, currentmeta);
			});
			
			
			
			
			
			
			if (newvboxitem.find('.widget-datepicker').get(0)) {
				newvboxitem.find('input').removeClass('hasDatepicker');
				widget_datePickerInit(newvboxitem);
			}
			
			if (newvboxitem.find('.widget-suggestedit').get(0)) {
				var input = newvboxitem.find('input[type=text]');
				input.removeAttr('jqac').removeAttr('autocomplete');
				
				// update id_target on metadata
				var metadata = window.babAddonWidgets.getMetadata(input.attr('id'));
				
				var idMatch = /^(.*?)-(\d+?)$/.exec(input.attr('id'));
				metadata.id_target = idMatch[1]+'_ID-'+idMatch[2];
				window.babAddonWidgets.addMetadata(input.attr('id'), metadata);
				
				window.babAddonWidgets.suggestedit_init(newvboxitem);
				
				
			}
			
		});


		// remove button
		
		if (jQuery(vboxitem).prev('.widget-layout-vbox-item').get(0)) {
			btn = jQuery('<small class="ui-default-state">'+meta.removelabel+'</small>').css('cursor', 'pointer');

			vboxitem.appendChild(btn.get(0));


			btn.click(function() {
				vboxitem = jQuery(this).parent();
				newlast = vboxitem.prev('.widget-layout-vbox-item').get(0);
				vboxitem.slideUp('fast', function() {
					jQuery(this).remove();
				});

				if (newlast) {
					window.babAddonWidgets.multiField_setBtn(newlast);
				}
			});

		}
	}
}


window.babAddonWidgets.initMultiField = function(domNode) {

	jQuery(domNode).find('.widget-multifield .widget-layout-vbox-item:last-child').not('.widget-init-done').each(function() {
	    jQuery(this).addClass('widget-init-done');
 		 window.babAddonWidgets.multiField_setBtn(this);
	});
}

window.bab.addInitFunction(window.babAddonWidgets.initMultiField);
